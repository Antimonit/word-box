object Modules {

    const val app = ":app"
    const val base = ":base"
    const val database = ":database"
    const val history = ":history"
}
