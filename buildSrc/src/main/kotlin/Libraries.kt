object Libraries {

    object Kotlin {
        private const val version = "1.3.61"
        const val jdk6 = "org.jetbrains.kotlin:kotlin-stdlib:$version"
        const val jdk7 = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$version"
        const val jdk8 = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
    }

    object AndroidX {
        const val appCompat = "androidx.appcompat:appcompat:1.2.0-alpha02"
        const val coreKtx = "androidx.core:core-ktx:1.2.0"
        const val browser = "androidx.browser:browser:1.2.0"
        const val recyclerView = "androidx.recyclerview:recyclerview:1.2.0-alpha01"
        const val vectorDrawable = "androidx.vectordrawable:vectordrawable:1.1.0"
        const val legacyPreference = "androidx.legacy:legacy-preference-v14:1.0.0"
        const val cardView = "androidx.cardview:cardview:1.0.0"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:1.1.2"
        const val multiDex = "androidx.multidex:multidex:2.0.1"

        object Test {
            const val jUnit = "androidx.test.ext:junit:1.1.1"
            const val espresso = "androidx.test.espresso:espresso-core:3.2.0"
        }
    }

    object Dagger {
        const val dagger = "com.google.dagger:dagger:2.21"
        const val android = "com.google.dagger:dagger-android-support:2.21"
        const val compiler = "com.google.dagger:dagger-compiler:2.21"
        const val androidProcessor = "com.google.dagger:dagger-android-processor:2.21"
    }

    object RxJava {
        const val rxJava = "io.reactivex.rxjava2:rxjava:2.2.17"
        const val rxAndroid = "io.reactivex.rxjava2:rxandroid:2.1.1"
        const val rxKotlin = "io.reactivex.rxjava2:rxkotlin:2.4.0"
    }

    object RxBinding {
        private const val version = "3.1.0"
        const val rxBinding = "com.jakewharton.rxbinding3:rxbinding:$version"
        const val core = "com.jakewharton.rxbinding3:rxbinding-core:$version"
        const val appCompat = "com.jakewharton.rxbinding3:rxbinding-appcompat:$version"
        const val drawerLayout = "com.jakewharton.rxbinding3:rxbinding-drawerlayout:$version"
        const val leanback = "com.jakewharton.rxbinding3:rxbinding-leanback:$version"
        const val recyclerView = "com.jakewharton.rxbinding3:rxbinding-recyclerview:$version"
        const val slidingPaneLayout = "com.jakewharton.rxbinding3:rxbinding-slidingpanelayout:$version"
        const val swipeRefreshLayout = "com.jakewharton.rxbinding3:rxbinding-swiperefreshlayout:$version"
        const val viewPager = "com.jakewharton.rxbinding3:rxbinding-viewpager:$version"
        const val viewPager2 = "com.jakewharton.rxbinding3:rxbinding-viewpager2:$version"
        const val material = "com.jakewharton.rxbinding3:rxbinding-material:$version"
    }

    object Epoxy {
        private const val version = "3.9.0"
        const val core = "com.airbnb.android:epoxy:$version"
        const val processor = "com.airbnb.android:epoxy-processor:$version"
    }

    object Lifecycle {
        const val runtime = "androidx.lifecycle:lifecycle-runtime:2.2.0"
        const val extensions = "androidx.lifecycle:lifecycle-extensions:2.2.0"
        const val compiler = "androidx.lifecycle:lifecycle-compiler:2.2.0"
    }

    object Anko {
        private const val version = "0.10.8"
        const val sdk15 = "org.jetbrains.anko:anko-sdk15:$version"
        const val sdk21Coroutines = "org.jetbrains.anko:anko-sdk21-coroutines:$version"
        const val appcompatV7 = "org.jetbrains.anko:anko-appcompat-v7:$version"
        const val design = "org.jetbrains.anko:anko-design:$version"
        const val recyclerviewV7 = "org.jetbrains.anko:anko-recyclerview-v7:$version"
        const val constraintLayout = "cz.ackee:anko-constraint-layout:1.1.2"
    }

    object Stetho {
        const val core = "com.facebook.stetho:stetho:1.5.0"
        const val okHttp = "com.facebook.stetho:stetho-okhttp3:1.5.0"
    }

    object Room {
        private const val version = "2.2.4"
        const val runtime = "androidx.room:room-runtime:$version"
        const val rxJava2 = "androidx.room:room-rxjava2:$version"
        const val ktx = "androidx.room:room-ktx:$version"
        const val compiler = "androidx.room:room-compiler:$version"
        const val testing = "androidx.room:room-testing:$version"
    }

    const val material = "com.google.android.material:material:1.2.0-alpha05"
    const val threeTenAbp = "com.jakewharton.threetenabp:threetenabp:1.2.2"
    const val timber = "com.jakewharton.timber:timber:4.7.1"
    const val easyPermission = "pub.devrel:easypermissions:3.0.0"
    const val chipsLayoutManager = "com.beloo.widget:ChipsLayoutManager:0.3.7@aar"
    const val floatingView = "com.github.recruit-lifestyle:FloatingView:2.4.0"
    const val vintageChroma = "com.pavelsikun:vintage-chroma:1.5"
    const val eventSubject = "me.khol:eventsubject:1.0.0"
    const val swipeStack = "link.fls:swipestack:0.3.0"
    const val materialMenu = "com.balysv.materialmenu:material-menu:2.0.0"
    const val prettyTime = "org.ocpsoft.prettytime:prettytime:4.0.1.Final"
    const val firebase = "com.google.firebase:firebase-core:17.2.2"
    const val crashlytics = "com.crashlytics.sdk.android:crashlytics:2.10.1"
    const val fastScroll = "com.l4digital.fastscroll:fastscroll:2.0.0"
    const val jUnit = "junit:junit:4.12"
}
