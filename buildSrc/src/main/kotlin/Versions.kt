object Versions {

    const val applicationId = "me.khol.wordbox"

    const val compileSdk = 29
    const val minSdk = 21
    const val targetSdk = 29
}
