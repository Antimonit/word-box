package me.khol.wordbox.history.view.strategy

import android.content.Context
import android.graphics.Canvas
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.history.model.HistoryMonth
import me.khol.wordbox.history.view.Choreographer
import me.khol.wordbox.history.view.Grid
import me.khol.wordbox.history.view.Value
import me.khol.wordbox.history.view.ValueType

internal class Yearly(
    context: Context,
    choreographer: Choreographer
) : Strategy<HistoryMonth>(context, choreographer) {

    companion object {
        const val YEAR_MONTHS_COUNT = 12
    }

    override val grid = Grid(YEAR_MONTHS_COUNT)
    override val values = grid.points.map { (index, point) ->
        Value(context, ValueType.YEARLY, grid, index, point.x, point.y, context.dpf(16), 0f)
    }

    override fun prepareStart() {
        // TODO
    }

    override fun draw(canvas: Canvas) {
        // TODO
    }

    override fun prepareEnd() {
        // TODO
    }
}

