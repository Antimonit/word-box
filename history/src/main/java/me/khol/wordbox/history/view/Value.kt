package me.khol.wordbox.history.view

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import me.khol.wordbox.base.extensions.dpf

internal class Value(
    private val context: Context,
    var type: ValueType,
    val grid: Grid,
    val index: Int,
    var x: DynamicFloat,
    var y: DynamicFloat,
    var width: DynamicFloat,
    var height: DynamicFloat
) {

    constructor(
        context: Context,
        type: ValueType,
        grid: Grid,
        index: Int,
        x: Float, y: Float,
        width: Float, height: Float
    ) : this(
        context,
        type,
        grid,
        index,
        DynamicFloat(x), DynamicFloat(y),
        DynamicFloat(width), DynamicFloat(height)
    )

    private val topCorner = DynamicFloat()
    private val bottomCorner = DynamicFloat()
    private val pivotY = DynamicFloat()

    var progress = 1f
    val DynamicFloat.now: Float get() = current(progress)

    private val rect = RectF()
    private val backgroundPaint = Paint().apply { color = 0x3F3F528E }

    fun draw(canvas: Canvas) {
        topCorner.end = when (type) {
            ValueType.MONTHLY -> width.end / 2
            else -> context.dpf(4)
        }
        bottomCorner.end = when (type) {
            ValueType.MONTHLY -> width.end / 2
            else -> 0f
        }
        pivotY.start = when (type) {
            ValueType.MONTHLY -> 0f
            else -> -height.start / 2
        }
        pivotY.end = when (type) {
            ValueType.MONTHLY -> 0f
            else -> -height.end / 2
        }

        val left = -width.now / 2
        val top = -height.now / 2
        val right = width.now / 2
        val bottom = height.now / 2

        canvas.saveAndRestore {
            translate(x.now, y.now)
            translate(0f, pivotY.now)

            // Draw background. Draw top half and bottom half separately because top and bottom corner
            // radii might differ.
            rect.set(left, top, right, bottom)

            saveAndRestore {
                clipRect(left, top, right, 0f)
                drawRoundRect(rect, topCorner.now, topCorner.now, backgroundPaint)
            }
            saveAndRestore {
                clipRect(left, 0f, right, bottom)
                drawRoundRect(rect, bottomCorner.now, bottomCorner.now, backgroundPaint)
            }
        }
    }

    fun animateChanges() {
        topCorner.start = topCorner.now
        bottomCorner.start = bottomCorner.now
        pivotY.start = pivotY.now
    }
}
