package me.khol.wordbox.history.view

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import me.khol.wordbox.history.model.HistoryDay
import me.khol.wordbox.history.model.HistoryMonth
import me.khol.wordbox.history.view.strategy.Monthly
import me.khol.wordbox.history.view.strategy.Weekly
import me.khol.wordbox.history.view.strategy.Yearly
import org.threeten.bp.LocalDate

@Suppress("MemberVisibilityCanBePrivate")
class Overview @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val choreographer = Choreographer(::invalidate)

    private val weeklyStrategy = Weekly(context, choreographer)
    private val monthlyStrategy = Monthly(context, choreographer)
    private val yearlyStrategy = Yearly(context, choreographer)

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        weeklyStrategy.draw(canvas)
        monthlyStrategy.draw(canvas)
        yearlyStrategy.draw(canvas)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        weeklyStrategy.onSizeChanged(w, h)
        monthlyStrategy.onSizeChanged(w, h)
        yearlyStrategy.onSizeChanged(w, h)
    }

    fun setWeekly(date: LocalDate, events: List<HistoryDay>) {
        weeklyStrategy.setData(date, events)
    }

    fun setMonthly(date: LocalDate, events: List<HistoryDay>) {
        monthlyStrategy.setData(date, events)
    }

    fun setYearly(date: LocalDate, events: List<HistoryMonth>) {
        yearlyStrategy.setData(date, events)
    }
}
