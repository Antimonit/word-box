package me.khol.wordbox.history.view.strategy

import android.content.Context
import android.graphics.Canvas
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.history.model.HistoryDay
import me.khol.wordbox.history.view.Choreographer
import me.khol.wordbox.history.view.Grid
import me.khol.wordbox.history.view.Value
import me.khol.wordbox.history.view.ValueType

internal class Monthly(
    context: Context,
    choreographer: Choreographer
) : Strategy<HistoryDay>(context, choreographer) {

    companion object {
        const val MONTH_DAYS_ROW_COUNT = 6
        const val MONTH_DAYS_COLUMN_COUNT = 7
    }

    override val grid = Grid(MONTH_DAYS_COLUMN_COUNT, MONTH_DAYS_ROW_COUNT)
    override val values = grid.points.map { (index, point) ->
        Value(context, ValueType.MONTHLY, grid, index, point.x, point.y, 0f, 0f)
    }

    override fun prepareStart() {
        val maxValue = events.maxBy { it.count }?.count ?: 1

        val firstDay = date.withDayOfMonth(1)
        val lastDay = firstDay.plusMonths(1).minusDays(1)
        val firstDayIndex = firstDay.dayOfWeek.value - 1
        val dayCount = lastDay.dayOfMonth
        val lastDayIndex = firstDayIndex + dayCount

//        // Hide placeholders that are not used
//        for (week in 4 until 6) {
//            placeholderMonthRows[week]!!.forEach {
//                it.isVisible = lastDayIndex > week * 7
//            }
//        }

        viewsWithProgress { view, index, progress ->
            val labelDayIndex = index - firstDayIndex + 1
            val active = index in firstDayIndex until lastDayIndex
            val value = events.find { it.day == labelDayIndex }?.count ?: 0
            val size = context.dpf(72) * value / maxValue

//            placeholder.text = if (active) "$labelDayIndex" else ""

            // Don't toggle view's visibility. It doesn't update view's position when
            // fading out.
//            it.animate().alpha(if (active) 1f else 0f).setDuration(500).start()

            view.height.start = view.height.current(progress)
            view.height.end = size
            view.width.start = view.width.current(progress)
            view.width.end = size
            view.animateChanges()
        }
    }

    override fun draw(canvas: Canvas) {
        viewsWithProgress { view, index, progress ->
            view.progress = progress
            view.draw(canvas)
        }
    }

    override fun prepareEnd() {
        viewsWithProgress { view, index, progress ->
            view.height.start = view.height.current(progress)
            view.height.end = context.dpf(0)
            view.width.start = view.width.current(progress)
            view.width.end = context.dpf(0)
            view.animateChanges()
        }
    }
}
