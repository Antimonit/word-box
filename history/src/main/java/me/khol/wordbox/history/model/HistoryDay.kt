package me.khol.wordbox.history.model

data class HistoryDay(
    val count: Int,
    val year: Int,
    val month: Int,
    val day: Int
)
