package me.khol.wordbox.history.view

internal enum class ValueType {
    WEEKLY,
    MONTHLY,
    YEARLY
}
