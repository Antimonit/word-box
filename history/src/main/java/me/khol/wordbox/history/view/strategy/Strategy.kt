package me.khol.wordbox.history.view.strategy

import android.content.Context
import android.graphics.Canvas
import me.khol.wordbox.history.view.Choreographer
import me.khol.wordbox.history.view.Grid
import me.khol.wordbox.history.view.Value
import org.threeten.bp.LocalDate

internal abstract class Strategy<T>(
    protected val context: Context,
    private val choreographer: Choreographer
) {

    var date: LocalDate = LocalDate.now()
    var events: List<T> = emptyList()

    abstract val grid: Grid
    abstract val values: List<Value>

    abstract fun prepareStart()
    abstract fun draw(canvas: Canvas)
    abstract fun prepareEnd()

    fun setData(date: LocalDate, events: List<T>) {
        this.date = date
        this.events = events
        choreographer.transitionToStrategy(this)
    }

    fun onSizeChanged(width: Number, height: Number) {
        grid.setSize(width, height)

        (0 until grid.points.size).map { index ->
            val point = grid.points[index]!!
            val value = values[index]
            value.x.both = point.x
            value.y.both = point.y
        }
    }

    fun viewsWithProgress(action: (Value, Int, Float) -> Unit) {
        values.forEachIndexed { index, view ->
            action(view, index, choreographer.progressFor(view))
        }
    }
}
