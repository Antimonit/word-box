package me.khol.wordbox.history.view

import android.animation.ValueAnimator
import androidx.core.animation.doOnEnd
import me.khol.wordbox.history.view.strategy.Strategy

/**
 * Manages animation for every view including delays. Furthermore handles transition between
 * two [Strategy].
 */
internal class Choreographer(val onProgressUpdate: () -> Unit) {

    companion object {
        private const val ANIMATION_BASE_DURATION = 400L
        private const val ANIMATION_DELAY_DURATION = 300L
        private const val ANIMATION_DURATION = ANIMATION_BASE_DURATION + ANIMATION_DELAY_DURATION
        private const val TOTAL_BASE_DURATION_RATIO = ANIMATION_DURATION.toFloat() / ANIMATION_BASE_DURATION
        private const val DELAY_BASE_DURATION_RATIO = ANIMATION_DELAY_DURATION.toFloat() / ANIMATION_BASE_DURATION
    }

    private var progress = 1f

    private var currentAnimation: ValueAnimator? = null
    private var currentStrategy: Strategy<*>? = null
    private var pendingStrategy: Strategy<*>? = null

    fun transitionToStrategy(newStrategy: Strategy<*>) {
        currentAnimation?.cancel()

        pendingStrategy = newStrategy
        currentStrategy?.prepareEnd()
        currentAnimation = animate {
            currentStrategy = pendingStrategy
            currentStrategy?.prepareStart()
            currentAnimation = animate()
        }
    }

    fun progressFor(value: Value): Float {
        return if (currentStrategy?.values?.contains(value) == true) {
            val delay = value.index.toFloat() / value.grid.points.size
            val offset = progress - delay * DELAY_BASE_DURATION_RATIO
            offset.coerceIn(0f, 1f)
        } else {
            1f
        }
    }

    private fun animate(onEnd: () -> Unit = {}): ValueAnimator {
        return ValueAnimator.ofFloat(0f, TOTAL_BASE_DURATION_RATIO).apply {
            duration = ANIMATION_DURATION
            addUpdateListener {
                progress = it.animatedValue as Float
                onProgressUpdate()
            }
            doOnEnd {
                onEnd()
            }
            start()
        }
    }
}
