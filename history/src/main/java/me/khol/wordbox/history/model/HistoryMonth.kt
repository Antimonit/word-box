package me.khol.wordbox.history.model

data class HistoryMonth(
    val count: Int,
    val year: Int,
    val month: Int
)

