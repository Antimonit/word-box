package me.khol.wordbox.history.view

import android.graphics.Canvas
import androidx.core.graphics.withClip
import androidx.core.graphics.withTranslation

/**
 * TODO: Replace with ktx extensions such as [withClip] or [withTranslation]
 */
internal inline fun Canvas.saveAndRestore(lambda: Canvas.() -> Unit) {
    save()
    lambda()
    restore()
}
