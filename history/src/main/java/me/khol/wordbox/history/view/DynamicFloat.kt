package me.khol.wordbox.history.view

import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator

/**
 * Helper to tween between [start] and [end] values.
 */
internal class DynamicFloat(
    var start: Float = 0f,
    var end: Float = start,
    var interpolator: Interpolator = AccelerateDecelerateInterpolator()
) {
    var both: Float
        get() = throw IllegalAccessException("No get")
        set(value) {
            start = value
            end = value
        }

    fun current(progress: Float): Float {
        val interpolation = interpolator.getInterpolation(progress)
        return start * (1 - interpolation) + end * interpolation
    }
}
