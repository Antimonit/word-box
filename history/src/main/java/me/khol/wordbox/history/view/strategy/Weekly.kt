package me.khol.wordbox.history.view.strategy

import android.content.Context
import android.graphics.Canvas
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.history.model.HistoryDay
import me.khol.wordbox.history.view.Choreographer
import me.khol.wordbox.history.view.Grid
import me.khol.wordbox.history.view.Value
import me.khol.wordbox.history.view.ValueType
import org.threeten.bp.DayOfWeek

internal class Weekly(
    context: Context,
    choreographer: Choreographer
) : Strategy<HistoryDay>(context, choreographer) {

    companion object {
        private const val WEEK_DAYS_COUNT = 7
    }

    override val grid = Grid(WEEK_DAYS_COUNT)
    override val values = grid.points.map { (index, point) ->
        Value(context, ValueType.WEEKLY, grid, index, point.x, point.y, context.dpf(16), 0f)
    }

    override fun prepareStart() {
        val maxValue = events.maxBy { it.count }?.count ?: 1

        viewsWithProgress { view, index, progress ->
            val labelDayIndex = date.with(DayOfWeek.of(index + 1)).dayOfMonth
            val value = events.find { it.day == labelDayIndex }?.count ?: 0
            val size = context.dpf(72) * value / maxValue

            view.height.start = view.height.current(progress)
            view.height.end = size
            view.animateChanges()
        }
    }

    override fun draw(canvas: Canvas) {
        viewsWithProgress { view, _, progress ->
            view.progress = progress
            view.draw(canvas)
        }
    }

    override fun prepareEnd() {
        viewsWithProgress { view, _, progress ->
            view.height.start = view.height.current(progress)
            view.height.end = context.dpf(0)
            view.animateChanges()
        }
    }
}
