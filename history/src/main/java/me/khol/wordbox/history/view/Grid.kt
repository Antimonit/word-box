package me.khol.wordbox.history.view

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint

/**
 * Holds [countX] * [countY] points that are equally spaced out across [width] x [height] area.
 * This is used just for calculating positions and does not draw or animate anything.
 *
 * Changing [width] and [height] will also affect position of each and every [Grid.Point].
 */
internal class Grid(private val countX: Int, private val countY: Int = 1) {

    companion object {
        private const val DEBUG = true
        private val DEBUG_PAINT = Paint().apply {
            color = Color.MAGENTA
            strokeWidth = 5f
            style = Paint.Style.STROKE
        }
    }

    // Actual width of the canvas
    var width: Float = 0f
    // Actual height of the canvas
    var height: Float = 0f

    var points: Map<Int, Point>
    var pointsColumns: Map<Int, Collection<Point>>
    var pointsRows: Map<Int, Collection<Point>>

    init {
        points =
            (0 until countY).flatMap { y ->
                (0 until countX).map { x ->
                    countX * y + x to Point(x, y)
                }
            }.toMap()

        pointsRows = points.entries.groupBy({ it.key / countY }, { it.value })
        pointsColumns = points.entries.groupBy({ it.key % countY }, { it.value })
    }

    fun setSize(width: Number, height: Number) {
        this.width = width.toFloat()
        this.height = height.toFloat()
    }

    @Suppress("ConstantConditionIf")
    fun draw(canvas: Canvas) {
        if (DEBUG) {
            canvas.drawRect(0f, 0f, width, height, DEBUG_PAINT)
            points.forEach { (_, point) ->
                point.draw(canvas)
            }
        }
    }

    /**
     * [x] and [y] position can dynamically change based on grid's [width] and [height].
     */
    inner class Point(private val indexX: Int, private val indexY: Int) {
        val x get() = center(width, indexX, countX)
        val y get() = center(height, indexY, countY)

        private fun center(width: Float, index: Int, count: Int): Float {
            return width * (index * 2 + 1) / (count * 2)
        }

        @Suppress("ConstantConditionIf")
        fun draw(canvas: Canvas) {
            if (DEBUG) {
                canvas.drawPoint(x, y, DEBUG_PAINT)
            }
        }
    }
}
