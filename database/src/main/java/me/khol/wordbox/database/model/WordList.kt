package me.khol.wordbox.database.model

import androidx.room.Ignore
import androidx.room.Relation
import me.khol.wordbox.database.model.internal.DBWordLabel
import me.khol.wordbox.database.model.internal.DBWordWord
import org.threeten.bp.Instant

/**
 * Used in the main screen with list of all words that meet filter criteria.
 */
internal class DBWordList(
    val wordId: Long,
    val text: String,
    val description: String,
    val scheduledTime: Instant
) {

    /**
     * Fetch a list of [WordLabel]s and map them to [Label]s
     */
    @Relation(parentColumn = "wordId", entityColumn = "wordId", entity = DBWordLabel::class)
    internal lateinit var wordLabels: List<WordLabel>

    @delegate:Ignore
    val labels: List<Label> by lazy { wordLabels.map { it.label } }

    /**
     * Fetch a list of [WordWordSimple]s and map them to [TranslationSimple]s
     */
    @Relation(parentColumn = "wordId", entityColumn = "wordId_from", entity = DBWordWord::class)
    internal lateinit var wordWords: List<WordWordSimple>

    @delegate:Ignore
    val translations: List<TranslationSimple> by lazy { wordWords.map { it.translation } }

    /**
     * Builds a clean version of [DBWordList] that does not expose internal fields such as
     * [wordLabels] and [wordWords] which should be accessed by
     * [labels] and [translations] respectively.
     */
    fun toClean() = WordList(wordId, text, description, scheduledTime, labels, translations)
}

data class WordList(
    val wordId: Long,
    val text: String,
    val description: String,
    val scheduledTime: Instant,
    val labels: List<Label>,
    val translations: List<TranslationSimple>
)
