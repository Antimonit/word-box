package me.khol.wordbox.database.model

import androidx.room.Ignore
import androidx.room.Relation
import me.khol.wordbox.database.model.internal.DBWord

/**
 * Word-Word is an M:N relation that stores information about words' translations.
 *
 * Because both [WordList] and [WordDetail] expect different contents of the translations, we
 * define two separate *transformers*. [WordWordSimple] is simpler and faster, because
 * it does not care about translations' languages whereas [WordWordFull] does an extra join
 * to fetch translations' languages.
 */
internal data class WordWordSimple(val wordId_from: Long, val wordId_to: Long) {
    /**
     * Fetch all [translations], can be safely reduced to a single [WordList.Translation]
     */
    @Relation(parentColumn = "wordId_to", entityColumn = "wordId", entity = DBWord::class)
    lateinit var translations: List<TranslationSimple>

    @delegate:Ignore
    val translation: TranslationSimple by lazy { translations[0] }
}

/**
 * Word-Word is an M:N relation that stores information about words' translations.
 *
 * Because both [WordList] and [WordDetail] expect different contents of the translations, we
 * define two separate *transformers*. [WordWordSimple] is simpler and faster, because
 * it does not care about translations' languages whereas [WordWordFull] does an extra join
 * to fetch translations' languages.
 */
internal data class WordWordFull(val wordId_from: Long, val wordId_to: Long) {
    /**
     * Fetch all [translations], can be safely reduced to a single [WordDetail.Translation]
     */
    @Relation(parentColumn = "wordId_to", entityColumn = "wordId", entity = DBWord::class)
    lateinit var translations: List<DBTranslationFull>

    @delegate:Ignore
    val translation: DBTranslationFull by lazy { translations[0] }
}
