package me.khol.wordbox.database.model

data class TranslationSimple(
    val wordId: Long,
    val text: String
)
