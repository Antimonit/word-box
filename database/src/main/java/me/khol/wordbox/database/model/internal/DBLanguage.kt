package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "language"
)
internal data class DBLanguage(
    @PrimaryKey(autoGenerate = true)
    val languageId: Long,
    val name: String,
    val shortName: String,
    val order: Int
)
