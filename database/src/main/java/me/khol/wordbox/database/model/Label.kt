package me.khol.wordbox.database.model

data class Label(
    val labelId: Long,
    val text: String,
    val color: Int,
    val order: Int
)
