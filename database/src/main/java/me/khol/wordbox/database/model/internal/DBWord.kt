package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey
import me.khol.wordbox.database.model.Word
import org.threeten.bp.Duration
import org.threeten.bp.Instant

@Entity(
    tableName = "word",
    foreignKeys = [
        ForeignKey(
            entity = DBLanguage::class,
            parentColumns = ["languageId"],
            childColumns = ["languageId"],
            onDelete = CASCADE
        )
    ],
    indices = [
        Index(value = ["languageId"])
    ]
)
internal data class DBWord(
    @PrimaryKey(autoGenerate = true)
    val wordId: Long,
    val languageId: Long,
    val text: String,
    val description: String = "",
    val timeCreated: Instant = Instant.now(),
    val timeModified: Instant = Instant.now(),
    // scheduling
    val easeFactor: Float = Word.DEFAULT_EASE_FACTOR,
    val averageEaseFactor: Float = Word.DEFAULT_AVERAGE_EASE_FACTOR,
    val averageSuccessRate: Float = Word.DEFAULT_AVERAGE_SUCCESS_RATE,
    val practiceRounds: Int = Word.DEFAULT_PRACTICE_ROUNDS,
    val interval: Duration = Word.DEFAULT_INTERVAL,
    val scheduledTime: Instant = Word.DEFAULT_SCHEDULED_TIME,
    val lastTime: Instant? = Word.DEFAULT_LAST_TIME
)
