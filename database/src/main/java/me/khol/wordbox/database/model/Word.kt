package me.khol.wordbox.database.model

import org.threeten.bp.Duration
import org.threeten.bp.Instant
import kotlin.random.Random

object Word {
    const val DEFAULT_EASE_FACTOR = 2f
    const val DEFAULT_AVERAGE_EASE_FACTOR = 2.0f
    const val DEFAULT_AVERAGE_SUCCESS_RATE = 0.5f
    const val DEFAULT_PRACTICE_ROUNDS = 0
    val DEFAULT_INTERVAL: Duration
        get() {
            // Make default interval slightly random so that words that were added right after
            // each other don't end up in practice in the same order as they were added.
            val day = Duration.ofDays(1).toMillis()
            val randomMultiplier = Random.nextDouble(1 / 1.1, 1.1)
            return Duration.ofMillis((day * randomMultiplier).toLong())
        }
    val DEFAULT_SCHEDULED_TIME: Instant
        get() = Instant.now()
    val DEFAULT_LAST_TIME: Instant? = null
}