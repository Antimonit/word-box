package me.khol.wordbox.database.model

data class PracticeDayHistory(
    val count: Int,
    val year: Int,
    val month: Int,
    val day: Int
)
