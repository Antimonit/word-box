package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Flowable
import me.khol.wordbox.database.model.internal.DBExample
import me.khol.wordbox.database.model.Example

@Dao
abstract class ExampleDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(examples: List<DBExample>): List<Long>

    fun insert(wordId: Long, examples: List<Example>): List<Long> {
        return insertRaw(examples.map {
            // Pass 0 as an exampleId and room will auto-generate a unique id
            DBExample(
                0,
                wordId,
                it.description
            )
        })
    }

    @Query("DELETE FROM example WHERE wordId = :wordId")
    abstract fun delete(wordId: Long)

    @Query("""
        SELECT exampleId, description
        FROM example
        WHERE example.wordId = :wordId
    """)
    abstract fun getWordExamples(wordId: Long): Flowable<List<Example>>
}
