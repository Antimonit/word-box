package me.khol.wordbox.database.model

enum class PracticeHistoryType {
    Easy,
    Hard,
    Reset
}
