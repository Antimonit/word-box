package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import io.reactivex.Flowable
import io.reactivex.Maybe
import me.khol.wordbox.database.model.internal.DBLanguage
import me.khol.wordbox.database.model.Language

@Dao
abstract class LanguageDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(item: DBLanguage): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(list: List<DBLanguage>): List<Long>

    @Update
    internal abstract fun updateRaw(item: DBLanguage)

    @Transaction
    @Update
    internal abstract fun updateRaw(list: List<DBLanguage>)

    private fun Language.toDBLanguage() = DBLanguage(languageId, name, shortName, order)

    fun insert(language: Language) = insertRaw(language.toDBLanguage())

    fun insert(languages: List<Language>) = insertRaw(languages.map { it.toDBLanguage() })

    fun update(language: Language) = updateRaw(language.toDBLanguage())

    fun update(languages: List<Language>) = updateRaw(languages.map { it.toDBLanguage() })


    @Query("DELETE FROM language WHERE languageId = :languageId")
    abstract fun delete(languageId: Long)

    @Query("SELECT * FROM language WHERE languageId = :languageId")
    abstract fun getLanguage(languageId: Long): Maybe<Language>

    @Query("""
        SELECT *
        FROM language
        ORDER BY `order`
    """)
    abstract fun getAllLanguages(): Flowable<List<Language>>

    @Query("SELECT COUNT(wordId) FROM word WHERE languageId = :languageId")
    abstract fun getLanguageWordCount(languageId: Long): Flowable<Int>

}
