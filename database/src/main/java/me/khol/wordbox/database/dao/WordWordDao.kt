package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import me.khol.wordbox.database.model.internal.DBWordWord
import me.khol.wordbox.database.model.TranslationFull

@Dao
abstract class WordWordDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(list: List<DBWordWord>): List<Long>

    internal fun insertBothWays(list: List<DBWordWord>) {
        insertRaw(list.flatMap {
            listOf(it, it.copy(wordId_from = it.wordId_to, wordId_to = it.wordId_from))
        })
    }

    fun insertBothWays(wordId: Long, translations: List<TranslationFull>) {
        insertBothWays(translations.map {
            DBWordWord(wordId, it.wordId)
        })
    }

    @Query("""
        DELETE FROM word_word
        WHERE
            (wordId_from = :wordIdFrom AND wordId_to = :wordIdTo)
        OR
            (wordId_from = :wordIdTo AND wordId_to = :wordIdFrom)
    """)
    abstract fun deleteBothWays(wordIdFrom: Long, wordIdTo: Long)

    @Query("""
        DELETE FROM word_word
        WHERE
            (wordId_from = :wordId)
        OR
            (wordId_to = :wordId)
    """)
    abstract fun deleteBothWays(wordId: Long)
}
