package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Query

@Dao
interface StatsDao {

    @Query("""SELECT count() FROM word""")
    fun getTotalWordCount(): Int
}
