package me.khol.wordbox.database.model

import org.threeten.bp.Instant

/**
 * Model of a Note used in detail screen of a single note.
 */
data class NoteDetail(
    val noteId: Long,
    val title: String,
    val contents: String,
    val timeCreated: Instant,
    val timeModified: Instant
)
