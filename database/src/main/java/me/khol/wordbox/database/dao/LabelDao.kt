package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import io.reactivex.Flowable
import io.reactivex.Single
import me.khol.wordbox.database.model.internal.DBLabel
import me.khol.wordbox.database.model.Label

@Dao
abstract class LabelDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(item: DBLabel): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(list: List<DBLabel>): List<Long>

    @Update
    internal abstract fun updateRaw(item: DBLabel)

    @Transaction
    @Update
    internal abstract fun updateRaw(list: List<DBLabel>)

    private fun Label.toDBLabel() = DBLabel(labelId, text, color, order)

    fun insert(label: Label) = insertRaw(label.toDBLabel())

    fun insert(labels: List<Label>) = insertRaw(labels.map { it.toDBLabel() })

    fun update(label: Label) = updateRaw(label.toDBLabel())

    fun update(labels: List<Label>) = updateRaw(labels.map { it.toDBLabel() })

    @Query("DELETE FROM label WHERE labelId = :labelId")
    abstract fun delete(labelId: Long)

    @Query("""
        SELECT *
        FROM label
        ORDER BY `order`
    """)
    abstract fun getAllLabels(): Flowable<List<Label>>

    @Query("""
        SELECT *
        FROM label
        WHERE label.labelId IN (:ids)
        ORDER BY `order`
    """)
    abstract fun getLabelsWithIds(ids: List<Long>): Single<List<Label>>

    @Query("""
        SELECT label.*
        FROM label
        JOIN word_label
        ON word_label.labelId = label.labelId
        WHERE word_label.wordId = :wordId
        ORDER BY `order`
    """)
    abstract fun getWordLabels(wordId: Long): Flowable<List<Label>>

    @Query("SELECT COUNT(wordId) FROM word_label WHERE labelId = :labelId")
    abstract fun getLabelWordCount(labelId: Long): Flowable<Int>
}
