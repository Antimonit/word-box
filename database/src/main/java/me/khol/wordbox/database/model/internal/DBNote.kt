package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(tableName = "note")
internal data class DBNote(
    @PrimaryKey(autoGenerate = true)
    val noteId: Long,
    val title: String,
    val contents: String,
    val timeCreated: Instant = Instant.now(),
    val timeModified: Instant = Instant.now()
)
