package me.khol.wordbox.database.model

/**
 * Model of a Note used in overview screen of all notes.
 */
data class NoteList(
    val noteId: Long,
    val title: String,
    val contents: String
)
