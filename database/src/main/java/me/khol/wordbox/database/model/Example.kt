package me.khol.wordbox.database.model

data class Example(
    val exampleId: Long,
    val description: String
)
