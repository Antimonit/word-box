package me.khol.wordbox.database

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase

/**
 * A singleton wrapper around [WordBoxDatabase]. The wrapper allows us to replace the database
 * object reference during runtime when we need to temporarily close and open the database.
 * Reopening the database creates a new reference. Other classes should not keep references to
 * database DAOs but rather query for the dao each time it needs to access data.
 *
 * Also encapsulates internal [WordBoxDatabase] extending [RoomDatabase] class that is available
 * only in the database module.
 */
class WordBoxDatabaseWrapper(private val context: Context) : DatabaseDaoProvider {

    companion object {
        const val VERSION = WordBoxDatabase.VERSION
        const val NAME = WordBoxDatabase.NAME
    }

    private var room: WordBoxDatabase? = null

    private val database: WordBoxDatabase
        get() = room ?: synchronized(this) {
            room ?: buildDatabase(context).also {
                room = it
            }
        }

    private fun buildDatabase(context: Context): WordBoxDatabase {
        lateinit var room: WordBoxDatabase

        val callback = object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
//                doAsync {
//                    // room.loadDummyData()
//                    // room.parseBackupFromLanguageCheck(context)
//                }
            }
        }

        room = Room
            .databaseBuilder(context, WordBoxDatabase::class.java, NAME)
            .addMigrations(
                WordBoxDatabase.MIGRATION_4_5,
                WordBoxDatabase.MIGRATION_5_6,
                WordBoxDatabase.MIGRATION_6_7,
                WordBoxDatabase.MIGRATION_7_8,
                WordBoxDatabase.MIGRATION_8_9
            )
            .addCallback(callback)
            .build()

        return room
    }

    fun closeDatabase() {
        if (database.isOpen) {
            database.close()
        }
    }

    fun openDatabase() {
        if (!database.isOpen) {
            room = null
            // Touch database to initialize it.
            // Also force run migrations by running any transaction.
            database.runInTransaction { /* do nothing */ }
        }
    }

    fun runInTransaction(body: () -> Unit): Unit = database.runInTransaction(body)

    fun <V> runInTransaction(body: () -> V): V = database.runInTransaction(body)

    override fun wordDao() = database.wordDao()
    override fun languageDao() = database.languageDao()
    override fun labelDao() = database.labelDao()
    override fun exampleDao() = database.exampleDao()
    override fun wordWordDao() = database.wordWordDao()
    override fun wordLabelDao() = database.wordLabelDao()
    override fun noteDao() = database.noteDao()
    override fun historyDao() = database.historyDao()
    override fun statsDao() = database.statsDao()
}