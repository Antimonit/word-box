package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "example",
    foreignKeys = [
        ForeignKey(
            entity = DBWord::class,
            parentColumns = ["wordId"],
            childColumns = ["wordId"],
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index(value = ["wordId"])
    ]
)
internal data class DBExample(
    @PrimaryKey(autoGenerate = true)
    val exampleId: Long,
    val wordId: Long,
    val description: String
)
