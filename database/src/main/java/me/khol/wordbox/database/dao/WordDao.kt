package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Completable
import io.reactivex.Flowable
import me.khol.wordbox.database.model.internal.DBWord
import me.khol.wordbox.database.model.DBWordDetail
import me.khol.wordbox.database.model.DBWordList
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.database.model.WordList
import me.khol.wordbox.database.model.WordSuggestion
import org.threeten.bp.Duration
import org.threeten.bp.Instant

@Dao
abstract class WordDao {

    companion object {

        /**
         * Used by [getWordsListInternal] and [getWordsListSearchInternal] queries.
         *
         * The queries can't be combined into a one because SQLite does not support MATCH keyword
         * that is conditioned by another variable. In our case [getWordsListSearchInternal] is
         * executed when search query is not empty and [getWordsListInternal] otherwise.
         */
        private const val ORDER = """
            ORDER BY
            CASE :order WHEN 'text' then word.text END ASC,
            CASE :order WHEN 'timeCreated' THEN word.timeCreated END DESC,
            CASE :order WHEN 'timeModified' THEN word.timeModified END DESC,
            CASE :order WHEN 'progress' THEN word.scheduledTime END DESC
        """
    }

    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(item: DBWord): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(list: List<DBWord>): List<Long>

    private fun WordDetail.toDBWord() = DBWord(
        0, // generate id
        language.languageId,
        text,
        description
    )

    fun insert(word: WordDetail): Long {
        return insertRaw(word.toDBWord())
    }

    fun insert(words: List<WordDetail>): List<Long> {
        return insertRaw(words.map { word -> word.toDBWord() })
    }

    @Query("DELETE FROM word WHERE wordId = :wordId")
    abstract fun delete(wordId: Long): Completable

    /**
     * Retrieves a Word with language, translations, examples and labels
     */
    @Query("""
        SELECT wordId, text, description, easeFactor, averageEaseFactor, averageSuccessRate,
        practiceRounds, interval, scheduledTime, lastTime, language.*
        FROM word
        JOIN language
        ON word.languageId = language.languageId
        WHERE word.wordId = :wordId
    """)
    @Transaction
    internal abstract fun getWordDetailInternal(wordId: Long): Flowable<DBWordDetail>

    fun getWordDetail(wordId: Long): Flowable<WordDetail> {
        return getWordDetailInternal(wordId).map { it.toClean() }
    }

    @Transaction
    @Query("""
        SELECT word.wordId, word.text, word.description, word.scheduledTime
        FROM word
        JOIN word_fts
        ON (word.wordId = word_fts.`rowid`)
        WHERE word_fts.text MATCH :term
        $ORDER
    """)
    internal abstract fun getWordsListSearchInternal(
        term: String,
        order: String
    ): Flowable<List<DBWordList>>

    @Query("""
        SELECT word.wordId, word.text, word.description, word.scheduledTime
        FROM word
        JOIN word_fts
        ON word.wordId = word_fts.`rowid`
        WHERE word.languageId = :languageId
        AND (
            NOT :hasLabels
            OR wordId in (
                SELECT wordId
                FROM word_label
                WHERE labelId IN (:labels)
            )
        )
        $ORDER
    """)
    @Transaction
    internal abstract fun getWordsListInternal(
        languageId: Long,
        hasLabels: Boolean,
        labels: List<Long>,
        order: String
    ): Flowable<List<DBWordList>>

    /**
     * Retrieves list of words that match [search] query specified by the user. If the [search]
     * query is empty then just display words that match [languageId] and [labels].
     */
    fun getWordsList(
        languageId: Long,
        labels: List<Long>,
        order: String,
        search: String
    ): Flowable<List<WordList>> {
        return if (search.isNotEmpty()) {
            getWordsListSearchInternal("*$search*", order)
        } else {
            getWordsListInternal(languageId, labels.isNotEmpty(), labels, order)
        }.map { words ->
            words.map { word ->
                word.toClean()
            }
        }
    }

    @Query("""
        SELECT wordId, text, description, language.*
        FROM word
        JOIN language
        ON word.languageId = language.languageId
        WHERE word.wordId NOT IN (:filterIds)
        AND word.text LIKE :query
        LIMIT 5
    """)
    @Transaction
    abstract fun getWordSuggestions(query: String, filterIds: List<Long>): List<WordSuggestion>

    @Query("""
        SELECT wordId, text, description, easeFactor, averageEaseFactor, averageSuccessRate,
        practiceRounds, interval, scheduledTime, lastTime, language.*
        FROM word
        JOIN language
        ON word.languageId = language.languageId
        AND word.languageId = :languageId
        WHERE word.scheduledTime = (
                SELECT MIN(scheduledTime)
                FROM word
                WHERE word.languageId = :languageId
        )
        LIMIT 1
    """)
    @Transaction
    internal abstract fun getPracticeWordInternal(languageId: Long): DBWordDetail

    fun getPracticeWord(languageId: Long): WordDetail {
        return getPracticeWordInternal(languageId).toClean()
    }

    @Query("""
        SELECT wordId, text, description, easeFactor, averageEaseFactor, averageSuccessRate,
        practiceRounds, interval, scheduledTime, lastTime, language.*
        FROM word
        JOIN language
        ON word.languageId = language.languageId
        AND word.languageId = :languageId
        ORDER BY word.scheduledTime ASC
        LIMIT :limit
    """)
    @Transaction
    internal abstract fun getPracticeWordsInternal(languageId: Long, limit: Int): List<DBWordDetail>

    fun getPracticeWords(languageId: Long, limit: Int): List<WordDetail> {
        return getPracticeWordsInternal(languageId, limit).map { it.toClean() }
    }

    @Query("""
        UPDATE word
        SET scheduledTime = :scheduledTime,
                lastTime = :lastTime,
                interval = :interval,
                averageEaseFactor = :averageEaseFactor,
                averageSuccessRate = :averageSuccessRate,
                easeFactor = :easeFactor,
                practiceRounds = :practiceRounds
        WHERE wordId = :wordId
    """)
    abstract fun updateAfterPracticeResult(
        wordId: Long,
        easeFactor: Float,
        averageEaseFactor: Float,
        averageSuccessRate: Float,
        practiceRounds: Int,
        interval: Duration,
        scheduledTime: Instant,
        lastTime: Instant?
    )

    @Query("""
        UPDATE word
        SET languageId = :languageId,
                text = :text,
                description = :description,
                timeModified = :timeModified,
                scheduledTime = :scheduledTime,
                lastTime = :lastTime,
                interval = :interval,
                averageEaseFactor = :averageEaseFactor,
                averageSuccessRate = :averageSuccessRate,
                easeFactor = :easeFactor,
                practiceRounds = :practiceRounds
        WHERE wordId = :wordId
    """)
    abstract fun updateAfterEdit(
        wordId: Long,
        languageId: Long,
        text: String,
        description: String,
        timeModified: Instant,
        scheduledTime: Instant,
        lastTime: Instant?,
        interval: Duration,
        averageEaseFactor: Float,
        averageSuccessRate: Float,
        easeFactor: Float,
        practiceRounds: Int
    )
}
