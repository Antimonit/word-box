package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index

@Entity(
    tableName = "word_word",
    primaryKeys = ["wordId_from", "wordId_to"],
    foreignKeys = [
        ForeignKey(
            entity = DBWord::class,
            parentColumns = ["wordId"],
            childColumns = ["wordId_from"],
            onDelete = CASCADE
        ),
        ForeignKey(
            entity = DBWord::class,
            parentColumns = ["wordId"],
            childColumns = ["wordId_to"],
            onDelete = CASCADE
        )
    ],
    indices = [
        Index(value = ["wordId_from"]),
        Index(value = ["wordId_to"])
    ]
)
internal data class DBWordWord(
    val wordId_from: Long,
    val wordId_to: Long
)
