package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Completable
import io.reactivex.Flowable
import me.khol.wordbox.database.model.internal.DBPracticeHistory
import me.khol.wordbox.database.model.PracticeDayHistory
import me.khol.wordbox.database.model.PracticeHistoryType
import me.khol.wordbox.database.model.PracticeMonthHistory
import org.threeten.bp.Instant

@Dao
abstract class HistoryDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(item: DBPracticeHistory): Completable

    fun insert(wordId: Long, type: PracticeHistoryType, time: Instant): Completable {
        return insertRaw(
            DBPracticeHistory(
                0, // generate id
                wordId,
                when (type) {
                    PracticeHistoryType.Easy -> DBPracticeHistory.Type.Easy
                    PracticeHistoryType.Hard -> DBPracticeHistory.Type.Hard
                    PracticeHistoryType.Reset -> DBPracticeHistory.Type.Reset
                },
                time
            )
        )
    }

    @Query("""
        SELECT
            COUNT() as count,
            cast(strftime('%m', time/1000, 'unixepoch') as INTEGER) as month,
            cast(strftime('%Y', time/1000, 'unixepoch') as INTEGER) as year
        FROM practice_history
        WHERE year = :year
        GROUP BY year, month
    """)
    @Transaction
    abstract fun getYearlyOverview(year: Int): Flowable<List<PracticeMonthHistory>>

    @Query("""
        SELECT
            COUNT() as count,
            cast(strftime('%d', time/1000, 'unixepoch') as INTEGER) as day,
            cast(strftime('%m', time/1000, 'unixepoch') as INTEGER) as month,
            cast(strftime('%Y', time/1000, 'unixepoch') as INTEGER) as year
        FROM practice_history
        WHERE year = :year AND month = :month
        GROUP BY year, month, day
    """)
    @Transaction
    abstract fun getMonthlyOverview(year: Int, month: Int): Flowable<List<PracticeDayHistory>>

    @Query("""
        SELECT
            COUNT() as count,
            cast(strftime('%d', time/1000, 'unixepoch') as INTEGER) as day,
            cast(strftime('%m', time/1000, 'unixepoch') as INTEGER) as month,
            cast(strftime('%Y', time/1000, 'unixepoch') as INTEGER) as year
        FROM practice_history
        WHERE time/1000 BETWEEN :start AND :end
        GROUP BY year, month, day
    """)
    @Transaction
    abstract fun getWeeklyOverview(start: Long, end: Long): Flowable<List<PracticeDayHistory>>
}
