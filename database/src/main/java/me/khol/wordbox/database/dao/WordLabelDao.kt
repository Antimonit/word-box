package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import me.khol.wordbox.database.model.internal.DBWordLabel
import me.khol.wordbox.database.model.Label

@Dao
abstract class WordLabelDao {

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(list: List<DBWordLabel>): List<Long>

    fun insert(wordId: Long, labels: List<Label>) {
        insertRaw(labels.map {
            DBWordLabel(wordId, it.labelId)
        })
    }

    @Query("SELECT wordId FROM word_label WHERE wordId = :wordId AND labelId = :labelId")
    abstract fun select(wordId: Long, labelId: Long): Long?

    @Query("DELETE FROM word_label WHERE wordId = :wordId")
    abstract fun delete(wordId: Long)
}
