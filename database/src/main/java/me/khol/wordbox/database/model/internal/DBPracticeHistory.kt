package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

// TODO: when a word is deleted -> keep? cascade delete? replace wordId with -1?

@Entity(tableName = "practice_history")
internal data class DBPracticeHistory(
    @PrimaryKey(autoGenerate = true)
    val id: Long,
    val wordId: Long,
    val type: Type,
    val time: Instant
) {

    enum class Type(val code: Int) {
        Easy(0),
        Hard(1),
        Reset(2)
    }
}
