package me.khol.wordbox.database

import android.content.Context
import androidx.room.Room

/**
 * Wrapper around in-memory [WordBoxDatabase] for testing purposes.
 *
 * TODO: Belongs to test fixtures.
 */
class WordBoxDatabaseTestWrapper(context: Context) : DatabaseDaoProvider {

    private val database: WordBoxDatabase = Room
        .inMemoryDatabaseBuilder(context, WordBoxDatabase::class.java)
        .build()

    override fun wordDao() = database.wordDao()
    override fun languageDao() = database.languageDao()
    override fun labelDao() = database.labelDao()
    override fun exampleDao() = database.exampleDao()
    override fun wordWordDao() = database.wordWordDao()
    override fun wordLabelDao() = database.wordLabelDao()
    override fun noteDao() = database.noteDao()
    override fun historyDao() = database.historyDao()
    override fun statsDao() = database.statsDao()
}