package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index

@Entity(
    tableName = "word_label",
    primaryKeys = ["labelId", "wordId"],
    foreignKeys = [
        ForeignKey(
            entity = DBWord::class,
            parentColumns = ["wordId"],
            childColumns = ["wordId"],
            onDelete = CASCADE
        ),
        ForeignKey(
            entity = DBLabel::class,
            parentColumns = ["labelId"],
            childColumns = ["labelId"],
            onDelete = CASCADE
        )
    ],
    indices = [
        Index(value = ["wordId"]),
        Index(value = ["labelId"])
    ]
)
internal data class DBWordLabel(
    val wordId: Long,
    val labelId: Long
)
