package me.khol.wordbox.database

import me.khol.wordbox.database.dao.ExampleDao
import me.khol.wordbox.database.dao.HistoryDao
import me.khol.wordbox.database.dao.LabelDao
import me.khol.wordbox.database.dao.LanguageDao
import me.khol.wordbox.database.dao.NoteDao
import me.khol.wordbox.database.dao.StatsDao
import me.khol.wordbox.database.dao.WordDao
import me.khol.wordbox.database.dao.WordLabelDao
import me.khol.wordbox.database.dao.WordWordDao

interface DatabaseDaoProvider {

    fun wordDao(): WordDao

    fun languageDao(): LanguageDao

    fun labelDao(): LabelDao

    fun exampleDao(): ExampleDao

    fun wordWordDao(): WordWordDao

    fun wordLabelDao(): WordLabelDao

    fun noteDao(): NoteDao

    fun historyDao(): HistoryDao

    fun statsDao(): StatsDao
}
