package me.khol.wordbox.database.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Transaction
import androidx.room.Update

/**
 * Base Dao defining common methods for inserting, deleting and updating of any DB classes.
 */
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(item: T): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(vararg items: T): List<Long>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(list: List<T>): List<Long>


    @Delete
    fun delete(item: T)

    @Delete
    fun delete(vararg item: T)


    @Update
    fun update(item: T)

    @Transaction
    @Update
    fun update(vararg items: T)

    @Transaction
    @Update
    fun update(items: List<T>)
}
