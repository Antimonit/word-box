package me.khol.wordbox.database.converters

import androidx.room.TypeConverter
import org.threeten.bp.Duration

/**
 * [Duration] converter for Room database.
 */
class DurationConverter {
    @TypeConverter
    fun stringToDuration(duration: Long): Duration {
        return Duration.ofMillis(duration)
    }

    @TypeConverter
    fun durationToString(duration: Duration): Long {
        return duration.toMillis()
    }
}
