package me.khol.wordbox.database.model

data class Language(
    val languageId: Long,
    val name: String,
    val shortName: String,
    val order: Int
) {

    companion object {
        val invalid = Language(-1, "No language", "", -1)
    }
}
