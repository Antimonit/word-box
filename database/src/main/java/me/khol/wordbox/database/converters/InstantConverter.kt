package me.khol.wordbox.database.converters

import androidx.room.TypeConverter
import org.threeten.bp.Instant

/**
 * [Instant] converter for Room database.
 */
class InstantConverter {
    @TypeConverter
    fun stringToInstant(time: Long?): Instant? {
        return time?.let { Instant.ofEpochMilli(it) }
    }

    @TypeConverter
    fun instantToString(time: Instant?): Long? {
        return time?.toEpochMilli()
    }
}
