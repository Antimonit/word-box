package me.khol.wordbox.database.model.internal

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "label")
internal data class DBLabel(
    @PrimaryKey(autoGenerate = true)
    val labelId: Long,
    val text: String,
    val color: Int,
    val order: Int
)
