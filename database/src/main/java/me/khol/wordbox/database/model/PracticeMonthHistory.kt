package me.khol.wordbox.database.model

data class PracticeMonthHistory(
    val count: Int,
    val year: Int,
    val month: Int
)

