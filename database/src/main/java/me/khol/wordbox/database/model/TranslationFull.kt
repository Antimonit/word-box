package me.khol.wordbox.database.model

import androidx.room.Ignore
import androidx.room.Relation
import me.khol.wordbox.database.model.internal.DBLanguage

internal data class DBTranslationFull(
    val wordId: Long,
    val text: String,
    val description: String,
    val languageId: Long
) {

    /**
     * Fetch a list of [Language]s, can be safely reduced to a single [Language]
     */
    @Relation(parentColumn = "languageId", entityColumn = "languageId", entity = DBLanguage::class)
    internal lateinit var languages: List<Language>

    @delegate:Ignore
    val language: Language by lazy { languages[0] }

    fun toClean() = TranslationFull(wordId, text, description, language)
}

data class TranslationFull(
    val wordId: Long,
    val text: String,
    val description: String,
    val language: Language
)
