package me.khol.wordbox.database.model

import androidx.room.Embedded

data class WordSuggestion(
    val wordId: Long,
    val text: String,
    val description: String,
    @Embedded
    val language: Language
)
