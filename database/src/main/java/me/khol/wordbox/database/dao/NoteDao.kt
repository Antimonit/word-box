package me.khol.wordbox.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import me.khol.wordbox.database.model.internal.DBNote
import me.khol.wordbox.database.model.NoteList
import me.khol.wordbox.database.model.NoteDetail
import org.threeten.bp.Instant

@Dao
abstract class NoteDao {

    @Insert(onConflict = OnConflictStrategy.ABORT)
    internal abstract fun insertRaw(item: DBNote): Single<Long>

    fun insert(note: NoteDetail) = insertRaw(
        DBNote(
            0, // generate id
            note.title,
            note.contents
        )
    )

    @Query("DELETE FROM note WHERE noteId = :noteId")
    abstract fun delete(noteId: Long): Completable

    @Query("""
        SELECT noteId, title, contents, timeModified, timeCreated
        FROM note
        WHERE noteId = :noteId
    """)
    @Transaction
    abstract fun getNoteDetail(noteId: Long): Flowable<NoteDetail>

    @Query("""
        SELECT noteId, title, contents
        FROM note
    """)
    abstract fun getNotes(): Flowable<List<NoteList>>

    @Query("""
        UPDATE note
        SET title = :title,
                contents = :contents,
                timeModified = :timeModified
        WHERE noteId = :noteId
    """)
    abstract fun updateAfterEdit(
        noteId: Long,
        title: String,
        contents: String,
        timeModified: Instant
    ): Completable
}
