package me.khol.wordbox.database.model

import androidx.room.Ignore
import androidx.room.Relation
import me.khol.wordbox.database.model.internal.DBLabel

internal data class WordLabel(val wordId: Long, val labelId: Long) {
    /**
     * Although we use a relation here, labels will always contain exactly one DBLabel.
     */
    @Relation(parentColumn = "labelId", entityColumn = "labelId", entity = DBLabel::class)
    internal lateinit var labels: List<Label>

    @delegate:Ignore
    val label: Label by lazy { labels[0] }
}
