package me.khol.wordbox.database

import android.graphics.Color
import me.khol.wordbox.database.model.internal.DBExample
import me.khol.wordbox.database.model.internal.DBLabel
import me.khol.wordbox.database.model.internal.DBLanguage
import me.khol.wordbox.database.model.internal.DBWord
import me.khol.wordbox.database.model.internal.DBWordLabel
import me.khol.wordbox.database.model.internal.DBWordWord

internal fun WordBoxDatabase.loadDummyData() {

    labelDao().insertRaw(listOf(
        DBLabel(1, "Body", Color.parseColor("#AC2F2F"), 0),
        DBLabel(2, "Verb", Color.parseColor("#EC7D8C"), 1),
        DBLabel(3, "Animal", Color.parseColor("#25AA32"), 2),
        DBLabel(4, "Cooking", Color.parseColor("#8937AB"), 3),
        DBLabel(5, "Noun", Color.parseColor("#235DD2"), 4),
        DBLabel(6, "Family", Color.parseColor("#9E8A52"), 5)
    ))

    val idEnglish = 1L
    val idKorean = 2L
    val idCzech = 3L

    languageDao().insertRaw(listOf(
        DBLanguage(idEnglish, "English", "EN", 0),
        DBLanguage(idKorean, "한국어", "한", 1),
        DBLanguage(idCzech, "Čeština", "CS", 2)
    ))

    wordDao().insertRaw(listOf(
        DBWord(1, idEnglish, "addiction", "the fact or condition of being addicted to a particular substance or activity"),
        DBWord(2, idEnglish, "completely"),
        DBWord(3, idEnglish, "car", "a road vehicle with four wheels"),
        DBWord(4, idEnglish, "question mark", "a punctuation mark (?) indicating a question"),
        DBWord(5, idEnglish, "exclamation mark"),
        DBWord(6, idEnglish, "to taste", "to test the flavor of something by taking a small part into the mouth"),
        DBWord(7, idEnglish, "to include"),
        DBWord(8, idEnglish, "to exclude")
    ) + listOf(
        DBWord(9, idKorean, "중독"),
        DBWord(10, idKorean, "완전히"),
        DBWord(11, idKorean, "자동차"),
        DBWord(12, idKorean, "물음표"),
        DBWord(13, idKorean, "느낌표"),
        DBWord(14, idKorean, "맛보다"),
        DBWord(15, idKorean, "포함하다"),
        DBWord(16, idKorean, "제외하다")
    ))

    wordLabelDao().insertRaw(listOf(
        DBWordLabel(1, 5),
        DBWordLabel(3, 5),
        DBWordLabel(4, 5),
        DBWordLabel(5, 5),
        DBWordLabel(6, 2),
        DBWordLabel(6, 4),
        DBWordLabel(7, 2),
        DBWordLabel(8, 2)
    ))

    wordWordDao().insertBothWays(listOf(
        DBWordWord(1, 9),
        DBWordWord(2, 10),
        DBWordWord(3, 11),
        DBWordWord(4, 12),
        DBWordWord(4, 13)
    ))

    wordDao().insertRaw(listOf(
        DBWord(20, idEnglish, "bro", "similar to brother"),
        DBWord(21, idCzech, "brácha"),
        DBWord(22, idEnglish, "brother", "a man or boy in relation to other sons and daughters of his parents"),
        DBWord(23, idCzech, "bratr"),
        DBWord(24, idKorean, "형", "Older brother for men"),
        DBWord(25, idKorean, "오빠", "Older brother for women"),
        DBWord(26, idKorean, "동생", "Younger brother")
    ))

    exampleDao().insertRaw(listOf(
        DBExample(1, 22, "My brother loves practicing yoga."),
        DBExample(2, 22, "I was very touched when I saw a girl caring for her younger brother.")
    ))

    wordLabelDao().insertRaw(listOf(
        DBWordLabel(20, 6),
        DBWordLabel(21, 6),
        DBWordLabel(22, 6),
        DBWordLabel(23, 6),
        DBWordLabel(24, 6),
        DBWordLabel(25, 6),
        DBWordLabel(26, 6)
    ))

    wordWordDao().insertBothWays(listOf(
        DBWordWord(20, 21),
        DBWordWord(22, 24),
        DBWordWord(22, 25),
        DBWordWord(22, 26),
        DBWordWord(22, 23),
        DBWordWord(23, 22),
        DBWordWord(23, 24),
        DBWordWord(23, 25),
        DBWordWord(23, 26),
        DBWordWord(24, 22),
        DBWordWord(24, 23),
        DBWordWord(25, 22),
        DBWordWord(25, 23),
        DBWordWord(26, 22),
        DBWordWord(26, 23)
    ))

    wordDao().insertRaw(listOf(
        DBWord(30, idEnglish, "dog", "Animal that barks"),
        DBWord(31, idCzech, "pes", "Štěkající zvíře"),
        DBWord(32, idKorean, "개", "멍멍이라고 하는 동물")
    ))

    exampleDao().insertRaw(listOf(
        DBExample(11, 30, "Walk the dog"),
        DBExample(12, 31, "Venčit psa"),
        DBExample(13, 31, "Skákal pes přes oves"),
        DBExample(14, 32, "개를 산책시키다")
    ))

    wordLabelDao().insertRaw(listOf(
        DBWordLabel(30, 3),
        DBWordLabel(31, 3),
        DBWordLabel(32, 3)
    ))

    wordWordDao().insertBothWays(listOf(
        DBWordWord(30, 31),
        DBWordWord(31, 32),
        DBWordWord(32, 30)
    ))
}
