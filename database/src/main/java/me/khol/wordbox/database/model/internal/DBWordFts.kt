package me.khol.wordbox.database.model.internal

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Fts4
import androidx.room.PrimaryKey

@Fts4(contentEntity = DBWord::class)
@Entity(tableName = "word_fts")
internal data class DBWordFts(
    @PrimaryKey
    @ColumnInfo(name = "rowid")
    val rowId: Int,
    val text: String
)