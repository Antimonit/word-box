package me.khol.wordbox.database.model

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import me.khol.wordbox.database.model.internal.DBExample
import me.khol.wordbox.database.model.internal.DBWordLabel
import me.khol.wordbox.database.model.internal.DBWordWord
import org.threeten.bp.Duration
import org.threeten.bp.Instant

/**
 * Used in the WordDetail, WordEdit and Practice screens
 */
internal data class DBWordDetail(
    val wordId: Long,
    val text: String,
    val description: String,
    val easeFactor: Float,
    val averageEaseFactor: Float,
    val averageSuccessRate: Float,
    val practiceRounds: Int,
    val interval: Duration,
    val scheduledTime: Instant,
    val lastTime: Instant?,
    @Embedded
    val language: Language
) {

    /**
     * Fetch a list of [WordLabel]s and map them to [Label]s
     */
    @Relation(parentColumn = "wordId", entityColumn = "wordId", entity = DBWordLabel::class)
    internal lateinit var wordLabels: List<WordLabel>

    @delegate:Ignore
    val labels: List<Label> by lazy { wordLabels.map { it.label } }

    /**
     * Fetch a list of [WordWordFull]s and map them to [TranslationFull]s
     */
    @Relation(parentColumn = "wordId", entityColumn = "wordId_from", entity = DBWordWord::class)
    internal lateinit var wordWords: List<WordWordFull>

    @delegate:Ignore
    val translations: List<TranslationFull> by lazy { wordWords.map { it.translation.toClean() } }

    /**
     * Fetch a list of [Example]s
     */
    @Relation(
        parentColumn = "wordId", entityColumn = "wordId", entity = DBExample::class,
        projection = ["exampleId", "description"]
    )
    lateinit var examples: List<Example>

    /**
     * Builds a clean version of [DBWordDetail] that does not expose internal fields such as
     * [wordLabels] and [wordWords] which should be accessed by
     * [labels] and [translations] respectively. Same thing applies to the
     * [DBTranslationFull.languages] field which should be accessed through
     * [TranslationFull.language].
     */
    fun toClean() = WordDetail(
        wordId, text, description, easeFactor, averageEaseFactor, averageSuccessRate,
        practiceRounds, interval, scheduledTime, lastTime, language, labels, translations,
        examples
    )
}

data class WordDetail(
    val wordId: Long,
    val text: String,
    val description: String,
    val easeFactor: Float = Word.DEFAULT_EASE_FACTOR,
    val averageEaseFactor: Float = Word.DEFAULT_AVERAGE_EASE_FACTOR,
    val averageSuccessRate: Float = Word.DEFAULT_AVERAGE_SUCCESS_RATE,
    val practiceRounds: Int = Word.DEFAULT_PRACTICE_ROUNDS,
    val interval: Duration = Word.DEFAULT_INTERVAL,
    val scheduledTime: Instant = Word.DEFAULT_SCHEDULED_TIME,
    val lastTime: Instant? = Word.DEFAULT_LAST_TIME,
    val language: Language,
    val labels: List<Label> = emptyList(),
    val translations: List<TranslationFull> = emptyList(),
    val examples: List<Example> = emptyList()
)
