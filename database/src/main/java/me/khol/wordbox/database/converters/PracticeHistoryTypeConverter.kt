package me.khol.wordbox.database.converters

import androidx.room.TypeConverter
import me.khol.wordbox.database.model.internal.DBPracticeHistory

internal class PracticeHistoryTypeConverter {

    @TypeConverter
    fun toType(code: Int) = DBPracticeHistory.Type.values().first { it.code == code }

    @TypeConverter
    fun toInteger(status: DBPracticeHistory.Type) = status.code
}
