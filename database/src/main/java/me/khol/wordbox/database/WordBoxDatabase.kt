package me.khol.wordbox.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import me.khol.wordbox.database.model.internal.DBExample
import me.khol.wordbox.database.model.internal.DBLabel
import me.khol.wordbox.database.model.internal.DBLanguage
import me.khol.wordbox.database.model.internal.DBNote
import me.khol.wordbox.database.model.internal.DBPracticeHistory
import me.khol.wordbox.database.model.internal.DBWord
import me.khol.wordbox.database.model.internal.DBWordFts
import me.khol.wordbox.database.model.internal.DBWordLabel
import me.khol.wordbox.database.model.internal.DBWordWord
import me.khol.wordbox.database.converters.DurationConverter
import me.khol.wordbox.database.converters.InstantConverter
import me.khol.wordbox.database.converters.PracticeHistoryTypeConverter

/**
 * Room Database description for providing DAOs
 */
@Database(
    entities = [
        DBLanguage::class,
        DBLabel::class,
        DBWord::class,
        DBWordFts::class,
        DBExample::class,
        DBWordLabel::class,
        DBWordWord::class,
        DBPracticeHistory::class,
        DBNote::class
    ],
    version = WordBoxDatabase.VERSION
)
@TypeConverters(
    InstantConverter::class,
    DurationConverter::class,
    PracticeHistoryTypeConverter::class
)
internal abstract class WordBoxDatabase : RoomDatabase(), DatabaseDaoProvider {

    companion object {
        const val VERSION = 9
        const val NAME = "wordbox.db"

        val MIGRATION_4_5 = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    CREATE TABLE history (
                            id INTEGER NOT NULL,
                            name TEXT NOT NULL,
                            PRIMARY KEY(id)
                    )
                """)
            }
        }

        val MIGRATION_5_6 = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    ALTER TABLE history
                    ADD COLUMN wordId INTEGER
                """)
            }
        }

        val MIGRATION_6_7 = object : Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    CREATE TABLE note (
                            noteId INTEGER NOT NULL,
                            title TEXT NOT NULL,
                            contents TEXT NOT NULL,
                            timeCreated INTEGER NOT NULL,
                            timeModified INTEGER NOT NULL,
                            PRIMARY KEY(noteId)
                    )
                """)
            }
        }

        val MIGRATION_7_8 = object : Migration(7, 8) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    DROP TABLE history
                """)
                database.execSQL("""
                    CREATE TABLE practice_history (
                            id INTEGER NOT NULL,
                            wordId INTEGER NOT NULL,
                            type INTEGER NOT NULL,
                            time INTEGER NOT NULL,
                            PRIMARY KEY(id)
                    )
                """)
            }
        }

        val MIGRATION_8_9 = object : Migration(8, 9) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    CREATE VIRTUAL TABLE IF NOT EXISTS `word_fts`
                    USING FTS4(
                        `text`,
                        content=`word`
                    )
                """)
                database.execSQL("""
                    INSERT INTO word_fts(word_fts)
                    VALUES ('rebuild')
                """)
            }
        }
    }
}
