plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(Versions.compileSdk)
    defaultConfig {
        minSdkVersion(Versions.minSdk)
        targetSdkVersion(Versions.targetSdk)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")

        kapt {
            arguments {
                arg("room.schemaLocation", "$projectDir/schemas")
            }
        }
    }

    buildTypes {
        named("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
}

dependencies {
    implementation(Libraries.Kotlin.jdk8)

    implementation(Libraries.RxJava.rxJava)
    implementation(Libraries.RxJava.rxAndroid)
    implementation(Libraries.RxJava.rxKotlin)

    // TODO: New Room allows Many-to-Many and One-to-One relationships!
    implementation(Libraries.Room.runtime)
    implementation(Libraries.Room.rxJava2)
    implementation(Libraries.Room.ktx)
    kapt(Libraries.Room.compiler)

    implementation(Libraries.threeTenAbp)

    testImplementation(Libraries.jUnit)
    testImplementation(Libraries.Room.testing)
    
    androidTestImplementation(Libraries.AndroidX.Test.jUnit)
    androidTestImplementation(Libraries.AndroidX.Test.espresso)
}
