plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
    id("io.fabric")
}

android {
    compileSdkVersion(Versions.compileSdk)
    defaultConfig {
        applicationId = Versions.applicationId
        minSdkVersion(Versions.minSdk)
        targetSdkVersion(Versions.targetSdk)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        multiDexEnabled = true

        vectorDrawables.useSupportLibrary = true
    }
    buildTypes {
        named("release") {
            isMinifyEnabled = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
        named("debug") {
            isMinifyEnabled = false
        }
    }
}

// Used to enable Parcelize
androidExtensions {
    isExperimental = true
}

kapt {
    correctErrorTypes = true
    mapDiagnosticLocations = true
}

dependencies {
    implementation(project(Modules.base))
    implementation(project(Modules.database))
    implementation(project(Modules.history))

    implementation(Libraries.Kotlin.jdk8)

    implementation(Libraries.AndroidX.multiDex)
    implementation(Libraries.AndroidX.appCompat)
    implementation(Libraries.AndroidX.coreKtx)
    implementation(Libraries.AndroidX.browser)
    implementation(Libraries.AndroidX.recyclerView)
    implementation(Libraries.AndroidX.vectorDrawable)
    implementation(Libraries.AndroidX.legacyPreference)
    implementation(Libraries.AndroidX.cardView)
    implementation(Libraries.AndroidX.constraintLayout)

    implementation(Libraries.material)

    implementation(Libraries.Dagger.dagger)
    implementation(Libraries.Dagger.android)
    kapt(Libraries.Dagger.compiler)
    kapt(Libraries.Dagger.androidProcessor)

    implementation(Libraries.RxJava.rxJava)
    implementation(Libraries.RxJava.rxAndroid)
    implementation(Libraries.RxJava.rxKotlin)

    implementation(Libraries.RxBinding.rxBinding)
    implementation(Libraries.RxBinding.core)
    implementation(Libraries.RxBinding.appCompat)
    implementation(Libraries.RxBinding.material)
    implementation(Libraries.RxBinding.recyclerView)

    implementation(Libraries.Lifecycle.runtime)
    implementation(Libraries.Lifecycle.extensions)
    kapt(Libraries.Lifecycle.compiler)

    implementation(Libraries.Anko.sdk15)
    implementation(Libraries.Anko.sdk21Coroutines)
    implementation(Libraries.Anko.appcompatV7)
    implementation(Libraries.Anko.design)
    implementation(Libraries.Anko.recyclerviewV7)
    implementation(Libraries.Anko.constraintLayout)

    implementation(Libraries.Epoxy.core)
    kapt(Libraries.Epoxy.processor)

    implementation(Libraries.Stetho.core)
    implementation(Libraries.Stetho.okHttp)

    implementation(Libraries.timber)
    implementation(Libraries.threeTenAbp)
    implementation(Libraries.easyPermission)
    implementation(Libraries.chipsLayoutManager)
    implementation(Libraries.vintageChroma)
    implementation(Libraries.swipeStack)
    implementation(Libraries.materialMenu)
    implementation(Libraries.fastScroll)
    implementation(Libraries.floatingView)
    implementation(Libraries.prettyTime)
    implementation(Libraries.eventSubject)
    implementation(Libraries.firebase)
    implementation(Libraries.crashlytics)

    testImplementation(Libraries.jUnit)
    androidTestImplementation(Libraries.AndroidX.Test.jUnit)
    androidTestImplementation(Libraries.AndroidX.Test.espresso)
}

apply(plugin = "com.google.gms.google-services")
