package me.khol.wordbox.interactor

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import io.reactivex.Single
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.dao.LabelDao
import me.khol.wordbox.database.dao.LanguageDao
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.screens.main.word.Sort
import javax.inject.Inject
import javax.inject.Singleton

interface PreferencesInteractor {
    fun getPracticeLanguage(): Single<Language>
    fun getSelectedLanguage(): Single<Language>
    fun getSelectedLabels(): Single<List<Label>>
    fun getSort(): Single<Sort>
    fun getSearchQuery(): Single<String>

    fun getFloatingButtonPositionX(default: Int): Int
    fun getFloatingButtonPositionY(default: Int): Int

    fun setFloatingButtonPositionX(value: Int)
    fun setFloatingButtonPositionY(value: Int)

    var practiceLanguage: Language
    var selectedLanguage: Language
    var sort: Sort
    var selectedLabels: List<Label>
    var searchQuery: String
}

@Singleton
class PreferencesInteractorImpl @Inject constructor(
    context: Context,
    private val languageDao: LanguageDao,
    private val labelDao: LabelDao
) : PreferencesInteractor {

    companion object {
        private const val KEY_PRACTICE_LANGUAGE = "practiceLanguage"
        private const val KEY_SELECTED_LANGUAGE = "selectedLanguage"
        private const val KEY_SORT_TYPE = "sortType"
        private const val KEY_SELECTED_LABELS = "selectedLabels"
        private const val KEY_SEARCH_QUERY = "searchQuery"
        private const val KEY_FLOATING_BUTTON_POSITION_X = "floatingButtonPositionX"
        private const val KEY_FLOATING_BUTTON_POSITION_Y = "floatingButtonPositionY"
    }

    private val sp: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    override fun getPracticeLanguage(): Single<Language> {
        val id = sp.getLong(KEY_PRACTICE_LANGUAGE, -1L)
        return languageDao.getLanguage(id).toSingle(Language.invalid)
            .subscribeOnIO()
            .observeOnMainThread()
    }

    override fun getSelectedLanguage(): Single<Language> {
        val id = sp.getLong(KEY_SELECTED_LANGUAGE, -1L)
        return languageDao.getLanguage(id).toSingle(Language.invalid)
            .subscribeOnIO()
            .observeOnMainThread()
    }

    override fun getSelectedLabels(): Single<List<Label>> {
        val ids = sp.getStringSet(KEY_SELECTED_LABELS, emptySet())!!
        return labelDao.getLabelsWithIds(ids.map { it.toLong() })
            .subscribeOnIO()
            .observeOnMainThread()
    }

    override fun getSort(): Single<Sort> {
        return Single.just(Sort.valueOf(sp.getString(KEY_SORT_TYPE, Sort.Alphabet.name)!!))
    }

    override fun getSearchQuery(): Single<String> {
        return Single.just(sp.getString(KEY_SEARCH_QUERY, "")!!)
    }

    override var practiceLanguage: Language
        get() = getPracticeLanguage().blockingGet()
        set(value) {
            val id = value.languageId
            sp.edit().putLong(KEY_PRACTICE_LANGUAGE, id).apply()
        }

    override var selectedLanguage: Language
        get() = getSelectedLanguage().blockingGet()
        set(value) {
            val id = value.languageId
            sp.edit().putLong(KEY_SELECTED_LANGUAGE, id).apply()
        }

    override var sort: Sort
        get() = getSort().blockingGet()
        set(value) = sp.edit().putString(KEY_SORT_TYPE, value.name).apply()

    override var selectedLabels: List<Label>
        get() = getSelectedLabels().blockingGet()
        set(value) {
            val ids = value.asSequence().map { it.labelId.toString() }.toSet()
            sp.edit().putStringSet(KEY_SELECTED_LABELS, ids).apply()
        }

    override var searchQuery: String
        get() = getSearchQuery().blockingGet()
        set(value) = sp.edit().putString(KEY_SEARCH_QUERY, value).apply()

    override fun getFloatingButtonPositionX(default: Int) = sp.getInt(KEY_FLOATING_BUTTON_POSITION_X, default)
    override fun getFloatingButtonPositionY(default: Int) = sp.getInt(KEY_FLOATING_BUTTON_POSITION_Y, default)

    override fun setFloatingButtonPositionX(value: Int) =
        sp.edit().putInt(KEY_FLOATING_BUTTON_POSITION_X, value).apply()

    override fun setFloatingButtonPositionY(value: Int) =
        sp.edit().putInt(KEY_FLOATING_BUTTON_POSITION_Y, value).apply()

}
