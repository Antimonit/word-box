package me.khol.wordbox.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import dagger.android.DaggerService
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewListener
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager
import me.khol.wordbox.R
import me.khol.wordbox.extensions.animateScaleAppear
import me.khol.wordbox.extensions.animateScaleDisappear
import me.khol.wordbox.extensions.color
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.extensions.drawable
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.roundDrawable
import me.khol.wordbox.interactor.PreferencesInteractor
import me.khol.wordbox.screens.add.AddActivity
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.displayMetrics
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.image
import org.jetbrains.anko.imageView
import org.jetbrains.anko.wrapContent
import javax.inject.Inject

/**
 * Service displaying a floating button similar to Facebook Messenger's chat head.
 *
 * Clicking on the button opens [activity for adding new words][AddActivity].
 */
class FloatingButton : DaggerService() {

    companion object {
        const val EXTRA_CUTOUT_SAFE_AREA = "cutout_safe_area"
        const val EXTRA_ACTION = "action"

        private const val EXTRA_ACTION_CONCEAL = "conceal"
        private const val EXTRA_ACTION_REVEAL = "reveal"

        private const val CHANNEL_ID = "floating_button_channel_id"
        private const val CHANNEL_NAME = "Floating button active"

        /**
         * True if the service is created and not destroyed yet
         */
        private var isCreated: Boolean = false

        /**
         * Plays [conceal][FloatingButton.conceal] animation on the [iconView].
         */
        fun conceal(context: Context) = sendAction(context, EXTRA_ACTION_CONCEAL)

        /**
         * Plays [reveal][FloatingButton.reveal] animation on the [iconView].
         */
        fun reveal(context: Context) = sendAction(context, EXTRA_ACTION_REVEAL)

        /**
         * Plays reveal or conceal animation on the [iconView].
         */
        private fun sendAction(context: Context, action: String) {
            if (isCreated) {
                val intent = Intent(context, FloatingButton::class.java).apply {
                    putExtra(EXTRA_ACTION, action)
                }
                ContextCompat.startForegroundService(context, intent)
            }
        }

        /**
         * Starts the [FloatingButton] service and displays floating [iconView].
         */
        fun popupFloatingButton(context: Context, safeArea: Rect) {
            val intent = Intent(context, FloatingButton::class.java).apply {
                putExtra(EXTRA_CUTOUT_SAFE_AREA, safeArea)
            }
            ContextCompat.startForegroundService(context, intent)
        }
    }

    @Inject
    lateinit var preferences: PreferencesInteractor

    private var iconView: FrameLayout? = null
    private var isHidden = false
    private var manager: FloatingViewManager? = null
    private val addIntent: Intent
        get() = Intent(this, AddActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
        }

    override fun onCreate() {
        super.onCreate()
        baseContext.setTheme(R.style.WordBoxTheme_NoActionBar)

        isCreated = true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_MIN)
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }

        val pendingAddIntent = PendingIntent.getActivity(this, 0, addIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        startForeground(1, NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(getString(R.string.floating_button_notification_title))
            .setContentText(getString(R.string.floating_button_notification_text))
            .setSmallIcon(R.drawable.ic_launcher_small)
            .setColorized(true)
            .setColor(color(R.color.colorPrimary))
            .setOngoing(false)
            .setContentIntent(pendingAddIntent)
            .setCategory(NotificationCompat.CATEGORY_SERVICE)
            .setPriority(NotificationCompat.PRIORITY_MIN)
            .build()
        )
    }

    override fun onDestroy() {
        if (manager != null) {
            manager?.removeAllViewToWindow()
            manager = null
        }
        isCreated = false
        super.onDestroy()
    }

    override fun onBind(intent: Intent?) = null

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        iconView?.let {
            when (intent.getStringExtra(EXTRA_ACTION)) {
                EXTRA_ACTION_CONCEAL -> conceal(it)
                EXTRA_ACTION_REVEAL -> reveal(it)
            }
            return Service.START_STICKY
        }

        iconView = frameLayout {
            imageView {
                backgroundDrawable = roundDrawable(color(R.color.colorPrimary), corner = dpf(32))
                image = drawable(R.drawable.ic_launcher_foreground)
                elevationCompat = dpf(4)
            }.lparams(dp(56), dp(56)) {
                gravity = Gravity.CENTER
            }
            setOnClickListener {
                showDialog()
            }
            layoutParams = FrameLayout.LayoutParams(wrapContent, wrapContent)
        }

        manager = FloatingViewManager(this, object : FloatingViewListener {
            override fun onFinishFloatingView() {
                stopSelf()
            }

            override fun onTouchFinished(isFinishing: Boolean, x: Int, y: Int) {
                if (!isFinishing) {
                    preferences.setFloatingButtonPositionX(x)
                    preferences.setFloatingButtonPositionY(y)
                }
            }
        }).apply {
            setFixedTrashIconImage(R.drawable.ic_trash_fixed)
            setActionTrashIconImage(R.drawable.ic_trash_action)
            setSafeInsetRect(intent.getParcelableExtra(EXTRA_CUTOUT_SAFE_AREA) as Rect)
            addViewToWindow(iconView, FloatingViewManager.Options().apply {
                floatingViewX = preferences.getFloatingButtonPositionX(displayMetrics.widthPixels)
                floatingViewY = preferences.getFloatingButtonPositionY(displayMetrics.heightPixels / 2)
                overMargin = dp(8)
                moveDirection = FloatingViewManager.MOVE_DIRECTION_THROWN
                animateInitialMove = true
            })
            setDisplayMode(FloatingViewManager.DISPLAY_MODE_HIDE_FULLSCREEN)
        }

        return Service.START_REDELIVER_INTENT
    }

    private fun showDialog() {
        startActivity(addIntent)

        // TODO: If you ever feel like wasting whole day for a minor enhancement, here is what you can do:

        // There is a problem with starting dialog-like activity from a service - it is still an
        // activity. That means it receives the same treatment as other activities when opening
        // recent apps menu. It is possible to annotate the activity with excludeFromRecents="true"
        // but it will still show up when user uses gesture navigation.

        // To display a dialog it needs a Window. Usually it uses Window from Activity it was
        // created in (through passed Context class I believe). When we pass service's Context,
        // it crashes because services don't have a Window.
        // It is possible to get around this by using system level AlertDialog instead. As soon as
        // the dialog is created, we can obtain dialog's Window and set it's type attribute to
        // WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY. As a result, when the dialog is
        // shown, it will get attached to system's Window that is floating above all other
        // activities.
        // Because the dialog is not attached to any activity, the dialog is always on the top even
        // when we navigate away from current top activity - including navigating to home screen or
        // displaying recent apps carousel. We can simply register a broadcast receiver listening
        // for Intent.ACTION_CLOSE_SYSTEM_DIALOGS and dismiss the dialog when home button or recent
        // apps presses are observed.

        // This approach is used by Facebook's Messenger app when displaying ChatHeads. It just
        // feels better than using an Activity. Google's Translate app is using Activity approach.

        // There are several serious drawbacks to using dialog approach though.
        //  - Because the dialog is not scoped to any activity, we cannot use ViewModels. Currently
        //    the logic is implemented inside a ViewModel that extends from another ViewModel. It
        //    would be quite difficult and also confusing to separate the logic from the ViewModel.
        //  - Because the dialog is not scoped to any Dagger-friendly Android component (Activity,
        //    Fragment, Service...) we would have to abuse Dagger a little bit to inject required
        //    dependencies to the dialog.
        //  - We cannot use android.intent.action.PROCESS_TEXT Intent filter on a dialog. It must
        //    be defined on an Activity (or possibly a Service but I haven't tried).
    }

    private fun reveal(iconView: View) {
        if (isHidden) {
            isHidden = false
            iconView.animateScaleAppear(onStart = {
                visibility = View.VISIBLE
            }, onEnd = {
                iconView.setOnClickListener {
                    showDialog()
                }
            })
        }
    }

    private fun conceal(iconView: View) {
        if (!isHidden) {
            isHidden = true
            iconView.animateScaleDisappear(onStart = {
                iconView.setOnClickListener(null)
            }, onEnd = {
                visibility = View.GONE
            })
        }
    }
}
