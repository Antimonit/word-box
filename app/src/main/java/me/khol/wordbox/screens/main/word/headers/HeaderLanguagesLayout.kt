package me.khol.wordbox.screens.main.word.headers

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import me.khol.wordbox.R
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.headerLayoutDrawable
import me.khol.wordbox.extensions.headerRadioButton
import me.khol.wordbox.extensions.headerRadioGroup
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.model.view.HeaderRadioGroup
import org.jetbrains.anko.allCaps
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.padding
import org.jetbrains.anko.space
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView

/**
 * A section displaying list of [languages][Language].
 */
class HeaderLanguagesLayout(parent: ViewGroup) : BaseHeaderLayout(parent) {

    override val headerRes = R.string.word_list_header_languages
    override val manageRes = R.string.word_list_header_languages_manage

    private lateinit var radioGroup: HeaderRadioGroup
    private lateinit var emptyView: View

    private var languages: List<Language>? = null
    var selectedLanguage: Language? = null
    var onLanguageChange: (Language) -> Unit = {}

    fun updateLanguages(languages: List<Language>, selectedLanguage: Language) {
        txtManage?.isVisible = !languages.isEmpty()
        emptyView.isVisible = languages.isEmpty()

        if (this.languages != languages) {
            this.languages = languages
            recreate(languages)
            radioGroup.checkLanguage(selectedLanguage)
        }

        if (this.selectedLanguage != selectedLanguage) {
            this.selectedLanguage = selectedLanguage
            radioGroup.checkLanguage(selectedLanguage)
        }
    }

    private fun recreate(languages: List<Language>) {
        radioGroup.apply {
            removeAllViews()

            fun item(language: Language, first: Boolean) {
                if (!first) {
                    space().lparams(0, 0, initWeight = 1f)
                }

                headerRadioButton {
                    tag = language
                    text = language.shortName
                }.lparams(48.dp, 48.dp)
            }

            languages.forEachIndexed { index, language ->
                item(language, first = index == 0)
            }
        }
    }

    override fun ViewManager.createContentView(): View {
        return frameLayout {
            elevationCompat = 0.dpf
            backgroundDrawable = headerLayoutDrawable()

            radioGroup = headerRadioGroup {
                setOnCheckedChangeListener { group, checkedId ->
                    group.findViewById<View?>(checkedId)?.let {
                        selectedLanguage = it.tag as Language
                        onLanguageChange(selectedLanguage!!)
                    }
                }
            }

            emptyView = linearLayout {
                elevationCompat = 8.dpf
                padding = 16.dp
                gravity = Gravity.CENTER_VERTICAL
                isClickable = true
                setOnClickListener { onManage() }

                textView(R.string.word_list_header_languages_empty) {
                    font = R.font.roboto_medium
                    textColor = Color.WHITE
                    textSize = 14f
                }.lparams(0, wrap, weight = 1f)

                textView(R.string.word_list_header_languages_create) {
                    padding = 8.dp
                    font = R.font.roboto_medium
                    textColor = Color.WHITE
                    backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackgroundBorderless)
                    allCaps = true
                    isDuplicateParentStateEnabled = true
                }.lparams(wrap, wrap)
            }.lparams(match, match)
        }
    }
}
