package me.khol.wordbox.screens.note

import android.content.Context
import android.content.Intent
import android.os.Bundle
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getLongNullable
import me.khol.wordbox.screens.base.BaseActivity

/**
 * Activity wrapper for [NoteDetailFragment].
 * TODO: Remove this unnecessary wrapper.
 */
class NoteActivity : BaseActivity() {

    companion object {
        private const val ARG_NOTE_ID = "note_id"

        fun getEditIntent(context: Context, wordId: Long): Intent {
            return Intent(context, NoteActivity::class.java).apply {
                putExtra(ARG_NOTE_ID, wordId)
            }
        }

        fun getAddIntent(context: Context): Intent {
            return Intent(context, NoteActivity::class.java)
        }
    }

    private lateinit var layout: NoteLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        layout = NoteLayout(rootView)
        setContentLayout(layout)

        if (savedInstanceState == null) {
            intent.extras?.getLongNullable(NoteActivity.ARG_NOTE_ID).let { noteId ->
                val fragment = if (noteId == null) {
                    NoteDetailFragment.newAddFragment()
                } else {
                    NoteDetailFragment.newEditFragment(noteId)
                }

                supportFragmentManager.beginTransaction()
                    .replace(R.id.note_detail_content, fragment, null)
                    .commit()
            }
        }
    }
}
