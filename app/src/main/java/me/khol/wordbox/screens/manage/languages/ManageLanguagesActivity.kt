package me.khol.wordbox.screens.manage.languages

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelTouchCallback
import com.airbnb.epoxy.EpoxyViewHolder
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.extensions.tintedDrawable
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.screens.base.BaseActivity

/**
 * Activity responsible for creating, updating and deleting languages used throughout the
 * application.
 */
class ManageLanguagesActivity : BaseActivity() {

    private lateinit var layout: ManageLanguagesLayout
    private lateinit var viewModel: ManageLanguagesViewModel
    private lateinit var controller: ManageLanguagesController

    private lateinit var swipeItemTouchHelper: ItemTouchHelper
    private lateinit var dragItemTouchHelper: ItemTouchHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        controller = ManageLanguagesController(
            object : ManageLanguageModel.Callbacks {
                override fun onStartDrag(model: EpoxyModel<*>) {
                    val viewHolder = layout.recycler.findViewHolderForItemId(model.id())
                    dragItemTouchHelper.startDrag(viewHolder)
                }

                override fun onEditStarted(language: Language) {
                    viewModel.editStarted(language)
                }

                override fun onEditCanceled(language: Language) {
                    viewModel.editCanceled(language)
                }

                override fun onEditConfirmed(language: Language, newName: String, newShortName: String) {
                    viewModel.editConfirmed(language, newName, newShortName)
                }
            },
            onAddClicked = { newLanguageName ->
                viewModel.addLanguage(newLanguageName)
            }
        )

        layout = ManageLanguagesLayout(rootView, controller.adapter)
        setContentLayout(layout)

        setSupportActionBar(layout.toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }

        setUpSwipe()
        setUpDrag()

        disposables += viewModel.observeState()
            .observeOnMainThread()
            .subscribe({ (languages, editModeLanguage) ->
                controller.languages = languages
                controller.editModeLanguage = editModeLanguage
            }, Throwable::printStackTrace)
    }

    private fun setUpSwipe() {
        val targetModelClass = ManageLanguageModel::class.java
        val touchCallback = object : EpoxyModelTouchCallback<ManageLanguageModel>(controller, targetModelClass) {
            @SuppressLint("ResourceType")
            private val icon = tintedDrawable(R.drawable.ic_delete, Color.WHITE)!!
            private val iconWidth = icon.intrinsicWidth
            private val iconHeight = icon.intrinsicHeight
            private val iconBounds = Rect()

            private val background = ColorDrawable()
            private val backgroundColor = color(R.color.delete_red)

            override fun onChildDraw(
                canvas: Canvas,
                recyclerView: RecyclerView,
                viewHolder: EpoxyViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                val view = viewHolder.itemView
                val itemHeight = view.bottom - view.top

                background.color = backgroundColor
                background.setBounds(view.left, view.top, view.right, view.bottom)
                background.draw(canvas)

                val iconMargin = (itemHeight - iconHeight) / 2
                iconBounds.top = view.top + iconMargin
                iconBounds.bottom = view.bottom - iconMargin
                if (dX > 0) {
                    iconBounds.left = view.left + iconMargin
                    iconBounds.right = view.left + iconMargin + iconWidth
                } else {
                    iconBounds.left = view.right - iconMargin - iconWidth
                    iconBounds.right = view.right - iconMargin
                }
                icon.bounds = iconBounds
                icon.draw(canvas)

                super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }

            override fun getMovementFlagsForModel(model: ManageLanguageModel?, adapterPosition: Int): Int {
                return makeMovementFlags(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)
            }

            override fun isTouchableModel(model: EpoxyModel<*>?): Boolean {
                if (controller.isInEditMode())
                    return false
                return super.isTouchableModel(model)
            }

            override fun isLongPressDragEnabled(): Boolean {
                return false
            }

            override fun onSwipeCompleted(model: ManageLanguageModel, itemView: View, position: Int, direction: Int) {
                val count = viewModel.getLanguageWordCount(model.language.languageId)

                val title = resources.getString(R.string.manage_delete_languages_dialog_title)
                val message =
                    resources.getQuantityString(R.plurals.manage_delete_languages_dialog_message, count, count)

                AlertDialog.Builder(this@ManageLanguagesActivity)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        viewModel.deleteLanguage(model.language)
                    }
                    .setNegativeButton(android.R.string.no) { _, _ ->
                        swipeItemTouchHelper.attachToRecyclerView(null)
                        swipeItemTouchHelper.attachToRecyclerView(layout.recycler)
                    }
                    .setOnCancelListener {
                        swipeItemTouchHelper.attachToRecyclerView(null)
                        swipeItemTouchHelper.attachToRecyclerView(layout.recycler)
                    }
                    .show()
            }
        }

        swipeItemTouchHelper = ItemTouchHelper(touchCallback)
        swipeItemTouchHelper.attachToRecyclerView(layout.recycler)
    }

    private fun setUpDrag() {
        val targetModelClass = ManageLanguageModel::class.java
        val touchCallback = object : EpoxyModelTouchCallback<ManageLanguageModel>(controller, targetModelClass) {
            override fun getMovementFlagsForModel(model: ManageLanguageModel, adapterPosition: Int): Int {
                return makeMovementFlags(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0)
            }

            override fun isTouchableModel(model: EpoxyModel<*>?): Boolean {
                if (controller.isInEditMode())
                    return false
                return super.isTouchableModel(model)
            }

            override fun isLongPressDragEnabled(): Boolean {
                return false
            }

            override fun onModelMoved(from: Int, to: Int, model: ManageLanguageModel, itemView: View) {
                viewModel.moveLanguage(model.language, from, to)
            }

            override fun onDragReleased(model: ManageLanguageModel, itemView: View) {
                viewModel.moveLanguagesFinished()
            }
        }

        dragItemTouchHelper = ItemTouchHelper(touchCallback)
        dragItemTouchHelper.attachToRecyclerView(layout.recycler)
    }
}
