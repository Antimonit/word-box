package me.khol.wordbox.screens.note

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.database.model.NoteDetail
import me.khol.wordbox.model.repository.NoteRepository
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import org.threeten.bp.Instant
import javax.inject.Inject

/**
 * ViewModel for [NoteDetailFragment].
 * It is used for both editing existing notes and creating new ones.
 */
class NoteDetailViewModel @Inject constructor(
    private val noteRepository: NoteRepository
) : BaseViewModel() {

    /**
     * Currently edited note.
     */
    private lateinit var note: NoteDetail

    /**
     * Originally loaded note. Used to decide whether the note was updated or not.
     */
    private lateinit var originalNote: NoteDetail

    /**
     * Whether loaded word is existing word in the database or a new one
     */
    private var isEditNote: Boolean = false

    private val noteSubject: FlowableProcessor<NoteDetail> = BehaviorProcessor.create<NoteDetail>()

    fun observeNote(): Flowable<NoteDetail> = noteSubject

    fun loadNewNote() {
        isEditNote = false
        note = NoteDetail(0, "", "", Instant.now(), Instant.now())
        originalNote = note
        noteSubject.onNext(note)
    }

    fun loadEditNote(noteId: Long) {
        isEditNote = true
        disposables += noteRepository.getNoteDetail(noteId)
            .subscribe({ note ->
                this.note = note
                this.originalNote = note
                this.noteSubject.onNext(note)
            }, Throwable::printStackTrace)
    }

    fun onTitleChanged(title: CharSequence) {
        note = note.copy(title = title.toString())
    }

    fun onContentsChanged(contents: CharSequence) {
        note = note.copy(contents = contents.toString())
    }

    /**
     * Persist all changes made to currently edited note
     */
    fun saveChanges(): Completable {
        return if (isEditNote) {
            noteRepository.updateNote(note)
        } else {
            noteRepository.insertNote(note).ignoreElement()
        }
    }

    /**
     * Delete currently edited note
     */
    fun delete(): Completable {
        return if (isEditNote) {
            noteRepository.deleteNote(note.noteId)
        } else {
            // There is nothing to delete
            Completable.complete()
        }
    }

    /**
     * Returns true if there are unsaved changes.
     */
    fun hasChanged(): Boolean {
        return note != originalNote
    }

    /**
     * Discards all changes made to the edited note.
     */
    fun discardChanges() {
        note = originalNote
    }
}
