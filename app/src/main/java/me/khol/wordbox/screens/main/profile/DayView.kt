package me.khol.wordbox.screens.main.profile

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.view.View
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.base.extensions.spf
import org.jetbrains.anko.withAlpha
import org.threeten.bp.temporal.ChronoUnit
import org.threeten.bp.temporal.TemporalUnit

open class DayView(context: Context) : View(context) {

    var type: TemporalUnit = ChronoUnit.MONTHS
        set(value) {
            field = value
            animateChanges()
        }

    private var progress = 1f

    private val rect = RectF()

    private val backgroundPaint = Paint().apply {
        color = color(R.color.colorPrimary).withAlpha(0x3F)
    }

    /**
     * Helper to tween between [start] and [end] values. [byType] parameter should return
     * value that matches the [end] value based on current [type].
     */
    private inner class AnimatedValue(private val byType: (TemporalUnit) -> Float) {
        var start = valueByType
        var end = valueByType
        val current get() = start * (1 - progress) + end * progress
        val valueByType get() = byType(type)
    }

    private val topCorner = AnimatedValue { type ->
        when (type) {
            ChronoUnit.MONTHS -> width / 2f
            else -> dpf(4)
        }
    }

    private val bottomCorner = AnimatedValue { type ->
        when (type) {
            ChronoUnit.MONTHS -> width / 2f
            else -> 0f
        }
    }

    private val textSize = AnimatedValue { type ->
        when (type) {
            ChronoUnit.MONTHS -> spf(12)
            else -> 0f
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        topCorner.end = topCorner.valueByType
        bottomCorner.end = bottomCorner.valueByType
        textSize.end = textSize.valueByType

        // Draw background. Draw top half and bottom half separately because top and bottom corner
        // radii might differ.
        rect.set(0f, 0f, width.toFloat(), height.toFloat())
        canvas.save()
        canvas.clipRect(0, 0, width, height / 2)
        canvas.drawRoundRect(rect, topCorner.current, topCorner.current, backgroundPaint)
        canvas.restore()
        canvas.save()
        canvas.clipRect(0, height / 2, width, height)
        canvas.drawRoundRect(rect, bottomCorner.current, bottomCorner.current, backgroundPaint)
        canvas.restore()
    }

    private fun animateChanges() {
        topCorner.start = topCorner.current
        bottomCorner.start = bottomCorner.current
        textSize.start = textSize.current
        ValueAnimator.ofFloat(0f, 1f).apply {
            duration = 500
            addUpdateListener {
                progress = it.animatedValue as Float
                invalidate()
            }
        }.start()
    }
}
