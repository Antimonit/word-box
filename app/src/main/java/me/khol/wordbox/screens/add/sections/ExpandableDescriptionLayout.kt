package me.khol.wordbox.screens.add.sections

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.EditText
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.disposables.Disposable
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.font
import me.khol.wordbox.screens.word.edit.WordCallbacks
import org.jetbrains.anko._LinearLayout
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.bottomPadding
import org.jetbrains.anko.editText
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.hintResource
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.topPadding

/**
 * Contains a simple EditText that changes word's description
 */
class ExpandableDescriptionLayout(
    context: Context,
    titleRes: Int,
    callbacks: WordCallbacks
) : ExpandableLayout(context, titleRes, callbacks) {

    lateinit var txtDescription: EditText

    private var disposable: Disposable? = null

    override fun _LinearLayout.createContentView(): View {
        return frameLayout {
            editText {
                textSize = 16f
                textColor = color(R.color.black)
                font = R.font.roboto
                hintResource = R.string.add_word_description_hint
                horizontalPadding = 0
                backgroundDrawable = null
                txtDescription = this
                topPadding = 0.dp
                bottomPadding = 8.dp
                horizontalPadding = 16.dp
                minHeight = 48.dp
            }.lparams(match, wrap) {
                gravity = Gravity.TOP
            }
        }
    }

    fun bind(description: String) {
        syncTitle(0)

        txtDescription.setText(description)
        disposable = txtDescription.textChanges()
            .skipInitialValue()
            .subscribe(callbacks::onDescriptionChanged)
    }

    fun unbind() {
        disposable?.dispose()
    }

}
