package me.khol.wordbox.screens.backup

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.material.snackbar.Snackbar
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.screens.backup.epoxy.BackupController
import me.khol.wordbox.screens.base.BaseActivity
import java.io.File

class BackupActivity : BaseActivity(),
    RestoreDialogFragment.Callbacks {

    companion object {
        private const val RC_OPEN_DOCUMENT_TREE = 1112
    }

    private lateinit var layout: BackupLayout
    private lateinit var viewModel: BackupViewModel
    private lateinit var controller: BackupController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        layout = BackupLayout(rootView)
        setContentView(layout.view)

        setSupportActionBar(layout.toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }

        controller = BackupController(
            onImportFile = {
                RestoreDialogFragment.newInstance(it).show(supportFragmentManager, "restore")
            }, onChooseFolder = {
                openDirectory()
            }, onExportFile = {
                viewModel.onExportFile()
            }
        )
        layout.list.adapter = controller.adapter

        disposables += viewModel.observeMessages()
            .observeOnMainThread()
            .subscribe { message ->
                Snackbar.make(layout.view, message, Snackbar.LENGTH_LONG).show()
            }

        disposables += viewModel.observeState()
            .observeOnMainThread()
            .subscribe { state ->
                when (state) {
                    is BackupState.NoFolderSelected -> {
                        controller.folderSelected = false
                        controller.files = emptyList()
                    }
                    is BackupState.Loaded -> {
                        controller.folderSelected = true
                        controller.files = state.files
                    }
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_backup, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_choose_folder -> {
                openDirectory()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openDirectory() {
        startActivityForResult(Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or
                Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION
        }, RC_OPEN_DOCUMENT_TREE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_OPEN_DOCUMENT_TREE && resultCode == Activity.RESULT_OK) {
            data?.data?.also { uri: Uri ->
                viewModel.onFolderSelected(uri)
            }
        }
    }

    override fun onBackupAccepted(file: File) {
        viewModel.onImportFile(file)
    }
}
