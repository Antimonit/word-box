package me.khol.wordbox.screens.base.epoxy

import com.airbnb.epoxy.EpoxyController
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * A delegate that automatically [EpoxyController.requestModelBuild] whenever value is set.
 * T class parameter can be nullable.
 */
class Prop<T>(var value: T) : ReadWriteProperty<EpoxyController, T> {

    override fun getValue(thisRef: EpoxyController, property: KProperty<*>): T {
        return value
    }

    override fun setValue(thisRef: EpoxyController, property: KProperty<*>, value: T) {
        this.value = value
        thisRef.requestModelBuild()
    }
}

/**
 * A delegate that automatically [EpoxyController.requestModelBuild] whenever value is set.
 * Mimics constraints of a lateinit property - must be assigned a value before it is used.
 */
class LateProp<T : Any> : ReadWriteProperty<EpoxyController, T> {

    lateinit var value: T

    override fun getValue(thisRef: EpoxyController, property: KProperty<*>): T {
        return value
    }

    override fun setValue(thisRef: EpoxyController, property: KProperty<*>, value: T) {
        this.value = value
        thisRef.requestModelBuild()
    }
}
