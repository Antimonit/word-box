package me.khol.wordbox.screens.manage.labels

import com.airbnb.epoxy.EpoxyController
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.base.epoxy.Prop

/**
 * Controller with a list of editable labels and a new label button
 */
class ManageLabelsController(
    private val callbacks: ManageLabelModel.Callbacks,
    private val onAddClicked: (String) -> Unit
) : EpoxyController() {

    var editModeLabel by Prop<Label?>(null)
    var labels by Prop<List<Label>>(emptyList())

    fun isInEditMode() = editModeLabel != null

    override fun buildModels() {
        labels.forEach { label ->
            manageLabel {
                id(label.labelId)
                label(label)
                callbacks(callbacks)
                isInEditMode(editModeLabel != null)
                isThisInEditMode(editModeLabel?.labelId == label.labelId)
            }
        }
        manageLabelNew {
            id("new_label")
            onAddClicked(onAddClicked)
        }
    }
}
