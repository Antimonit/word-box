package me.khol.wordbox.screens.main.profile

import android.content.Context
import android.view.View
import android.view.ViewGroup
import me.khol.wordbox.extensions.customView
import me.khol.wordbox.database.model.PracticeDayHistory
import me.khol.wordbox.database.model.PracticeMonthHistory
import me.khol.wordbox.history.model.HistoryDay
import me.khol.wordbox.history.model.HistoryMonth
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.history.view.Overview
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.wrapContent
import org.threeten.bp.LocalDate

/**
 * History layout
 */
class HistoryLayout(
    parent: ViewGroup
) : BaseLayout(parent) {

    private lateinit var datePanel: DatePanelView
    private lateinit var historyOverview: HistoryOverviewView
    private lateinit var overview: Overview

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.verticalLayout {
            datePanel = customView<DatePanelView>().lparams(match, wrap)

            historyOverview = customView<HistoryOverviewView>().lparams(match, 60.dp)

            overview = customView<Overview>().lparams(matchParent, 360.dp)

            layoutParams = ViewGroup.LayoutParams(match, wrapContent)
        }
    }

    fun setHistory(history: History) {
        when (history) {
            is History.Weekly -> setWeekly(history.date, history.events)
            is History.Monthly -> setMonthly(history.date, history.events)
            is History.Yearly -> setYearly(history.date, history.events)
        }
    }

    private fun setWeekly(date: LocalDate, events: List<PracticeDayHistory>) {
        datePanel.setWeekly(date, events.sumBy { it.count })
        historyOverview.setWeekly(date, events)
        overview.setWeekly(date, events.map { HistoryDay(it.count, it.year, it.month, it.day) })
    }

    private fun setMonthly(date: LocalDate, events: List<PracticeDayHistory>) {
        datePanel.setMonthly(date, events.sumBy { it.count })
        historyOverview.setMonthly(date, events)
        overview.setMonthly(date, events.map { HistoryDay(it.count, it.year, it.month, it.day) })
    }

    private fun setYearly(date: LocalDate, events: List<PracticeMonthHistory>) {
        datePanel.setYearly(date, events.sumBy { it.count })
        historyOverview.setYearly(date, events)
        overview.setYearly(date, events.map { HistoryMonth(it.count, it.year, it.month) })
    }

    fun setOnPreviousClickListener(listener: (() -> Unit)?) {
        datePanel.setOnPreviousClickListener(listener)
    }

    fun setOnNextClickListener(listener: (() -> Unit)?) {
        datePanel.setOnNextClickListener(listener)
    }
}
