package me.khol.wordbox.screens.word.edit.epoxy.labels

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyRecyclerView
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.padding

@EpoxyModelClass
open class LabelsModel : BaseEpoxyModel<LabelsModel.Layout>() {

    @EpoxyAttribute
    lateinit var labels: List<Label>
    @EpoxyAttribute
    lateinit var checkedLabels: List<Label>
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var callbacks: LabelCallbacks

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        listLabels.suppressLayout(false)
        listLabels.withModels {
            labels.forEach { label ->
                labelButton {
                    id(label.labelId)
                    label(label)
                    checked(checkedLabels.contains(label))
                    onChecked(callbacks::onLabelChecked)
                }
            }
        }
        listLabels.suppressLayout(true)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var listLabels: EpoxyRecyclerView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.epoxyRecyclerView {
                padding = 12.dp    // remove 4.dp because of the spacing item decoration
                layoutManager = ChipsLayoutManager.newBuilder(context)
                    .setRowStrategy(ChipsLayoutManager.STRATEGY_FILL_VIEW)
                    .setChildGravity(Gravity.START)
                    .setScrollingEnabled(false)
                    .build()
                addItemDecoration(SpacingItemDecoration(8.dp, 8.dp))
                itemAnimator = null
            }.apply {
                listLabels = this
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
