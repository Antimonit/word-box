package me.khol.wordbox.screens.manage.languages

import com.airbnb.epoxy.EpoxyController
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.screens.base.epoxy.Prop

/**
 * Controller with a list of editable languages and a new language button
 */
class ManageLanguagesController(
    private val callbacks: ManageLanguageModel.Callbacks,
    private val onAddClicked: (String) -> Unit
) : EpoxyController() {

    var editModeLanguage by Prop<Language?>(null)
    var languages by Prop<List<Language>>(emptyList())

    fun isInEditMode() = editModeLanguage != null

    override fun buildModels() {
        languages.forEach { language ->
            manageLanguage {
                id(language.languageId)
                language(language)
                callbacks(callbacks)
                isInEditMode(editModeLanguage != null)
                isThisInEditMode(editModeLanguage?.languageId == language.languageId)
            }
        }
        manageLanguageNew {
            id("new_language")
            onAddClicked(onAddClicked)
        }
    }
}
