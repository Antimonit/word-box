package me.khol.wordbox.screens.word.edit.epoxy.examples

import me.khol.wordbox.database.model.Example

interface ExampleCallbacks {
    fun onExampleChanged(example: Example)
    fun onDeleteExample(example: Example)
    fun onAddExample()
}
