package me.khol.wordbox.screens.main.note

import io.reactivex.Flowable
import me.khol.wordbox.database.dao.NoteDao
import me.khol.wordbox.database.model.NoteList
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * ViewModel for notes fragment
 */
class NoteListViewModel @Inject constructor(
    private val noteDao: NoteDao
) : BaseViewModel() {

    fun observeNotes(): Flowable<List<NoteList>> = noteDao.getNotes()
}
