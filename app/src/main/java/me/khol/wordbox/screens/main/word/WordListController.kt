package me.khol.wordbox.screens.main.word

import android.os.Handler
import com.airbnb.epoxy.EpoxyController
import me.khol.wordbox.database.model.WordList
import me.khol.wordbox.screens.base.epoxy.Prop
import me.khol.wordbox.screens.main.word.epoxy.word

/**
 * Controller displaying a list of word for a specific language or search query.
 */
class WordListController(
    handler: Handler,
    private val onWordClick: (WordList) -> Unit
) : EpoxyController(handler, handler) {

    var words by Prop<List<WordList>>(emptyList())

    override fun buildModels() {
        words.forEach { word ->
            word {
                id(word.wordId)
                word(word)
                onClick(onWordClick)
            }
        }
    }
}
