package me.khol.wordbox.screens.add.sections

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.screens.word.edit.WordCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.translations.addTranslation
import me.khol.wordbox.screens.word.edit.epoxy.translations.translation
import org.jetbrains.anko._LinearLayout

/**
 * Contains a list of translations that can be modified, added and removed.
 */
class ExpandableTranslationsLayout(
    context: Context,
    titleRes: Int,
    callbacks: WordCallbacks
) : ExpandableLayout(context, titleRes, callbacks) {

    lateinit var list: EpoxyRecyclerView

    override fun _LinearLayout.createContentView(): View {
        return epoxyRecyclerView {
            layoutManager = object : LinearLayoutManager(context) {
                override fun canScrollVertically() = false
            }
            isNestedScrollingEnabled = false
            itemAnimator = null
            list = this
        }.lparams(match, wrap)
    }

    fun bind(allLanguages: List<Language>, translations: List<TranslationFull>) {
        syncTitle(translations.size)

        list.withModels {
            translations.forEach { translation ->
                translation {
                    id("translation", translation.wordId)
                    languages(allLanguages)
                    translation(translation)
                    callbacks(callbacks)
                }
            }
            addTranslation {
                id("add_translation")
                onAddClick(callbacks::onAddTranslation)
            }
        }
    }
}
