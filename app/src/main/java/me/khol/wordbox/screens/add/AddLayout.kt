package me.khol.wordbox.screens.add

import android.content.Context
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import cz.ackee.ankoconstraintlayout._ConstraintLayout
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.customLayout
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.endPadding
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.languagesButton
import me.khol.wordbox.extensions.languagesButtonStyleBorderless
import me.khol.wordbox.extensions.startPadding
import me.khol.wordbox.model.view.LanguagesButton
import me.khol.wordbox.screens.add.sections.ExpandableDescriptionLayout
import me.khol.wordbox.screens.add.sections.ExpandableExamplesLayout
import me.khol.wordbox.screens.add.sections.ExpandableLabelsLayout
import me.khol.wordbox.screens.add.sections.ExpandableTranslationsLayout
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.screens.word.edit.WordCallbacks
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.AnkoViewDslMarker
import org.jetbrains.anko.appcompat.v7.toolbar
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.bottomPadding
import org.jetbrains.anko.button
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.editText
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.hintResource
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.padding
import org.jetbrains.anko.space
import org.jetbrains.anko.support.v4.nestedScrollView
import org.jetbrains.anko.topPadding
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.verticalPadding

/**
 * Layout for [AddActivity].
 */
class AddLayout(
    context: Context,
    val callbacks: WordCallbacks
) : BaseLayout(context) {

    lateinit var toolbar: Toolbar
    lateinit var languagesButton: LanguagesButton
    lateinit var mainWord: EditText

    lateinit var btnCancel: Button
    lateinit var btnAdd: Button
    lateinit var btnAddMore: Button

    private val descriptionLayout =
        ExpandableDescriptionLayout(context, R.string.add_word_description_title, callbacks)
    private val labelsLayout =
        ExpandableLabelsLayout(context, R.string.add_word_labels_title, callbacks)
    private val examplesLayout =
        ExpandableExamplesLayout(context, R.string.add_word_examples_title, callbacks)
    private val translationsLayout =
        ExpandableTranslationsLayout(context, R.string.add_word_translations_title, callbacks)

    val setDescription = descriptionLayout::bind
    val setLabels = labelsLayout::bind
    val setExamples = examplesLayout::bind
    val setTranslations = translationsLayout::bind

    private fun ViewManager.topToolbar(init: (@AnkoViewDslMarker Toolbar).() -> Unit): Toolbar {
        return toolbar {
            minimumHeight = getDimensionAttr(R.attr.actionBarSize)
            startPadding = 0.dp
            endPadding = 4.dp
            setContentInsetsRelative(0.dp, 0.dp)

            frameLayout {
                setOnClickListener {
                    languagesButton.performClick()
                }

                languagesButton = languagesButton {
                    languagesButtonStyleBorderless()
                    isClickable = false
                    isDuplicateParentStateEnabled = true
                }.lparams(wrap, wrap) {
                    gravity = Gravity.CENTER
                }
            }.lparams(56.dp, 56.dp)

            mainWord = editText {
                verticalPadding = 0.dp
                horizontalPadding = 0.dp
                hintResource = R.string.add_word_title_hint
                backgroundDrawable = null
                textSize = 20f
                font = R.font.roboto_medium
                gravity = Gravity.CENTER_VERTICAL
                maxLines = 1
                inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE
                imeOptions += EditorInfo.IME_FLAG_NO_FULLSCREEN
                post { requestFocus() }
            }.lparams(match, match)

            init()
        }
    }

    private fun ViewManager.bottomButtons(init: (@AnkoViewDslMarker View).() -> Unit): _ConstraintLayout {
        return constraintLayout {
            backgroundColorResource = R.color.white
            elevationCompat = 6.dpf

            btnCancel = button(R.string.add_word_cancel) {
                backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)
            }.lparams(wrap, 48.dp)

            btnAddMore = button(R.string.add_word_add_more) {
                backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)
            }.lparams(wrap, 48.dp)

            btnAdd = button(R.string.add_word_add) {
                backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)
            }.lparams(wrap, 48.dp)

            constraints {
                btnCancel.connect(
                    STARTS of parentId,
                    VERTICAL of parentId
                )
                btnAddMore.connect(
                    END to START of btnAdd,
                    VERTICAL of parentId
                )
                btnAdd.connect(
                    ENDS of parentId,
                    VERTICAL of parentId
                )
            }

            init()
        }
    }

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.frameLayout {

            themedAppBarLayout(R.style.WordBoxTheme_DarkAppBarOverlay) {
                toolbar = topToolbar {
                }.lparams(match, 56.dp)
            }.lparams(match, 56.dp) {
                gravity = Gravity.TOP
            }

            nestedScrollView {
                backgroundColorResource = R.color.white
                topPadding = 56.dp
                bottomPadding = 48.dp

                verticalLayout {
                    padding = 4.dp
                    clipToPadding = false

                    customLayout(descriptionLayout)
                    space().lparams(match, 4.dp)
                    customLayout(labelsLayout)
                    space().lparams(match, 4.dp)
                    customLayout(examplesLayout)
                    space().lparams(match, 4.dp)
                    customLayout(translationsLayout)
                }.lparams(match, match)

            }.lparams(match, match)

            bottomButtons {
                backgroundColorResource = R.color.white
            }.lparams(match, 48.dp) {
                gravity = Gravity.BOTTOM
            }

            layoutParams = ViewGroup.LayoutParams(match, wrap)
        }
    }
}
