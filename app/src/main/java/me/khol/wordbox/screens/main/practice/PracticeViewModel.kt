package me.khol.wordbox.screens.main.practice

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.interactor.PreferencesInteractor
import me.khol.wordbox.database.dao.LabelDao
import me.khol.wordbox.database.dao.LanguageDao
import me.khol.wordbox.database.dao.WordDao
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.PracticeHistoryType
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.model.repository.PracticeHistoryRepository
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import org.threeten.bp.Instant
import javax.inject.Inject

/**
 * ViewModel for practice fragment
 */
class PracticeViewModel @Inject constructor(
    private val practiceHistoryRepository: PracticeHistoryRepository,
    private val languageDao: LanguageDao,
    private val labelDao: LabelDao,
    private val wordDao: WordDao,
    private val preferences: PreferencesInteractor
) : BaseViewModel() {

    private val selectedLanguageSubject: FlowableProcessor<Language> = BehaviorProcessor.create()

    init {
        disposables += preferences.getPracticeLanguage().subscribe { it -> languageChanged(it) }
    }

    fun observePracticeWords(languageId: Long): Flowable<List<WordDetail>> {
        return Flowable.generate { emitter ->
            val words = wordDao.getPracticeWords(languageId, 3)
            emitter.onNext(words)
        }
    }

    /**
     * Reset practice progress as if the word was just created.
     */
    fun wordSwipedReset(word: WordDetail): Completable {
        return update(PracticeScheduler.resetProgress(word), PracticeHistoryType.Reset)
    }

    /**
     * Update practice progress and reschedule the word for practice relatively soon
     */
    fun wordSwipedHard(word: WordDetail): Completable {
        return update(PracticeScheduler.updateProgress(word, false), PracticeHistoryType.Hard)
    }

    /**
     * Update practice progress and reschedule the word for practice relatively late
     */
    fun wordSwipedEasy(word: WordDetail): Completable {
        return update(PracticeScheduler.updateProgress(word, true), PracticeHistoryType.Easy)
    }

    private fun update(updatedWord: WordDetail, type: PracticeHistoryType): Completable {
        return Completable.fromAction {
            wordDao.updateAfterPracticeResult(
                updatedWord.wordId,
                updatedWord.easeFactor,
                updatedWord.averageEaseFactor,
                updatedWord.averageSuccessRate,
                updatedWord.practiceRounds,
                updatedWord.interval,
                updatedWord.scheduledTime,
                updatedWord.lastTime
            )
        }.concatWith(
            practiceHistoryRepository.insertEvent(updatedWord.wordId, type, Instant.now())
        ).subscribeOnIO()
    }

    fun observeLanguages(): Flowable<Pair<List<Language>, Language>> {
        return Flowable.combineLatest(
            languageDao.getAllLanguages(),
            selectedLanguageSubject,
            BiFunction { allLanguages: List<Language>, selectedLanguage: Language ->
                Pair(allLanguages, selectedLanguage)
            }
        )
    }

    fun languageChanged(newLanguage: Language) {
        preferences.practiceLanguage = newLanguage
        selectedLanguageSubject.onNext(newLanguage)
    }
}
