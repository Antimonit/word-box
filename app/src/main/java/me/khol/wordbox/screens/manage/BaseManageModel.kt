package me.khol.wordbox.screens.manage

import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout._ConstraintLayout
import cz.ackee.ankoconstraintlayout._ConstraintSet
import me.khol.wordbox.R
import me.khol.wordbox.extensions.beginDelayedTransition
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.imageView
import org.jetbrains.anko.padding

/**
 * Base epoxy model for manageable items.
 *
 * When the item's text is being edited, drag handle is hidden and for all items in the list
 * and check mark is displayed for this one.
 *
 * TODO: UX is not good. Try something better.
 */
abstract class BaseManageModel<Layout : BaseManageModel.Layout> : BaseEpoxyModel<Layout>() {

    @EpoxyAttribute
    @JvmField
    var isInEditMode: Boolean = false

    @EpoxyAttribute
    @JvmField
    var isThisInEditMode: Boolean = false

    open fun Layout.baseBind() {
        // For some reason we cannot call [beginDelayedTransition] right after we bind the
        // model as it causes the whole layout fade out and in. Delaying the operation fixes
        // the problem.
        root.post {
            when {
                isThisInEditMode -> showDoneButton()
                isInEditMode -> showNone()
                else -> showHandle()
            }
        }

        done.setOnClickListener {
            confirmEdit(this)
        }

        handle.setOnTouchListener { _, event ->
            if (event.actionMasked == MotionEvent.ACTION_DOWN) {
                onStartDrag()
            }
            false
        }
    }

    abstract fun confirmEdit(layout: Layout)

    abstract fun onStartDrag()

    open fun Layout.baseUnbind() {
        done.setOnClickListener(null)
        handle.setOnTouchListener(null)
    }

    abstract class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var root: _ConstraintLayout

        lateinit var done: View
        lateinit var handle: View

        private lateinit var doneConstraints: _ConstraintSet
        private lateinit var handleConstraints: _ConstraintSet
        private lateinit var noneConstraints: _ConstraintSet

        private fun applyConstraints(constraints: _ConstraintSet) {
            root.beginDelayedTransition()
            constraints.applyTo(root)
        }

        fun showDoneButton() = applyConstraints(doneConstraints)
        fun showHandle() = applyConstraints(handleConstraints)
        fun showNone() = applyConstraints(noneConstraints)

        fun _ConstraintLayout.commonConstraints() {

            done = imageView(R.drawable.ic_check) {
                padding = 16.dp
            }

            handle = imageView(R.drawable.ic_drag_vertical) {
                padding = 16.dp
            }

            constraints {
                done.connect(
                    VERTICAL of parentId,
                    ENDS of parentId
                ).size(56.dp, 56.dp)

                handle.connect(
                    VERTICAL of parentId,
                    ENDS of parentId
                ).size(56.dp, 56.dp)
            }

            doneConstraints = prepareConstraints {
                done.connect(ENDS of parentId).clear(START)
                handle.connect(START to END of parentId).clear(END)
            }

            handleConstraints = prepareConstraints {
                done.connect(START to END of parentId).clear(END)
                handle.connect(ENDS of parentId).clear(START)
            }

            noneConstraints = prepareConstraints {
                done.connect(START to END of parentId).clear(END)
                handle.connect(START to END of parentId).clear(END)
            }

            handleConstraints.applyTo(this)
            root = this
        }
    }
}
