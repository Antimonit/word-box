package me.khol.wordbox.screens.base

import io.reactivex.disposables.CompositeDisposable
import me.khol.wordbox.screens.base.viewmodel.ViewModelFactory

/**
 * Common interface for all custom BaseFragments
 */
interface CommonBaseFragment {

    val activity: BaseActivity
    val disposables: CompositeDisposable
    var vmFactory: ViewModelFactory

    fun onBackPressed(): Boolean = false
}