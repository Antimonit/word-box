package me.khol.wordbox.screens.word.edit

import me.khol.wordbox.database.model.Language
import me.khol.wordbox.screens.word.edit.epoxy.description.DescriptionCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.examples.ExampleCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.labels.LabelCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.practice.PracticeCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.translations.TranslationCallbacks

interface WordCallbacks :
    DescriptionCallbacks,
    ExampleCallbacks,
    LabelCallbacks,
    PracticeCallbacks,
    TranslationCallbacks {

    fun onTextChanged(word: CharSequence)
    fun onLanguageSelected(language: Language)
}
