package me.khol.wordbox.screens.main.word.epoxy

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.extensions.labelView
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.model.view.LabelView
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext

/**
 * Epoxy model of a single [Label]
 */
@EpoxyModelClass
open class LabelViewModel : BaseEpoxyModel<LabelViewModel.Layout>() {

    @EpoxyAttribute
    lateinit var label: Label

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        labelView.text = label.text
        labelView.color = label.color
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var labelView: LabelView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.labelView {
                labelView = this
                layoutParams = ViewGroup.LayoutParams(48.dp, 16.dp)
            }
        }
    }
}
