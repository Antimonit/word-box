package me.khol.wordbox.screens.main.practice

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Completable
import io.reactivex.FlowableSubscriber
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.internal.subscriptions.SubscriptionHelper
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.model.view.SwipeStack
import me.khol.wordbox.screens.base.BaseFragment
import org.reactivestreams.Subscription
import kotlin.reflect.KFunction1

class PracticeFragment : BaseFragment() {

    private lateinit var layout: PracticeLayout
    private lateinit var viewModel: PracticeViewModel
    private lateinit var adapter: SwipeStackAdapter
    private lateinit var practiceWordSubscriber: PracticeWordsSubscriber

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)
        adapter = SwipeStackAdapter(
            onClickedReveal = {
                layout.swipeStack.allowedSwipeDirection = SwipeStack.SWIPE_ALL
            }
        )

        practiceWordSubscriber = PracticeWordsSubscriber(2) { words: List<WordDetail> ->
            adapter.data += words.filter { it !in adapter.data.takeLast(3) }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        practiceWordSubscriber.cancel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layout = PracticeLayout(container!!, adapter)
        return layout.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity.setSupportActionBar(layout.toolbar)
        activity.supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(false)
            it.setTitle(R.string.nav_title_practice)
        }

        layout.languagesHeader.onLanguageChange = viewModel::languageChanged

        disposables += viewModel.observeLanguages()
            .observeOnMainThread()
            .subscribe({ (allLanguages, selectedLanguage) ->
                layout.languagesHeader.updateLanguages(allLanguages, selectedLanguage)
            }, Throwable::printStackTrace)

        layout.btnStart.setOnClickListener {
            val language = layout.languagesHeader.selectedLanguage ?: return@setOnClickListener
            startPractice(language.languageId)
        }

        layout.swipeStack.listener = object : SwipeStack.SwipeStackListener {

            private fun swipe(callback: KFunction1<WordDetail, Completable>, position: Int) {
                disposables += callback(adapter.getItem(position))
                    .subscribe {
                        practiceWordSubscriber.loadNext()
                    }
                layout.swipeStack.allowedSwipeDirection = SwipeStack.SWIPE_NONE
            }

            override fun onViewSwipedToTop(position: Int) = swipe(viewModel::wordSwipedReset, position)

            override fun onViewSwipedToLeft(position: Int) = swipe(viewModel::wordSwipedEasy, position)

            override fun onViewSwipedToRight(position: Int) = swipe(viewModel::wordSwipedHard, position)

            override fun onStackEmpty() = endPractice()
        }
    }

    private fun startPractice(languageId: Long) {
        layout.emptyStack.isVisible = false
        viewModel.observePracticeWords(languageId)
            .subscribeOnIO()
            // By default, the scheduler buffers 128 items but that could load incorrect
            // words for practice if a translation is rescheduled for earlier time than one
            // of the 128 items.
            .observeOn(AndroidSchedulers.mainThread(), false, 1)
            .subscribe(practiceWordSubscriber)
    }

    private fun endPractice() {
        layout.emptyStack.isVisible = true
    }
}

class PracticeWordsSubscriber(
    private val initialCount: Long,
    private val onNext: (List<WordDetail>) -> Unit
) : FlowableSubscriber<List<WordDetail>> {
    private var subscription: Subscription? = null

    override fun onComplete() {
        // do nothing
    }

    override fun onSubscribe(sub: Subscription) {
        subscription = sub
        sub.request(initialCount)
    }

    override fun onNext(t: List<WordDetail>) {
        onNext.invoke(t)
    }

    override fun onError(t: Throwable) {
        t.printStackTrace()
    }

    fun loadNext() {
        subscription?.request(1)
    }

    fun cancel() {
        subscription?.cancel()
        subscription = SubscriptionHelper.CANCELLED
    }
}
