package me.khol.wordbox.screens.main.word.headers

import android.graphics.Color
import android.graphics.drawable.InsetDrawable
import android.graphics.drawable.LayerDrawable
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import me.khol.wordbox.R
import me.khol.wordbox.extensions.headerItemDrawable
import me.khol.wordbox.extensions.headerLayoutDrawable
import me.khol.wordbox.extensions.headerRadioButton
import me.khol.wordbox.extensions.headerRadioGroup
import me.khol.wordbox.extensions.selectableDrawable
import me.khol.wordbox.extensions.tintedDrawable
import me.khol.wordbox.model.view.HeaderRadioButton
import me.khol.wordbox.model.view.HeaderRadioGroup
import me.khol.wordbox.screens.main.word.Sort
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.space
import org.jetbrains.anko.withAlpha

/**
 * A section containing several options of how translations in the main word list are sorted.
 */
class HeaderSortLayout(parent: ViewGroup) : BaseHeaderLayout(parent) {

    override val headerRes = R.string.word_list_header_sort_by

    private lateinit var radioGroup: HeaderRadioGroup
    private var selectedSort: Sort? = null
    var onSortChange: (Sort) -> Unit = {}

    fun updateSort(selectedSort: Sort) {
        if (this.selectedSort != selectedSort) {
            this.selectedSort = selectedSort
            radioGroup.findViewWithTag<HeaderRadioButton>(selectedSort).isChecked = true
        }
    }

    private fun recreate() {
        radioGroup.apply {
            removeAllViews()

            fun item(sort: Sort, first: Boolean) {
                if (!first) {
                    space().lparams(0, 0, initWeight = 1f)
                }

                headerRadioButton {
                    tag = sort
                    backgroundDrawable = context.selectableDrawable(
                        LayerDrawable(arrayOf(
                            headerItemDrawable(),
                            InsetDrawable(tintedDrawable(sort.icon, Color.WHITE), 12.dp)
                        )),
                        Color.WHITE.withAlpha(50)
                    )
                }.lparams(48.dp, 48.dp)
            }

            item(Sort.Alphabet, true)
            item(Sort.Progress, false)
            item(Sort.TimeCreated, false)
            item(Sort.TimeModified, false)
        }
    }

    override fun ViewManager.createContentView(): View {
        return headerRadioGroup {
            backgroundDrawable = headerLayoutDrawable()
            radioGroup = this
            setOnCheckedChangeListener { group, checkedId ->
                group.findViewById<View?>(checkedId)?.let {
                    selectedSort = it.tag as Sort
                    onSortChange(selectedSort!!)
                }
            }
            recreate()
        }
    }
}
