package me.khol.wordbox.screens.word.edit.epoxy.description

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.disposables.Disposable
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.font
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.editText
import org.jetbrains.anko.horizontalMargin
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColor

@EpoxyModelClass
open class DescriptionModel : BaseEpoxyModel<DescriptionModel.Layout>() {

    @EpoxyAttribute
    lateinit var descriptionText: CharSequence
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var callbacks: DescriptionCallbacks

    private lateinit var textChanges: Disposable

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        txtDescription.setText(descriptionText)
        textChanges = txtDescription.textChanges().skipInitialValue()
            .subscribe(callbacks::onDescriptionChanged)
    }

    override fun Layout.unbind() {
        textChanges.dispose()
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtDescription: EditText

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.editText {
                textSize = 16f
                textColor = color(R.color.black)
                font = R.font.roboto
                hint = "Description"
                horizontalPadding = 0
                backgroundDrawable = null
            }.apply {
                txtDescription = this
                layoutParams = ViewGroup.MarginLayoutParams(match, wrap).apply {
                    horizontalMargin = 16.dp
                    bottomMargin = 16.dp
                    topMargin = 4.dp
                }
            }
        }
    }
}

