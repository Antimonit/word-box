package me.khol.wordbox.screens.word.edit.epoxy.examples

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.jakewharton.rxbinding3.widget.textChanges
import cz.ackee.ankoconstraintlayout.constraintLayout
import io.reactivex.disposables.Disposable
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colorWithAlpha
import me.khol.wordbox.extensions.drawable
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.database.model.Example
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.editText
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.imageButton
import org.jetbrains.anko.leftPadding
import org.jetbrains.anko.rightPadding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.verticalPadding
import org.jetbrains.anko.view

/**
 * Epoxy model of a single [example][Example].
 */
@EpoxyModelClass
open class ExampleModel : BaseEpoxyModel<ExampleModel.Layout>() {

	@EpoxyAttribute
	lateinit var example: Example
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var callbacks: ExampleCallbacks

	private lateinit var textChanges: Disposable

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun Layout.bind() {
		btnDelete.setOnClickListener {
			callbacks.onDeleteExample(example)
		}

		txtExample.setText(example.description)
		textChanges = txtExample.textChanges().skipInitialValue().subscribe {
			callbacks.onExampleChanged(example.copy(description = it.toString()))
		}
	}

	override fun Layout.unbind() {
		textChanges.dispose()
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtExample: EditText
		lateinit var btnDelete: ImageButton

		override fun createView(ui: AnkoContext<Context>): View {
			return ui.frameLayout {

				constraintLayout {
					leftPadding = 16.dp
					rightPadding = 8.dp

					txtExample = editText {
						minimumHeight = 56.dp
						backgroundDrawable = null
						horizontalPadding = 0.dp
						verticalPadding = 8.dp
						textSize = 15f
						textColor = color(R.color.black)
						setLineSpacing(6.dpf, 1f)
						font = R.font.roboto
						gravity = Gravity.CENTER_VERTICAL
					}

					btnDelete = imageButton {
						setImageDrawable(drawable(R.drawable.ic_close))
						backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackgroundBorderless)
					}

					constraints {
						txtExample.connect(
								STARTS of parentId,
								END to START of btnDelete,
								VERTICAL of parentId
						).size(matchConstraint, wrapContent)

						btnDelete.connect(
								ENDS of parentId,
								VERTICAL of parentId
						).size(40.dp, 40.dp)
					}

				}.lparams(match, match)

				view {
					backgroundColor = colorWithAlpha(R.color.black, 20)
				}.lparams(match, 1.dp) {
					gravity = Gravity.BOTTOM
				}

			}.apply {
				layoutParams = ViewGroup.LayoutParams(match, wrap)
			}
		}
	}
}
