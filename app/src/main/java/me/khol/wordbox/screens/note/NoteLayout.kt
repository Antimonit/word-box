package me.khol.wordbox.screens.note

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import me.khol.wordbox.R
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent

/**
 * Layout for [NoteActivity].
 */
class NoteLayout(parent: ViewGroup) : BaseLayout(parent) {

    lateinit var content: FrameLayout

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.frameLayout {
            id = R.id.note_detail_content
        }.apply {
            content = this
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }
}
