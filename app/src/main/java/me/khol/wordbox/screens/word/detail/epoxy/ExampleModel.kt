package me.khol.wordbox.screens.word.detail.epoxy

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.graphics.drawable.shapes.RectShape
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.backgroundTintListCompat
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.startMargin
import me.khol.wordbox.database.model.Example
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.leftPadding
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalPadding
import org.jetbrains.anko.view

/**
 * Epoxy model of a single [example][Example].
 */
@EpoxyModelClass
open class ExampleModel : BaseEpoxyModel<ExampleModel.Layout>() {

    @EpoxyAttribute
    lateinit var example: Example

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        txtExample.text = example.description
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtExample: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.linearLayout {
                verticalPadding = 8.dp
                horizontalPadding = 16.dp

                // bullet
                view {
                    backgroundColor = color(R.color.black)
                    backgroundDrawable = ShapeDrawable(RectShape())
                    backgroundDrawable = ShapeDrawable(OvalShape())
                    backgroundTintListCompat = ColorStateList.valueOf(color(R.color.black))
                }.lparams(4.dp, 4.dp) {
                    startMargin = 8.dp
                    topMargin = 8.dp
                }

                txtExample = textView {
                    textSize = 15f
                    textColor = color(R.color.black)
                    setLineSpacing(6.dpf, 1f)
                    leftPadding = 16.dp
                }.lparams(0, wrap, weight = 1f) {
                    gravity = Gravity.CENTER_VERTICAL
                }

            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
