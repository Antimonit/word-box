package me.khol.wordbox.screens.word.edit.epoxy.labels

import me.khol.wordbox.database.model.Label

interface LabelCallbacks {
    fun onLabelChecked(label: Label, checked: Boolean)
}
