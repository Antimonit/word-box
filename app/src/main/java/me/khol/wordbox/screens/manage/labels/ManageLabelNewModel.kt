package me.khol.wordbox.screens.manage.labels

import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.manage.BaseManageNewModel

/**
 * Epoxy model used for creating new [Label].
 */
@EpoxyModelClass
open class ManageLabelNewModel : BaseManageNewModel<ManageLabelNewModel.Layout>() {

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    class Layout(parent: ViewGroup) : BaseManageNewModel.Layout(parent) {

        override val nameHint: String = context.getString(R.string.manage_add_label)
    }
}
