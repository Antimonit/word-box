package me.khol.wordbox.screens.base.layout

import android.content.Context
import android.view.View
import android.view.ViewGroup
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.base.extensions.sp
import me.khol.wordbox.base.extensions.spf
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.wrapContent

/**
 * Base layout that all other custom layouts should extend
 */
abstract class BaseLayout(val context: Context) : AnkoComponent<Context> {

    companion object {
        val LAYOUT_TAG = "layout".hashCode()

        val match = matchParent
        val wrap = wrapContent
    }

    val Number.dp: Int get() = context.dp(this)
    val Number.dpf: Float get() = context.dpf(this)
    val Number.sp: Int get() = context.sp(this)
    val Number.spf: Float get() = context.spf(this)

    constructor(parent: ViewGroup) : this(parent.context)

    val view: View by lazy {
        createView(AnkoContext.create(context)).apply {
            layout = this@BaseLayout
        }
    }
}

inline fun <reified T : BaseLayout> View.layout(): T {
    return getTag(BaseLayout.LAYOUT_TAG) as T
}

var View.layout: BaseLayout
    get() = getTag(BaseLayout.LAYOUT_TAG) as BaseLayout
    set(value) = setTag(BaseLayout.LAYOUT_TAG, value)
