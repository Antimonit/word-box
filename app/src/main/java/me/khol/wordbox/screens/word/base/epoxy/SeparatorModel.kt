package me.khol.wordbox.screens.word.base.epoxy

import android.content.Context
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.horizontalMargin
import org.jetbrains.anko.verticalMargin
import org.jetbrains.anko.view

@EpoxyModelClass
open class SeparatorModel : BaseEpoxyModel<SeparatorModel.Layout>() {

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.view {
                //                backgroundColorResource = R.color.colorPrimary
                layoutParams = ViewGroup.MarginLayoutParams(match, 1.dp).apply {
                    horizontalMargin = 16.dp
                    verticalMargin = 8.dp
                }
            }
        }
    }
}

