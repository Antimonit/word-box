package me.khol.wordbox.screens.backup.epoxy

import com.airbnb.epoxy.EpoxyController
import me.khol.wordbox.screens.backup.FileBackup
import me.khol.wordbox.screens.base.epoxy.Prop
import java.io.File

/**
 * Displays a list of application database backups.
 */
class BackupController(
    val onImportFile: (File) -> Unit,
    val onChooseFolder: () -> Unit,
    val onExportFile: () -> Unit
) : EpoxyController() {

    var folderSelected: Boolean by Prop(false)
    var files: List<FileBackup> by Prop(emptyList())

    override fun buildModels() {
        if (folderSelected) {
            add {
                id("add")
                onClick {
                    onExportFile()
                }
            }
            files.forEach { (file, properties) ->
                backup {
                    id(file.path)
                    file(file)
                    properties(properties)
                    onClick { file ->
                        onImportFile(file)
                    }
                }
            }
        } else {
            chooseFolder {
                id("chooseFolder")
                onClick {
                    onChooseFolder()
                }
            }
        }
    }
}
