package me.khol.wordbox.screens.word.detail

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.model.repository.WordRepository
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * ViewModel for [WordDetailFragment].
 * Word IS NOT editable on this screen so we can continuously listen for changes in db.
 */
class WordDetailViewModel @Inject constructor(
    private val wordRepository: WordRepository
) : BaseViewModel() {

    private val wordSubject: FlowableProcessor<WordDetail> = BehaviorProcessor.create<WordDetail>()
    var loadedWordId: Long = -1

    fun observeWord(): Flowable<WordDetail> = wordSubject

    fun loadWordDetail(wordId: Long) {
        loadedWordId = wordId

        disposables += wordRepository.getWordDetail(wordId)
            .subscribe({ word ->
                wordSubject.onNext(word)
            }, Throwable::printStackTrace)
    }

    fun delete(): Completable {
        return wordRepository.deleteWord(loadedWordId)
    }
}
