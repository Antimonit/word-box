package me.khol.wordbox.screens.word.edit.epoxy.translations

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.ImageButton
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colorWithAlpha
import me.khol.wordbox.extensions.drawable
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.extensions.languagesButtonStyle
import me.khol.wordbox.extensions.startPadding
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.autoCompleteTextView
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.imageButton
import org.jetbrains.anko.padding
import org.jetbrains.anko.singleLine
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.view

/**
 * Epoxy model of a single [translation][TranslationFull].
 */
@EpoxyModelClass
open class TranslationModel : BaseEpoxyModel<TranslationModel.Layout>() {

    @EpoxyAttribute
    lateinit var languages: List<Language>
    @EpoxyAttribute
    lateinit var translation: TranslationFull
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var callbacks: TranslationCallbacks

    override fun createViewLayout(parent: ViewGroup) = Layout(parent, callbacks)

    override fun Layout.bind() {
        txtLanguages.text = translation.language.shortName

        btnDelete.isVisible = true
        btnDelete.setOnClickListener {
            callbacks.onDelete(translation)
        }

        txtTranslation.setText(translation.text)
        txtTranslation.dismissDropDown()
        txtTranslation.setOnItemClickListener { _, _, position, id ->
            val oldTranslation = translation
            val suggestion = suggestionsAdapter.getItem(position)
            val newTranslation =
                TranslationFull(suggestion.wordId, suggestion.text, suggestion.description, suggestion.language)
            callbacks.onReplace(oldTranslation, newTranslation)
            txtTranslation.setText(newTranslation.text)
            txtLanguages.text = suggestion.language.shortName
        }
    }

    override fun Layout.unbind() {
        btnDelete.setOnClickListener(null)
        txtTranslation.onItemClickListener = null
    }

    class Layout(parent: ViewGroup, callbacks: TranslationCallbacks) : BaseLayout(parent) {

        lateinit var txtLanguages: TextView
        lateinit var txtTranslation: AutoCompleteTextView
        lateinit var btnDelete: ImageButton

        val suggestionsAdapter = TranslationSuggestionsAdapter(context, callbacks)

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.frameLayout {

                constraintLayout {
                    horizontalPadding = 8.dp

                    txtLanguages = textView {
                        languagesButtonStyle()
                    }

                    txtTranslation = autoCompleteTextView {
                        backgroundDrawable = null
                        padding = 0.dp
                        textSize = 16f
                        textColor = color(R.color.black)
                        font = R.font.roboto_medium
                        gravity = Gravity.CENTER_VERTICAL
                        startPadding = 56.dp
                        threshold = 1
                        setAdapter(suggestionsAdapter)
                        singleLine = true
                    }

                    btnDelete = imageButton {
                        setImageDrawable(drawable(R.drawable.ic_close))
                        backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackgroundBorderless)
                    }

                    constraints {
                        txtLanguages.connect(
                            STARTS of parentId with 8.dp,
                            VERTICAL of parentId
                        ).size(32.dp, 32.dp)

                        txtTranslation.connect(
                            STARTS of parentId,
                            ENDS of parentId with 40.dp,
                            VERTICAL of parentId
                        ).size(matchConstraint, matchConstraint)

                        btnDelete.connect(
                            ENDS of parentId,
                            VERTICAL of parentId
                        ).size(40.dp, 40.dp)
                    }

                }.lparams(match, match)

                view {
                    backgroundColor = colorWithAlpha(R.color.black, 20)
                }.lparams(match, 1.dp) {
                    gravity = Gravity.BOTTOM
                }

            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, 56.dp)
            }
        }
    }
}
