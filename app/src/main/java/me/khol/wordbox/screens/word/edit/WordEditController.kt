package me.khol.wordbox.screens.word.edit

import com.airbnb.epoxy.AutoModel
import com.airbnb.epoxy.EpoxyController
import me.khol.wordbox.R
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.screens.word.base.epoxy.HeaderModel_
import me.khol.wordbox.screens.word.base.epoxy.SeparatorModel_
import me.khol.wordbox.screens.word.edit.epoxy.description.description
import me.khol.wordbox.screens.word.edit.epoxy.examples.addExample
import me.khol.wordbox.screens.word.edit.epoxy.examples.example
import me.khol.wordbox.screens.word.edit.epoxy.labels.labels
import me.khol.wordbox.screens.word.edit.epoxy.practice.practice
import me.khol.wordbox.screens.word.edit.epoxy.save
import me.khol.wordbox.screens.word.edit.epoxy.translations.addTranslation
import me.khol.wordbox.screens.word.edit.epoxy.translations.translation

/**
 * Controller containing all models used to modify data of a [WordDetail]
 */
class WordEditController(
    private val onSaveClick: () -> Unit,
    private val callbacks: WordCallbacks
) : EpoxyController() {

    // used for language selection in translations section
    lateinit var allLanguages: List<Language>
    lateinit var allLabels: List<Label>
    lateinit var word: WordDetail

    @AutoModel
    lateinit var headerDescription: HeaderModel_
    @AutoModel
    lateinit var headerLabels: HeaderModel_
    @AutoModel
    lateinit var headerExamples: HeaderModel_
    @AutoModel
    lateinit var headerTranslations: HeaderModel_
    @AutoModel
    lateinit var headerPractice: HeaderModel_
    @AutoModel
    lateinit var separatorLabels: SeparatorModel_
    @AutoModel
    lateinit var separatorTranslations: SeparatorModel_
    @AutoModel
    lateinit var separatorExamples: SeparatorModel_
    @AutoModel
    lateinit var separatorPractice: SeparatorModel_

    override fun buildModels() {

        headerDescription.text(R.string.word_detail_header_description).addTo(this)

        description {
            id("description")
            descriptionText(word.description)
            callbacks(callbacks)
        }

        separatorLabels.addTo(this)

        headerLabels.text(R.string.word_detail_header_labels).addTo(this)

        labels {
            id("labels")
            labels(allLabels)
            checkedLabels(word.labels)
            callbacks(callbacks)
        }

        separatorExamples.addTo(this)

        headerExamples.text(R.string.word_detail_header_examples).addTo(this)

        word.examples.forEach { example ->
            example {
                id("example", example.exampleId)
                example(example)
                callbacks(callbacks)
            }
        }

        addExample {
            id("add_example")
            onAddClick(callbacks::onAddExample)
        }

        separatorTranslations.addTo(this)

        headerTranslations.text(R.string.word_detail_header_translations).addTo(this)

        word.translations.forEach { translation ->
            translation {
                id("translation", translation.wordId)
                languages(allLanguages)
                translation(translation)
                callbacks(callbacks)
            }
        }
        addTranslation {
            id("add_translation")
            onAddClick(callbacks::onAddTranslation)
        }

        separatorPractice.addTo(this)

        headerPractice.text(R.string.word_detail_header_practice).addTo(this)

        practice {
            id("practice")
            scheduledPractice(word.scheduledTime)
            lastPractice(word.lastTime)
            interval(word.interval)
            onResetClick(callbacks::onResetClicked)
        }

        save {
            id("save")
            changed(true)
            onSaveClick(onSaveClick)
        }
    }
}
