package me.khol.wordbox.screens.base.epoxy

import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelWithView
import me.khol.wordbox.R
import me.khol.wordbox.screens.base.layout.BaseLayout

/**
 * Epoxy model with [BaseLayout] implementation
 */
abstract class BaseEpoxyModel<T : BaseLayout> : EpoxyModelWithView<View>() {

    @EpoxyAttribute
    var spanSize: Int = 1

    override fun getSpanSize(totalSpanCount: Int, position: Int, itemCount: Int): Int {
        return spanSize
    }

    abstract fun createViewLayout(parent: ViewGroup): T

    final override fun buildView(parent: ViewGroup): View {
        val layout: T = createViewLayout(parent)
        val view = layout.view
        view.setTag(R.id.epoxy_view_layout, layout)
        return view
    }

    final override fun bind(view: View) {
        super.bind(view)
        @Suppress("UNCHECKED_CAST")
        (view.getTag(R.id.epoxy_view_layout) as T).bind()
    }

    open fun T.bind() {

    }

    override fun unbind(view: View) {
        super.unbind(view)
        @Suppress("UNCHECKED_CAST")
        (view.getTag(R.id.epoxy_view_layout) as T).unbind()
    }

    open fun T.unbind() {

    }

}
