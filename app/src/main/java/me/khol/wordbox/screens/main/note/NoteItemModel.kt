package me.khol.wordbox.screens.main.note

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.cardView
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.database.model.NoteList
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColorResource

@EpoxyModelClass
open class NoteItemModel : BaseEpoxyModel<NoteItemModel.Layout>() {

    @EpoxyAttribute
    lateinit var note: NoteList
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onClick: (NoteList) -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        view.setOnClickListener {
            onClick(note)
        }
        txtTitle.text = note.title
        txtTitle.isVisible = note.title.isNotEmpty()
        txtContents.text = note.contents
        txtContents.isVisible = note.contents.isNotEmpty()
    }

    override fun Layout.unbind() {
        view.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtTitle: TextView
        lateinit var txtContents: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.cardView {
                cardElevation = 4.dpf
                radius = 4.dpf
                isClickable = true
                foreground = getDrawableAttr(R.attr.selectableItemBackground)

                // For some unknown reason, gone margin doesn't work programmatically and had to use
                // XML layout. Layout resource contains only "layout_*" attributes.
                addView(LayoutInflater.from(context).inflate(R.layout.note_item_model, this, false).apply {
                    padding = 12.dp

                    txtTitle = findViewById<TextView>(R.id.note_title).apply {
                        textColorResource = R.color.black
                        textSize = 16f
                        font = R.font.roboto_medium
                    }

                    txtContents = findViewById<TextView>(R.id.note_contents).apply {
                        textSize = 14f
                        font = R.font.roboto
                        maxLines = 10
                        ellipsize = TextUtils.TruncateAt.END
                    }
                }, match, wrap)

            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
