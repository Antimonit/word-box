package me.khol.wordbox.screens.word.detail.epoxy

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyRecyclerView
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.screens.word.edit.epoxy.labels.labelButton
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.padding

/**
 * Epoxy model of a all [labels][Label] that can be toggled on and off.
 */
@EpoxyModelClass
open class LabelsModel : BaseEpoxyModel<LabelsModel.Layout>() {

    @EpoxyAttribute
    lateinit var labels: List<Label>

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        listLabels.suppressLayout(false)
        listLabels.withModels {
            labels.forEach { label ->
                labelButton {
                    id(label.labelId)
                    label(label)
                    checked(true)
                }
            }
        }
        listLabels.suppressLayout(true)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var listLabels: EpoxyRecyclerView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.epoxyRecyclerView {
                padding = 12.dp    // remove 4.dp because of the spacing item decoration
                layoutManager = ChipsLayoutManager.newBuilder(context)
                    .setRowStrategy(ChipsLayoutManager.STRATEGY_FILL_VIEW)
                    .setChildGravity(Gravity.START)
                    .setScrollingEnabled(false)
                    .build()
                addItemDecoration(SpacingItemDecoration(8.dp, 8.dp))
                itemAnimator = null
            }.apply {
                listLabels = this
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
