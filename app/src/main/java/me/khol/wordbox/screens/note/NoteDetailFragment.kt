package me.khol.wordbox.screens.note

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.MenuItemCompat
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.extensions.getLongNullable
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.database.model.NoteDetail
import me.khol.wordbox.screens.base.BaseFragment
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.support.v4.withArguments

/**
 * Fragment for editing and creating new notes.
 */
class NoteDetailFragment : BaseFragment() {

    companion object {
        private const val ARG_NOTE_ID = "note_id"

        fun newEditFragment(wordId: Long): NoteDetailFragment {
            return NoteDetailFragment().withArguments(ARG_NOTE_ID to wordId)
        }

        fun newAddFragment(): NoteDetailFragment {
            return NoteDetailFragment()
        }
    }

    private lateinit var layout: NoteDetailLayout
    private lateinit var viewModel: NoteDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        setHasOptionsMenu(true)

        val noteId = arguments?.getLongNullable(ARG_NOTE_ID)
        if (noteId == null) {
            viewModel.loadNewNote()
        } else {
            viewModel.loadEditNote(noteId)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layout = NoteDetailLayout(container!!)
        return layout.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.setSupportActionBar(layout.toolbar)
        activity.supportActionBar?.let { actionBar ->
            actionBar.setDisplayShowTitleEnabled(false)
            actionBar.setDisplayShowCustomEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        disposables += layout.txtTitle.textChanges()
            .skipInitialValue()
            .subscribe {
                viewModel.onTitleChanged(it)
            }
        disposables += layout.txtContents.textChanges()
            .skipInitialValue()
            .subscribe {
                viewModel.onContentsChanged(it)
            }

        disposables += viewModel.observeNote()
            .observeOnMainThread()
            .subscribe({ note: NoteDetail ->
                layout.txtTitle.setText(note.title)
                layout.txtContents.setText(note.contents)
            }, Throwable::printStackTrace)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_note_detail, menu)

        if (arguments?.containsKey(ARG_NOTE_ID) != true) {
            menu.findItem(R.id.delete).isVisible = false
        }
        MenuItemCompat.setIconTintList(menu.findItem(R.id.delete), context!!.colors(R.color.black))
        MenuItemCompat.setIconTintList(menu.findItem(R.id.save), context!!.colors(R.color.black))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete -> showDeleteDialog()
            R.id.save -> save()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onBackPressed(): Boolean {
        return if (viewModel.hasChanged()) {
            showDiscardDialog()
            true
        } else {
            false
        }
    }

    private fun showDiscardDialog() {
        AlertDialog.Builder(context!!)
            .setMessage(R.string.edit_note_discard_dialog_message)
            .setPositiveButton(R.string.edit_note_discard_dialog_positive) { _, _ ->
                save()
            }
            .setNegativeButton(R.string.edit_note_discard_dialog_negative) { _, _ ->
                viewModel.discardChanges()
                activity.onBackPressed()
            }
            .show()
    }

    private fun showDeleteDialog() {
        AlertDialog.Builder(context!!)
            .setMessage(R.string.edit_note_delete_dialog_message)
            .setPositiveButton(R.string.edit_note_delete_dialog_positive) { _, _ ->
                delete()
            }
            .setNegativeButton(R.string.edit_note_delete_dialog_negative) { _, _ ->
                /* do nothing */
            }
            .show()
    }

    private fun save() {
        disposables += viewModel.saveChanges()
            .observeOnMainThread()
            .subscribe({
                toast(R.string.edit_note_save_success)
                getActivity()?.finish()
            }, {
                toast(R.string.edit_note_save_error)
                it.printStackTrace()
            })
    }

    private fun delete() {
        disposables += viewModel.delete()
            .observeOnMainThread()
            .subscribe({
                toast(R.string.edit_note_delete_success)
                getActivity()?.finish()
            }, {
                toast(R.string.edit_note_delete_error)
                it.printStackTrace()
            })
    }
}
