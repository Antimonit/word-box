package me.khol.wordbox.screens.word.edit.epoxy.translations

import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.database.model.WordSuggestion

interface TranslationCallbacks {
    fun getSuggestions(query: String): List<WordSuggestion>
    fun onReplace(old: TranslationFull, new: TranslationFull)
    fun onDelete(translation: TranslationFull)
    fun onAddTranslation()
}
