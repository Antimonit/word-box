package me.khol.wordbox.screens.main.word.headers

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import android.widget.LinearLayout
import android.widget.TextView
import me.khol.wordbox.R
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.allCaps
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.verticalPadding
import org.jetbrains.anko.withAlpha

/**
 * Base layout for Header layouts. Contains Header text, optional manage button and contents
 * defined in subclasses via [createContentView].
 */
abstract class BaseHeaderLayout(
    parent: ViewGroup
) : BaseLayout(parent) {

    /**
     * Invoked whenever user clicks on an empty view or Manage button.
     */
    var onManage: () -> Unit = { /* open appropriate manage screen from fragment/activity */ }

    protected abstract val headerRes: Int
    protected open val manageRes: Int? = null

    protected var txtManage: TextView? = null

    abstract fun ViewManager.createContentView(): View

    override fun createView(ui: AnkoContext<Context>): LinearLayout {
        return ui.verticalLayout {
            frameLayout {
                textView(headerRes) {
                    font = R.font.roboto_medium
                    textColor = Color.WHITE
                    textSize = 14f
                    horizontalPadding = 16.dp
                    verticalPadding = 8.dp
                }

                manageRes?.let {
                    txtManage = textView(it) {
                        font = R.font.roboto_medium
                        textColor = Color.WHITE.withAlpha(0x7F)
                        textSize = 12f
                        allCaps = true
                        backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackgroundBorderless)
                        isClickable = true
                        horizontalPadding = 16.dp
                        verticalPadding = 8.dp
                        setOnClickListener { onManage() }
                    }.lparams {
                        gravity = Gravity.END or Gravity.BOTTOM
                    }
                }
            }

            createContentView()
        }
    }
}
