package me.khol.wordbox.screens.settings

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getColorAttr
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.verticalLayout

/**
 * Layout for [SettingsActivity].
 */
class SettingsLayout(parent: ViewGroup) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var content: ViewGroup

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.verticalLayout {
            toolbar = themedToolbar(R.style.WordBoxTheme_DarkAppBarOverlay) {
                title = "Settings"
                backgroundColor = getColorAttr(R.attr.colorPrimary)
                minimumHeight = getDimensionAttr(R.attr.actionBarSize)
            }.lparams(match, wrap)

            content = frameLayout {
                id = R.id.fragment
            }.lparams(match, match)

        }.apply {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }
}
