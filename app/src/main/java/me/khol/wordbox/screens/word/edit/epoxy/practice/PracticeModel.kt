package me.khol.wordbox.screens.word.edit.epoxy.practice

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colorWithAlpha
import me.khol.wordbox.extensions.relatedToNow
import me.khol.wordbox.extensions.roundDrawable
import me.khol.wordbox.extensions.selectableDrawable
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.button
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.verticalMargin
import org.jetbrains.anko.verticalPadding
import org.threeten.bp.Duration
import org.threeten.bp.Instant

/**
 * Epoxy model of a word's practice progress and schedule
 */
@EpoxyModelClass
open class PracticeModel : BaseEpoxyModel<PracticeModel.Layout>() {

    @EpoxyAttribute
    lateinit var scheduledPractice: Instant
    @EpoxyAttribute
    var lastPractice: Instant? = null
    @EpoxyAttribute
    lateinit var interval: Duration
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onResetClick: () -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        txtScheduled.text = scheduledPractice.relatedToNow()
        txtLast.text = lastPractice?.relatedToNow() ?: "-"
        btnReset.setOnClickListener {
            onResetClick()
        }
    }

    override fun Layout.unbind() {
        btnReset.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtScheduled: TextView
        lateinit var txtLast: TextView
        lateinit var btnReset: Button

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.verticalLayout {
                verticalPadding = 8.dp
                horizontalPadding = 16.dp
                clipToPadding = false

                fun addRow(labelText: String): TextView {
                    lateinit var txtValue: TextView
                    linearLayout {
                        textView(labelText) {
                            textSize = 15f
                            textColor = color(R.color.black)
                        }

                        txtValue = textView {
                            textSize = 15f
                            textColor = color(R.color.black)
                            gravity = Gravity.END
                        }.lparams(0, wrap, weight = 1f)
                    }
                    return txtValue
                }

                txtScheduled = addRow(context.getString(R.string.word_detail_scheduled_practice))

                txtLast = addRow(context.getString(R.string.word_detail_last_practice))

                btnReset = button(R.string.word_edit_reset_progress) {
                    horizontalPadding = 16.dp
                    backgroundDrawable = selectableDrawable(
                        roundDrawable(
                            color(R.color.white),
                            color(R.color.colorPrimary)
                        ),
                        colorWithAlpha(R.color.colorPrimary, 50)
                    )
                    textColor = color(R.color.colorPrimary)
                }.lparams(wrap, 40.dp) {
                    verticalMargin = 16.dp
                }

            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
