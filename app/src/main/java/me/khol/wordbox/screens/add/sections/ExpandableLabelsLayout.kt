package me.khol.wordbox.screens.add.sections

import android.content.Context
import android.view.Gravity
import android.view.View
import com.airbnb.epoxy.EpoxyRecyclerView
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.word.edit.WordCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.labels.labelButton
import org.jetbrains.anko._LinearLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.verticalPadding

/**
 * Contains a list of labels that can be toggled on and off.
 */
class ExpandableLabelsLayout(
    context: Context,
    titleRes: Int,
    callbacks: WordCallbacks
) : ExpandableLayout(context, titleRes, callbacks) {

    lateinit var list: EpoxyRecyclerView

    override fun _LinearLayout.createContentView(): View {
        return epoxyRecyclerView {
            horizontalPadding = 12.dp
            verticalPadding = 8.dp
            layoutManager = ChipsLayoutManager.newBuilder(context)
                .setRowStrategy(ChipsLayoutManager.STRATEGY_FILL_VIEW)
                .setChildGravity(Gravity.START)
                .setScrollingEnabled(false)
                .build()
            addItemDecoration(SpacingItemDecoration(8.dp, 8.dp))
            itemAnimator = null
            list = this
        }.lparams(match, wrap)
    }

    fun bind(labels: List<Label>, checkedLabels: List<Label>) {
        syncTitle(checkedLabels.size)

        list.withModels {
            labels.forEach { label ->
                labelButton {
                    id(label.labelId)
                    label(label)
                    checked(checkedLabels.contains(label))
                    onChecked { label, checked ->
                        // TODO: sync title state
//						if (checked) localCheckedLabels.add(label) else localCheckedLabels.remove(label)
                        callbacks.onLabelChecked(label, checked)
//						syncTitle(layout)
                    }
                }
            }
        }
    }
}
