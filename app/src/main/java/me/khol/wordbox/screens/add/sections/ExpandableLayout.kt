package me.khol.wordbox.screens.add.sections

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.StringRes
import me.khol.wordbox.R
import me.khol.wordbox.extensions.collapse
import me.khol.wordbox.extensions.darker
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.expand
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.roundDrawable
import me.khol.wordbox.extensions.setElevationStateList
import me.khol.wordbox.extensions.startMargin
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.screens.word.edit.WordCallbacks
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko._LinearLayout
import org.jetbrains.anko.allCaps
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.imageView
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalLayout

abstract class ExpandableLayout(
    context: Context,
    @StringRes
    val titleRes: Int,
    val callbacks: WordCallbacks
) : BaseLayout(context) {

    lateinit var sectionLayout: View
    lateinit var viewContent: View
    lateinit var txtTitle: TextView

    fun syncTitle(size: Int) {
        val title = context.getString(titleRes)
        txtTitle.text = if (size > 0) "$title ($size)" else title
    }

    abstract fun _LinearLayout.createContentView(): View

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.verticalLayout {
            elevationCompat = 4.dpf
            backgroundDrawable = roundDrawable(Color.WHITE, Color.WHITE.darker, stroke = 1.dp, corner = 4.dpf)
            setElevationStateList()
            // TODO: This is what we want to animate section's height, but it also introduces
            // some weird resizing animation bug
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                layoutTransition = LayoutTransition().apply {
//                    enableTransitionType(LayoutTransition.CHANGING)
//                }
//            }

            linearLayout {
                backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackground)
                gravity = Gravity.CENTER_VERTICAL

                lateinit var expandButton: View

                setOnClickListener {
                    viewContent.findFocus()?.clearFocus()
                    isActivated = !isActivated
                    sectionLayout.isActivated = isActivated
                    if (isActivated) {
                        viewContent.expand()
                        expandButton.animate().rotation(179.9f).start()
                    } else {
                        viewContent.collapse()
                        expandButton.animate().rotation(0f).start()
                    }
                }

                txtTitle = textView {
                    textColorResource = R.color.colorPrimary
                    allCaps = false
                    textSize = 14f
                    font = R.font.roboto_medium
                }.lparams(wrap, wrap) {
                    startMargin = 16.dp
                }

                expandButton = imageView(R.drawable.ic_expand_more) {
                    isClickable = false
                    isFocusable = false
                    backgroundDrawable = null
                }.lparams(24.dp, 24.dp) {
                    startMargin = 8.dp
                }
            }.lparams(match, 48.dp)

            viewContent = createContentView().lparams(match, 0)

            sectionLayout = this
            layoutParams = ViewGroup.LayoutParams(match, wrap)
        }
    }
}
