package me.khol.wordbox.screens.main.word.headers

import android.content.Context
import android.view.View
import android.view.ViewGroup
import me.khol.wordbox.R
import me.khol.wordbox.extensions.customLayout
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.padding
import org.jetbrains.anko.topPadding
import org.jetbrains.anko.verticalLayout

/**
 * Layout displaying three section:
 *  - [languages][HeaderLanguagesLayout]
 *  - [sort by][HeaderSortLayout]
 *  - [labels][HeaderLabelsLayout]
 */
class HeaderLayout(
    parent: ViewGroup
) : BaseLayout(parent) {

    lateinit var headerLabels: HeaderLabelsLayout
    lateinit var headerSort: HeaderSortLayout
    lateinit var headerLanguages: HeaderLanguagesLayout

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.verticalLayout {
            padding = 16.dp
            topPadding = getDimensionAttr(R.attr.actionBarSize)
            clipToPadding = false
            clipChildren = false

            headerLanguages = customLayout(HeaderLanguagesLayout(this)) {
                lparams(match, wrap)
            }

            headerSort = customLayout(HeaderSortLayout(this)) {
                lparams(match, wrap)
            }

            headerLabels = customLayout(HeaderLabelsLayout(this)) {
                lparams(match, wrap)
            }
        }
    }
}
