package me.khol.wordbox.screens.main.practice

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.centeredToolbar
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.customLayout
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getColorAttr
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.extensions.roundDrawable
import me.khol.wordbox.extensions.selectableDrawable
import me.khol.wordbox.extensions.swipeStack
import me.khol.wordbox.model.view.SwipeCard
import me.khol.wordbox.model.view.SwipeStack
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.screens.main.word.headers.HeaderLanguagesLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.bottomPadding
import org.jetbrains.anko.button
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.margin
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.topPadding
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.withAlpha

/**
 * Layout for [PracticeFragment].
 *
 * Contains swipe stack when practice is in progress and language selector when it is not.
 */
class PracticeLayout(
    parent: ViewGroup,
    private val swipeAdapter: SwipeStackAdapter
) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var swipeStack: SwipeStack
    lateinit var languagesHeader: HeaderLanguagesLayout
    lateinit var btnStart: Button
    lateinit var emptyStack: LinearLayout

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.verticalLayout {
            clipChildren = false
            backgroundColor = getColorAttr(R.attr.colorPrimary)

            toolbar = centeredToolbar().lparams(match, wrap)

            constraintLayout {
                clipChildren = false
                clipToPadding = false
                horizontalPadding = 24.dp
                bottomPadding = 24.dp
                topPadding = 8.dp

                val txtFinishedTitle = textView("All done!") {
                    textSize = 20f
                    textColorResource = R.color.white
                    font = R.font.roboto_medium
                    isVisible = false
                }

                val txtFinishedSubtitle = textView("You can take a break for the rest of the day") {
                    textSize = 12f
                    textColorResource = R.color.white
                    font = R.font.roboto
                    isVisible = false
                }

                emptyStack = verticalLayout {

                    textView("Choose a language to practice") {
                        textSize = 16f
                        textColorResource = R.color.white
                        font = R.font.roboto
                    }.lparams(wrap, wrap) {
                        gravity = Gravity.CENTER
                    }

                    languagesHeader = customLayout(HeaderLanguagesLayout(this)) {
                        lparams(match, wrap) {
                            margin = 24.dp
                        }
                    }

                    btnStart = button("Start") {
                        horizontalPadding = 16.dp
                        backgroundDrawable = selectableDrawable(
                            roundDrawable(
                                color(R.color.colorPrimary),
                                color(R.color.white),
                                dp(1)
                            ),
                            Color.WHITE.withAlpha(50)
                        )
                        textColor = color(R.color.white)
                    }.lparams(wrap, wrap) {
                        gravity = Gravity.CENTER
                    }
                }

                swipeStack = swipeStack {
                    viewRotation = 0
                    numberOfStackedViews = 2
                    scaleFactor = 0.95f
                    viewSpacing = 0.dp
                    allowedSwipeDirection = SwipeStack.SWIPE_NONE

                    adapter = swipeAdapter
                    swipeProgressListener = object : SwipeCard.SwipeProgressListener {
                        override fun onSwipeStart(view: View) {
                            // do nothing
                        }

                        override fun onSwipeProgress(view: View, progressX: Float, progressY: Float) {
                            swipeAdapter.getLayoutForView(view).setProgress(progressX, progressY)
                        }

                        override fun onSwipeEnd(view: View) {
                            swipeAdapter.getLayoutForView(view).setProgress(0f, 0f)
                        }
                    }
                }

                constraints {
                    arrayOf(txtFinishedTitle, txtFinishedSubtitle)
                        .chainPacked(TOP of parentId, BOTTOM of parentId)
                        .forEach {
                            it.connect(HORIZONTAL of parentId)
                        }

                    emptyStack.connect(ALL of parentId)
                        .size(matchConstraint, wrap)

                    swipeStack
                        .connect(ALL of parentId)
                        .size(matchConstraint, matchConstraint)
                }

            }.lparams(match, 0, 1f) {
                gravity = Gravity.CENTER
            }

        }.apply {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }
}
