package me.khol.wordbox.screens.word

import android.content.Context
import android.content.Intent
import android.os.Bundle
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getLongNullable
import me.khol.wordbox.screens.base.BaseActivity
import me.khol.wordbox.screens.word.detail.WordDetailFragment

/**
 * Show detailed information about a word in [WordDetailFragment] and allow edit of a word in
 * [WordEditFragment][me.khol.wordbox.screens.word.edit.WordEditFragment]
 */
class WordActivity : BaseActivity() {

    companion object {
        private const val ARG_WORD_ID = "word_id"

        fun getIntent(context: Context, wordId: Long): Intent {
            return Intent(context, WordActivity::class.java).apply {
                putExtra(ARG_WORD_ID, wordId)
            }
        }
    }

    private lateinit var layout: WordLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        layout = WordLayout(rootView)
        setContentLayout(layout)

        if (savedInstanceState == null) {
            intent.extras?.getLongNullable(ARG_WORD_ID)?.let {
                replaceDetailWord(it)
            }
        }
    }

    fun replaceDetailWord(wordId: Long) {
        val fragment = WordDetailFragment.newFragment(wordId)

        supportFragmentManager.beginTransaction()
            .replace(R.id.word_detail_content, fragment, null)
            .commit()
    }
}
