package me.khol.wordbox.screens.word.detail.epoxy

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.horizontalMargin
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalPadding

/**
 * Epoxy model of a description.
 */
@EpoxyModelClass
open class DescriptionModel : BaseEpoxyModel<DescriptionModel.Layout>() {

    @EpoxyAttribute
    lateinit var description: String

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        txtDescription.text = description
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtDescription: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.textView {
                textSize = 14f
                textColor = color(R.color.black)
                setLineSpacing(6.dpf, 1f)
                verticalPadding = 8.dp
            }.apply {
                txtDescription = this
                layoutParams = ViewGroup.MarginLayoutParams(match, wrap).apply {
                    horizontalMargin = 16.dp
                }
            }
        }
    }
}
