package me.khol.wordbox.screens.base.viewmodel

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

/**
 * A ViewModel that automatically dispose its [disposables]
 */
abstract class BaseViewModel : ViewModel() {

    protected var disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}
