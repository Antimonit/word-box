package me.khol.wordbox.screens.main

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import me.khol.wordbox.R
import me.khol.wordbox.screens.base.BaseActivity
import me.khol.wordbox.screens.main.note.NoteListFragment
import me.khol.wordbox.screens.main.practice.PracticeFragment
import me.khol.wordbox.screens.main.profile.ProfileFragment
import me.khol.wordbox.screens.main.word.WordListFragment

/**
 * The activity responsible for switching between four main section of the application.
 */
class MainActivity : BaseActivity() {

    private lateinit var layout: MainLayout
    private lateinit var fragment: Fragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        layout = MainLayout(
            rootView,
            changeFragment = { itemId ->
                fragment = when (itemId) {
                    R.id.nav_practice -> PracticeFragment()
                    R.id.nav_words -> WordListFragment()
                    R.id.nav_notes -> NoteListFragment()
                    R.id.nav_profile -> ProfileFragment()
                    else -> throw IllegalArgumentException()
                }
                supportFragmentManager.beginTransaction()
                    .replace(R.id.main_content, fragment, null)
                    .commit()
            }
        )
        setContentLayout(layout)

        if (savedInstanceState == null) {
            layout.navigation.selectedItemId = R.id.nav_words
        } else {
            fragment = supportFragmentManager.findFragmentById(R.id.main_content)!!
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> fragment.onOptionsItemSelected(item)
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent) {
        if (intent.action == Intent.ACTION_SEARCH) {
            val query = intent.getStringExtra(SearchManager.QUERY)

            (fragment as? WordListFragment)?.setSearchQuery(query)
        }
    }
}
