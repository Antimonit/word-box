package me.khol.wordbox.screens.main.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.model.repository.DatabaseRepository
import me.khol.wordbox.screens.backup.BackupActivity
import me.khol.wordbox.screens.base.BaseFragment
import me.khol.wordbox.screens.manage.labels.ManageLabelsActivity
import me.khol.wordbox.screens.manage.languages.ManageLanguagesActivity
import me.khol.wordbox.screens.settings.SettingsActivity
import javax.inject.Inject

class ProfileFragment : BaseFragment() {

    @Inject
    lateinit var dbRepository: DatabaseRepository

    private lateinit var layout: ProfileLayout
    private lateinit var viewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layout = ProfileLayout(container!!)
        return layout.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposables += viewModel.observeHistory()
            .observeOnMainThread()
            .subscribe(layout.historyLayout::setHistory, Throwable::printStackTrace)

        activity.setSupportActionBar(layout.toolbar)
        activity.supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(false)
            it.setTitle(R.string.nav_title_profile)
        }

        layout.tabSelectedListener = viewModel::setUnit

        layout.historyLayout.setOnPreviousClickListener(viewModel::previous)
        layout.historyLayout.setOnNextClickListener(viewModel::next)

        layout.manageBackup.setOnClickListener {
            startActivity(Intent(activity, BackupActivity::class.java))
        }

        layout.manageLabels.setOnClickListener {
            startActivity(Intent(activity, ManageLabelsActivity::class.java))
        }

        layout.manageLanguages.setOnClickListener {
            startActivity(Intent(activity, ManageLanguagesActivity::class.java))
        }

        layout.settings.setOnClickListener {
            startActivity(Intent(activity, SettingsActivity::class.java))
        }
    }
}
