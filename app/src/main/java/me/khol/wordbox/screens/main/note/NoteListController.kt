package me.khol.wordbox.screens.main.note

import com.airbnb.epoxy.EpoxyController
import me.khol.wordbox.database.model.NoteList
import me.khol.wordbox.screens.base.epoxy.Prop

/**
 * Controller to display list of notes in [NoteListFragment]
 */
class NoteListController(
    private val onNoteClick: (NoteList) -> Unit
) : EpoxyController() {

    var notes by Prop(emptyList<NoteList>())

    override fun buildModels() {
        notes.forEach { note ->
            noteItem {
                id(note.noteId)
                note(note)
                onClick(onNoteClick)
            }
        }
    }
}
