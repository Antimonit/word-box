package me.khol.wordbox.screens.base

import android.view.MenuItem
import android.view.ViewGroup
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.screens.base.viewmodel.ViewModelFactory
import javax.inject.Inject

/**
 * Base activity that all other custom activities should extend
 */
abstract class BaseActivity : DaggerAppCompatActivity() {

    protected val rootView: ViewGroup by lazy { findViewById<ViewGroup>(android.R.id.content) }
    protected val disposables = CompositeDisposable()

    @Inject
    lateinit var vmFactory: ViewModelFactory

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun setContentLayout(layout: BaseLayout) {
        setContentView(layout.view)
    }

    /*
     * Give a chance to handle back press to every fragment attached to this activity.
     */
    override fun onBackPressed() {
        supportFragmentManager.fragments
            .filterIsInstance<CommonBaseFragment>()
            .forEach {
                if (it.onBackPressed()) return
            }
        super.onBackPressed()
    }
}
