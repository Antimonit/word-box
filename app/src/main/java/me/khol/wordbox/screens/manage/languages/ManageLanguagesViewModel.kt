package me.khol.wordbox.screens.manage.languages

import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.extensions.moveItem
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.model.repository.LanguageRepository
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * ViewModel for [ManageLanguagesActivity].
 */
class ManageLanguagesViewModel @Inject constructor(
    private val languageRepository: LanguageRepository
) : BaseViewModel() {

    data class State(val language: List<Language>, val editModeLanguage: Language?)

    // Keep a copy of the languages when we perform drag. When the drag operation is finished, flush
    // reordered languages to the repository and the DB.
    private var languages: List<Language> = listOf()
    private var editModeLanguage: Language? = null

    private val stateSubject: FlowableProcessor<State> = BehaviorProcessor.create()

    fun observeState(): Flowable<State> = stateSubject

    init {
        disposables += languageRepository.observeAllLanguages()
            .subscribe({ newLanguages ->
                languages = newLanguages
                stateSubject.onNext(State(languages, editModeLanguage))
            }, Throwable::printStackTrace)
    }

    fun deleteLanguage(language: Language) {
        disposables += languageRepository.deleteLanguage(language).subscribe()
    }

    fun editStarted(language: Language) {
        editModeLanguage = language
        stateSubject.onNext(State(languages, editModeLanguage))
    }

    fun editCanceled(language: Language) {
        editModeLanguage = null
        stateSubject.onNext(State(languages, editModeLanguage))
    }

    fun editConfirmed(language: Language, name: String, shortName: String) {
        editModeLanguage = null
        disposables += languageRepository.updateLanguage(language.copy(name = name, shortName = shortName)).subscribe()
    }

    fun getLanguageWordCount(languageId: Long): Int {
        return languageRepository.getLanguageWordCount(languageId).blockingFirst()
    }


    /**
     * It is okay if two languages have the same [Language.order] or if there are holes in the
     * ordering.
     * Languages get reordered based on their position in the list, not on the [Language.order].
     */
    fun moveLanguage(languageToMove: Language, fromPosition: Int, toPosition: Int) {
        languages = languages
            .moveItem(fromPosition, toPosition)
            .mapIndexed { index, language ->
                language.copy(order = index)
            }
        stateSubject.onNext(State(languages, editModeLanguage))
    }

    fun moveLanguagesFinished() {
        disposables += languageRepository.updateLanguages(languages).subscribe()
    }

    fun addLanguage(newLanguageName: String) {
        val shortName = ""
        val size = languages.size
        languageRepository.insertLanguage(Language(0, newLanguageName, shortName, size)).subscribe()
    }
}
