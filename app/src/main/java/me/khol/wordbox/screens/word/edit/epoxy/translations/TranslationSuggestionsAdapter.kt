package me.khol.wordbox.screens.word.edit.epoxy.translations

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.extensions.languagesButtonStyle
import me.khol.wordbox.database.model.WordSuggestion
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.screens.base.layout.layout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView

class TranslationSuggestionsAdapter(
    context: Context,
    private val callbacks: TranslationCallbacks,
    private var suggestions: List<WordSuggestion> = emptyList()
) : ArrayAdapter<WordSuggestion>(context, android.R.layout.simple_list_item_1, suggestions) {

    private val listFilter = object : Filter() {
        private val lock = Any()

        override fun performFiltering(prefix: CharSequence?): FilterResults {
            val results = FilterResults()

            if (prefix.isNullOrBlank()) {
                synchronized(lock) {
                    results.values = ArrayList<String>()
                    results.count = 0
                }

            } else {
                val queryLowerCase = prefix.toString().toLowerCase()
                val suggestions: List<WordSuggestion> = callbacks.getSuggestions(queryLowerCase)

                results.values = suggestions
                results.count = suggestions.size
            }

            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            suggestions = if (results.values != null) {
                @Suppress("UNCHECKED_CAST")
                results.values as List<WordSuggestion>
            } else {
                emptyList()
            }

            if (results.count > 0) {
                notifyDataSetChanged()
            } else {
                notifyDataSetInvalidated()
            }
        }
    }

    override fun getFilter(): Filter = listFilter

    override fun getCount(): Int = suggestions.size

    override fun getItem(position: Int): WordSuggestion = suggestions[position]

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: Layout(parent).view
        return view.apply {
            layout<Layout>().bind(getItem(position))
        }
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        private lateinit var root: View
        private lateinit var txtLanguage: TextView
        private lateinit var txtTranslation: TextView
        private lateinit var txtDescription: TextView

        fun bind(item: WordSuggestion) {
            txtLanguage.text = item.language.shortName
            txtTranslation.text = item.text
            txtDescription.text = item.description
            txtDescription.isVisible = !item.description.isBlank()
            root.layoutParams.height = if (item.description.isBlank()) 48.dp else 72.dp
        }

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.constraintLayout {
                root = this

                txtLanguage = textView {
                    languagesButtonStyle()
                }

                txtTranslation = textView {
                    textSize = 16f
                    textColor = color(R.color.black)
                    font = R.font.roboto_medium
                }

                txtDescription = textView {
                    textSize = 14f
                    textColor = color(R.color.black)
                }

                constraints {
                    txtLanguage.connect(
                        VERTICAL of txtTranslation,
                        STARTS of parentId with 8.dp
                    ).size(wrapContent, wrapContent)

                    txtTranslation.connect(
                        STARTS of parentId with 56.dp,
                        ENDS of parentId,
                        TOPS of parentId with 12.dp
                    ).size(matchConstraint, 24.dp)

                    txtDescription.connect(
                        STARTS of txtTranslation,
                        ENDS of parentId,
                        TOP to BOTTOM of txtTranslation with 4.dp,
                        BOTTOMS of parentId with 12.dp
                    ).size(matchConstraint, 20.dp)
                }

                layoutParams = ViewGroup.LayoutParams(match, 48.dp)
            }
        }
    }
}
