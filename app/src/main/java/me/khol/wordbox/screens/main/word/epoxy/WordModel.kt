package me.khol.wordbox.screens.main.word.epoxy

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyItemSpacingDecorator
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyRecyclerView
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.backgroundTintListCompat
import me.khol.wordbox.extensions.colorWithAlpha
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.roundDrawable
import me.khol.wordbox.database.model.WordList
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.view
import org.threeten.bp.Duration
import org.threeten.bp.Instant

@EpoxyModelClass
open class WordModel : BaseEpoxyModel<WordModel.Layout>() {

    @EpoxyAttribute
    lateinit var word: WordList
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onClick: (WordList) -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        view.setOnClickListener {
            onClick(word)
        }
        viewProgress.backgroundTintListCompat = context.colors(when {
            word.scheduledTime < Instant.now() -> R.color.progressBad
            word.scheduledTime < Instant.now() + Duration.ofDays(7) -> R.color.progressNeutral
            else -> R.color.progressGood
        })
        txtWord.text = word.text
        txtTranslations.text = word.translations.joinToString("\n") { it.text }
        listLabels.suppressLayout(false)
        listLabels.withModels {
            word.labels.forEach { label ->
                labelView {
                    id(label.labelId)
                    label(label)
                }
            }
        }
        listLabels.suppressLayout(true)
    }

    override fun Layout.unbind() {
        view.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtWord: TextView
        lateinit var txtTranslations: TextView
        lateinit var viewProgress: View
        lateinit var listLabels: EpoxyRecyclerView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.constraintLayout {
                isClickable = true
                backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)

                viewProgress = view {
                    backgroundDrawable = roundDrawable(
                        backgroundColor = Color.BLACK,
                        corner = 12.dpf
                    )
                }.lparams(24.dp, 24.dp)

                txtWord = textView {
                    textColorResource = R.color.black
                    textSize = 16f
                    font = R.font.roboto_medium
                }

                txtTranslations = textView {
                    textColorResource = R.color.black
                    textSize = 16f
                    font = R.font.roboto_light_italic
                    gravity = Gravity.END
                }

                listLabels = epoxyRecyclerView {
                    padding = 12.dp
                    isClickable = false
                    isFocusable = false
                    itemAnimator = null
                    layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    setHasFixedSize(true)
                    addItemDecoration(EpoxyItemSpacingDecorator(4.dp))
                }

                val divider = view {
                    backgroundColor = colorWithAlpha(R.color.black, 20)
                }

                constraints {
                    viewProgress.connect(
                        STARTS of parentId with 16.dp,
                        TOPS of parentId with 16.dp
                    )

                    txtWord.connect(
                        STARTS of parentId with 72.dp,
                        TOPS of parentId with 12.dp
                    )

                    txtTranslations.connect(
                        ENDS of parentId with 72.dp,
                        TOP to BOTTOM of txtWord,
                        BOTTOMS of parentId with 12.dp
                    )

                    listLabels.connect(
                        ENDS of parentId,
                        VERTICAL of parentId
                    ).size(72.dp, wrapContent)

                    divider.connect(
                        BOTTOMS of parentId,
                        HORIZONTAL of parentId with 12.dp
                    ).size(matchConstraint, 1.dp)
                }
            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
