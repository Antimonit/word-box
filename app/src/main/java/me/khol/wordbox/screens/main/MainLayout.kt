package me.khol.wordbox.screens.main

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import me.khol.wordbox.R
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.design.bottomNavigationView
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.verticalLayout

/**
 * Layout for [MainActivity].
 */
class MainLayout(
    parent: ViewGroup,
    private val changeFragment: (Int) -> Unit
) : BaseLayout(parent) {

    lateinit var content: FrameLayout
    lateinit var navigation: BottomNavigationView

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.verticalLayout {
            content = frameLayout {
                id = R.id.main_content
            }.lparams(match, 0, weight = 1f)

            navigation = bottomNavigationView {
                id = R.id.bottom_navigation
                backgroundColorResource = R.color.white
                isItemHorizontalTranslationEnabled = false
                labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED

                inflateMenu(R.menu.menu_navigation)
                setOnNavigationItemSelectedListener { item ->
                    changeFragment(item.itemId)
                    true
                }
            }.lparams(match, wrap)

        }.apply {
            layoutParams = ViewGroup.LayoutParams(match, match)
        }
    }
}
