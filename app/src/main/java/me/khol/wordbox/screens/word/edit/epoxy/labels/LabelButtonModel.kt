package me.khol.wordbox.screens.word.edit.epoxy.labels

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.extensions.labelButton
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.model.view.LabelButton
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.wrapContent

/**
 * Epoxy model of a single [label][Label].
 */
@EpoxyModelClass
open class LabelButtonModel : BaseEpoxyModel<LabelButtonModel.Layout>() {

	@EpoxyAttribute
	lateinit var label: Label
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	var checked: Boolean = false
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	var dark: Boolean = false
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	var onChecked: ((Label, Boolean) -> Unit)? = null

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun Layout.bind() {
		labelButton.apply {
			text = label.text
			accentColor = label.color
			if (dark) {
				backgroundColor = Color.TRANSPARENT
				textColor = Color.WHITE
			}
			isChecked = checked
			if (onChecked == null) {
				isClickable = false
			} else {
				setOnCheckedChangeListener { _, checked ->
					onChecked?.invoke(label, checked)
				}
			}
		}
	}

	override fun Layout.unbind() {
		labelButton.setOnCheckedChangeListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var labelButton: LabelButton

		override fun createView(ui: AnkoContext<Context>): View {
			return ui.labelButton {
				labelButton = this
				layoutParams = ViewGroup.LayoutParams(wrapContent, 40.dp)
			}
		}
	}
}
