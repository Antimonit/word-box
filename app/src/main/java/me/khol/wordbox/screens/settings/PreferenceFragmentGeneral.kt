package me.khol.wordbox.screens.settings

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import me.khol.wordbox.BuildConfig
import me.khol.wordbox.R

class PreferenceFragmentGeneral : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.pref_main)

        if (BuildConfig.DEBUG) {
            // keep the reference locally, because preferences keep only week references
            val developerListener = Preference.OnPreferenceClickListener {
                fragmentManager!!
                    .beginTransaction()
                    .replace(R.id.fragment, PreferenceFragmentDeveloper())
                    .addToBackStack(PreferenceFragmentDeveloper::class.java.simpleName)
                    .commit()
                true
            }
            findPreference("pref_developer_options")?.onPreferenceClickListener = developerListener
        } else {
            findPreference("pref_developer_options")?.isVisible = false
        }


        val aboutListener = Preference.OnPreferenceClickListener {
            AlertDialog.Builder(context!!)
                .setTitle("About")
                .setMessage("About details")
                .setPositiveButton(android.R.string.ok, null)
                .show()
            true
        }
        findPreference("pref_about")?.onPreferenceClickListener = aboutListener

        val changelogListener = Preference.OnPreferenceClickListener {
            AlertDialog.Builder(context!!)
                .setTitle("Changelog")
                .setMessage("Changelog details")
                .setPositiveButton(android.R.string.ok, null)
                .show()
            true
        }
        findPreference("pref_changelog")?.onPreferenceClickListener = changelogListener

        bindPreferenceSummaryToValue(findPreference("example_text"))
        bindPreferenceSummaryToValue(findPreference("pref_word_list_compactness"))
    }
}
