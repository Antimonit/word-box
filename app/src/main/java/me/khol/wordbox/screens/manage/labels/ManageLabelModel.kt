package me.khol.wordbox.screens.manage.labels

import android.animation.LayoutTransition
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.OvalShape
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.backgroundTintListCompat
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.hideKeyboard
import me.khol.wordbox.extensions.setTextWithRetainedSelection
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.manage.BaseManageModel
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.editText
import org.jetbrains.anko.hintResource
import org.jetbrains.anko.view

/**
 * Epoxy model used for setting name and color of a [Label] and dragging it up and down in its
 * containing recycler view.
 */
@EpoxyModelClass
open class ManageLabelModel : BaseManageModel<ManageLabelModel.Layout>() {

    interface Callbacks {
        fun onStartDrag(model: EpoxyModel<*>)
        fun onColorClicked(label: Label)
        fun onEditStarted(label: Label)
        fun onEditCanceled(label: Label)
        fun onEditConfirmed(label: Label, newText: String)
    }

    @EpoxyAttribute
    lateinit var label: Label

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var callbacks: Callbacks

    /**
     * Because of the messy logic behind activating, canceling and confirming edit mode, we use
     * this flag to distinguish between canceling and confirming actions when EditText loses focus.
     */
    private var isConfirmed: Boolean = false

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        baseBind()
        name.setTextWithRetainedSelection(label.text)
        name.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                confirmEdit(this)
                true
            } else {
                false
            }
        }
        name.setOnFocusChangeListener { _, hasFocus ->
            name.isCursorVisible = hasFocus

            root.elevationCompat = if (hasFocus) 4.dpf else 2.dpf
            if (hasFocus) {
                callbacks.onEditStarted(label)
            } else {
                if (!isConfirmed) {
                    callbacks.onEditCanceled(label)
                    name.setText(label.text)
                }
            }
        }

        color.backgroundTintListCompat = ColorStateList.valueOf(label.color)
        color.setOnClickListener {
            callbacks.onColorClicked(label)
        }
    }

    override fun confirmEdit(layout: Layout) {
        val newText = layout.name.text.toString()
        callbacks.onEditConfirmed(label, newText)
        isConfirmed = true
        layout.name.clearFocus()
        isConfirmed = false
        layout.name.hideKeyboard()
    }

    override fun onStartDrag() {
        callbacks.onStartDrag(this@ManageLabelModel)
    }

    override fun Layout.unbind() {
        baseUnbind()
        name.setOnFocusChangeListener(null)
        name.setOnEditorActionListener(null)
        color.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseManageModel.Layout(parent) {

        lateinit var color: View
        lateinit var name: EditText

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.constraintLayout {
                backgroundColorResource = R.color.white
                elevationCompat = 2.dpf
                layoutTransition = LayoutTransition()

                color = view {
                    backgroundDrawable = ShapeDrawable(OvalShape())
                    isFocusable = true
                }

                name = editText {
                    textSize = 16f
                    font = R.font.roboto_medium
                    backgroundDrawable = null
                    hintResource = R.string.manage_label_name_hint
                    ellipsize = TextUtils.TruncateAt.END
                    maxLines = 1
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                    imeOptions = EditorInfo.IME_ACTION_DONE
                }

                constraints {
                    color.connect(
                        VERTICAL of parentId,
                        STARTS of parentId with 12.dp
                    ).size(32.dp, 32.dp)

                    name.connect(
                        VERTICAL of parentId,
                        STARTS of parentId with 72.dp,
                        ENDS of parentId with 56.dp
                    ).width(matchConstraint)
                }

                commonConstraints()

                layoutParams = ViewGroup.MarginLayoutParams(match, 56.dp)
            }
        }
    }
}
