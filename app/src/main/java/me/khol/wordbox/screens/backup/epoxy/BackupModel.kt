package me.khol.wordbox.screens.backup.epoxy

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.cardView
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.model.repository.DatabaseRepository
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.wrapContent
import org.ocpsoft.prettytime.PrettyTime
import org.threeten.bp.Instant
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import java.io.File
import java.util.*

@EpoxyModelClass
open class BackupModel : BaseEpoxyModel<BackupModel.Layout>() {

    @EpoxyAttribute
    lateinit var file: File
    @EpoxyAttribute
    lateinit var properties: Properties
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onClick: (File) -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        val version: String? = properties.getProperty(DatabaseRepository.METADATA_VERSION)
        val wordCount: String? = properties.getProperty(DatabaseRepository.METADATA_WORD_COUNT)
        val timestamp: String? = properties.getProperty(DatabaseRepository.METADATA_TIMESTAMP)

        txtVersion.text = "Version $version"
        txtWordCount.text = "$wordCount words"

        fun unknownTimestamp() {
            txtDifference.text = "Unknown"
            txtDate.text = "Unknown date"
        }

        if (timestamp != null) {
            try {
                val lastModified = timestamp.toLong()
                val modified = Instant.ofEpochMilli(lastModified)
                val offset = ZoneId.systemDefault().rules.getOffset(modified)
                val then = LocalDateTime.ofInstant(modified, offset)
                val today = LocalDateTime.now(offset)

                val formatter = DateTimeFormatter.ofPattern(
                    when {
                        then.year != today.year -> "d MMMM yyyy, HH:mm"
                        else -> "d MMMM, HH:mm"
                    }
                )
                val prettyTime = PrettyTime(Locale.getDefault())
                prettyTime.format(Date(lastModified))
                txtDifference.text = prettyTime.format(Date(lastModified))
                txtDate.text = formatter.format(then)
            } catch (ex: NumberFormatException) {
                unknownTimestamp()
            }
        } else {
            unknownTimestamp()
        }
        view.setOnClickListener {
            onClick(file)
        }
    }

    override fun Layout.unbind() {
        view.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtDifference: TextView
        lateinit var txtDate: TextView
        lateinit var txtVersion: TextView
        lateinit var txtWordCount: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.cardView {
                cardElevation = 1.dpf
                radius = 4.dpf
                isClickable = true
                setContentPadding(16.dp, 16.dp, 16.dp, 16.dp)
                foreground = getDrawableAttr(R.attr.selectableItemBackground)

                constraintLayout {
                    txtDifference = textView {
                        textSize = 17f
                        textColorResource = R.color.black
                    }

                    txtDate = textView {
                        textSize = 12f
                        textColorResource = R.color.black
                    }

                    txtVersion = textView {
                        textSize = 12f
                    }

                    txtWordCount = textView {
                        textSize = 12f
                    }

                    constraints {
                        txtDifference.connect(
                            STARTS of parentId,
                            TOPS of parentId
                        )

                        txtDate.connect(
                            STARTS of parentId,
                            TOP to BOTTOM of txtDifference
                        )

                        txtVersion.connect(
                            STARTS of parentId,
                            TOP to BOTTOM of txtDate with 8.dp
                        )

                        txtWordCount.connect(
                            STARTS of parentId,
                            TOP to BOTTOM of txtVersion
                        )
                    }
                }
            }.apply {
                layoutParams = ViewGroup.MarginLayoutParams(match, wrapContent)
            }
        }
    }
}
