package me.khol.wordbox.screens.main.word

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyRecyclerView
import com.balysv.materialmenu.MaterialMenuDrawable
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.l4digital.fastscroll.FastScroller
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.backdropAppBarLayout
import me.khol.wordbox.extensions.backdropToolbarLayout
import me.khol.wordbox.extensions.beginDelayedTransition
import me.khol.wordbox.extensions.centeredToolbar
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colorDrawable
import me.khol.wordbox.extensions.colorWithAlpha
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.extensions.customLayout
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.extensions.fastScroller
import me.khol.wordbox.extensions.getColorAttr
import me.khol.wordbox.extensions.getDimension
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.extensions.mainContentBackground
import me.khol.wordbox.extensions.mainContentDimForeground
import me.khol.wordbox.extensions.selectableDrawable
import me.khol.wordbox.extensions.startPadding
import me.khol.wordbox.extensions.tintedDrawable
import me.khol.wordbox.model.view.BackdropAppBarLayout
import me.khol.wordbox.screens.base.layout.BaseLayout
import me.khol.wordbox.screens.main.word.headers.HeaderLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.floatingActionButton
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.image
import org.jetbrains.anko.margin
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.support.v4.nestedScrollView
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.withAlpha

/**
 * Layout for [WordListFragment].
 */
class WordListLayout(
    parent: ViewGroup,
    private val recyclerAdapter: RecyclerView.Adapter<*>,
    private val onAddClicked: () -> Unit = {}
) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var words: EpoxyRecyclerView
    lateinit var header: HeaderLayout

    private lateinit var layoutStatusHeader: ConstraintLayout
    private lateinit var txtStatusLanguage: TextView
    private lateinit var txtStatusWordCount: TextView

    lateinit var appBarLayout: BackdropAppBarLayout
    private lateinit var collapsingToolbarLayout: ViewGroup
    private lateinit var frontLayerDim: FrameLayout
    private lateinit var materialMenu: MaterialMenuDrawable

    private lateinit var visibleStatusHeaderSet: ConstraintSet
    private lateinit var hiddenStatusLanguageSet: ConstraintSet

    var statusLanguage: String = ""
        set(value) {
            field = value
            txtStatusLanguage.text = value
        }

    var statusWordCount: Int = 0
        set(value) {
            field = value
            txtStatusWordCount.text = context.resources.getQuantityString(
                R.plurals.word_list_header_word_count, value, value
            )
        }

    var isStatusLanguageVisible: Boolean = true
        set(value) {
            if (field != value) {
                field = value
                if (field) {
                    visibleStatusHeaderSet.applyTo(layoutStatusHeader)
                } else {
                    hiddenStatusLanguageSet.applyTo(layoutStatusHeader)
                }
                layoutStatusHeader.beginDelayedTransition()
            }
        }

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.frameLayout {
            backgroundColor = getColorAttr(R.attr.colorPrimary)
            fitsSystemWindows = true

            // We emulate Backdrop with [AppBarLayout], [CollapsingToolbarLayout] and [Toolbar]
            coordinatorLayout {

                nestedScrollView {
                    isNestedScrollingEnabled = false

                    addOnLayoutChangeListener { v, _, _, _, _, _, _, _, _ ->
                        collapsingToolbarLayout.layoutParams.height = v.height
                        collapsingToolbarLayout.requestLayout()
                    }

                    // Back layer contents
                    header = customLayout(HeaderLayout(this)) {
                        lparams(match, wrap)
                    }
                }.lparams(match, wrap) {
                    bottomMargin = 48.dp
                    gravity = Gravity.TOP
                }

                // Expandable back layer
                appBarLayout = backdropAppBarLayout {
                    onExpanded = { expanded ->
                        frontLayerDim.isVisible = expanded

                        if (expanded) {
                            materialMenu.animateIconState(MaterialMenuDrawable.IconState.X)
                        } else {
                            materialMenu.animateIconState(MaterialMenuDrawable.IconState.BURGER)
                        }
                    }

                    collapsingToolbarLayout = backdropToolbarLayout {

                        addOnLayoutChangeListener { _, _, _, _, _, _, _, _, _ ->
                            appBarLayout.setExpanded(appBarLayout.isExpanded(), false)
                        }

                        toolbar = centeredToolbar {
                            popupTheme = R.style.WordBoxTheme_PopupOverlay
                            // Toggle Backdrop
                            backgroundDrawable = context.selectableDrawable(
                                colorDrawable(R.color.colorPrimary),
                                Color.WHITE.withAlpha(50)
                            )
                            setOnClickListener {
                                appBarLayout.toggleExpanded()
                            }
                            materialMenu = MaterialMenuDrawable(context, Color.WHITE, MaterialMenuDrawable.Stroke.THIN)
                            navigationIcon = materialMenu
                        }.lparams(match, getDimensionAttr(R.attr.actionBarSize)) {
                            collapseMode = CollapsingToolbarLayout.LayoutParams.COLLAPSE_MODE_PIN
                        }

                    }.lparams(match, getDimensionAttr(R.attr.actionBarSize)) {
                        scrollFlags =
                            AppBarLayout.LayoutParams.SCROLL_FLAG_EXIT_UNTIL_COLLAPSED or
                                AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                    }

                }.lparams(match, wrap)

                // Front layer contents
                coordinatorLayout {
                    elevationCompat = 8.dpf
                    backgroundDrawable = mainContentBackground()

                    // header
                    layoutStatusHeader = constraintLayout {
                        id = R.id.word_header
                        backgroundDrawable = mainContentBackground()
                        elevationCompat = 2.dpf
                        isFocusable = true
                        isClickable = true
                        horizontalPadding = 24.dp
                        clipChildren = false

                        txtStatusLanguage = textView {
                            textSize = 14f
                            textColor = colorWithAlpha(R.color.black, 50)
                        }

                        val txtStatusDot = textView(" • ") {
                            textSize = 14f
                            textColor = colorWithAlpha(R.color.black, 50)
                        }

                        txtStatusWordCount = textView {
                            textSize = 14f
                            textColor = colorWithAlpha(R.color.black, 50)
                        }

                        constraints {
                            txtStatusLanguage.connect(
                                VERTICAL of parentId
                            )
                            txtStatusDot.connect(
                                VERTICAL of parentId
                            )
                            txtStatusWordCount.connect(
                                VERTICAL of parentId,
                                ENDS of parentId
                            ).width(matchConstraint)
                        }

                        visibleStatusHeaderSet = prepareConstraints {
                            txtStatusLanguage
                                .clear(END)
                                .connect(STARTS of parentId)
                            txtStatusDot
                                .clear(END)
                                .connect(START to END of txtStatusLanguage)
                            txtStatusWordCount
                                .connect(START to END of txtStatusDot)
                        }

                        hiddenStatusLanguageSet = prepareConstraints {
                            txtStatusLanguage
                                .clear(START)
                                .connect(END to START of txtStatusDot)
                            txtStatusDot
                                .clear(START)
                                .connect(END to START of txtStatusWordCount)
                            txtStatusWordCount
                                .connect(STARTS of parentId)
                        }

                        visibleStatusHeaderSet.applyTo(this)

                    }.lparams(match, 48.dp)

                    // words list
                    words = epoxyRecyclerView {
                        id = R.id.word_list
                        isNestedScrollingEnabled = false
                        layoutManager = LinearLayoutManager(context)
                        itemAnimator = DefaultItemAnimator()
                        adapter = recyclerAdapter
                        layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                        clipToPadding = true
                    }.lparams(match, match) {
                        topMargin = 48.dp
                        behavior = AppBarLayout.ScrollingViewBehavior()
                    }

                    fastScroller {
                        setBubbleColor(color(R.color.colorPrimary))
                        setBubbleTextColor(color(R.color.white))
                        setTrackColor(color(R.color.colorPrimary))
                        setHandleColor(color(R.color.colorPrimary))

                        setFastScrollListener(object : FastScroller.FastScrollListener {
                            override fun onFastScrollStop(fastScroller: FastScroller?) {
                                // do nothing
                            }

                            override fun onFastScrollStart(fastScroller: FastScroller?) {
                                words.stopScroll()
                            }
                        })

                        findViewById<View>(com.l4digital.fastscroll.R.id.fastscroll_handle).apply {
                            startPadding = 16.dp
                        }
                    }.also {
                        // must be attached after fastScroller is attached to parent
                        it.attachRecyclerView(words)
                    }.lparams(wrap, match) {
                        topMargin = 48.dp + 12.dp
                        bottomMargin = 12.dp
                        gravity = Gravity.END
                    }

                    // add FAB
                    floatingActionButton {
                        id = R.id.fab_add
                        backgroundTintList = colors(R.color.colorPrimary)
                        image = tintedDrawable(R.drawable.ic_add, color(R.color.white))
                        compatElevation = 8.dpf
                        setOnClickListener {
                            onAddClicked()
                        }
                    }.lparams(wrap, wrap) {
                        margin = getDimension(R.dimen.fab_margin)
                        gravity = Gravity.BOTTOM or Gravity.END
                    }

                    // dim front layer when back layer is expanded
                    frontLayerDim = frameLayout {
                        elevationCompat = 10.dpf
                        backgroundDrawable = mainContentDimForeground()
                        setOnClickListener {
                            toolbar.performClick()
                        }
                    }.lparams(match, match)

                }.lparams(match, match) {
                    behavior = AppBarLayout.ScrollingViewBehavior()
                }

            }.lparams(match, match)

//            // Performs a collapse animation after view is created to give the user a hint that
//            // there is hidden content behind the front layer
//            appBarLayout.postDelayed({
//                appBarLayout.setExpanded(false, animate = true)
//            }, 300)
            appBarLayout.setExpanded(false, animate = false)

        }.apply {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }
}
