package me.khol.wordbox.screens.main.practice

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.screens.main.practice.epoxy.practiceLanguage
import me.khol.wordbox.screens.main.practice.epoxy.practiceTranslation
import me.khol.wordbox.screens.word.WordActivity

class SwipeStackAdapter(
    val onClickedReveal: () -> Unit
) : BaseAdapter() {

    var data: List<WordDetail> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getCount(): Int = data.size

    override fun getItem(position: Int) = data[position]

    override fun getItemId(position: Int) = position.toLong()

    fun getLayoutForView(convertView: View) = convertView.tag as PracticeItemLayout

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val word = getItem(position)

        return if (convertView == null) {
            PracticeItemLayout(parent, onClickedReveal)
        } else {
            getLayoutForView(convertView)
        }.apply {
            view.tag = this

            txtMainLanguage.text = word.language.name
            txtMainWord.text = word.text
            txtMainDescription.text = word.description
            layoutMainWord.setOnClickListener {
                parent.context.openWordDetail(word.wordId)
            }
            listTranslations.withModels {
                word.translations
                    .groupBy { it.language }
                    .forEach { (language, translations) ->
                        practiceLanguage {
                            id("language", language.languageId)
                            language(language)
                        }
                        translations.forEach { translation ->
                            practiceTranslation {
                                id(translation.wordId)
                                translation(translation)
                                onTranslationClick {
                                    parent.context.openWordDetail(translation.wordId)
                                }
                            }
                        }
                    }
            }
        }.view
    }

    private fun Context.openWordDetail(wordId: Long) {
        startActivity(WordActivity.getIntent(this, wordId))
    }

}
