package me.khol.wordbox.screens.backup.epoxy

import android.content.Context
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.cardView
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.imageView
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.wrapContent

@EpoxyModelClass
open class AddModel : BaseEpoxyModel<AddModel.Layout>() {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onClick: () -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        view.setOnClickListener {
            onClick()
        }
    }

    override fun Layout.unbind() {
        view.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var icon: ImageView
        lateinit var txtTitle: TextView

        override fun createView(ui: AnkoContext<Context>) = ui.cardView {
            cardElevation = 1.dpf
            radius = 4.dpf
            isClickable = true
            setContentPadding(16.dp, 16.dp, 16.dp, 16.dp)
            foreground = getDrawableAttr(R.attr.selectableItemBackground)

            constraintLayout {
                icon = imageView(R.drawable.ic_add)

                txtTitle = textView(R.string.settings_backup_new_backup) {
                    textSize = 17f
                    textColorResource = R.color.black
                }.lparams(matchConstraint, wrapContent)

                constraints {
                    icon.connect(
                        TOPS of parentId,
                        STARTS of parentId
                    )
                    txtTitle.connect(
                        VERTICAL of icon,
                        START to END of icon with 16.dp,
                        ENDS of parentId
                    )
                }
            }
        }.apply {
            layoutParams = ViewGroup.MarginLayoutParams(match, wrapContent)
        }
    }
}
