package me.khol.wordbox.screens.word.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.MenuItemCompat
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.screens.base.BaseFragment
import me.khol.wordbox.screens.word.WordActivity
import me.khol.wordbox.screens.word.edit.WordEditFragment
import org.jetbrains.anko.support.v4.toast

class WordDetailFragment : BaseFragment() {

    companion object {
        private const val ARG_WORD_ID = "word_id"

        fun newFragment(wordId: Long): WordDetailFragment {
            return WordDetailFragment().apply {
                arguments = Bundle().apply {
                    putLong(ARG_WORD_ID, wordId)
                }
            }
        }
    }

    private lateinit var layout: WordDetailLayout
    private lateinit var controller: WordDetailController
    private lateinit var viewModel: WordDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        setHasOptionsMenu(true)

        arguments?.let {
            viewModel.loadWordDetail(it.getLong(ARG_WORD_ID))
        }

        controller = WordDetailController(onTranslationClick = { translation ->
            (activity as WordActivity).replaceDetailWord(translation.wordId)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layout = WordDetailLayout(container!!, controller.adapter)
        return layout.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.setSupportActionBar(layout.toolbar)
        activity.supportActionBar?.let { actionBar ->
            actionBar.setDisplayShowTitleEnabled(false)
            actionBar.setDisplayShowCustomEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        disposables += viewModel.observeWord()
            .observeOnMainThread()
            .subscribe({ word: WordDetail ->
                layout.txtTitle.text = word.text
                controller.word = word
            }, Throwable::printStackTrace)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_word_detail, menu)

        MenuItemCompat.setIconTintList(menu.findItem(R.id.delete), context!!.colors(R.color.black))
        MenuItemCompat.setIconTintList(menu.findItem(R.id.edit), context!!.colors(R.color.black))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.delete -> showDeleteDialog()
            R.id.edit -> {
                fragmentManager?.let {
                    val fragment = WordEditFragment.newFragment(viewModel.loadedWordId)
                    it.beginTransaction()
                        .replace(R.id.word_detail_content, fragment)
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .addToBackStack("detail")
                        .commit()
                }
            }
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun showDeleteDialog() {
        AlertDialog.Builder(context!!)
            .setTitle(R.string.word_edit_delete_dialog_title)
            .setMessage(R.string.word_edit_delete_dialog_message)
            .setPositiveButton(R.string.word_edit_delete_dialog_positive) { _, _ ->
                delete()
            }
            .setNegativeButton(R.string.word_edit_delete_dialog_negative) { _, _ ->
                /* do nothing */
            }
            .show()
    }

    private fun delete() {
        disposables += viewModel.delete()
            .observeOnMainThread()
            .subscribe({
                toast(R.string.word_edit_delete_success)
                getActivity()?.finish()
            }, {
                toast(R.string.word_edit_delete_error)
                it.printStackTrace()
            })
    }
}
