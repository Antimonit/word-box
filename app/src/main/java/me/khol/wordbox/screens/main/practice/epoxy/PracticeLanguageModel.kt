package me.khol.wordbox.screens.main.practice.epoxy

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.textView
import org.jetbrains.anko.topPadding
import org.jetbrains.anko.wrapContent

@EpoxyModelClass
open class PracticeLanguageModel : BaseEpoxyModel<PracticeLanguageModel.Layout>() {

    @EpoxyAttribute
    lateinit var language: Language

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        txtLanguage.text = language.name
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtLanguage: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.textView {
                gravity = Gravity.CENTER
                textSize = 12f
                topPadding = 8.dp
                layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
            }.also {
                txtLanguage = it
            }
        }
    }
}
