package me.khol.wordbox.screens.note

import android.content.Context
import android.text.InputType
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.bottomPadding
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.editText
import org.jetbrains.anko.hintResource
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.singleLine
import org.jetbrains.anko.support.v4.nestedScrollView
import org.jetbrains.anko.textColor
import org.jetbrains.anko.topPadding
import org.jetbrains.anko.verticalPadding

/**
 * Layout for [NoteDetailFragment].
 */
class NoteDetailLayout(
    parent: ViewGroup
) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var txtTitle: EditText
    lateinit var txtContents: EditText

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.coordinatorLayout {
            backgroundColorResource = R.color.window_background

            appBarLayout {
                backgroundColor = color(R.color.white)

                toolbar = themedToolbar(R.style.WordBoxTheme_LightAppBarOverlay) {
                    minimumHeight = getDimensionAttr(R.attr.actionBarSize)
                }.lparams(match, getDimensionAttr(R.attr.actionBarSize)) {
                    scrollFlags = 0 /* don't scroll */
                }

                txtTitle = editText {
                    textSize = 20f
                    textColor = color(R.color.black)
                    backgroundDrawable = null
                    font = R.font.roboto_medium
                    gravity = Gravity.CENTER_VERTICAL
                    topPadding = 0.dp
                    bottomPadding = 0.dp
                    hintResource = R.string.edit_note_title_hint
                    singleLine = true
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                    horizontalPadding = 16.dp
                }.lparams(match, 48.dp) {
                    bottomMargin = 8.dp
                }

            }.lparams(match, wrap)

            nestedScrollView {
                isFillViewport = true

                txtContents = editText {
                    textSize = 16f
                    horizontalPadding = 16.dp
                    verticalPadding = 24.dp
                    backgroundDrawable = null
                    hintResource = R.string.edit_note_note_hint
                    gravity = Gravity.TOP
                    inputType = InputType.TYPE_CLASS_TEXT or
                        InputType.TYPE_TEXT_FLAG_CAP_SENTENCES or
                        InputType.TYPE_TEXT_FLAG_MULTI_LINE
                }.lparams(match, match)

            }.lparams(match, match) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }

        }.apply {
            layoutParams = ViewGroup.LayoutParams(match, match)
        }
    }
}
