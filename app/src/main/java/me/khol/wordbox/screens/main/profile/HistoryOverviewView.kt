package me.khol.wordbox.screens.main.profile

import android.content.Context
import android.transition.ChangeBounds
import android.transition.Fade
import android.transition.TransitionSet
import android.view.Gravity
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.TextView
import cz.ackee.ankoconstraintlayout._ConstraintLayout
import cz.ackee.ankoconstraintlayout._ConstraintSet
import me.khol.wordbox.R
import me.khol.wordbox.extensions.beginDelayedTransition
import me.khol.wordbox.extensions.customView
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.database.model.PracticeDayHistory
import me.khol.wordbox.database.model.PracticeMonthHistory
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.padding
import org.jetbrains.anko.space
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.view
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.Month
import org.threeten.bp.format.TextStyle
import org.threeten.bp.temporal.ChronoUnit
import java.util.*
import kotlin.math.max

/**
 * Overview for practice history. Displays statistical data in chosen year / month / week grouped
 * by months / days / days.
 */
open class HistoryOverviewView(context: Context) : _ConstraintLayout(context) {

    /**
     * At the start of each animation, calculate number of items that are going to be animated.
     *
     * For WEEK->MONTH animation, we will animate 7 DAYS of the week into their locations in the
     * month and then slide in the rest. No two days occupy the same place so there is no need to
     * fade out anything.
     *
     * For MONTH->YEAR animation, we will animate up to 31 DAYS of the month into the location of
     * the same month in the year, fade out all but one of them, and then slide in the rest of the
     * months.
     *
     * For WEEK->YEAR animation, we will animate 7 DAYS of the month into possibly the locations of
     * possibly two months in the year, fade out all but one of them for each month, and then slide
     * in the rest of the months.
     *
     * For the opposite direction, we do exactly the opposite.
     *
     * For YEAR->WEEK animation, first we have slide out months that are not in our interest. Fade
     * in extra days to each month of interest, and finally animate 7 days of the week.
     */

    /*
    Note: Working with constraint layout's Placeholders was really unintuitive. Setting
    placeholder's visibility had no effect whatsoever, setting visibility of a view worked
    only if it was not in a placeholder, etc. Furthermore placeholders limited size constraints.
    Performance also seemed to improve by removing placeholders.
    Instead we just create simple view and later instead of `placeholder.setContent(View)` we
    just call `view.connect(ALL of placeholder)`.
     */

    /**
     * Placeholders for monthly overview. There are 6*7 placeholders.
     */
    private val placeholderMonthDays: Map<Int, TextView>

    /**
     * Row placeholders for monthly overview. There are 6 rows with 7 placeholders in each row.
     */
    private val placeholderMonthRows: Map<Int, Collection<View>>

    /**
     * Column placeholders for monthly overview. There are 7 columns with 6 placeholders in each column.
     */
    private val placeholderMonthColumns: Map<Int, Collection<View>>

    /**
     * Placeholders for weekly overview. There are 7 placeholders.
     */
    private val placeholderWeekDays: Map<Int, View>

    /**
     * Placeholders for yearly overview. There are 12 placeholders.
     */
    private val placeholderYearMonths: Map<Int, View>

    /**
     * Label for each day of a month. There are 6*7 labels
     */
    private val monthDays: List<DayView>

    /**
     * Label for each day of a week. There are 7 labels
     */
    private val weekDays: List<DayView>

    /**
     * Label for each day of a week. There are 12 labels
     */
    private val yearMonths: List<DayView>

    /**
     * Labels for each day of a week. There are 7 labels (Mon, Tue, Wed, Thu, Fri, Sat, Sun)
     */
    private val dayLabels: List<TextView>

    /**
     * Labels for each month of a year. There are 12 labels (Jan, Feb, Mar, Apr, May, June, ...)
     */
    private val monthLabels: List<TextView>

    private val baseline: View

    private val weeklyOverviewConstraints: _ConstraintSet
    private val monthlyOverviewConstraints: _ConstraintSet
    private val yearlyOverviewConstraints: _ConstraintSet

    init {
        clipToPadding = false
        padding = dp(16)

        // Create label for each day of a week
        dayLabels = (0 until 7)
            .map { DayOfWeek.of(it + 1) }
            .map { it.getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault()) }
            .map {
                textView(it) {
                    gravity = Gravity.CENTER
                    textColorResource = R.color.black
                }.lparams(dp(40), dp(24))
            }

        // Create label for each month of a year
        monthLabels = (0 until 12)
            .map { Month.of(it + 1) }
            .map { it.getDisplayName(TextStyle.SHORT_STANDALONE, Locale.getDefault()) }
            .map {
                textView(it) {
                    gravity = Gravity.CENTER
                    textColorResource = R.color.black
                }.lparams(wrapContent, dp(24))
            }

        // Create placeholder for each day in a month.
        placeholderMonthDays = (0 until 6 * 7)
            .map {
                it to textView {
                    textSize = 12f
                    gravity = Gravity.CENTER
                }.lparams(dp(48), dp(48))
            }.toMap()

        placeholderMonthRows = placeholderMonthDays.entries.groupBy({ it.key / 7 }, { it.value })
        placeholderMonthColumns = placeholderMonthDays.entries.groupBy({ it.key % 7 }, { it.value })

        // Create placeholder for each day in a week.
        placeholderWeekDays = (0 until 7)
            .map { it to space().lparams(dp(16), dp(96)) }
            .toMap()

        // Create placeholder for each month in a year.
        placeholderYearMonths = (0 until 12)
            .map { it to space().lparams(dp(16), dp(96)) }
            .toMap()

        // Create number for each day in a month.
        monthDays = (0 until 6 * 7).map { customView<DayView>() }

        weekDays = (0 until 7).map { customView<DayView>() }

        yearMonths = (0 until 12).map { customView<DayView>() }

        baseline = view {
            backgroundColorResource = R.color.black
        }

        constraints {
            baseline.connect(
                VERTICAL of parentId,
                HORIZONTAL of parentId
            ).size(matchConstraint, dp(2))

            // Connect MONTH placeholders (with a label) vertically
            placeholderMonthColumns
                .entries
                .map { (index, column) -> column.plus(dayLabels[index]) }
                .map { it.toTypedArray() }
                .forEach { it.chainSpread(TOP of parentId, BOTTOM of parentId) }

            // Connect MONTH placeholders horizontally
            placeholderMonthRows
                .values
                .map { it.toTypedArray() }
                .forEach { it.chainSpread(START of parentId, END of parentId) }

            // Connect WEEK placeholders
            placeholderWeekDays
                .values
                .toTypedArray()
                .chainSpread(START of parentId, END of parentId)
                .forEach { it.connect(BOTTOMS of baseline) }

            // Connect YEAR placeholders
            placeholderYearMonths
                .values
                .toTypedArray()
                .chainSpread(START of parentId, END of parentId)
                .forEach { it.connect(BOTTOMS of baseline) }

            placeholderWeekDays.values.forEach { it.visibility(View.GONE) }
            placeholderMonthDays.values.forEach { it.visibility(View.GONE) }
            placeholderYearMonths.values.forEach { it.visibility(View.GONE) }
            monthLabels.forEach { it.visibility(View.GONE) }
            dayLabels.forEach { it.visibility(View.GONE) }
            baseline.visibility(View.GONE)
            monthDays.forEach { it.visibility(View.GONE) }
            weekDays.forEach { it.visibility(View.GONE) }
            yearMonths.forEach { it.visibility(View.GONE) }
        }

        weeklyOverviewConstraints = prepareConstraints {
            weekDays.forEach { it.visibility(View.VISIBLE) }
            placeholderWeekDays.values.forEach { it.visibility(View.VISIBLE) }
            dayLabels.forEach { it.visibility(View.VISIBLE) }
            baseline.visibility(View.VISIBLE)

            // Connect all labels below each dayLabel
            dayLabels.forEachIndexed { index, dayLabel ->
                val weekDay = placeholderWeekDays[index]!!
                dayLabel.connect(
                    HORIZONTAL of weekDay,
                    TOP to BOTTOM of baseline with dp(16)
                ).clear(BOTTOM)
            }
        }

        monthlyOverviewConstraints = prepareConstraints {
            monthDays.forEach { it.visibility(View.VISIBLE) }
            placeholderMonthDays.values.forEach { it.visibility(View.VISIBLE) }
            dayLabels.forEach { it.visibility(View.VISIBLE) }

            // Connect all labels horizontally
            dayLabels
                .toTypedArray()
                .chainSpread(START of parentId, END of parentId)
        }

        yearlyOverviewConstraints = prepareConstraints {
            yearMonths.forEach { it.visibility(View.VISIBLE) }
            placeholderYearMonths.values.forEach { it.visibility(View.VISIBLE) }
            monthLabels.forEach { it.visibility(View.VISIBLE) }
            baseline.visibility(View.VISIBLE)

            // Connect all labels below each dayLabel
            monthLabels.forEachIndexed { index, monthLabel ->
                val month = placeholderYearMonths[index]!!
                monthLabel.connect(
                    HORIZONTAL of month,
                    TOP to BOTTOM of baseline with dp(16)
                ).clear(BOTTOM)
            }
        }
    }

    private fun beginDelayedTransition() {
        beginDelayedTransition {
            TransitionSet().apply {
                interpolator = LinearInterpolator()
                duration = 500
                ordering = TransitionSet.ORDERING_TOGETHER
                addTransition(Fade(Fade.OUT))
                addTransition(ChangeBounds())
                addTransition(Fade(Fade.IN))
            }
        }
    }

    fun setWeekly(date: LocalDate, events: List<PracticeDayHistory>) {
        weekDays.forEach { it.type = ChronoUnit.WEEKS }
        beginDelayedTransition()
        weeklyOverviewConstraints.applyTo(this)

        val maxValue = events.maxBy { it.count }?.count ?: 1

        constraints {
            placeholderWeekDays.forEach { (index, placeholder) ->
                val labelDayIndex = date.with(DayOfWeek.of(index + 1)).dayOfMonth
                val value = events.find { it.day == labelDayIndex }?.count ?: 0
                val size = max(1, dp(72) * value / maxValue)

                placeholder.height(size)

                weekDays.getOrNull(index)?.let {
                    it.connect(
                        HORIZONTAL of placeholder,
                        BOTTOMS of placeholder
                    ).size(matchConstraint, size)
                }
            }
        }
    }

    fun setMonthly(date: LocalDate, events: List<PracticeDayHistory>) {
        monthDays.forEach { it.type = ChronoUnit.MONTHS }
        beginDelayedTransition()
        monthlyOverviewConstraints.applyTo(this)

        val maxValue = events.maxBy { it.count }?.count ?: 1

        val firstDay = date.withDayOfMonth(1)
        val lastDay = firstDay.plusMonths(1).minusDays(1)
        val firstDayIndex = firstDay.dayOfWeek.value - 1
        val dayCount = lastDay.dayOfMonth
        val lastDayIndex = firstDayIndex + dayCount

        constraints {
            placeholderMonthDays.forEach { (index, placeholder) ->
                val labelDayIndex = index - firstDayIndex + 1
                val active = index in firstDayIndex until lastDayIndex
                val value = events.find { it.day == labelDayIndex }?.count ?: 0
                val size = max(1, dp(72) * value / maxValue)

                placeholder.text = if (active) "$labelDayIndex" else ""

                monthDays.getOrNull(index)?.let {
                    it.connect(ALL of placeholder)
                    it.size(size, size)
                    // Don't toggle view's visibility. It doesn't update view's position when
                    // fading out.
                    it.animate().alpha(if (active) 1f else 0f).setDuration(500).start()
                }
            }
        }

        // Hide placeholders that are not used
        for (week in 4 until 6) {
            placeholderMonthRows[week]!!.forEach {
                it.isVisible = lastDayIndex > week * 7
            }
        }
    }

    fun setYearly(date: LocalDate, events: List<PracticeMonthHistory>) {
        yearMonths.forEach { it.type = ChronoUnit.YEARS }
        beginDelayedTransition()
        yearlyOverviewConstraints.applyTo(this)

        val maxValue = events.maxBy { it.count }?.count ?: 1

        constraints {
            placeholderYearMonths.forEach { (index, placeholder) ->
                val labelMonthIndex = index + 1
                val value = events.find { it.month == labelMonthIndex }?.count ?: 0
                val size = max(1, dp(72) * value / maxValue)

                yearMonths.getOrNull(index)?.let {
                    it.connect(
                        HORIZONTAL of placeholder,
                        BOTTOMS of placeholder
                    ).size(matchConstraint, size)
                }
            }
        }
    }
}
