package me.khol.wordbox.screens.word.edit.epoxy

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.backgroundTintListCompat
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.button
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.verticalMargin

@EpoxyModelClass
open class SaveModel : BaseEpoxyModel<SaveModel.Layout>() {

    @EpoxyAttribute
    var changed: Boolean = false
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onSaveClick: () -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        btnSave.isEnabled = changed
        btnSave.setOnClickListener {
            onSaveClick()
        }
    }

    override fun Layout.unbind() {
        btnSave.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var btnSave: Button

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.frameLayout {
                btnSave = button("Save changes") {
                    backgroundTintListCompat = colors(android.R.color.holo_green_dark)
                    textColor = color(R.color.white)
                    horizontalPadding = 24.dp
                }.lparams(wrap, 56.dp) {
                    gravity = Gravity.CENTER_HORIZONTAL
                    verticalMargin = 24.dp
                }
            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
