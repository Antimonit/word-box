package me.khol.wordbox.screens.word.detail.epoxy

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colorWithAlpha
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.extensions.roundDrawable
import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalPadding
import org.jetbrains.anko.view

/**
 * Epoxy model of a single [translation][TranslationFull].
 */
@EpoxyModelClass
open class TranslationModel : BaseEpoxyModel<TranslationModel.Layout>() {

	@EpoxyAttribute
	lateinit var translation: TranslationFull
	@EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
	lateinit var onItemClick: (TranslationFull) -> Unit

	override fun createViewLayout(parent: ViewGroup) = Layout(parent)

	override fun Layout.bind() {
		txtLanguages.text = translation.language.shortName
		txtTranslation.text = translation.text
		txtDescription.text = translation.description
		txtDescription.isVisible = !translation.description.isBlank()
		view.setOnClickListener {
			onItemClick(translation)
		}
	}

	override fun Layout.unbind() {
		view.setOnClickListener(null)
	}

	class Layout(parent: ViewGroup) : BaseLayout(parent) {

		lateinit var txtLanguages: TextView
		lateinit var txtTranslation: TextView
		lateinit var txtDescription: TextView

		override fun createView(ui: AnkoContext<Context>): View {
			return ui.frameLayout {
				foreground = getDrawableAttr(android.R.attr.selectableItemBackground)
				backgroundColorResource = R.color.white
				elevationCompat = 1.dpf

				constraintLayout {
					verticalPadding = 12.dp
					horizontalPadding = 16.dp
					clipToPadding = false

					txtLanguages = textView {
						textSize = 14f
						padding = 0.dp
						textColor = color(R.color.white)
						backgroundDrawable = roundDrawable(
							backgroundColor = color(R.color.colorPrimary),
							stroke = 0.dp,
							corner = 2.dpf
						)
						gravity = Gravity.CENTER
					}

					txtTranslation = textView {
						textSize = 16f
						textColor = color(R.color.black)
						font = R.font.roboto_medium
					}

					txtDescription = textView {
						textSize = 14f
						textColor = colorWithAlpha(R.color.black, 50)
					}

					constraints {
						txtLanguages.connect(
							STARTS of parentId,
							BASELINES of txtTranslation
						).size(32.dp, 32.dp)

						txtTranslation.connect(
							STARTS of parentId with 56.dp,
							ENDS of parentId,
							TOPS of parentId
						).size(matchConstraint, wrapContent)

						txtDescription.connect(
							STARTS of txtTranslation,
							ENDS of parentId,
							TOP to BOTTOM of txtTranslation with 8.dp,
							BOTTOMS of parentId
						).size(matchConstraint, wrapContent)
					}
				}.lparams(match, wrap)

				view {
					backgroundColor = colorWithAlpha(R.color.black, 20)
				}.lparams(match, 1.dp) {
					gravity = Gravity.BOTTOM
				}

			}.apply {
				layoutParams = ViewGroup.LayoutParams(match, wrap)
			}
		}
	}
}
