package me.khol.wordbox.screens.main.word

import android.app.SearchManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.MenuItemCompat
import io.reactivex.rxkotlin.plusAssign
import jp.co.recruit_lifestyle.android.floatingview.FloatingViewManager
import me.khol.wordbox.R
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.screens.add.AddActivity
import me.khol.wordbox.screens.base.BaseFragment
import me.khol.wordbox.screens.main.MainActivity
import me.khol.wordbox.screens.manage.labels.ManageLabelsActivity
import me.khol.wordbox.screens.manage.languages.ManageLanguagesActivity
import me.khol.wordbox.screens.word.WordActivity
import me.khol.wordbox.service.FloatingButton
import org.jetbrains.anko.horizontalMargin
import org.jetbrains.anko.horizontalPadding

class WordListFragment : BaseFragment() {

    private lateinit var layout: WordListLayout
    private lateinit var viewModel: WordListViewModel
    private lateinit var controller: WordListController

    private lateinit var searchView: SearchView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        setHasOptionsMenu(true)

        val handlerThread = HandlerThread("EpoxyWordList").also { it.start() }
        val handler = Handler(handlerThread.looper)

        controller = WordListController(
            handler = handler,
            onWordClick = {
                startActivity(WordActivity.getIntent(layout.context, it.wordId))
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layout = WordListLayout(container!!,
            controller.adapter,
            onAddClicked = {
                startActivity(Intent(layout.context, AddActivity::class.java).apply {
                    val safeArea = FloatingViewManager.findCutoutSafeArea(activity)
                    putExtra(FloatingButton.EXTRA_CUTOUT_SAFE_AREA, safeArea)
                })
            }
        )
        return layout.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.setSupportActionBar(layout.toolbar)
        activity.supportActionBar?.apply {
            setDisplayShowCustomEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            setTitle(R.string.nav_title_words)
        }

        layout.header.headerLanguages.onLanguageChange = viewModel::languageChanged
        layout.header.headerSort.onSortChange = viewModel::sortChanged
        layout.header.headerLabels.onLabelsChange = viewModel::labelsChanged
        layout.header.headerLanguages.onManage = {
            startActivity(Intent(activity, ManageLanguagesActivity::class.java))
        }
        layout.header.headerLabels.onManage = {
            startActivity(Intent(activity, ManageLabelsActivity::class.java))
        }

        var animateWordsList = true

        disposables += viewModel.observeAllWords()
            .observeOnMainThread()
            .subscribe({ words ->
                layout.statusWordCount = words.size
                controller.words = words
                if (animateWordsList) {
                    animateWordsList = false
                    layout.words.scheduleLayoutAnimation()
                }
            }, Throwable::printStackTrace)

        disposables += viewModel.observeLanguages()
            .observeOnMainThread()
            .subscribe({ (allLanguages, selectedLanguage) ->
                layout.statusLanguage = selectedLanguage.name
                layout.header.headerLanguages.updateLanguages(allLanguages, selectedLanguage)
                animateWordsList = true
            }, Throwable::printStackTrace)

        disposables += viewModel.observeSort()
            .observeOnMainThread()
            .subscribe({ selectedSort ->
                layout.header.headerSort.updateSort(selectedSort)
            }, Throwable::printStackTrace)

        disposables += viewModel.observeLabels()
            .observeOnMainThread()
            .subscribe({ (allLabels, selectedLabels) ->
                layout.header.headerLabels.updateLabels(allLabels, selectedLabels)
            }, Throwable::printStackTrace)

        disposables += viewModel.observeSearch()
            .observeOnMainThread()
            .subscribe({ query ->
                layout.isStatusLanguageVisible = query.isEmpty()
            }, Throwable::printStackTrace)
    }

    override fun onCreateOptionsMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchItem = menu.findItem(R.id.search)
        MenuItemCompat.setIconTintList(searchItem, context!!.colors(R.color.white))

        searchView = searchItem.actionView as SearchView
        searchView.apply {
            maxWidth = Integer.MAX_VALUE

            // remove left margin when SearchView is expanded so that texts align correctly
            // remove right margin when SearchView is expanded so that icons align correctly
            findViewById<LinearLayout>(R.id.search_edit_frame).apply {
                (layoutParams as ViewGroup.MarginLayoutParams).horizontalMargin = 0
            }

            // remove padding from editText so that texts align correctly
            findViewById<TextView>(R.id.search_src_text).apply {
                horizontalPadding = 0
            }

            // add padding to close button so that icons align correctly
            findViewById<ImageView>(R.id.search_close_btn).apply {
                horizontalPadding = dp(16)
            }

            val componentName = ComponentName(context, MainActivity::class.java)
            val searchManager = context!!.getSystemService(AppCompatActivity.SEARCH_SERVICE) as SearchManager
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextChange(newText: String): Boolean {
                    viewModel.searchChanged(newText)
                    return true
                }

                override fun onQueryTextSubmit(query: String): Boolean {
                    if (!searchView.isIconified) {
                        searchView.isIconified = true
                    }
                    searchItem.collapseActionView()
                    return false
                }
            })
            setQuery("", false)

            requestLayout()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                layout.appBarLayout.toggleExpanded()
                true
            }
            R.id.search -> {
                layout.appBarLayout.setExpanded(false)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    fun setSearchQuery(query: String) {
        searchView.setQuery(query, false)
    }
}
