package me.khol.wordbox.screens.add

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.circularConceal
import me.khol.wordbox.extensions.circularReveal
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.extensions.targetApi
import me.khol.wordbox.database.model.Example
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.screens.base.BaseActivity
import me.khol.wordbox.service.FloatingButton
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions

/**
 * Primary place where user can add new [words][me.khol.wordbox.model.db.plain.WordDetail] to
 * their dictionary.
 * Activity's layout is divided into header, four expandable sections and buttons panel.
 *
 * - In the **header** user can define
 * [language][me.khol.wordbox.model.db.plain.WordDetail.language] and
 * [text][me.khol.wordbox.model.db.plain.WordDetail.text] of a new word.
 *
 * - Four **sections** provide ways to define
 * [description][me.khol.wordbox.model.db.plain.WordDetail.description],
 * [translations][me.khol.wordbox.model.db.plain.WordDetail.translations],
 * [labels][me.khol.wordbox.model.db.plain.WordDetail.labels] and
 * [examples][me.khol.wordbox.model.db.plain.WordDetail.examples].
 * Each section is implemented as a fragment that all access the same viewModel provided by
 * this activity.
 *
 * - **Buttons panel** allow to:
 *     - [add][me.khol.wordbox.screens.add.AddViewModel.add] provided information as a new word
 *     and close the activity
 *     - [add more][me.khol.wordbox.screens.add.AddViewModel.addMore] provided information as
 *     a new word and start adding a new word
 *     - Dismiss user-defined information and close the activity
 *
 * User can define multiple words that are translations of each other using [add more] button.
 * Previous words will be automatically added to the list of translations of the new word.
 */
class AddActivity : BaseActivity() {

    companion object {
        private const val REQUEST_PERMISSION_OVERLAY = 1
        private const val REQUEST_MANAGE_OVERLAY = 2
    }

    private lateinit var layout: AddLayout
    private lateinit var viewModel: AddViewModel

    private var cancelReveal: () -> Unit = {}
    private var concealing: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        layout = AddLayout(this, viewModel.callbacks)
        setContentLayout(layout)

        // description
        disposables += viewModel.observeWord()
            .map { it.description }
            .observeOnMainThread()
            .subscribe({ description: String ->
                layout.setDescription(description)
            }, Throwable::printStackTrace)

        // labels
        disposables += Flowable.combineLatest(
            viewModel.observeLabels(),
            viewModel.observeWord(),
            BiFunction { labels: List<Label>, word: WordDetail ->
                Pair(labels, word.labels)
            })
            .observeOnMainThread()
            .subscribe({ (labels, checkedLabels) ->
                layout.setLabels(labels, checkedLabels)
            }, Throwable::printStackTrace)

        // translations
        disposables += Flowable.combineLatest(
            viewModel.observeLanguages(),
            viewModel.observeWord(),
            BiFunction { languages: List<Language>, word: WordDetail ->
                Pair(languages, word.translations)
            })
            .distinctUntilChanged()
            .observeOnMainThread()
            .subscribe({ (allLanguages, translations) ->
                layout.setTranslations(allLanguages, translations)
            }, Throwable::printStackTrace)

        // examples
        disposables += viewModel.observeWord()
            .map { it.examples }
            .observeOnMainThread()
            .subscribe({ examples: List<Example> ->
                layout.setExamples(examples)
            }, Throwable::printStackTrace)


        disposables += viewModel.observeLanguages()
            .observeOnMainThread()
            .subscribe({ languages ->
                layout.languagesButton.languages = languages
                layout.languagesButton.selectedLanguage = languages[0]
            }, Throwable::printStackTrace)

        layout.mainWord.requestFocus()
        if (targetApi(Build.VERSION_CODES.M)) {
            layout.mainWord.setText(intent.getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT))
        }

        // finish on click outside of the window
        window.decorView.findViewById<View>(android.R.id.content).setOnClickListener {
            finish()
        }

        layout.btnAddMore.setOnClickListener {
            viewModel.addMore()
                .observeOnMainThread()
                .subscribe({
                    layout.mainWord.text = null
                }, Throwable::printStackTrace)
        }

        layout.btnAdd.setOnClickListener {
            viewModel.add()
                .observeOnMainThread()
                .subscribe({
                    finish()
                }, Throwable::printStackTrace)
        }

        layout.btnCancel.setOnClickListener {
            finish()
        }

        layout.languagesButton.onLanguageChanged = viewModel.callbacks::onLanguageSelected
        disposables += layout.mainWord.textChanges().skipInitialValue().subscribe {
            viewModel.callbacks.onTextChanged(it)
        }

        setSupportActionBar(layout.toolbar)

        FloatingButton.conceal(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cancelReveal = layout.view.circularReveal(delay = 200, duration = 400)
        }
    }

    override fun finish() {
        if (concealing)
            return

        cancelReveal()
        concealing = true

        FloatingButton.reveal(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            layout.view.circularConceal(duration = 250, onEnd = {
                super.finish()
            })
        } else {
            super.finish()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_add_translation, menu)

        if (!intent.hasExtra(FloatingButton.EXTRA_CUTOUT_SAFE_AREA)) {
            menu.findItem(R.id.action_floating_button).isVisible = false
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.action_floating_button -> {
                showFloatingOverlay()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    @SuppressLint("MissingPermission")
    @AfterPermissionGranted(REQUEST_PERMISSION_OVERLAY)
    private fun showFloatingOverlay() {
        // TODO: check logic pre Android 23
        checkCanDrawOverlays()
    }

    /*
     * Before Android 23 there was no restriction if an app may draw over other apps and we didn't
     * have to ask user to allow overlay in settings.
     */
    private fun checkCanDrawOverlays() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:$packageName"))
            startActivityForResult(intent, REQUEST_MANAGE_OVERLAY)
        } else {
            checkHasPermission()
        }
    }

    /**
     * If the app is installed from Play store, SYSTEM_ALERT_WINDOW permission is implicitly granted
     * even without asking for the permission. Actually, asking for permissions will always fail.
     *
     * In such case we don't have to [requestPermissions].
     */
    private fun checkHasPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M &&
            !EasyPermissions.hasPermissions(this, Manifest.permission.SYSTEM_ALERT_WINDOW)) {
            EasyPermissions.requestPermissions(
                this,
                getString(R.string.floating_button_permission),
                REQUEST_PERMISSION_OVERLAY,
                Manifest.permission.SYSTEM_ALERT_WINDOW
            )
        } else {
            startChatHeadService()
        }
    }

    private fun startChatHeadService() {
        val safeArea = intent.getParcelableExtra(FloatingButton.EXTRA_CUTOUT_SAFE_AREA) as Rect
        FloatingButton.popupFloatingButton(this, safeArea)
        finish()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_MANAGE_OVERLAY) {
            checkHasPermission()
        }
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

}
