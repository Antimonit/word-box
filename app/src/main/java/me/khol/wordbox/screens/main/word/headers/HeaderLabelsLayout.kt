package me.khol.wordbox.screens.main.word.headers

import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import com.airbnb.epoxy.EpoxyRecyclerView
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration
import me.khol.wordbox.R
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.headerLayoutDrawable
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.word.edit.epoxy.labels.labelButton
import org.jetbrains.anko.allCaps
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView

/**
 * A section displaying toggleable list of [labels][Label].
 */
class HeaderLabelsLayout(parent: ViewGroup) : BaseHeaderLayout(parent) {

    override val headerRes = R.string.word_list_header_labels
    override val manageRes = R.string.word_list_header_labels_manage

    private var allLabels: List<Label> = emptyList()
    private var selectedLabels: List<Label> = emptyList()
    /**
     * Invoked every time user toggles one of the [allLabels] at which point [selectedLabels] are
     * updated and passed to [onLabelsChange].
     */
    var onLabelsChange: (List<Label>) -> Unit = {}

    private lateinit var labelsList: EpoxyRecyclerView
    private lateinit var emptyView: View

    fun updateLabels(allLabels: List<Label>, selectedLabels: List<Label>) {
        txtManage?.isVisible = !allLabels.isEmpty()
        emptyView.isVisible = allLabels.isEmpty()

        if (this.allLabels != allLabels) {
            this.allLabels = allLabels
            this.selectedLabels = selectedLabels
            recreate()
        }
    }

    private fun recreate() {
        labelsList.withModels {
            allLabels.forEach { label ->
                labelButton {
                    id(label.labelId)
                    label(label)
                    checked(label in selectedLabels)
                    dark(true)
                    onChecked { label, checked ->
                        selectedLabels = selectedLabels.toMutableSet().apply {
                            if (checked) {
                                add(label)
                            } else {
                                remove(label)
                            }
                        }.sortedBy { it.order }
                        onLabelsChange(selectedLabels)
                    }
                }
            }
        }
    }

    override fun ViewManager.createContentView(): View {
        return frameLayout {
            backgroundDrawable = headerLayoutDrawable()
            elevationCompat = 0.dpf

            labelsList = epoxyRecyclerView {
                padding = 12.dp    // remove 4.dp because of the spacing item decoration

                layoutManager = ChipsLayoutManager.newBuilder(context)
                    .setRowStrategy(ChipsLayoutManager.STRATEGY_FILL_VIEW)
                    .setChildGravity(Gravity.START)
                    .setScrollingEnabled(false)
                    .build()
                addItemDecoration(SpacingItemDecoration(8.dp, 8.dp))
            }

            emptyView = linearLayout {
                padding = 16.dp
                gravity = Gravity.CENTER_VERTICAL
                isClickable = true
                setOnClickListener { onManage() }

                textView(R.string.word_list_header_labels_empty) {
                    font = R.font.roboto_medium
                    textColor = Color.WHITE
                    textSize = 14f
                }.lparams(0, wrap, weight = 1f)

                textView(R.string.word_list_header_labels_create) {
                    padding = 8.dp
                    font = R.font.roboto_medium
                    textColor = Color.WHITE
                    backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackgroundBorderless)
                    allCaps = true
                    isDuplicateParentStateEnabled = true
                }.lparams(wrap, wrap)
            }.lparams(match, match)
        }
    }
}
