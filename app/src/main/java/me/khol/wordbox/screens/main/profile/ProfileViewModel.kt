package me.khol.wordbox.screens.main.profile

import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.model.PracticeDayHistory
import me.khol.wordbox.database.model.PracticeMonthHistory
import me.khol.wordbox.model.repository.PracticeHistoryRepository
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.ChronoUnit
import org.threeten.bp.temporal.TemporalUnit
import javax.inject.Inject

/**
 * ViewModel for Profile fragment
 */
class ProfileViewModel @Inject constructor(
    private val repo: PracticeHistoryRepository
) : BaseViewModel() {

    private var date = LocalDate.now()
    private var unit: TemporalUnit = ChronoUnit.MONTHS

    private val unitSubject = BehaviorProcessor.createDefault<TemporalUnit>(ChronoUnit.MONTHS)
    private val dateSubject = BehaviorProcessor.createDefault<LocalDate>(date)
    private val historySubject = BehaviorProcessor.create<History>()

    init {
        disposables += Flowable.combineLatest(
            unitSubject,
            dateSubject,
            BiFunction { t1: TemporalUnit, t2: LocalDate ->
                t1 to t2
            })
            .subscribeOnIO()
            .switchMap { (unit, date) ->
                when (unit) {
                    ChronoUnit.WEEKS -> repo.weeklyEvents(date).map { History.Weekly(date, it) }
                    ChronoUnit.MONTHS -> repo.monthlyEvents(date).map { History.Monthly(date, it) }
                    ChronoUnit.YEARS -> repo.yearlyEvents(date).map { History.Yearly(date, it) }
                    else -> throw Throwable("Unsupported unit type")
                }
            }
            .subscribe(historySubject::onNext, Throwable::printStackTrace)
    }

    fun observeHistory(): Flowable<History> = historySubject

    fun setUnit(newUnit: TemporalUnit) {
        unit = newUnit
        unitSubject.onNext(unit)
    }

    fun previous() {
        date = date.minus(1, unit)
        dateSubject.onNext(date)
    }

    fun next() {
        date = date.plus(1, unit)
        dateSubject.onNext(date)
    }
}

sealed class History(val date: LocalDate) {
    class Weekly(date: LocalDate, val events: List<PracticeDayHistory>) : History(date)
    class Monthly(date: LocalDate, val events: List<PracticeDayHistory>) : History(date)
    class Yearly(date: LocalDate, val events: List<PracticeMonthHistory>) : History(date)
}
