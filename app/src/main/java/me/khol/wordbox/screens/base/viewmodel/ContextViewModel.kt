package me.khol.wordbox.screens.base.viewmodel

import androidx.lifecycle.AndroidViewModel
import io.reactivex.disposables.CompositeDisposable
import me.khol.wordbox.App

/**
 * A ViewModel with context that automatically disposes of its [disposables]
 */
abstract class ContextViewModel(app: App) : AndroidViewModel(app) {

    /**
     * Automatically release all disposables when this view model is no longer needed
     */
    val disposables = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    /**
     * Short syntax for getApplication()
     */
    val context: App
        get() = getApplication()
}
