package me.khol.wordbox.screens.backup

import android.content.ContentResolver
import android.content.Intent
import android.net.Uri
import androidx.documentfile.provider.DocumentFile
import io.reactivex.Observable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.BehaviorSubject
import me.khol.reactive.subjects.EventSubject
import me.khol.wordbox.App
import me.khol.wordbox.R
import me.khol.wordbox.model.repository.DatabaseRepository
import me.khol.wordbox.screens.base.viewmodel.ContextViewModel
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Properties
import javax.inject.Inject

/**
 * ViewModel for [BackupActivity].
 */
class BackupViewModel @Inject constructor(
    app: App,
    private val dbRepository: DatabaseRepository
) : ContextViewModel(app) {

    companion object {
        private const val EXTENSION = ".bak"
    }

    private val resolver: ContentResolver = context.contentResolver
    private var backupFolder: DocumentFile? = null

    private val messageSubject = EventSubject.create<String>()
    private val stateSubject = BehaviorSubject.createDefault<BackupState>(BackupState.Initial)

    fun observeMessages(): Observable<String> = messageSubject
    fun observeState(): BehaviorSubject<BackupState> = stateSubject

    init {
        val uris = resolver.persistedUriPermissions
        if (uris.isNotEmpty()) {
            onFolderSelected(uris.first().uri)
        } else {
            stateSubject.onNext(BackupState.NoFolderSelected)
        }
    }

    private fun loadFolder(folder: DocumentFile) {
        val files = folder.listFiles().filter {
            it.canRead() && it.name!!.toLowerCase().endsWith(EXTENSION)
        }

        stateSubject.onNext(BackupState.Loaded(files
            .sortedByDescending(DocumentFile::lastModified)
            .map {
                val localFile = File(context.filesDir, it.name!!)
                resolver.openInputStream(it.uri)!!.use { input ->
                    FileOutputStream(localFile).use { output ->
                        input.copyTo(output)
                    }
                }
                localFile to dbRepository.metadata(localFile)
            }
            .filter { (_, properties) ->
                properties != null
            }
            .map { (file, properties) ->
                FileBackup(file, properties!!)
            })
        )
    }

    fun onFolderSelected(uri: Uri) {
        val flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION

        resolver.persistedUriPermissions.forEach {
            if (it.uri != uri) {
                resolver.releasePersistableUriPermission(it.uri, flags)
            }
        }
        resolver.takePersistableUriPermission(uri, flags)

        backupFolder = DocumentFile.fromTreeUri(context, uri)?.also {
            loadFolder(it)
        }
    }

    fun onExportFile() {
        val folder = backupFolder ?: return
        val dateString = SimpleDateFormat("yyyy-MM-dd HH.mm.ss").format(Date())
        val fullName = "wordbox_$dateString$EXTENSION"
        val externalFile = folder.createFile("application/octet-stream", fullName)!!
        val localFile = File(context.filesDir, fullName)
        disposables += dbRepository.export(localFile)
            .doOnComplete {
                resolver.openOutputStream(externalFile.uri)!!.use { output ->
                    FileInputStream(localFile).use { input ->
                        input.copyTo(output)
                    }
                }
            }
            .doOnComplete {
                loadFolder(folder)
            }
            .subscribe({
                messageSubject.onNext(context.getString(R.string.settings_backup_export_success))
            }, { e ->
                e.printStackTrace()
                messageSubject.onNext(
                    context.getString(R.string.settings_backup_export_failed, e.message)
                )
            })
    }

    fun onImportFile(file: File) {
        disposables += dbRepository.import(file).subscribe({
            messageSubject.onNext(context.getString(R.string.settings_backup_import_success))
        }, { e ->
            e.printStackTrace()
            messageSubject.onNext(
                context.getString(R.string.settings_backup_import_failed, e.message)
            )
        })
    }
}

data class FileBackup(
    val file: File,
    val properties: Properties
)

sealed class BackupState {
    object Initial : BackupState()
    object NoFolderSelected : BackupState()
    class Loaded(val files: List<FileBackup>) : BackupState()
}
