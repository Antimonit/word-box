package me.khol.wordbox.screens.main.word

import io.reactivex.Flowable
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function4
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.interactor.PreferencesInteractor
import me.khol.wordbox.database.dao.LabelDao
import me.khol.wordbox.database.dao.LanguageDao
import me.khol.wordbox.database.dao.WordDao
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.WordList
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * ViewModel for [WordListFragment].
 */
class WordListViewModel @Inject constructor(
    private val languageDao: LanguageDao,
    private val labelDao: LabelDao,
    private val wordDao: WordDao,
    private val preferences: PreferencesInteractor
) : BaseViewModel() {

    private val wordsSubject: FlowableProcessor<List<WordList>> = BehaviorProcessor.create()
    private val selectedLanguageSubject: FlowableProcessor<Language> = BehaviorProcessor.create()
    private val sortSubject: FlowableProcessor<Sort> = BehaviorProcessor.create()
    private val selectedLabelsSubject: FlowableProcessor<List<Label>> = BehaviorProcessor.create()
    private val searchSubject: FlowableProcessor<String> = BehaviorProcessor.create()

    fun observeAllWords(): Flowable<List<WordList>> = wordsSubject

    fun observeLabels(): Flowable<Pair<List<Label>, List<Label>>> {
        return Flowable.combineLatest(
            labelDao.getAllLabels(),
            selectedLabelsSubject,
            BiFunction { allLabels: List<Label>, selectedLabels: List<Label> ->
                Pair(allLabels, selectedLabels)
            }
        )
    }

    fun observeSort(): Flowable<Sort> {
        return sortSubject
    }

    fun observeSearch(): Flowable<String> {
        return searchSubject
    }

    fun observeLanguages(): Flowable<Pair<List<Language>, Language>> {
        return Flowable.combineLatest(
            languageDao.getAllLanguages(),
            selectedLanguageSubject,
            BiFunction { allLanguages: List<Language>, selectedLanguage: Language ->
                Pair(allLanguages, selectedLanguage)
            }
        )
    }

    init {
        disposables += preferences.getSelectedLanguage().subscribe { it -> languageChanged(it) }
        disposables += preferences.getSort().subscribe { it -> sortChanged(it) }
        disposables += preferences.getSelectedLabels().subscribe { it -> labelsChanged(it) }
        disposables += preferences.getSearchQuery().subscribe { it -> searchChanged(it) }
        disposables += Flowable.combineLatest(
            selectedLanguageSubject.distinctUntilChanged(),
            sortSubject.distinctUntilChanged(),
            selectedLabelsSubject.distinctUntilChanged(),
            searchSubject.distinctUntilChanged(),
            Function4 { language: Language, sort: Sort, labels: List<Label>, search: String ->
                wordDao.getWordsList(
                    language.languageId,
                    labels.map { it.labelId },
                    sort.databaseColumn,
                    search
                )
            })
            .switchMap { it }
            .subscribeOnIO()
            .observeOnMainThread()
            .subscribe({ words: List<WordList> ->
                wordsSubject.onNext(words)
            }, Throwable::printStackTrace)
    }

    fun languageChanged(newLanguage: Language) {
        preferences.selectedLanguage = newLanguage
        selectedLanguageSubject.onNext(newLanguage)
    }

    fun sortChanged(newSort: Sort) {
        preferences.sort = newSort
        sortSubject.onNext(newSort)
    }

    fun labelsChanged(newLabels: List<Label>) {
        preferences.selectedLabels = newLabels
        selectedLabelsSubject.onNext(newLabels)
    }

    fun searchChanged(newText: String) {
        preferences.searchQuery = newText
        searchSubject.onNext(newText)
    }
}
