package me.khol.wordbox.screens.manage.languages

import android.animation.LayoutTransition
import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.hideKeyboard
import me.khol.wordbox.extensions.roundDrawable
import me.khol.wordbox.extensions.setTextWithRetainedSelection
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.screens.manage.BaseManageModel
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.autoCompleteTextView
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.editText
import org.jetbrains.anko.hintResource
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColorResource
import java.util.*

/**
 * Epoxy model used for setting name and short name (max 3 characters) of a [Language] and
 * dragging it up and down in its containing recycler view.
 *
 * Selecting language name from suggestions list will also set short name as defined by [Locale]
 * created for each language in [Locale.getISOLanguages].
 */
@EpoxyModelClass
open class ManageLanguageModel : BaseManageModel<ManageLanguageModel.Layout>() {

    companion object {
        private val nameToCode: MutableMap<String, String> = HashMap()
        private val languageList: List<String>

        init {
            Locale.getISOLanguages().forEach { lang: String ->
                val locale = Locale(lang)
                nameToCode[locale.getDisplayLanguage().capitalize()] = lang
                nameToCode[locale.getDisplayLanguage(locale).capitalize()] = lang
            }

            languageList = nameToCode
                .toList()
                .map { (key, _) -> key }
        }
    }

    interface Callbacks {
        fun onStartDrag(model: EpoxyModel<*>)
        fun onEditStarted(language: Language)
        fun onEditCanceled(language: Language)
        fun onEditConfirmed(language: Language, newName: String, newShortName: String)
    }

    @EpoxyAttribute
    lateinit var language: Language

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var callbacks: Callbacks

    /**
     * Because of the messy logic behind activating, canceling and confirming edit mode, we use
     * this flag to distinguish between canceling and confirming actions when EditText loses focus.
     */
    private var isConfirmed: Boolean = false

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        baseBind()
        val editorActionListener = TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                confirmEdit(this)
                true
            } else {
                false
            }
        }

        var anyHasFocus = false

        val focusChangeListener = View.OnFocusChangeListener { textView, hasFocus ->
            (textView as TextView).isCursorVisible = hasFocus

            val newHasFocus = name.hasFocus() || shortName.hasFocus()
            root.elevationCompat = if (newHasFocus) 4.dpf else 2.dpf
            if (anyHasFocus != newHasFocus) {
                anyHasFocus = newHasFocus
                if (anyHasFocus) {
                    callbacks.onEditStarted(language)
                } else {
                    if (!isConfirmed) {
                        // TODO: display confirmation dialog or make UX better some other way.
                        callbacks.onEditCanceled(language)
                        name.setText(language.name)
                        shortName.setText(language.shortName)
                    }
                }
            }
        }

        name.threshold = 1
        name.setAdapter(ArrayAdapter<String>(view.context, android.R.layout.simple_spinner_dropdown_item, languageList))
        name.onItemClickListener = AdapterView.OnItemClickListener { parent, _, position, _ ->
            val language = parent.getItemAtPosition(position)
            shortName.setText(nameToCode[language], TextView.BufferType.EDITABLE)
        }
        name.setTextWithRetainedSelection(language.name)
        name.setOnEditorActionListener(editorActionListener)
        name.setOnFocusChangeListener(focusChangeListener)

        shortName.setTextWithRetainedSelection(language.shortName)
        shortName.setOnEditorActionListener(editorActionListener)
        shortName.setOnFocusChangeListener(focusChangeListener)
    }

    override fun confirmEdit(layout: Layout) {
        val newName = layout.name.text.toString()
        val newShortName = layout.shortName.text.toString()
        callbacks.onEditConfirmed(language, newName, newShortName)
        isConfirmed = true
        layout.shortName.clearFocus()
        layout.name.clearFocus()
        isConfirmed = false
        layout.name.hideKeyboard()
    }

    override fun onStartDrag() {
        callbacks.onStartDrag(this@ManageLanguageModel)
    }

    override fun Layout.unbind() {
        baseUnbind()
        name.setOnFocusChangeListener(null)
        name.setOnEditorActionListener(null)
        shortName.setOnFocusChangeListener(null)
        shortName.setOnEditorActionListener(null)
    }

    class Layout(parent: ViewGroup) : BaseManageModel.Layout(parent) {

        lateinit var shortName: EditText
        lateinit var name: AutoCompleteTextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.constraintLayout {
                backgroundColorResource = R.color.white
                elevationCompat = 2.dpf
                layoutTransition = LayoutTransition()

                shortName = editText {
                    textSize = 13f
                    font = R.font.roboto_medium
                    padding = 0.dp
                    gravity = Gravity.CENTER
                    textColorResource = R.color.white
                    filters += InputFilter.LengthFilter(3)
                    filters += InputFilter.AllCaps()
                    maxLines = 1
                    inputType = InputType.TYPE_CLASS_TEXT or
                        InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS or
                        InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
                    backgroundDrawable = roundDrawable(
                        backgroundColor = color(R.color.colorPrimary),
                        stroke = 0.dp,
                        corner = 2.dpf
                    )
                }

                name = autoCompleteTextView {
                    textSize = 16f
                    font = R.font.roboto_medium
                    backgroundDrawable = null
                    hintResource = R.string.manage_language_name_hint
                    ellipsize = TextUtils.TruncateAt.END
                    maxLines = 1
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                }

                constraints {
                    shortName.connect(
                        VERTICAL of parentId,
                        STARTS of parentId with 14.dp
                    ).size(32.dp, 32.dp)

                    name.connect(
                        VERTICAL of parentId,
                        STARTS of parentId with 72.dp,
                        ENDS of parentId with 56.dp
                    ).size(matchConstraint, matchConstraint)
                }

                commonConstraints()

                layoutParams = ViewGroup.MarginLayoutParams(match, 56.dp)
            }
        }
    }
}
