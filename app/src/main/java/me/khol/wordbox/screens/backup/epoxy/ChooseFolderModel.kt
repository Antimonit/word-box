package me.khol.wordbox.screens.backup.epoxy

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.cardView
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.*

@EpoxyModelClass
open class ChooseFolderModel : BaseEpoxyModel<ChooseFolderModel.Layout>() {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onClick: () -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        view.setOnClickListener {
            onClick()
        }
    }

    override fun Layout.unbind() {
        view.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        private lateinit var icon: ImageView
        private lateinit var txtTitle: TextView
        private lateinit var txtDescription: TextView
        private lateinit var txtDescription2: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.cardView {
                cardElevation = 1.dpf
                radius = 4.dpf
                isClickable = true
                setContentPadding(16.dp, 16.dp, 16.dp, 16.dp)
                foreground = getDrawableAttr(R.attr.selectableItemBackground)

                constraintLayout {
                    icon = imageView(R.drawable.ic_search)

                    txtTitle = textView(R.string.settings_backup_choose_folder) {
                        textSize = 18f
                        font = R.font.roboto_medium
                        textColorResource = R.color.black
                    }.lparams(matchConstraint, wrapContent)

                    txtDescription = textView(R.string.settings_backup_choose_folder_description) {
                        textSize = 16f
                        font = R.font.roboto
                    }.lparams(matchConstraint, wrapContent)

                    txtDescription2 = textView(R.string.settings_backup_choose_folder_description_2) {
                        textSize = 16f
                        font = R.font.roboto
                    }.lparams(matchConstraint, wrapContent)

                    constraints {
                        icon.connect(
                            TOPS of parentId,
                            STARTS of parentId
                        )
                        txtTitle.connect(
                            VERTICAL of icon,
                            START to END of icon with 16.dp,
                            ENDS of parentId
                        )
                        txtDescription.connect(
                            HORIZONTAL of txtTitle,
                            TOP to BOTTOM of txtTitle with 24.dp
                        )
                        txtDescription2.connect(
                            HORIZONTAL of txtTitle,
                            TOP to BOTTOM of txtDescription with 8.dp
                        )
                    }
                }
            }.apply {
                layoutParams = ViewGroup.MarginLayoutParams(match, wrap)
            }
        }
    }
}
