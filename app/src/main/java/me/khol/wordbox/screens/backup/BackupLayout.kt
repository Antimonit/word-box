package me.khol.wordbox.screens.backup

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyItemSpacingDecorator
import com.airbnb.epoxy.EpoxyRecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.extensions.getColorAttr
import me.khol.wordbox.extensions.getDimension
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.extensions.tintedDrawable
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.floatingActionButton
import org.jetbrains.anko.image
import org.jetbrains.anko.margin
import org.jetbrains.anko.padding

/**
 * Layout for [BackupActivity].
 */
class BackupLayout(parent: ViewGroup) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var list: EpoxyRecyclerView

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.coordinatorLayout {
            appBarLayout {
                toolbar = themedToolbar(R.style.WordBoxTheme_DarkAppBarOverlay) {
                    titleResource = R.string.settings_backup_title
                    backgroundColor = getColorAttr(R.attr.colorPrimary)
                    minimumHeight = getDimensionAttr(R.attr.actionBarSize)
                }.lparams(match, wrap) {
                    scrollFlags = 0 /* don't scroll */
                }
            }.lparams(match, wrap)

            list = epoxyRecyclerView {
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(EpoxyItemSpacingDecorator(8.dp))
                padding = 8.dp
                clipToPadding = false
            }.lparams(match, match) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }.apply {
            layoutParams = ViewGroup.LayoutParams(match, match)
        }
    }
}
