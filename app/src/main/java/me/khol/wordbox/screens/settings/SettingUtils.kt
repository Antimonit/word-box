package me.khol.wordbox.screens.settings

import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceManager

/**
 * A preference value change listener that updates the preference's summary
 * to reflect its new value.
 */
private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
    val stringValue = value?.toString()

    if (preference is ListPreference) {
        val index = preference.findIndexOfValue(stringValue)
        preference.summary = if (index >= 0) preference.entries[index] else null

    } else {
        preference.summary = stringValue
    }
    true
}

/**
 * Binds a preference's summary to its value. More specifically, when the
 * preference's value is changed, its summary (line of text below the
 * preference title) is updated to reflect the value. The summary is also
 * immediately updated upon calling this method. The exact display format is
 * dependent on the type of preference.
 */
fun bindPreferenceSummaryToValue(preference: Preference) {
    // Set the listener to watch for value changes.
    preference.onPreferenceChangeListener = sBindPreferenceSummaryToValueListener

    // Trigger the listener immediately with the preference's
    // current value.
    sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
        PreferenceManager
            .getDefaultSharedPreferences(preference.context)
            .getString(preference.key, ""))
}
