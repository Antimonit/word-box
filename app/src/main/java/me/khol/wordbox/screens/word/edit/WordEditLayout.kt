package me.khol.wordbox.screens.word.edit

import android.animation.ObjectAnimator
import android.animation.StateListAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyRecyclerView
import com.google.android.material.appbar.AppBarLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.backgroundTintListCompat
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colorWithAlpha
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getColorAttr
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.extensions.targetApi
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.bottomPadding
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.themedAppBarLayout
import org.jetbrains.anko.editText
import org.jetbrains.anko.horizontalMargin
import org.jetbrains.anko.singleLine
import org.jetbrains.anko.textColor
import org.jetbrains.anko.topPadding
import org.jetbrains.anko.verticalPadding

/**
 * Layout for [WordEditFragment].
 */
class WordEditLayout(
    parent: ViewGroup,
    private val recyclerAdapter: RecyclerView.Adapter<*>
) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var txtTitle: EditText
    lateinit var contents: EpoxyRecyclerView

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.coordinatorLayout {
            backgroundColorResource = R.color.window_background

            themedAppBarLayout {
                if (targetApi(Build.VERSION_CODES.LOLLIPOP)) {
                    // gives a permanent elevation
                    stateListAnimator = StateListAnimator().apply {
                        addState(
                            intArrayOf(),
                            ObjectAnimator.ofFloat(this@themedAppBarLayout, "elevation", 4.dpf)
                        )
                    }
                }

                toolbar = themedToolbar(R.style.WordBoxTheme_DarkAppBarOverlay) {
                    backgroundColor = getColorAttr(R.attr.colorPrimary)
                    minimumHeight = getDimensionAttr(R.attr.actionBarSize)
                    setContentInsetsRelative(0.dp, 0.dp)
                }.lparams(match, wrap) {
                    scrollFlags = 0 /* don't scroll */
                }

                txtTitle = editText {
                    textSize = 20f
                    textColor = color(R.color.white)
                    backgroundTintListCompat =
                        ColorStateList.valueOf(colorWithAlpha(android.R.color.white, 50))
                    font = R.font.roboto_medium
                    gravity = Gravity.CENTER_VERTICAL
                    topPadding = 0.dp
                    bottomPadding = 0.dp
                    singleLine = true
                }.lparams(match, 48.dp) {
                    horizontalMargin = 16.dp - 4.dp
                    bottomMargin = 8.dp
                }

            }.lparams(match, wrap)

            contents = epoxyRecyclerView {
                verticalPadding = 16.dp
                clipToPadding = false
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                itemAnimator = DefaultItemAnimator()
                adapter = recyclerAdapter
                layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                scheduleLayoutAnimation()
            }.lparams(match, match) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }

        }.apply {
            layoutParams = ViewGroup.LayoutParams(match, match)
        }
    }
}
