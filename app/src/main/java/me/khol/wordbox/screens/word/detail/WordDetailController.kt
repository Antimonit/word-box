package me.khol.wordbox.screens.word.detail

import com.airbnb.epoxy.EpoxyController
import me.khol.wordbox.R
import me.khol.wordbox.database.model.Example
import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.screens.base.epoxy.LateProp
import me.khol.wordbox.screens.word.base.epoxy.header
import me.khol.wordbox.screens.word.base.epoxy.separator
import me.khol.wordbox.screens.word.detail.epoxy.*

/**
 * Controller containing all information about [WordDetail].
 */
class WordDetailController(
    val onTranslationClick: (TranslationFull) -> Unit
) : EpoxyController() {

    var word by LateProp<WordDetail>()

    override fun buildModels() {
        var separatorCount = 0
        fun separator(id: String) {
            if (separatorCount > 0) {
                separator { id("separator", id) }
            }
            separatorCount++
        }

        if (word.description.isNotBlank()) {
            separator("description")
            header {
                id("header", "description")
                text(R.string.word_detail_header_description)
            }
            description {
                id("description")
                description(word.description)
            }
        }

        if (word.labels.isNotEmpty()) {
            separator("labels")
            header {
                id("header", "labels")
                text(R.string.word_detail_header_labels)
            }
            labels {
                id("labels")
                labels(word.labels)
            }
        }

        if (word.examples.isNotEmpty()) {
            separator("examples")
            header {
                id("header", "examples")
                text(R.string.word_detail_header_examples)
            }
            word.examples.forEach { example: Example ->
                example {
                    id(example.exampleId)
                    example(example)
                }
            }
        }

        if (word.translations.isNotEmpty()) {
            separator("translation")
            header {
                id("header", "translations")
                text(R.string.word_detail_header_translations)
            }
            word.translations.forEach { translation: TranslationFull ->
                translation {
                    id(translation.wordId)
                    translation(translation)
                    onItemClick { translation ->
                        onTranslationClick(translation)
                    }
                }
            }
        }

        separator("practice")
        header {
            id("header", "practice")
            text(R.string.word_detail_header_practice)
        }
        practice {
            id("practice_model")
            scheduledPractice(word.scheduledTime)
            lastPractice(word.lastTime)
            interval(word.interval)
            easeFactor(word.easeFactor)
            averageEaseFactor(word.averageEaseFactor)
            averageSuccessRate(word.averageSuccessRate)
            practiceRounds(word.practiceRounds)
        }
    }
}
