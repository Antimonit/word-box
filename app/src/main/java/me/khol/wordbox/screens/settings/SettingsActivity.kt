package me.khol.wordbox.screens.settings

import android.os.Bundle
import me.khol.wordbox.screens.base.BaseActivity

class SettingsActivity : BaseActivity() {

    lateinit var layout: SettingsLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        layout = SettingsLayout(rootView)
        setContentLayout(layout)

        setSupportActionBar(layout.toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowTitleEnabled(true)
            it.setDisplayShowCustomEnabled(false)
        }

        supportFragmentManager
            .beginTransaction()
            .replace(layout.content.id, PreferenceFragmentGeneral())
            .commit()
    }
}
