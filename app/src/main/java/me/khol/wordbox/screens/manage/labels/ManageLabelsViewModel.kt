package me.khol.wordbox.screens.manage.labels

import android.graphics.Color
import androidx.annotation.ColorInt
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.extensions.moveItem
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.model.repository.LabelRepository
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import javax.inject.Inject

/**
 * ViewModel for [ManageLabelsActivity].
 */
class ManageLabelsViewModel @Inject constructor(
    private val labelRepository: LabelRepository
) : BaseViewModel() {

    data class State(val labels: List<Label>, val editModeLabel: Label?)

    // Keep a copy of the labels when we perform drag. When the drag operation is finished, flush
    // reordered labels to the repository and the DB.
    private var labels: List<Label> = listOf()
    private var editModeLabel: Label? = null

    private val stateSubject: FlowableProcessor<State> = BehaviorProcessor.create()

    fun observeState(): Flowable<State> = stateSubject

    init {
        disposables += labelRepository.observeAllLabels()
            .subscribe({ newLabels ->
                labels = newLabels
                stateSubject.onNext(State(labels, editModeLabel))
            }, Throwable::printStackTrace)
    }

    fun deleteLabel(label: Label) {
        disposables += labelRepository.deleteLabel(label).subscribe()
    }

    fun updateLabelColor(label: Label, @ColorInt color: Int) {
        disposables += labelRepository.updateLabel(label.copy(color = color)).subscribe()
    }

    fun editStarted(label: Label) {
        editModeLabel = label
        stateSubject.onNext(State(labels, editModeLabel))
    }

    fun editCanceled(label: Label) {
        editModeLabel = null
        stateSubject.onNext(State(labels, editModeLabel))
    }

    fun editConfirmed(label: Label, text: String) {
        editModeLabel = null
        disposables += labelRepository.updateLabel(label.copy(text = text)).subscribe()
    }

    fun getLabelWordCount(labelId: Long): Int {
        return labelRepository.getLabelWordCount(labelId).blockingFirst()
    }

    /**
     * It is okay if two labels have the same [Label.order] or if there are holes in the
     * ordering.
     * Labels get reordered based on their position in the list, not on the [Label.order].
     */
    fun moveLabel(labelToMove: Label, fromPosition: Int, toPosition: Int) {
        labels = labels
            .moveItem(fromPosition, toPosition)
            .mapIndexed { index, label ->
                label.copy(order = index)
            }
        stateSubject.onNext(State(labels, editModeLabel))
    }

    fun moveLabelsFinished() {
        disposables += labelRepository.updateLabels(labels).subscribe()
    }

    fun addLabel(newLabelName: String) {
        val color = Color.BLACK
        val size = labels.size
        labelRepository.insertLabel(Label(0, newLabelName, color, size)).subscribe()
    }
}
