package me.khol.wordbox.screens.main.practice

import android.content.Context
import android.transition.AutoTransition
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyItemSpacingDecorator
import com.airbnb.epoxy.EpoxyRecyclerView
import cz.ackee.ankoconstraintlayout._ConstraintLayout
import cz.ackee.ankoconstraintlayout._ConstraintSet
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.beginDelayedTransition
import me.khol.wordbox.extensions.customView
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.model.view.SwipeCard
import me.khol.wordbox.model.view.SwipeDirection
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.view

class PracticeItemLayout(
    parent: ViewGroup,
    private val onClickedReveal: () -> Unit
) : BaseLayout(parent) {

    lateinit var txtMainLanguage: TextView
    lateinit var txtMainWord: TextView
    lateinit var txtMainDescription: TextView
    lateinit var layoutMainWord: ConstraintLayout
    lateinit var listTranslations: EpoxyRecyclerView

    private lateinit var divider: View
    private lateinit var txtTapToReveal: TextView

    private lateinit var txtHardStamp: TextView
    private lateinit var txtEasyStamp: TextView
    private lateinit var txtResetStamp: TextView

    private lateinit var constraintLayout: _ConstraintLayout
    private lateinit var unrevealed: _ConstraintSet
    private lateinit var revealed: _ConstraintSet

    private var bouncedView: View? = null
    private var bouncedViewScale: Float = 0f

    fun setProgress(progressX: Float, progressY: Float) {
        val direction = SwipeDirection.of(progressX, progressY)
        when (direction) {
            is SwipeDirection.LEFT -> txtEasyStamp
            is SwipeDirection.RIGHT -> txtHardStamp
            is SwipeDirection.TOP -> txtResetStamp
            is SwipeDirection.NONE -> null
        }.bounceView(if (direction.progress > 1f) 1f else 0.5f)
    }

    private fun View?.bounceView(targetScale: Float) {
        if (bouncedView != this || bouncedViewScale != targetScale) {
            bouncedView?.scaleView(0f)
            bouncedView = this
            bouncedView?.scaleView(targetScale)
            bouncedViewScale = targetScale
        }
    }

    private fun View.scaleView(targetScale: Float) {
        animate()
            .scaleX(targetScale)
            .scaleY(targetScale)
            .alpha(targetScale)
            .setInterpolator(OvershootInterpolator(2f))
            .setDuration(300)
            .start()
    }

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.customView<SwipeCard> {
            cardElevation = 4.dpf
            radius = 8.dpf

            constraintLayout = constraintLayout {

                layoutMainWord = constraintLayout {
                    backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)

                    txtMainLanguage = textView {
                        gravity = Gravity.CENTER
                        textSize = 12f
                    }

                    txtMainWord = textView {
                        gravity = Gravity.CENTER
                        textSize = 36f
                        textColorResource = R.color.black
                        horizontalPadding = 16.dp
                    }

                    txtMainDescription = textView {
                        gravity = Gravity.CENTER
                        textSize = 14f
                        horizontalPadding = 16.dp
                    }

                    constraints {
                        txtMainLanguage.connect(
                            TOPS of parentId,
                            HORIZONTAL of parentId
                        )

                        txtMainWord.connect(
                            TOP to BOTTOM of txtMainLanguage,
                            HORIZONTAL of parentId
                        ).width(matchConstraint)

                        txtMainDescription.connect(
                            TOP to BOTTOM of txtMainWord with 8.dp,
                            HORIZONTAL of parentId
                        ).width(matchConstraint)
                    }
                }

                divider = view {
                    backgroundColorResource = R.color.gray
                }

                listTranslations = epoxyRecyclerView {
                    itemAnimator = DefaultItemAnimator()
                    layoutManager = object : LinearLayoutManager(context) {
                        override fun canScrollVertically() = true
                    }
                    isNestedScrollingEnabled = true
                    addItemDecoration(EpoxyItemSpacingDecorator(8.dp))
                }

                txtTapToReveal = textView("Tap to reveal") {
                    textSize = 16f
                    isClickable = true
                    isFocusable = true
                    backgroundDrawable = getDrawableAttr(android.R.attr.selectableItemBackground)
                    gravity = Gravity.CENTER
                    setOnClickListener {
                        constraintLayout.post {
                            constraintLayout.beginDelayedTransition {
                                AutoTransition().apply {
                                    interpolator = LinearInterpolator()
                                    duration = 150
                                }
                            }
                            revealed.applyTo(constraintLayout)
                            constraintLayout.invalidate()
                        }
                        onClickedReveal()
                    }
                }

                txtHardStamp = textView(R.string.practice_result_hard) {
                    textSize = 20f
                    textColorResource = R.color.practice_hard
                }

                txtEasyStamp = textView(R.string.practice_result_easy) {
                    textSize = 20f
                    textColorResource = R.color.practice_easy
                }

                txtResetStamp = textView(R.string.practice_result_reset) {
                    textSize = 20f
                    textColorResource = R.color.practice_reset
                }

                unrevealed = constraints {
                    layoutMainWord.connect(
                        TOPS of parentId with 48.dp,
                        HORIZONTAL of parentId
                    ).width(matchConstraint)

                    divider.connect(
                        BOTTOMS of parentId with 256.dp,
                        HORIZONTAL of parentId
                    ).size(matchConstraint, 1.dp)

                    txtTapToReveal.connect(
                        TOP to BOTTOM of divider,
                        BOTTOMS of parentId,
                        HORIZONTAL of parentId
                    ).size(matchConstraint, matchConstraint)

                    listTranslations.connect(
                        TOP to BOTTOM of divider with 48.dp,
                        HORIZONTAL of parentId
                    ).width(matchConstraint)
                        .visibility(View.GONE)

                    arrayOf(txtHardStamp, txtEasyStamp, txtResetStamp)
                        .forEach {
                            it.visibility(View.INVISIBLE)
                            it.scale(0f)
                            it.connect(
                                BOTTOMS of parentId with 32.dp,
                                HORIZONTAL of parentId
                            )
                        }
                }

                revealed = prepareConstraints {
                    divider.connect(
                        TOP to BOTTOM of layoutMainWord with 48.dp
                    ).clear(BOTTOM)

                    txtTapToReveal.visibility(View.GONE)

                    listTranslations.visibility(View.VISIBLE)

                    arrayOf(txtHardStamp, txtEasyStamp, txtResetStamp)
                        .forEach {
                            it.visibility(View.VISIBLE)
                        }
                }

                unrevealed.applyTo(this)
            }
        }.apply {
            layoutParams = ViewGroup.LayoutParams(match, match)
        }
    }
}
