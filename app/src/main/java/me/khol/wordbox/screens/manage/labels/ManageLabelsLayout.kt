package me.khol.wordbox.screens.manage.labels

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.khol.wordbox.R
import me.khol.wordbox.screens.manage.BaseManageLayout

/**
 * Layout for [ManageLabelsActivity].
 */
class ManageLabelsLayout(
    parent: ViewGroup,
    adapter: RecyclerView.Adapter<*>
) : BaseManageLayout(parent, adapter) {

    override val toolbarTitleRes = R.string.manage_labels_title
}
