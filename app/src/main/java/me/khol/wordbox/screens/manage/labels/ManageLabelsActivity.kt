package me.khol.wordbox.screens.manage.labels

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.EpoxyModelTouchCallback
import com.airbnb.epoxy.EpoxyViewHolder
import com.pavelsikun.vintagechroma.ChromaDialog
import com.pavelsikun.vintagechroma.IndicatorMode
import com.pavelsikun.vintagechroma.colormode.ColorMode
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.extensions.tintedDrawable
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.screens.base.BaseActivity

/**
 * Activity responsible for creating, updating and deleting labels used throughout the application.
 */
class ManageLabelsActivity : BaseActivity() {

    private lateinit var layout: ManageLabelsLayout
    private lateinit var viewModel: ManageLabelsViewModel
    private lateinit var controller: ManageLabelsController

    private lateinit var swipeItemTouchHelper: ItemTouchHelper
    private lateinit var dragItemTouchHelper: ItemTouchHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        controller = ManageLabelsController(
            object : ManageLabelModel.Callbacks {
                override fun onStartDrag(model: EpoxyModel<*>) {
                    val viewHolder = layout.recycler.findViewHolderForItemId(model.id())
                    dragItemTouchHelper.startDrag(viewHolder)
                }

                override fun onColorClicked(label: Label) {
                    ChromaDialog.Builder()
                        .initialColor(label.color)
                        .colorMode(ColorMode.HSV)
                        .indicatorMode(IndicatorMode.DECIMAL)
                        .onColorSelected { color ->
                            viewModel.updateLabelColor(label, color)
                        }
                        .create()
                        .show(supportFragmentManager, "ChromaDialog")
                }

                override fun onEditStarted(label: Label) {
                    viewModel.editStarted(label)
                }

                override fun onEditCanceled(label: Label) {
                    viewModel.editCanceled(label)
                }

                override fun onEditConfirmed(label: Label, newText: String) {
                    viewModel.editConfirmed(label, newText)
                }
            },
            onAddClicked = { newLabelName ->
                viewModel.addLabel(newLabelName)
            }
        )

        layout = ManageLabelsLayout(rootView, controller.adapter)
        setContentLayout(layout)

        setSupportActionBar(layout.toolbar)
        supportActionBar?.let {
            it.setDisplayShowTitleEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }

        setUpSwipe()
        setUpDrag()

        disposables += viewModel.observeState()
            .observeOnMainThread()
            .subscribe({ (labels, editModeLabel) ->
                controller.labels = labels
                controller.editModeLabel = editModeLabel
            }, Throwable::printStackTrace)
    }

    private fun setUpSwipe() {
        val targetModelClass = ManageLabelModel::class.java
        val touchCallback = object : EpoxyModelTouchCallback<ManageLabelModel>(controller, targetModelClass) {
            @SuppressLint("ResourceType")
            private val icon = tintedDrawable(R.drawable.ic_delete, Color.WHITE)!!
            private val iconWidth = icon.intrinsicWidth
            private val iconHeight = icon.intrinsicHeight
            private val iconBounds = Rect()

            private val background = ColorDrawable()
            private val backgroundColor = color(R.color.delete_red)

            override fun onChildDraw(
                canvas: Canvas,
                recyclerView: RecyclerView,
                viewHolder: EpoxyViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                val view = viewHolder.itemView
                val itemHeight = view.bottom - view.top

                background.color = backgroundColor
                background.setBounds(view.left, view.top, view.right, view.bottom)
                background.draw(canvas)

                val iconMargin = (itemHeight - iconHeight) / 2
                iconBounds.top = view.top + iconMargin
                iconBounds.bottom = view.bottom - iconMargin
                if (dX > 0) {
                    iconBounds.left = view.left + iconMargin
                    iconBounds.right = view.left + iconMargin + iconWidth
                } else {
                    iconBounds.left = view.right - iconMargin - iconWidth
                    iconBounds.right = view.right - iconMargin
                }
                icon.bounds = iconBounds
                icon.draw(canvas)

                super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }

            override fun getMovementFlagsForModel(model: ManageLabelModel?, adapterPosition: Int): Int {
                return makeMovementFlags(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT)
            }

            override fun isTouchableModel(model: EpoxyModel<*>?): Boolean {
                if (controller.isInEditMode())
                    return false
                return super.isTouchableModel(model)
            }

            override fun isLongPressDragEnabled(): Boolean {
                return false
            }

            override fun onSwipeCompleted(model: ManageLabelModel, itemView: View, position: Int, direction: Int) {
                val count = viewModel.getLabelWordCount(model.label.labelId)
                val title = resources.getString(R.string.manage_delete_labels_dialog_title)
                val message = resources.getQuantityString(R.plurals.manage_delete_labels_dialog_message, count, count)

                AlertDialog.Builder(this@ManageLabelsActivity)
                    .setTitle(title)
                    .setMessage(message)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        viewModel.deleteLabel(model.label)
                    }
                    .setNegativeButton(android.R.string.no) { _, _ ->
                        swipeItemTouchHelper.attachToRecyclerView(null)
                        swipeItemTouchHelper.attachToRecyclerView(layout.recycler)
                    }
                    .setOnCancelListener {
                        swipeItemTouchHelper.attachToRecyclerView(null)
                        swipeItemTouchHelper.attachToRecyclerView(layout.recycler)
                    }
                    .show()
            }
        }

        swipeItemTouchHelper = ItemTouchHelper(touchCallback)
        swipeItemTouchHelper.attachToRecyclerView(layout.recycler)
    }

    private fun setUpDrag() {
        val targetModelClass = ManageLabelModel::class.java
        val touchCallback = object : EpoxyModelTouchCallback<ManageLabelModel>(controller, targetModelClass) {
            override fun getMovementFlagsForModel(model: ManageLabelModel, adapterPosition: Int): Int {
                return makeMovementFlags(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0)
            }

            override fun isTouchableModel(model: EpoxyModel<*>?): Boolean {
                if (controller.isInEditMode())
                    return false
                return super.isTouchableModel(model)
            }

            override fun isLongPressDragEnabled(): Boolean {
                return false
            }

            override fun onModelMoved(from: Int, to: Int, model: ManageLabelModel, itemView: View) {
                viewModel.moveLabel(model.label, from, to)
            }

            override fun onDragReleased(model: ManageLabelModel, itemView: View) {
                viewModel.moveLabelsFinished()
            }
        }

        dragItemTouchHelper = ItemTouchHelper(touchCallback)
        dragItemTouchHelper.attachToRecyclerView(layout.recycler)
    }
}
