package me.khol.wordbox.screens.settings

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import me.khol.wordbox.R

class PreferenceFragmentDeveloper : PreferenceFragmentCompat() {

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.pref_developer)
    }
}
