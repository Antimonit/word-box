package me.khol.wordbox.screens.manage

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.EpoxyItemSpacingDecorator
import com.airbnb.epoxy.EpoxyRecyclerView
import me.khol.wordbox.R
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.extensions.getColorAttr
import me.khol.wordbox.extensions.getDimensionAttr
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.appcompat.v7.themedToolbar
import org.jetbrains.anko.appcompat.v7.titleResource
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.verticalPadding

/**
 * Base layout for screens with managing functionality.
 */
abstract class BaseManageLayout(
    parent: ViewGroup,
    private val recyclerAdapter: RecyclerView.Adapter<*>
) : BaseLayout(parent) {

    lateinit var recycler: EpoxyRecyclerView
    lateinit var toolbar: Toolbar

    abstract val toolbarTitleRes: Int

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.verticalLayout {
            backgroundColorResource = R.color.window_background

            toolbar = themedToolbar(R.style.WordBoxTheme_DarkAppBarOverlay) {
                titleResource = toolbarTitleRes
                isFocusable = true
                isFocusableInTouchMode = true
                backgroundColor = getColorAttr(R.attr.colorPrimary)
                minimumHeight = getDimensionAttr(R.attr.actionBarSize)
            }.lparams(match, wrap)

            recycler = epoxyRecyclerView {
                verticalPadding = 12.dp
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                itemAnimator = DefaultItemAnimator()
                addItemDecoration(EpoxyItemSpacingDecorator(2))
                adapter = recyclerAdapter
            }.lparams(match, 0, weight = 1f)
        }.apply {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }
}
