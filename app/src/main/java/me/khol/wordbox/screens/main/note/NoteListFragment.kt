package me.khol.wordbox.screens.main.note

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.screens.base.BaseFragment
import me.khol.wordbox.screens.note.NoteActivity

/**
 * Displays a list of notes and a FAB button to create a new one. Clicking either on a note or on
 * the FAB button starts [me.khol.wordbox.screens.note.NoteDetailFragment].
 */
class NoteListFragment : BaseFragment() {

    private lateinit var layout: NoteListLayout
    private lateinit var viewModel: NoteListViewModel
    private lateinit var controller: NoteListController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        controller = NoteListController(
            onNoteClick = { note ->
                startActivity(NoteActivity.getEditIntent(layout.context, note.noteId))
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layout = NoteListLayout(
            container!!,
            controller.adapter,
            onAddClicked = {
                startActivity(NoteActivity.getAddIntent(layout.context))
            }
        )
        return layout.view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disposables += viewModel.observeNotes()
            .observeOnMainThread()
            .subscribe({ notes ->
                controller.notes = notes
                layout.notes.scheduleLayoutAnimation()
            }, Throwable::printStackTrace)

        activity.setSupportActionBar(layout.toolbar)
        activity.supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(false)
            it.setTitle(R.string.nav_title_notes)
        }
    }
}
