package me.khol.wordbox.screens.main.note

import android.content.Context
import android.graphics.Rect
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.appbar.AppBarLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.centeredToolbar
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.getColorAttr
import me.khol.wordbox.extensions.getDimension
import me.khol.wordbox.extensions.tintedDrawable
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.floatingActionButton
import org.jetbrains.anko.image
import org.jetbrains.anko.margin
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.padding
import org.jetbrains.anko.recyclerview.v7.recyclerView
import kotlin.math.min

/**
 * Layout for [NoteListFragment]
 */
class NoteListLayout(
    parent: ViewGroup,
    private val notesAdapter: RecyclerView.Adapter<*>,
    private val onAddClicked: () -> Unit
) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var notes: RecyclerView

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.coordinatorLayout {
            backgroundColor = getColorAttr(R.attr.colorPrimary)

            val appBarLayout = appBarLayout {
                toolbar = centeredToolbar().lparams(match, wrap) {
                    scrollFlags = 0 /* don't scroll */
                }
            }.lparams(match, wrap)

            notes = recyclerView {
                clipToPadding = false
                padding = 4.dp
                layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
                itemAnimator = DefaultItemAnimator()
                addItemDecoration(SimpleSpacingItemDecoration(4.dp))
                layoutAnimation = AnimationUtils.loadLayoutAnimation(context, R.anim.layout_fall_down)
                adapter = notesAdapter

                addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        appBarLayout.elevationCompat = min(8.dpf, computeVerticalScrollOffset() / 2.dpf)
                    }
                })
            }.lparams(match, match) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }

            // add FAB
            floatingActionButton {
                id = R.id.fab_add
                backgroundTintList = colors(R.color.white)
                image = tintedDrawable(R.drawable.ic_add, color(R.color.colorPrimary))
                compatElevation = 8.dpf
                setOnClickListener {
                    onAddClicked()
                }
            }.lparams(wrap, wrap) {
                margin = getDimension(R.dimen.fab_margin)
                gravity = Gravity.BOTTOM or Gravity.END
            }

        }.apply {
            layoutParams = ViewGroup.LayoutParams(matchParent, matchParent)
        }
    }
}

class RecyclerItemDecoration(private val spanCount: Int, private val spacing: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        val spacing = Math.round(spacing * parent.context.resources.displayMetrics.density)
        val position = parent.getChildAdapterPosition(view)
        val column = position % spanCount

        outRect.left = spacing - column * spacing / spanCount
        outRect.right = (column + 1) * spacing / spanCount

        outRect.top = if (position < spanCount) spacing else 0
        outRect.bottom = spacing
    }
}
