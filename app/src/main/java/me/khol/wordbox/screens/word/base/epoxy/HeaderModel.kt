package me.khol.wordbox.screens.word.base.epoxy

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.StringRes
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.font
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.bottomPadding
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.topPadding

@EpoxyModelClass
open class HeaderModel : BaseEpoxyModel<HeaderModel.Layout>() {

    @EpoxyAttribute
    @StringRes
    var text: Int = 0

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        header.textResource = text
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var header: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.textView {
                textSize = 14f
                gravity = Gravity.START or Gravity.CENTER_VERTICAL
                font = R.font.roboto_medium
                textColorResource = R.color.colorPrimary
                horizontalPadding = 16.dp
                minHeight = 16.dp
                bottomPadding = 8.dp
                topPadding = 8.dp
            }.apply {
                header = this
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}

