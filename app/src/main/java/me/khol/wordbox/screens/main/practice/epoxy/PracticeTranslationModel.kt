package me.khol.wordbox.screens.main.practice.epoxy

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.wrapContent

@EpoxyModelClass
open class PracticeTranslationModel : BaseEpoxyModel<PracticeTranslationModel.Layout>() {

    @EpoxyAttribute
    lateinit var translation: TranslationFull
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onTranslationClick: () -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        txtWord.text = translation.text
        txtDescription.text = translation.description
        view.setOnClickListener {
            onTranslationClick()
        }
    }

    override fun Layout.unbind() {
        view.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtWord: TextView
        lateinit var txtDescription: TextView

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.verticalLayout {
                backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)

                txtWord = textView {
                    gravity = Gravity.CENTER
                    textSize = 28f
                    textColorResource = R.color.black
                    horizontalPadding = 16.dp
                }

                txtDescription = textView {
                    gravity = Gravity.CENTER
                    textSize = 12f
                    horizontalPadding = 16.dp
                }

                layoutParams = ViewGroup.LayoutParams(matchParent, wrapContent)
            }
        }
    }
}
