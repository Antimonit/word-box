package me.khol.wordbox.screens.main.word

import androidx.annotation.DrawableRes
import me.khol.wordbox.R

enum class Sort(@DrawableRes val icon: Int, val databaseColumn: String) {
    Alphabet(R.drawable.ic_sort_by_alpha, "text"),
    Progress(R.drawable.ic_trending_up, "progress"),
    TimeCreated(R.drawable.ic_time_added, "timeCreated"),
    TimeModified(R.drawable.ic_time_edited, "timeModified")
}
