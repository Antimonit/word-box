package me.khol.wordbox.screens.add

import io.reactivex.Completable
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.model.repository.LabelRepository
import me.khol.wordbox.model.repository.LanguageRepository
import me.khol.wordbox.model.repository.WordRepository
import me.khol.wordbox.screens.word.edit.WordEditViewModel
import javax.inject.Inject

/**
 * ViewModel for [AddActivity] and all sections contained within.
 *
 * Because all logic contained in [WordEditViewModel] is also required by this ViewModel we use
 * it as a super-class and define two additional actions: [add] and [addMore].
 */
class AddViewModel @Inject constructor(
    languageRepository: LanguageRepository,
    wordRepository: WordRepository,
    labelRepository: LabelRepository
) : WordEditViewModel(languageRepository, wordRepository, labelRepository) {

    init {
        word = WordDetail(0, "", "", language = Language.invalid)
        wordSubject.onNext(word)
    }

    /**
     * Saves user-defined information as a new word to the database.
     */
    fun add(): Completable = wordRepository
        .insertWord(word)
        .ignoreElement()

    /**
     * Saves user-defined information as a new word to the database and prepares the viewModel
     * for addition of another word.
     * The new word will inherit the same language, labels and translations as the previous word.
     * The previous word will also be added to the list of translations of the new word.
     */
    fun addMore(): Completable = wordRepository
        .insertWord(word)
        .flatMap { wordId: Long ->
            wordRepository
                .getWordDetail(wordId)
                .firstOrError()
        }
        .map { word: WordDetail ->
            TranslationFull(word.wordId, word.text, word.description, word.language)
        }
        .doOnSuccess { translation: TranslationFull ->
            word = WordDetail(0, "", "",
                language = word.language,
                labels = word.labels,
                translations = word.translations + translation,
                examples = emptyList()
            )
            wordSubject.onNext(word)
        }
        .ignoreElement()
}
