package me.khol.wordbox.screens.main.practice

import me.khol.wordbox.database.model.Word
import me.khol.wordbox.database.model.WordDetail
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import timber.log.Timber
import kotlin.math.log10
import kotlin.math.max
import kotlin.math.min
import kotlin.random.Random

object PracticeScheduler {

    private const val TARGET_SUCCESS_RATE = 0.85f
    private const val MAX_EASE_CHANGE = 1.2f

    fun resetProgress(word: WordDetail) = word.copy(
        easeFactor = Word.DEFAULT_EASE_FACTOR,
        averageEaseFactor = Word.DEFAULT_AVERAGE_EASE_FACTOR,
        averageSuccessRate = Word.DEFAULT_AVERAGE_SUCCESS_RATE,
        practiceRounds = Word.DEFAULT_PRACTICE_ROUNDS,
        interval = Word.DEFAULT_INTERVAL,
        scheduledTime = Word.DEFAULT_SCHEDULED_TIME,
        lastTime = Instant.now()
    )

    fun updateProgress(word: WordDetail, success: Boolean) = word
        .addSuccessToHistory(success)
        .newEase()
        .addEaseToHistory()
        .incrementPracticeRounds()
        .updateInterval()
        .reschedule()

    private fun WordDetail.addSuccessToHistory(success: Boolean): WordDetail {
        val bonus = if (success) 1 else 0
        val newAverageSuccess = (averageSuccessRate * practiceRounds + bonus) / (practiceRounds + 1)
        Timber.d("newAverageSuccess: $newAverageSuccess")
        return copy(averageSuccessRate = newAverageSuccess)
    }

    private fun WordDetail.newEase(): WordDetail {
        val clampedAverageSuccessRate = min(0.99f, max(0.01f, averageSuccessRate))
        val old = easeFactor
        val new = averageEaseFactor * log10(TARGET_SUCCESS_RATE) / log10(clampedAverageSuccessRate)
        val clampedEase = min(old * MAX_EASE_CHANGE, max(old / MAX_EASE_CHANGE, new))
        Timber.d("newEaseFactor: $clampedEase")
        return copy(easeFactor = clampedEase)
    }

    private fun WordDetail.addEaseToHistory(): WordDetail {
        val newAverageEase = (averageEaseFactor * practiceRounds + easeFactor) / (practiceRounds + 1)
        Timber.d("newAverageEase: $newAverageEase")
        return copy(averageEaseFactor = newAverageEase)
    }

    private fun WordDetail.incrementPracticeRounds(): WordDetail {
        val newPracticeRounds = practiceRounds + 1
        Timber.d("newPracticeRounds: $newPracticeRounds")
        return copy(practiceRounds = newPracticeRounds)
    }

    private fun WordDetail.updateInterval(): WordDetail {
        val randomMultiplier = Random.nextDouble(1 / 1.1, 1.1)
        val newInterval = Duration.ofMillis((interval.toMillis() * easeFactor * randomMultiplier).toLong())
        Timber.d("newInterval: $newInterval")
        return copy(interval = newInterval)
    }

    private fun WordDetail.reschedule(): WordDetail {
        val newLastTime = Instant.now()
        val newScheduledTime = newLastTime + interval
        Timber.d("newScheduledTime: $newScheduledTime")
        return copy(lastTime = newLastTime, scheduledTime = newScheduledTime)
    }
}
