package me.khol.wordbox.screens.word.edit.epoxy.examples

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.database.model.Example
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.hintResource
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView

/**
 * Epoxy model for adding new [example][Example].
 */
@EpoxyModelClass
open class AddExampleModel : BaseEpoxyModel<AddExampleModel.Layout>() {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onAddClick: () -> Unit

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        view.setOnClickListener {
            onAddClick()
        }
    }

    override fun Layout.unbind() {
        view.setOnClickListener(null)
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.frameLayout {
                foreground = getDrawableAttr(android.R.attr.selectableItemBackground)
                elevationCompat = 0.dpf

                textView {
                    hintResource = R.string.word_edit_add_example
                    textSize = 14f
                    textColor = color(R.color.black)
                    font = R.font.roboto_medium
                    gravity = Gravity.CENTER_VERTICAL
                    horizontalPadding = 16.dp
                    setLineSpacing(6.dpf, 1f)
                }.lparams(match, match)

            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, 48.dp)
            }
        }
    }
}
