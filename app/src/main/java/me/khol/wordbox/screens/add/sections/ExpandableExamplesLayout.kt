package me.khol.wordbox.screens.add.sections

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.airbnb.epoxy.EpoxyRecyclerView
import me.khol.wordbox.extensions.epoxyRecyclerView
import me.khol.wordbox.database.model.Example
import me.khol.wordbox.screens.word.edit.WordCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.examples.addExample
import me.khol.wordbox.screens.word.edit.epoxy.examples.example
import org.jetbrains.anko._LinearLayout

/**
 * Contains a list of examples that can be modified, added and removed.
 */
class ExpandableExamplesLayout(
    context: Context,
    titleRes: Int,
    callbacks: WordCallbacks
) : ExpandableLayout(context, titleRes, callbacks) {

    lateinit var list: EpoxyRecyclerView

    override fun _LinearLayout.createContentView(): View {
        return epoxyRecyclerView {
            layoutManager = object : LinearLayoutManager(context) {
                override fun canScrollVertically() = false
            }
            isNestedScrollingEnabled = false
            itemAnimator = null
            list = this
        }.lparams(match, wrap)
    }

    fun bind(examples: List<Example>) {
        syncTitle(examples.size)

        list.withModels {
            examples.forEach { example ->
                example {
                    id("example", example.exampleId)
                    example(example)
                    callbacks(callbacks)
                }
            }

            addExample {
                id("add_example")
                onAddClick(callbacks::onAddExample)
            }
        }
    }

}
