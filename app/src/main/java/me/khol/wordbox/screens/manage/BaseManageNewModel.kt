package me.khol.wordbox.screens.manage

import android.content.Context
import android.text.InputType
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.airbnb.epoxy.EpoxyAttribute
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundColorResource
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.editText

/**
 * Base epoxy model for adding manageable items.
 */
abstract class BaseManageNewModel<Layout : BaseManageNewModel.Layout> : BaseEpoxyModel<Layout>() {

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onAddClicked: (String) -> Unit

    override fun Layout.bind() {
        editName.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                onAddClicked(textView.text.toString())
                textView.clearFocus()
                true
            } else {
                false
            }
        }
    }

    override fun Layout.unbind() {
        editName.setOnEditorActionListener(null)
    }

    abstract class Layout(parent: ViewGroup) : BaseLayout(parent) {

        abstract val nameHint: String

        lateinit var editName: EditText

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.constraintLayout {
                backgroundColorResource = R.color.white
                elevationCompat = 2.dpf

                editName = editText {
                    textSize = 16f
                    backgroundDrawable = null
                    hint = nameHint
                    ellipsize = TextUtils.TruncateAt.END
                    maxLines = 1
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
                    imeOptions = EditorInfo.IME_ACTION_DONE
                }

                constraints {
                    editName.connect(
                        VERTICAL of parentId,
                        STARTS of parentId with 72.dp,
                        ENDS of parentId with 56.dp
                    ).width(matchConstraint)
                }

                layoutParams = ViewGroup.LayoutParams(match, 56.dp)
            }
        }
    }
}
