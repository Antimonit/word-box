package me.khol.wordbox.screens.main.profile

import android.content.Context
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import cz.ackee.ankoconstraintlayout._ConstraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.extensions.drawable
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.imageView
import org.jetbrains.anko.padding
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

/**
 * A panel containing text of currently selected period (week / month / year) and smaller text
 * showing counter for practice rounds made during that period.
 * User can navigate to preceding and following periods using buttons on both sides of the view.
 */
open class DatePanelView(context: Context) : _ConstraintLayout(context) {

    companion object {
        private val dayFormatter = DateTimeFormatter.ofPattern("d")
        private val dayMonthFormatter = DateTimeFormatter.ofPattern("d MMMM")
        private val dayMonthYearFormatter = DateTimeFormatter.ofPattern("d MMMM yyyy")
        private val monthYearFormatter = DateTimeFormatter.ofPattern("MMMM yyyy")
        private val yearFormatter = DateTimeFormatter.ofPattern("yyyy")
    }

    private var btnPrevious: FrameLayout
    private var btnNext: FrameLayout
    private var txtTitle: TextView
    private var txtSubtitle: TextView

    init {
        padding = dp(8)

        btnPrevious = frameLayout {
            isClickable = true
            isFocusable = true
            imageView {
                scaleType = ImageView.ScaleType.CENTER
                isDuplicateParentStateEnabled = true
                backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackgroundBorderless)
                setImageDrawable(drawable(R.drawable.ic_chevron_left))
            }.lparams(dp(24), dp(24)) {
                gravity = Gravity.CENTER
            }
        }

        txtTitle = textView {
            textSize = 16f
            font = R.font.roboto_medium
            textColorResource = R.color.black
        }

        txtSubtitle = textView {
            textSize = 13f
            font = R.font.roboto
        }

        btnNext = frameLayout {
            isClickable = true
            isFocusable = true
            imageView {
                scaleType = ImageView.ScaleType.CENTER
                isDuplicateParentStateEnabled = true
                backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackgroundBorderless)
                setImageDrawable(drawable(R.drawable.ic_chevron_right))
            }.lparams(dp(24), dp(24)) {
                gravity = Gravity.CENTER
            }
        }

        constraints {
            btnPrevious.connect(
                STARTS of parentId,
                VERTICAL of parentId
            ).size(dp(48), dp(48))

            btnNext.connect(
                ENDS of parentId,
                VERTICAL of parentId
            ).size(dp(48), dp(48))

            arrayOf(txtTitle, txtSubtitle)
                .chainPacked(TOP of parentId, BOTTOM of parentId)
                .forEach {
                    it.connect(
                        START to END of btnPrevious,
                        END to START of btnNext
                    )
                }
        }
    }

    fun setWeekly(date: LocalDate, totalValue: Int) {
        val start = date.with(DayOfWeek.MONDAY)
        val end = date.with(DayOfWeek.SUNDAY)

        // same month, same year -> 10 - 16 December 2018
        // diff month, same year -> 26 November - 2 December 2018
        // diff month, diff year -> 31 December 2018 - 6 January 2019
        val startFormatter = when {
            start.year != end.year -> dayMonthYearFormatter
            start.month != end.month -> dayMonthFormatter
            else -> dayFormatter
        }

        txtTitle.text = "${start.format(startFormatter)} ‐ ${end.format(dayMonthYearFormatter)}"
        txtSubtitle.text = context.getString(R.string.history_view_practice_rounds, totalValue)
    }

    fun setMonthly(date: LocalDate, totalValue: Int) {
        txtTitle.text = date.format(monthYearFormatter)
        txtSubtitle.text = context.getString(R.string.history_view_practice_rounds, totalValue)
    }

    fun setYearly(date: LocalDate, totalValue: Int) {
        txtTitle.text = date.format(yearFormatter)
        txtSubtitle.text = context.getString(R.string.history_view_practice_rounds, totalValue)
    }

    fun setOnPreviousClickListener(listener: (() -> Unit)?) {
        if (listener == null) {
            btnPrevious.setOnClickListener(null)
        } else {
            btnPrevious.setOnClickListener { listener() }
        }
    }

    fun setOnNextClickListener(listener: (() -> Unit)?) {
        if (listener == null) {
            btnNext.setOnClickListener(null)
        } else {
            btnNext.setOnClickListener { listener() }
        }
    }
}
