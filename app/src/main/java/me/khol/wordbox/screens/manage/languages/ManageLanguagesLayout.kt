package me.khol.wordbox.screens.manage.languages

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.khol.wordbox.R
import me.khol.wordbox.screens.manage.BaseManageLayout

class ManageLanguagesLayout(
    parent: ViewGroup,
    adapter: RecyclerView.Adapter<*>
) : BaseManageLayout(parent, adapter) {

    override val toolbarTitleRes = R.string.manage_languages_title
}
