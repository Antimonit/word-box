package me.khol.wordbox.screens.word.detail.epoxy

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import me.khol.wordbox.R
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.isVisible
import me.khol.wordbox.extensions.relatedToNow
import me.khol.wordbox.screens.base.epoxy.BaseEpoxyModel
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko._LinearLayout
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.linearLayout
import org.jetbrains.anko.textColorResource
import org.jetbrains.anko.textView
import org.jetbrains.anko.verticalLayout
import org.jetbrains.anko.verticalPadding
import org.threeten.bp.Duration
import org.threeten.bp.Instant

/**
 * Epoxy model of a practice related information.
 */
@EpoxyModelClass
open class PracticeModel : BaseEpoxyModel<PracticeModel.Layout>() {

    @EpoxyAttribute
    lateinit var scheduledPractice: Instant
    @EpoxyAttribute
    var lastPractice: Instant? = null
    @EpoxyAttribute
    lateinit var interval: Duration
    @EpoxyAttribute
    var easeFactor: Float = 0f
    @EpoxyAttribute
    var averageEaseFactor: Float = 0f
    @EpoxyAttribute
    var averageSuccessRate: Float = 0f
    @EpoxyAttribute
    var practiceRounds: Int = 0

    override fun createViewLayout(parent: ViewGroup) = Layout(parent)

    override fun Layout.bind() {
        txtScheduled.text = scheduledPractice.relatedToNow()
        txtLast.text = lastPractice?.relatedToNow() ?: "-"
        txtInterval.text = interval.relatedToNow()
        txtEaseFactor.text = easeFactor.toString()
        txtAverageEaseFactor.text = averageEaseFactor.toString()
        txtAverageSuccessRate.text = averageSuccessRate.toString()
        txtPracticeRounds.text = practiceRounds.toString()
    }

    class Layout(parent: ViewGroup) : BaseLayout(parent) {

        lateinit var txtScheduled: TextView
        lateinit var txtLast: TextView
        lateinit var txtInterval: TextView
        lateinit var txtEaseFactor: TextView
        lateinit var txtAverageEaseFactor: TextView
        lateinit var txtAverageSuccessRate: TextView
        lateinit var txtPracticeRounds: TextView

        private lateinit var layoutMainContent: View
        private lateinit var layoutDetailedContent: View

        private fun _LinearLayout.addRow(labelRes: Int, colorRes: Int = R.color.black): TextView {
            lateinit var txtValue: TextView
            linearLayout {
                textView(labelRes) {
                    textSize = 15f
                    textColorResource = colorRes
                }

                txtValue = textView {
                    textSize = 15f
                    textColorResource = colorRes
                    gravity = Gravity.END
                }.lparams(0, wrap, weight = 1f)
            }.lparams(match, wrap) {
                bottomMargin = 4.dp
            }
            return txtValue
        }

        override fun createView(ui: AnkoContext<Context>): View {
            return ui.verticalLayout {
                layoutMainContent = verticalLayout {
                    verticalPadding = 8.dp
                    horizontalPadding = 16.dp
                    backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)
                    setOnClickListener {
                        layoutDetailedContent.isVisible = !layoutDetailedContent.isVisible
                    }

                    txtScheduled = addRow(R.string.word_detail_scheduled_practice)
                    txtLast = addRow(R.string.word_detail_last_practice)
                }.lparams(match, wrap)

                layoutDetailedContent = verticalLayout {
                    verticalPadding = 8.dp
                    horizontalPadding = 16.dp
                    isVisible = false

                    txtInterval = addRow(R.string.word_detail_interval, R.color.gray)
                    txtEaseFactor = addRow(R.string.word_detail_ease_factor, R.color.gray)
                    txtAverageEaseFactor = addRow(R.string.word_detail_average_ease_factor, R.color.gray)
                    txtAverageSuccessRate = addRow(R.string.word_detail_average_success_rate, R.color.gray)
                    txtPracticeRounds = addRow(R.string.word_detail_practice_rounds, R.color.gray)
                }.lparams(match, wrap)

            }.apply {
                layoutParams = ViewGroup.LayoutParams(match, wrap)
            }
        }
    }
}
