package me.khol.wordbox.screens.word.edit

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.processors.FlowableProcessor
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.database.model.Example
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.TranslationFull
import me.khol.wordbox.database.model.Word
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.database.model.WordSuggestion
import me.khol.wordbox.model.repository.LabelRepository
import me.khol.wordbox.model.repository.LanguageRepository
import me.khol.wordbox.model.repository.WordRepository
import me.khol.wordbox.screens.base.viewmodel.BaseViewModel
import me.khol.wordbox.screens.word.edit.epoxy.description.DescriptionCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.examples.ExampleCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.labels.LabelCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.practice.PracticeCallbacks
import me.khol.wordbox.screens.word.edit.epoxy.translations.TranslationCallbacks
import javax.inject.Inject

/**
 * ViewModel for [WordEditFragment].
 * Word IS editable on this screen so we load the word only once. If the currently edited word is
 * updated from outside by any means, its changes won't be reflected in the currently edited word.
 */
open class WordEditViewModel @Inject constructor(
    protected val languageRepository: LanguageRepository,
    protected val wordRepository: WordRepository,
    protected val labelRepository: LabelRepository
) : BaseViewModel() {

    /**
     * Currently edited word. Changes on the screen are reflected in this object.
     * This instance is scoped to this screen only and is not persisted unless clicked on the
     * save button.
     */
    protected lateinit var word: WordDetail

    /**
     * Originally loaded word. Used to decide whether the word was updated or not.
     */
    private lateinit var originalWord: WordDetail

    /**
     * Emit new word only in case we add or remove a model. In case of plain text update, no
     * emission is needed and is even detrimental. Updating model after clicking on a label
     * prevents ripple animation from showing properly, etc.
     */
    protected val wordSubject: FlowableProcessor<WordDetail> = BehaviorProcessor.create<WordDetail>()

    /**
     * Utility to remove boilerplate. Used by callbacks defined below.
     */
    private fun update(emit: Boolean = false, block: WordDetail.() -> WordDetail) {
        word = word.block()
        if (emit) {
            wordSubject.onNext(word)
        }
    }

    /**
     * Callback for whenever description is changed.
     */
    private val descriptionCallbacks = object : DescriptionCallbacks {
        override fun onDescriptionChanged(description: CharSequence) = update {
            copy(description = description.toString())
        }
    }

    /**
     * Callbacks for whenever example is added, changed or deleted.
     */
    private val exampleCallbacks = object : ExampleCallbacks {
        private fun updateExamples(
            emit: Boolean = false,
            block: MutableList<Example>.() -> Unit
        ) = update(emit) {
            copy(examples = examples.toMutableList().apply(block))
        }

        override fun onExampleChanged(example: Example) = updateExamples {
            set(indexOfFirst { it.exampleId == example.exampleId }, example)
        }

        override fun onDeleteExample(example: Example) = updateExamples(true) {
            remove(example)
        }

        /**
         * Assign unique negative ids to new examples so that epoxy controller can be happy
         */
        private var newExampleId = -1L

        override fun onAddExample() = updateExamples(true) {
            newExampleId--
            add(Example(newExampleId, ""))
        }
    }

    /**
     * Callback for whenever label is checked.
     */
    private val labelCallbacks = object : LabelCallbacks {
        override fun onLabelChecked(label: Label, checked: Boolean) = update {
            copy(labels = labels.toMutableList().apply {
                if (checked) {
                    add(label)
                } else {
                    remove(label)
                }
            })
        }
    }

    /**
     * Callbacks for whenever translation is added, replaced or deleted. Also provides suggestions.
     */
    private val translationCallbacks = object : TranslationCallbacks {
        private fun updateTranslations(
            emit: Boolean = false,
            block: MutableList<TranslationFull>.() -> Unit
        ) = update(emit) {
            copy(translations = translations.toMutableList().apply(block))
        }

        override fun getSuggestions(query: String): List<WordSuggestion> {
            return wordRepository.getWordSuggestions(query, word.translations.map { it.wordId })
        }

        override fun onReplace(old: TranslationFull, new: TranslationFull) = updateTranslations(true) {
            set(indexOfFirst { it.wordId == old.wordId }, new)
        }

        override fun onDelete(translation: TranslationFull) = updateTranslations(true) {
            remove(translation)
        }

        /**
         * Assign unique negative ids to new translations so that epoxy controller can be happy
         */
        private var newTranslationId = -1L

        override fun onAddTranslation() = updateTranslations(true) {
            newTranslationId--
            add(TranslationFull(newTranslationId, "", "", Language.invalid))
        }
    }

    /**
     * Callback for whenever practice progress is reset.
     */
    private val practiceCallbacks = object : PracticeCallbacks {
        override fun onResetClicked() = update(true) {
            copy(
                easeFactor = Word.DEFAULT_EASE_FACTOR,
                averageEaseFactor = Word.DEFAULT_AVERAGE_EASE_FACTOR,
                averageSuccessRate = Word.DEFAULT_AVERAGE_SUCCESS_RATE,
                practiceRounds = Word.DEFAULT_PRACTICE_ROUNDS,
                interval = Word.DEFAULT_INTERVAL,
                scheduledTime = Word.DEFAULT_SCHEDULED_TIME
            )
        }
    }

    /**
     * The main callbacks object for updating word's description, examples, labels, etc.
     * Includes callbacks for whenever word's text or language is changed and all other callbacks
     * defined above.
     */
    val callbacks: WordCallbacks = object : WordCallbacks,
        DescriptionCallbacks by descriptionCallbacks,
        ExampleCallbacks by exampleCallbacks,
        LabelCallbacks by labelCallbacks,
        PracticeCallbacks by practiceCallbacks,
        TranslationCallbacks by translationCallbacks {

        override fun onTextChanged(word: CharSequence) = update {
            copy(text = word.toString())
        }

        override fun onLanguageSelected(language: Language) = update {
            copy(language = language)
        }
    }

    /**
     * Load the word for edit. Should be called only once when the screen is first created.
     */
    fun loadWordDetail(wordId: Long) {
        disposables += wordRepository.getWordDetail(wordId)
            .firstOrError()
            .subscribe({ word ->
                this.word = word
                this.originalWord = word
                this.wordSubject.onNext(word)
            }, Throwable::printStackTrace)
    }

    /**
     * Observe raw languages from the DB
     */
    fun observeLanguages(): Flowable<List<Language>> = languageRepository.observeAllLanguages()

    /**
     * Observe raw labels from the DB
     */
    fun observeLabels(): Flowable<List<Label>> = labelRepository.observeAllLabels()

    /**
     * Observe changes of currently edited word
     */
    fun observeWord(): Flowable<WordDetail> = wordSubject

    /**
     * Persist all changes made to currently edited word
     */
    fun saveChanges(): Completable {
        return wordRepository.updateWord(word)
    }

    /**
     * Returns true if there are unsaved changes.
     */
    fun hasChanged(): Boolean {
        return word != originalWord
    }

    /**
     * Discards all changes made to the edited word.
     */
    fun discardChanges() {
        word = originalWord
    }
}
