package me.khol.wordbox.screens.backup

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import me.khol.wordbox.R
import me.khol.wordbox.screens.base.BaseDialogFragment
import java.io.File

class RestoreDialogFragment : BaseDialogFragment() {

    companion object {
        private const val ARG_FILE = "file"

        fun newInstance(file: File): RestoreDialogFragment {
            return RestoreDialogFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_FILE, file)
                }
            }
        }
    }

    private val file by lazy { arguments!!.getSerializable(ARG_FILE)!! as File }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
            .setTitle(R.string.settings_backup_restore_confirmation)
            .setMessage(file.name)
            .setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }
            .setPositiveButton(R.string.ok) { dialog, _ ->
                ((activity as? Callbacks) ?: (targetFragment as? Callbacks))
                    ?.onBackupAccepted(file)
                dialog.cancel()
            }
            .setCancelable(true)
            .create()
    }

    interface Callbacks {
        fun onBackupAccepted(file: File)
    }

}
