package me.khol.wordbox.screens.main.profile

import android.content.Context
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import cz.ackee.ankoconstraintlayout.constraintLayout
import me.khol.wordbox.R
import me.khol.wordbox.extensions.centeredToolbar
import me.khol.wordbox.extensions.color
import me.khol.wordbox.extensions.customLayout
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.getDrawableAttr
import me.khol.wordbox.extensions.startPadding
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.design.appBarLayout
import org.jetbrains.anko.design.coordinatorLayout
import org.jetbrains.anko.design.tabLayout
import org.jetbrains.anko.textView
import org.jetbrains.anko.withAlpha
import org.threeten.bp.temporal.ChronoUnit
import org.threeten.bp.temporal.TemporalUnit

/**
 * Layout for [ProfileFragment].
 */
class ProfileLayout(parent: ViewGroup) : BaseLayout(parent) {

    lateinit var toolbar: Toolbar
    lateinit var manageLabels: TextView
    lateinit var manageLanguages: TextView
    lateinit var manageBackup: TextView
    lateinit var settings: TextView

    lateinit var historyLayout: HistoryLayout
    lateinit var tabLayout: TabLayout

    var tabSelectedListener: ((TemporalUnit) -> Unit)? = null

    override fun createView(ui: AnkoContext<Context>): View {
        return ui.coordinatorLayout {
            appBarLayout {
                toolbar = centeredToolbar().lparams(match, wrap) {
                    scrollFlags = 0 /* don't scroll */
                }
                tabLayout = tabLayout {
                    tabMode = TabLayout.MODE_FIXED
                    tabGravity = TabLayout.GRAVITY_CENTER
                    setTabTextColors(color(R.color.white).withAlpha(0xAF), color(R.color.white))
                    setSelectedTabIndicatorColor(color(R.color.white))

                    addTab(newTab().setText("Week").setTag(ChronoUnit.WEEKS))
                    addTab(newTab().setText("Month").setTag(ChronoUnit.MONTHS))
                    addTab(newTab().setText("Year").setTag(ChronoUnit.YEARS))
                    addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                        override fun onTabReselected(p0: TabLayout.Tab?) {
                            // do nothing
                        }

                        override fun onTabUnselected(p0: TabLayout.Tab?) {
                            // do nothing
                        }

                        override fun onTabSelected(tab: TabLayout.Tab) {
                            tabSelectedListener?.invoke(tab.tag as TemporalUnit)
                        }
                    })
                }
            }.lparams(match, wrap)

            constraintLayout {

                historyLayout = customLayout(HistoryLayout(this))

                fun item(text: Int): TextView {
                    return textView(text) {
                        backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackground)
                        textSize = 14f
                        font = R.font.roboto_medium
                        startPadding = 72.dp
                        gravity = Gravity.CENTER_VERTICAL
                    }.lparams(matchConstraint, 48.dp)
                }

                manageLanguages = item(R.string.settings_manage_languages)
                manageLabels = item(R.string.settings_manage_labels)
                manageBackup = item(R.string.settings_manage_backup)
                settings = item(R.string.settings_manage_settings)

                constraints {
                    historyLayout.view.connect(
                        TOPS of parentId,
                        HORIZONTAL of parentId
                    ).width(matchConstraint)

                    manageBackup.connect(
                        BOTTOMS of parentId,
                        HORIZONTAL of parentId
                    )

                    manageLabels.connect(
                        BOTTOM to TOP of manageBackup,
                        HORIZONTAL of parentId
                    )

                    manageLanguages.connect(
                        BOTTOM to TOP of manageLabels,
                        HORIZONTAL of parentId
                    )

                    settings.connect(
                        BOTTOM to TOP of manageLanguages,
                        HORIZONTAL of parentId
                    )
                }
            }.lparams(match, match) {
                behavior = AppBarLayout.ScrollingViewBehavior()
            }
        }.apply {
            layoutParams = ViewGroup.LayoutParams(match, match)
        }
    }
}
