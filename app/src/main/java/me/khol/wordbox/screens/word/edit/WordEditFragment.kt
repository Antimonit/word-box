package me.khol.wordbox.screens.word.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.MenuItemCompat
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.Flowable
import io.reactivex.functions.Function3
import io.reactivex.rxkotlin.plusAssign
import me.khol.wordbox.R
import me.khol.wordbox.extensions.colors
import me.khol.wordbox.extensions.getViewModel
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.database.model.Label
import me.khol.wordbox.database.model.Language
import me.khol.wordbox.database.model.WordDetail
import me.khol.wordbox.screens.base.BaseFragment
import org.jetbrains.anko.support.v4.toast

/**
 * Fragment where user can change details of a [WordDetail]. This screen can be reached only from
 * [WordDetailFragment][me.khol.wordbox.screens.word.detail.WordDetailFragment].
 */
class WordEditFragment : BaseFragment() {

    companion object {
        private const val ARG_WORD_ID = "word_id"

        fun newFragment(wordId: Long): WordEditFragment {
            return WordEditFragment().apply {
                arguments = Bundle().apply {
                    putLong(ARG_WORD_ID, wordId)
                }
            }
        }
    }

    private lateinit var layout: WordEditLayout
    private lateinit var controller: WordEditController
    private lateinit var viewModel: WordEditViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = getViewModel(vmFactory)

        if (savedInstanceState == null) {
            arguments?.let {
                viewModel.loadWordDetail(it.getLong(ARG_WORD_ID))
            }
        }

        setHasOptionsMenu(true)

        controller = WordEditController(
            onSaveClick = this::save,
            callbacks = viewModel.callbacks
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layout = WordEditLayout(
            container!!,
            controller.adapter
        )
        return layout.view
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean("recreated", true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.setSupportActionBar(layout.toolbar)
        activity.supportActionBar?.let { actionBar ->
            actionBar.setDisplayShowTitleEnabled(false)
            actionBar.setDisplayShowCustomEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(true)
        }

        disposables += layout.txtTitle.textChanges()
            .skipInitialValue()
            .subscribe {
                viewModel.callbacks.onTextChanged(it)
            }

        disposables += Flowable.combineLatest(
            viewModel.observeLanguages(),
            viewModel.observeLabels(),
            viewModel.observeWord(),
            Function3 { languages: List<Language>, labels: List<Label>, word: WordDetail ->
                Triple(languages, labels, word)
            })
            .observeOnMainThread()
            .subscribe({ (languages, labels, word) ->
                layout.txtTitle.setText(word.text)

                controller.allLanguages = languages
                controller.allLabels = labels
                controller.word = word
                controller.requestModelBuild()
            }, Throwable::printStackTrace)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_word_edit, menu)

        MenuItemCompat.setIconTintList(menu.findItem(R.id.save), context!!.colors(R.color.white))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.save -> save()
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    override fun onBackPressed(): Boolean {
        return if (viewModel.hasChanged()) {
            showDiscardDialog()
            true
        } else {
            false
        }
    }

    private fun showDiscardDialog() {
        AlertDialog.Builder(context!!)
            .setTitle(R.string.word_edit_discard_dialog_title)
            .setMessage(R.string.word_edit_discard_dialog_message)
            .setPositiveButton(R.string.word_edit_discard_dialog_positive) { _, _ ->
                viewModel.discardChanges()
                activity.onBackPressed()
            }
            .setNegativeButton(R.string.word_edit_discard_dialog_negative) { _, _ ->
                /* do nothing */
            }
            .show()
    }

    private fun save() {
        disposables += viewModel.saveChanges()
            .observeOnMainThread()
            .subscribe({
                toast(R.string.word_edit_save_success)
                fragmentManager?.popBackStack()
            }, {
                toast(R.string.word_edit_save_error)
                it.printStackTrace()
            })
    }
}
