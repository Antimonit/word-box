package me.khol.wordbox.screens.base

import dagger.android.support.DaggerDialogFragment
import io.reactivex.disposables.CompositeDisposable
import me.khol.wordbox.screens.base.viewmodel.ViewModelFactory
import javax.inject.Inject

/**
 * Base DialogFragment that all other custom DialogFragments should extend
 */
abstract class BaseDialogFragment : DaggerDialogFragment(), CommonBaseFragment {

    override val activity: BaseActivity
        get() = getActivity()!! as BaseActivity

    override val disposables = CompositeDisposable()

    @Inject
    override lateinit var vmFactory: ViewModelFactory

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.clear()
    }

    override fun onBackPressed(): Boolean = false
}
