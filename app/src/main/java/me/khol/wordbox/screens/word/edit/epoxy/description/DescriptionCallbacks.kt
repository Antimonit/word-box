package me.khol.wordbox.screens.word.edit.epoxy.description

interface DescriptionCallbacks {
    fun onDescriptionChanged(description: CharSequence)
}
