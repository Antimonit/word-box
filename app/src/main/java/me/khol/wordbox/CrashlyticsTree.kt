package me.khol.wordbox

import android.util.Log
import com.crashlytics.android.Crashlytics
import timber.log.Timber

class CrashlyticsTree : Timber.Tree() {

    companion object {

        private const val KEY_PRIORITY = "priority"
        private const val KEY_TAG = "tag"
        private const val KEY_MESSAGE = "message"
    }

    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }

        Crashlytics.setInt(KEY_PRIORITY, priority)
        Crashlytics.setString(KEY_TAG, tag)
        Crashlytics.setString(KEY_MESSAGE, message)
        if (throwable == null) {
            Crashlytics.log(priority, tag, message)
        } else {
            Crashlytics.logException(throwable)
        }
    }
}
