package me.khol.wordbox

import android.content.Context
import androidx.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.fabric.sdk.android.Fabric
import me.khol.wordbox.di.DaggerAppComponent
import me.khol.wordbox.extensions.isDebug
import timber.log.Timber

/**
 * Application class
 */
class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<App> = DaggerAppComponent.builder().create(this)

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()

        if (isDebug) {
            Stetho.initializeWithDefaults(this)
        }

        if (isDebug) {
            Timber.plant(Timber.DebugTree())
        }

        Timber.plant(CrashlyticsTree())
        Fabric.with(this, Crashlytics())

        AndroidThreeTen.init(this)
    }
}
