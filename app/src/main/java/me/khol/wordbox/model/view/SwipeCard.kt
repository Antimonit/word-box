package me.khol.wordbox.model.view

import android.animation.Animator
import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.view.animation.OvershootInterpolator

@Suppress("MemberVisibilityCanBePrivate")
open class SwipeCard @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : androidx.cardview.widget.CardView(context, attrs, defStyleAttr) {

    companion object {
        private const val ANIMATION_DURATION = 300
        private const val SWIPE_ROTATION = 10f

        private const val INVALID_POINTER = -1
    }

    var initialX: Float = 0f
    var initialY: Float = 0f

    private var downX: Float = 0f
    private var downY: Float = 0f
    private var pointerId: Int = 0
    private var isBeingDragged = false

    private val touchSlop = ViewConfiguration.get(context).scaledTouchSlop
    private val swipeStack: SwipeStack
        get() = parent as SwipeStack? ?: throw Exception("Not attached to parent.")

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) {
            isBeingDragged = false
            return false
        }
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                pointerId = event.getPointerId(0)
                val pointerIndex = event.findPointerIndex(pointerId)
                downX = event.getX(pointerIndex)
                downY = event.getY(pointerIndex)
                isBeingDragged = false
            }
            MotionEvent.ACTION_MOVE -> {
                if (pointerId == INVALID_POINTER) {
                    return false
                }
                val pointerIndex = event.findPointerIndex(pointerId)
                if (pointerIndex == -1) {
                    return false
                }

                val absDeltaX = Math.abs(event.getX(pointerIndex) - downX)
                val absDeltaY = Math.abs(event.getY(pointerIndex) - downY)
                if (absDeltaX > touchSlop || absDeltaY > touchSlop) {
                    parent?.requestDisallowInterceptTouchEvent(true)
                    isBeingDragged = true
                }
            }
            MotionEvent.ACTION_CANCEL -> {
                isBeingDragged = false
            }
            MotionEvent.ACTION_UP -> {
                isBeingDragged = false
            }
        }
        return isBeingDragged
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) {
            return false
        }
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                if (!isEnabled) {
                    return false
                }
                pointerId = event.getPointerId(0)
                val pointerIndex = event.findPointerIndex(pointerId)
                downX = event.getX(pointerIndex)
                downY = event.getY(pointerIndex)
                swipeStack.swipeProgressListener?.onSwipeStart(this)
            }
            MotionEvent.ACTION_MOVE -> {
                if (pointerId == INVALID_POINTER) {
                    return true
                }
                val pointerIndex = event.findPointerIndex(pointerId)
                if (pointerIndex == -1) {
                    return true
                }

                val deltaX = event.getX(pointerIndex) - downX
                val deltaY = event.getY(pointerIndex) - downY

                val absDeltaX = Math.abs(deltaX)
                val absDeltaY = Math.abs(deltaY)
                if (!isBeingDragged && (absDeltaX > touchSlop || absDeltaY > touchSlop)) {
                    parent?.requestDisallowInterceptTouchEvent(true)
                    isBeingDragged = true
                }

                if (isBeingDragged) {
                    x += deltaX
                    y += deltaY

                    val swipeProgressX = Math.min(Math.max((x - initialX) / swipeStack.width, -1f), 1f)
                    val swipeProgressY = Math.min(Math.max((y - initialY) / swipeStack.height, -1f), 1f)

                    swipeStack.swipeProgressListener?.onSwipeProgress(this, swipeProgressX, swipeProgressY)

                    rotation = SWIPE_ROTATION * swipeProgressX
                }
            }
            MotionEvent.ACTION_CANCEL -> {
                if (isBeingDragged) {
                    pointerId = INVALID_POINTER
                    isBeingDragged = false
                }
            }
            MotionEvent.ACTION_UP -> {
                if (isBeingDragged) {
                    pointerId = INVALID_POINTER
                    isBeingDragged = false

                    swipeStack.swipeProgressListener?.onSwipeEnd(this)

                    if (!isEnabled) {
                        resetViewPosition()
                        return false
                    }

                    val progressX = x / width
                    val progressY = y / height

                    val progress = SwipeDirection.of(progressX, progressY)
                    if (progress.progress > 1f && swipeStack.isAllSwipeAllowed()) {
                        when (progress) {
                            is SwipeDirection.LEFT -> swipeViewToLeft(ANIMATION_DURATION)
                            is SwipeDirection.RIGHT -> swipeViewToRight(ANIMATION_DURATION)
                            is SwipeDirection.TOP -> swipeViewToTop(ANIMATION_DURATION)
                            is SwipeDirection.NONE -> resetViewPosition()
                        }
                    } else {
                        resetViewPosition()
                    }
                }
            }
        }
        return true
    }

    private fun resetViewPosition() {
        animate()
            .x(initialX)
            .y(initialY)
            .rotation(0f)
            .alpha(1f)
            .setDuration(ANIMATION_DURATION.toLong())
            .setInterpolator(OvershootInterpolator(1.4f))
            .setListener(null)
    }

    private fun swipeView(duration: Int,
        horizontal: Boolean,
        offset: Float,
        rotation: Float,
        onAnimationEnd: () -> Unit
    ) {
        if (!isEnabled) {
            return
        }
        isEnabled = false
        animate().cancel()
        animate()
            .apply {
                if (horizontal) {
                    x(offset)
                } else {
                    y(offset)
                }
            }
            .rotation(rotation)
            .alpha(0f)
            .setDuration(duration.toLong())
            .setListener(object : SimpleAnimationListener() {
                override fun onAnimationEnd(animation: Animator) {
                    onAnimationEnd()
                }
            })
    }

    fun swipeViewToTop(duration: Int = ANIMATION_DURATION) {
        swipeView(duration, false, -swipeStack.height + y, 0f) {
            swipeStack.onViewSwipedToTop()
        }
    }

    fun swipeViewToLeft(duration: Int = ANIMATION_DURATION) {
        swipeView(duration, true, -swipeStack.width + x, -SWIPE_ROTATION) {
            swipeStack.onViewSwipedToLeft()
        }
    }

    fun swipeViewToRight(duration: Int = ANIMATION_DURATION) {
        swipeView(duration, true, swipeStack.width + x, SWIPE_ROTATION) {
            swipeStack.onViewSwipedToRight()
        }
    }

    interface SwipeProgressListener {
        fun onSwipeStart(view: View)
        fun onSwipeProgress(view: View, progressX: Float, progressY: Float)
        fun onSwipeEnd(view: View)
    }
}

