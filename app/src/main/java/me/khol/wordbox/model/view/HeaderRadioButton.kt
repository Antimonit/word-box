package me.khol.wordbox.model.view

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.StateListDrawable
import android.view.Gravity
import android.widget.RadioButton
import me.khol.wordbox.R
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.headerItemDrawable
import me.khol.wordbox.extensions.selectableDrawable
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.singleLine
import org.jetbrains.anko.textColor
import org.jetbrains.anko.withAlpha

class HeaderRadioButton(
    context: Context
) : RadioButton(context) {

    init {
        buttonDrawable = StateListDrawable()
        backgroundDrawable = selectableDrawable(
            headerItemDrawable(),
            Color.WHITE.withAlpha(50)
        )
        textColor = Color.WHITE
        gravity = Gravity.CENTER
        textSize = 16f
        singleLine = true
        font = R.font.roboto_medium
    }
}
