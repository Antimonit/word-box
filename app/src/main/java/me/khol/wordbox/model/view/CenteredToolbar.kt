package me.khol.wordbox.model.view

import android.content.Context
import android.graphics.Color
import android.text.TextUtils
import android.view.Gravity
import androidx.appcompat.widget.Toolbar
import me.khol.wordbox.R
import me.khol.wordbox.extensions.font
import org.jetbrains.anko.appcompat.v7._Toolbar
import org.jetbrains.anko.singleLine
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView
import org.jetbrains.anko.wrapContent

/**
 * Just like [Toolbar] but has title text horizontally centered.
 */
open class CenteredToolbar(
    context: Context
) : _Toolbar(context) {

    private val txtTitle = textView {
        textSize = 20f
        textColor = Color.WHITE
        font = R.font.roboto_medium
        singleLine = true
        ellipsize = TextUtils.TruncateAt.END
    }.lparams(wrapContent, wrapContent) {
        gravity = Gravity.CENTER
    }

    override fun setTitle(title: CharSequence?) {
        txtTitle.text = title
    }
}
