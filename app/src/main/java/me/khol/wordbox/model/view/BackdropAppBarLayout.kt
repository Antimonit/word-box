package me.khol.wordbox.model.view

import android.content.Context
import android.os.Build
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import me.khol.wordbox.extensions.targetApi
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.design._AppBarLayout

/**
 * AppBarLayout that behaves as part of Backdrop material component.
 */
open class BackdropAppBarLayout(
    context: Context
) : _AppBarLayout(context), CoordinatorLayout.AttachedBehavior {

    private var expanded: Boolean = false

    var onExpanded: (Boolean) -> Unit = {}

    init {
        // Backdrop has zero elevation - remove background and shadow effect
        backgroundDrawable = null
        if (targetApi(Build.VERSION_CODES.LOLLIPOP)) {
            outlineProvider = null    // disables elevation when collapsed
        }
    }

    fun isExpanded(): Boolean {
        return expanded
    }

    override fun setExpanded(expanded: Boolean) {
        setExpanded(expanded, isExpanded() != expanded)
    }

    fun toggleExpanded() {
        setExpanded(!isExpanded())
    }

    override fun setExpanded(expanded: Boolean, animate: Boolean) {
        this.expanded = expanded
        onExpanded(expanded)
        super.setExpanded(expanded, animate)
    }

    override fun getBehavior(): CoordinatorLayout.Behavior<AppBarLayout> {
        // Disable dragging of the AppBarLayout
        // Allow opening and closing only by clicking on the AppBar header
        return AppBarLayout.Behavior().apply {
            setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
                override fun canDrag(appBarLayout: AppBarLayout): Boolean {
                    return false
                }
            })
        }
    }
}
