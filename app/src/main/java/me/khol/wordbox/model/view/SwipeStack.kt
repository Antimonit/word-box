/*
 * Copyright (C) 2016 Frederik Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package me.khol.wordbox.model.view

import android.content.Context
import android.database.DataSetObserver
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.FrameLayout
import me.khol.wordbox.R
import me.khol.wordbox.base.extensions.dp
import java.util.*

@Suppress("MemberVisibilityCanBePrivate")
open class SwipeStack @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attrs, defStyleAttr) {

    companion object {
        private const val KEY_SUPER_STATE = "superState"
        private const val KEY_CURRENT_INDEX = "currentIndex"
        private const val TAG_NEW_VIEW = R.id.new_view

        const val SWIPE_NONE = 0
        const val SWIPE_LEFT = 1
        const val SWIPE_RIGHT = 2
        const val SWIPE_HORIZONTAL = SWIPE_LEFT or SWIPE_RIGHT
        const val SWIPE_TOP = 4
        const val SWIPE_ALL = SWIPE_HORIZONTAL or SWIPE_TOP
    }

    fun isAllSwipeAllowed() = allowedSwipeDirection and SWIPE_ALL == SWIPE_ALL

    fun isTopSwipeAllowed() = allowedSwipeDirection and SWIPE_TOP == SWIPE_TOP

    fun isLeftSwipeAllowed() = allowedSwipeDirection and SWIPE_LEFT == SWIPE_LEFT

    fun isRightSwipeAllowed() = allowedSwipeDirection and SWIPE_RIGHT == SWIPE_RIGHT

    /**
     * The Adapter which is responsible for maintaining the data backing this list and for
     * producing a view to represent an item in that data set.
     */
    var adapter: Adapter? = null
        set(value) {
            field?.unregisterDataSetObserver(dataObserver)
            field = value
            field?.registerDataSetObserver(dataObserver)
        }

    protected var dataObserver = object : DataSetObserver() {
        override fun onChanged() {
            super.onChanged()
            invalidate()
            requestLayout()
        }
    }

    private var random = Random()

    var allowedSwipeDirection = SWIPE_NONE
    var numberOfStackedViews = 3
    var currentViewIndex = 0
    var viewSpacing = context.dp(12)
    var viewRotation = 8
    var scaleFactor = 1f
    var disableHwAcceleration = false

    private var isFirstLayout = true

    /**
     * The view from the top of the stack or null if empty.
     */
    var topView: SwipeCard? = null
        private set

    /**
     * Register a callback to be invoked when the user has swiped the top view
     * left / right or when the stack gets empty.
     */
    var listener: SwipeStackListener? = null

    /**
     * Register a callback to be invoked when the user starts / stops interacting
     * with the top view of the stack.
     */
    var swipeProgressListener: SwipeCard.SwipeProgressListener? = null

    /**
     * Current adapter position.
     */
    val currentPosition: Int
        get() = currentViewIndex - childCount

    init {
        clipToPadding = false
        clipChildren = false
    }

    public override fun onSaveInstanceState(): Parcelable? {
        return Bundle().apply {
            putParcelable(KEY_SUPER_STATE, super.onSaveInstanceState())
            putInt(KEY_CURRENT_INDEX, currentViewIndex - childCount)
        }
    }

    public override fun onRestoreInstanceState(state: Parcelable?) {
        (state as Bundle).let {
            currentViewIndex = it.getInt(KEY_CURRENT_INDEX)
            super.onRestoreInstanceState(it.getParcelable(KEY_SUPER_STATE))
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        if (adapter == null || adapter!!.isEmpty) {
            currentViewIndex = 0
            removeAllViewsInLayout()
            return
        }

        var childIndex = childCount
        while (childIndex < numberOfStackedViews && currentViewIndex < adapter!!.count) {
            addNextView()
            childIndex++
        }

        for (x in 0 until childIndex) {
            val childView = getChildAt(x) as SwipeCard
            val topViewIndex = childIndex - 1

            val distanceToViewAbove = topViewIndex * viewSpacing - x * viewSpacing
            val newPositionX = (width - childView.measuredWidth) / 2
            val newPositionY = distanceToViewAbove + paddingTop

            childView.layout(
                newPositionX,
                paddingTop,
                newPositionX + childView.measuredWidth,
                paddingTop + childView.measuredHeight)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                childView.translationZ = x.toFloat()
            }

            val isNewView = childView.getTag(TAG_NEW_VIEW) as Boolean
            val scaleFactor = Math.pow(scaleFactor.toDouble(), (childIndex - x - 1).toDouble()).toFloat()

            if (x == topViewIndex) {
                topView?.isEnabled = false
                topView = childView
                childView.isEnabled = true
                childView.initialX = newPositionX.toFloat()
                childView.initialY = newPositionY.toFloat()
            }

            if (!isFirstLayout) {

                if (isNewView) {
                    childView.setTag(TAG_NEW_VIEW, false)
                    childView.alpha = 0f
                    childView.y = newPositionY.toFloat()
                    childView.scaleY = scaleFactor
                    childView.scaleX = scaleFactor
                }

                childView.animate()
                    .y(newPositionY.toFloat())
                    .scaleX(scaleFactor)
                    .scaleY(scaleFactor)
                    .alpha(1f)
                    .duration = 300L

            } else {
                childView.setTag(TAG_NEW_VIEW, false)
                childView.y = newPositionY.toFloat()
                childView.scaleY = scaleFactor
                childView.scaleX = scaleFactor
            }
        }

        isFirstLayout = false
    }

    private fun addNextView() {
        if (currentViewIndex < adapter!!.count) {
            val bottomView = adapter!!.getView(currentViewIndex, null, this)
            bottomView.setTag(TAG_NEW_VIEW, true)

            if (!disableHwAcceleration) {
                bottomView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
            }

            if (viewRotation > 0) {
                bottomView.rotation = (random.nextInt(viewRotation) - viewRotation / 2).toFloat()
            }

            val width = width - (paddingLeft + paddingRight)
            val height = height - (paddingTop + paddingBottom)

            val params = bottomView.layoutParams ?: ViewGroup.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT
            )

            val measureSpecWidth = if (params.width == ViewGroup.LayoutParams.MATCH_PARENT) {
                View.MeasureSpec.EXACTLY
            } else {
                View.MeasureSpec.AT_MOST
            }

            val measureSpecHeight = if (params.height == ViewGroup.LayoutParams.MATCH_PARENT) {
                View.MeasureSpec.EXACTLY
            } else {
                View.MeasureSpec.AT_MOST
            }

            bottomView.measure(measureSpecWidth or width, measureSpecHeight or height)

            addViewInLayout(bottomView, 0, params, true)

            currentViewIndex++
        }
    }

    private fun removeTopView() {
        if (topView != null) {
            // removing a view automatically calls request layout and invalidate
            removeView(topView)
            topView = null
        }

        if (childCount == 0) {
            if (listener != null) {
                listener!!.onStackEmpty()
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        measureChildren(widthMeasureSpec, heightMeasureSpec)
        val width = View.MeasureSpec.getSize(widthMeasureSpec)
        val height = View.MeasureSpec.getSize(heightMeasureSpec)
        setMeasuredDimension(width, height)
    }

    fun onViewSwipedToTop() {
        listener?.onViewSwipedToTop(currentPosition)
        removeTopView()
    }

    fun onViewSwipedToLeft() {
        listener?.onViewSwipedToLeft(currentPosition)
        removeTopView()
    }

    fun onViewSwipedToRight() {
        listener?.onViewSwipedToRight(currentPosition)
        removeTopView()
    }

    /**
     * Programmatically dismiss the top view to the top.
     */
    fun swipeTopViewToTop() {
        if (childCount == 0) return
        topView?.swipeViewToTop()
    }

    /**
     * Programmatically dismiss the top view to the right.
     */
    fun swipeTopViewToRight() {
        if (childCount == 0) return
        topView?.swipeViewToRight()
    }

    /**
     * Programmatically dismiss the top view to the left.
     */
    fun swipeTopViewToLeft() {
        if (childCount == 0) return
        topView?.swipeViewToLeft()
    }

    /**
     * Resets the current adapter position and repopulates the stack.
     */
    fun resetStack() {
        currentViewIndex = 0
        removeAllViewsInLayout()
        requestLayout()
    }

    /**
     * Interface definition for a callback to be invoked when the top view was
     * swiped to the left / right or when the stack gets empty.
     */
    interface SwipeStackListener {
        /**
         * Called when a view has been dismissed to the top.
         *
         * @param position The position of the view in the adapter currently in use.
         */
        fun onViewSwipedToTop(position: Int)

        /**
         * Called when a view has been dismissed to the left.
         *
         * @param position The position of the view in the adapter currently in use.
         */
        fun onViewSwipedToLeft(position: Int)

        /**
         * Called when a view has been dismissed to the right.
         *
         * @param position The position of the view in the adapter currently in use.
         */
        fun onViewSwipedToRight(position: Int)

        /**
         * Called when the last view has been dismissed.
         */
        fun onStackEmpty()
    }
}

