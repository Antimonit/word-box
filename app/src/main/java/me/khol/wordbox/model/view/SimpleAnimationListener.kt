package me.khol.wordbox.model.view

import android.animation.Animator

open class SimpleAnimationListener : Animator.AnimatorListener {
    override fun onAnimationEnd(animation: Animator) {}
    override fun onAnimationStart(animation: Animator) {}
    override fun onAnimationCancel(animation: Animator) {}
    override fun onAnimationRepeat(animation: Animator) {}
}
