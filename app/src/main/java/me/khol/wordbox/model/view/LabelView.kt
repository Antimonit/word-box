package me.khol.wordbox.model.view

import android.R.attr.state_activated
import android.R.attr.state_enabled
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.view.Gravity
import android.widget.TextView
import androidx.annotation.ColorInt
import me.khol.wordbox.R
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.extensions.font
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.singleLine

/**
 * Static view showing name and color of a label.
 */
class LabelView constructor(
    context: Context
) : TextView(context) {

    companion object {
        private val STATE_ON_ENABLED = intArrayOf(state_activated, state_enabled)
        private val STATE_OFF_ENABLED = intArrayOf(state_enabled)
    }

    var cornerRadius: Float = dpf(8)
        set(value) {
            field = value
            updateBackground()
        }

    var borderWidth: Int = dp(1f)
        set(value) {
            field = value
            updateBackground()
        }

    var color: Int = Color.BLACK
        set(value) {
            field = value
            updateBackground()
        }

    init {
        gravity = Gravity.CENTER
        textSize = 10f
        isActivated = true
        horizontalPadding = dp(4)
        font = R.font.roboto
        singleLine = true
        setTextColor(textColorList())
        minWidth = dp(32)
        minHeight = dp(16)

        updateBackground()
    }

    private fun updateBackground() {
        backgroundDrawable = labelDrawable(color, cornerRadius, borderWidth)
    }

    private fun labelDrawable(
        @ColorInt color: Int,
        radius: Float,
        border: Int
    ): Drawable {
        val enabled = GradientDrawable().apply {
            cornerRadius = radius
            setStroke(border, color)
            setColor(Color.WHITE)
        }

        val activatedEnabled = GradientDrawable().apply {
            cornerRadius = radius
            setStroke(border, Color.WHITE)
            setColor(color)
        }

        return StateListDrawable().apply {
            addState(STATE_ON_ENABLED, activatedEnabled)
            addState(STATE_OFF_ENABLED, enabled)
        }
    }

    private fun textColorList(): ColorStateList {
        return ColorStateList(
            arrayOf(STATE_ON_ENABLED, STATE_OFF_ENABLED),
            intArrayOf(Color.WHITE, Color.BLACK)
        )
    }
}
