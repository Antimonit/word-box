package me.khol.wordbox.model.repository

import android.content.Context
import io.reactivex.Completable
import me.khol.wordbox.extensions.ZipUtils
import me.khol.wordbox.extensions.observeOnMainThread
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.WordBoxDatabaseWrapper
import java.io.File
import java.util.*
import java.util.zip.ZipException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * The repository allows to [export] and [import] the database to and from a file stored in
 * an external storage. The database needs to be closed (or at least flushed) when exporting the
 * data. It also needs to be closed when importing the data.
 */
@Singleton
class DatabaseRepository @Inject constructor(
    private val context: Context,
    private val database: WordBoxDatabaseWrapper
) {

    companion object {
        const val METADATA_VERSION = "VERSION"
        const val METADATA_WORD_COUNT = "WORD_COUNT"
        const val METADATA_TIMESTAMP = "TIMESTAMP"
    }

    private fun getDatabaseMetadata(): Properties {
        val stats = database.statsDao()
        return Properties().apply {
            // TODO: define more metadata
            setProperty(METADATA_VERSION, WordBoxDatabaseWrapper.VERSION.toString())
            setProperty(METADATA_WORD_COUNT, stats.getTotalWordCount().toString())
            setProperty(METADATA_TIMESTAMP, System.currentTimeMillis().toString())
        }
    }

    fun import(backup: File): Completable {
        return Completable.create { emitter ->
            try {
                val current = context.getDatabasePath(WordBoxDatabaseWrapper.NAME)
                ZipUtils.unzipDatabase(backup, current)
                emitter.onComplete()
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
            .subscribeOnIO()
            .doOnSubscribe { database.closeDatabase() }
            .doOnTerminate { database.openDatabase() }
            .observeOnMainThread()
    }

    fun export(backup: File): Completable {
        return Completable.create { emitter ->
            try {
                val current = context.getDatabasePath(WordBoxDatabaseWrapper.NAME)
                ZipUtils.zipDatabaseWithMetadata(current, getDatabaseMetadata(), backup)
                emitter.onComplete()
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
            .subscribeOnIO()
            .doOnSubscribe { database.closeDatabase() }
            .doOnTerminate { database.openDatabase() }
            .observeOnMainThread()
    }

    fun metadata(backup: File): Properties? {
        return try {
            ZipUtils.unzipMetadata(backup)
        } catch (ex: ZipException) {
            null
        }
    }
}
