package me.khol.wordbox.model.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.dao.LanguageDao
import me.khol.wordbox.database.model.Language
import javax.inject.Inject

class LanguageRepository @Inject constructor(
    private val languageDao: LanguageDao
) {

    fun observeAllLanguages(): Flowable<List<Language>> = languageDao.getAllLanguages()

    fun getLanguageWordCount(languageId: Long) = languageDao.getLanguageWordCount(languageId)

    fun insertLanguage(language: Language): Completable {
        return Completable.fromAction {
            languageDao.insert(language)
        }.subscribeOnIO()
    }

    fun deleteLanguage(language: Language): Completable {
        return Completable.fromAction {
            languageDao.delete(language.languageId)
        }.subscribeOnIO()
    }

    fun updateLanguage(language: Language): Completable {
        return Completable.fromAction {
            languageDao.update(language)
        }.subscribeOnIO()
    }

    fun updateLanguages(languages: List<Language>): Completable {
        return Completable.fromAction {
            languageDao.update(languages)
        }.subscribeOnIO()
    }
}
