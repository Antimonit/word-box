package me.khol.wordbox.model.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.dao.LabelDao
import me.khol.wordbox.database.model.Label
import javax.inject.Inject

class LabelRepository @Inject constructor(
    private val labelDao: LabelDao
) {

    fun observeAllLabels(): Flowable<List<Label>> = labelDao.getAllLabels()

    fun getLabelWordCount(labelId: Long) = labelDao.getLabelWordCount(labelId)

    fun insertLabel(label: Label): Completable {
        return Completable.fromAction {
            labelDao.insert(label)
        }.subscribeOnIO()
    }

    fun deleteLabel(label: Label): Completable {
        return Completable.fromAction {
            labelDao.delete(label.labelId)
        }.subscribeOnIO()
    }

    fun updateLabel(label: Label): Completable {
        return Completable.fromAction {
            labelDao.update(label)
        }.subscribeOnIO()
    }

    fun updateLabels(labels: List<Label>): Completable {
        return Completable.fromAction {
            labelDao.update(labels)
        }.subscribeOnIO()
    }
}
