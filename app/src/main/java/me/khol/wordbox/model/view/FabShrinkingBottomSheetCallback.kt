package me.khol.wordbox.model.view

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import me.khol.wordbox.extensions.animateScaleAppear
import me.khol.wordbox.extensions.animateScaleDisappear

/**
 * Created by David Khol [david@khol.me] on 30. 7. 2017.
 */
class FabShrinkingBottomSheetCallback(
    private val translateFab: FloatingActionButton,
    private val addFab: FloatingActionButton
) : BottomSheetBehavior.BottomSheetCallback() {

    private var shrinking: Boolean = false

    override fun onStateChanged(bottomSheet: View, newState: Int) {
    }

    override fun onSlide(bottomSheet: View, slideOffset: Float) {
        if (slideOffset <= 0f) {
            if (shrinking) {
                shrinking = false
                showFab()
            }
        } else {
            if (!shrinking) {
                shrinking = true
                hideFab()
            }
        }
    }

    private fun showFab() {
        translateFab.animateScaleAppear(onStart = { visibility = View.VISIBLE })
        addFab.animateScaleAppear(onStart = { visibility = View.VISIBLE })
    }

    private fun hideFab() {
        translateFab.animateScaleDisappear(onEnd = { visibility = View.INVISIBLE })
        addFab.animateScaleDisappear(onEnd = { visibility = View.INVISIBLE })
    }

}
