package me.khol.wordbox.model.repository

import io.reactivex.Completable
import io.reactivex.Single
import me.khol.wordbox.database.WordBoxDatabaseWrapper
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.dao.ExampleDao
import me.khol.wordbox.database.dao.WordDao
import me.khol.wordbox.database.dao.WordLabelDao
import me.khol.wordbox.database.dao.WordWordDao
import me.khol.wordbox.database.model.WordDetail
import org.threeten.bp.Instant
import javax.inject.Inject

class WordRepository @Inject constructor(
    private val database: WordBoxDatabaseWrapper,
    private val wordDao: WordDao,
    private val wordWordDao: WordWordDao,
    private val wordLabelDao: WordLabelDao,
    private val exampleDao: ExampleDao
) {

    fun getWordDetail(wordId: Long) = wordDao.getWordDetail(wordId).subscribeOnIO()

    fun getWordSuggestions(query: String, filterIds: List<Long>) = wordDao.getWordSuggestions("$query%", filterIds)

    fun insertWord(word: WordDetail): Single<Long> {
        return Single.fromCallable {
            database.runInTransaction<Long> {
                val wordId = wordDao.insert(word)

                wordLabelDao.insert(wordId, word.labels)

                exampleDao.insert(wordId, word.examples)

                wordWordDao.insertBothWays(wordId, word.translations)

                wordId
            }
        }.subscribeOnIO()
    }

    fun updateWord(word: WordDetail): Completable {
        return Completable.fromAction {
            database.runInTransaction {
                wordDao.updateAfterEdit(
                    word.wordId,
                    word.language.languageId,
                    word.text,
                    word.description,
                    timeModified = Instant.now(),
                    scheduledTime = word.scheduledTime,
                    lastTime = word.lastTime,
                    interval = word.interval,
                    averageEaseFactor = word.averageEaseFactor,
                    averageSuccessRate = word.averageSuccessRate,
                    easeFactor = word.easeFactor,
                    practiceRounds = word.practiceRounds
                )

                wordLabelDao.delete(word.wordId)
                wordLabelDao.insert(word.wordId, word.labels)

                exampleDao.delete(word.wordId)
                exampleDao.insert(word.wordId, word.examples)

                wordWordDao.deleteBothWays(word.wordId)
                wordWordDao.insertBothWays(word.wordId, word.translations)
            }
        }.subscribeOnIO()
    }

    fun deleteWord(wordId: Long): Completable {
        // automatically cascade deletes labels, examples and translations
        return wordDao.delete(wordId).subscribeOnIO()
    }
}
