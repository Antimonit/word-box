package me.khol.wordbox.model.view

import android.content.Context
import org.jetbrains.anko.design._CollapsingToolbarLayout

/**
 * ToolbarLayout that behaves as part of Backdrop material component.
 */
open class BackdropToolbarLayout(
    context: Context
) : _CollapsingToolbarLayout(context) {

    init {
        // Disables title animation when expanded
        isTitleEnabled = false
    }
}
