package me.khol.wordbox.model.view

import android.content.Context
import androidx.appcompat.widget.AppCompatButton
import me.khol.wordbox.extensions.languagesButtonStyle
import me.khol.wordbox.database.model.Language

/**
 * Cycles through languages on click
 */
open class LanguagesButton(context: Context) : AppCompatButton(context) {

    var languages: List<Language> = emptyList()

    /**
     * When searching for index of [Language.invalid] in the [languages], -1 is returned because it
     * cannot be found. But that is okay, because on button click we increment the index to 0, which
     * is the first element of [languages].
     */
    private var selectedLanguageIndex
        get() = languages.indexOf(selectedLanguage)
        set(value) {
            selectedLanguage = languages[value]
        }

    var selectedLanguage: Language = Language.invalid
        set(value) {
            field = value
            onLanguageChanged?.invoke(value)
            syncText()
        }

    var onLanguageChanged: ((Language) -> Unit)? = null

    init {
        languagesButtonStyle()
        syncText()
    }

    override fun performClick(): Boolean {
        if (languages.isNotEmpty()) {
            selectedLanguageIndex = (selectedLanguageIndex + 1) % languages.size
        }
        return super.performClick()
    }

    private fun syncText() {
        text = selectedLanguage.shortName
    }
}
