package me.khol.wordbox.model.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.dao.NoteDao
import me.khol.wordbox.database.model.NoteDetail
import org.threeten.bp.Instant
import javax.inject.Inject

class NoteRepository @Inject constructor(
    private val noteDao: NoteDao
) {

    fun getNoteDetail(noteId: Long): Flowable<NoteDetail> {
        return noteDao.getNoteDetail(noteId).subscribeOnIO()
    }

    fun insertNote(note: NoteDetail): Single<Long> {
        return noteDao.insert(note).subscribeOnIO()
    }

    fun updateNote(note: NoteDetail): Completable {
        return noteDao.updateAfterEdit(
            note.noteId,
            note.title,
            note.contents,
            timeModified = Instant.now()
        ).subscribeOnIO()
    }

    fun deleteNote(noteId: Long): Completable {
        return noteDao.delete(noteId).subscribeOnIO()
    }
}


