package me.khol.wordbox.model.view

import android.R.attr.state_checked
import android.R.attr.state_enabled
import android.R.attr.state_pressed
import android.animation.ObjectAnimator
import android.animation.StateListAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.os.Build
import android.view.Gravity
import android.widget.ToggleButton
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import me.khol.wordbox.R
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.extensions.font
import me.khol.wordbox.extensions.lighter
import me.khol.wordbox.extensions.rippleDrawable
import me.khol.wordbox.extensions.targetApi
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.horizontalPadding
import org.jetbrains.anko.singleLine

/**
 * Interactive button showing name and color of label that can be toggled on and off.
 */
class LabelButton constructor(
    context: Context
) : ToggleButton(context) {

    companion object {
        private val STATE_ON_PRESSED = intArrayOf(state_checked, state_pressed)
        private val STATE_ON_ENABLED = intArrayOf(state_checked, state_enabled)
        private val STATE_OFF_PRESSED = intArrayOf(state_pressed)
        private val STATE_OFF_ENABLED = intArrayOf(state_enabled)
    }

    var cornerRadius: Float = dpf(12)
        set(value) {
            field = value
            updateBackground()
        }

    var borderWidth: Int = dp(2f)
        set(value) {
            field = value
            updateBackground()
        }

    var accentColor: Int = Color.BLACK
        set(value) {
            field = value
            updateBackground()
            updateTextColor()
        }

    var backgroundColor: Int = Color.WHITE
        @JvmName("backgroundColor")
        set(value) {
            field = value
            updateBackground()
            updateTextColor()
        }

    var textColor: Int = Color.BLACK
        @JvmName("textColor")
        set(value) {
            field = value
            updateBackground()
            updateTextColor()
        }

    init {
        gravity = Gravity.CENTER
        textSize = 13f
        font = R.font.roboto_medium
        singleLine = true
        horizontalPadding = dp(12)
        minWidth = dp(32)
        minHeight = dp(16)
        minimumWidth = dp(32)
        minimumHeight = dp(16)

        updateAnimator()
        updateBackground()
        updateTextColor()
    }

    private fun updateAnimator() {
        if (targetApi(Build.VERSION_CODES.LOLLIPOP)) {
            stateListAnimator = StateListAnimator().apply {
                val duration = 300L
                val targetElevation = context.dpf(4)

                // Checked
                addState(
                    intArrayOf(state_enabled, state_checked),
                    ObjectAnimator.ofFloat(this@LabelButton, "elevation", targetElevation).setDuration(duration)
                )

                // Pressed
                addState(
                    intArrayOf(state_enabled, state_pressed),
                    ObjectAnimator.ofFloat(this@LabelButton, "elevation", targetElevation).setDuration(duration)
                )

                // Unpressed
                addState(
                    intArrayOf(state_enabled, state_enabled, -state_pressed),
                    ObjectAnimator.ofFloat(this@LabelButton, "elevation", 0f).setDuration(duration)
                )

                // Disabled state
                addState(
                    IntArray(0),
                    ObjectAnimator.ofFloat(this@LabelButton, "elevation", 0f).setDuration(0)
                )
            }
        } else {
            // do nothing
        }
    }

    private fun updateBackground() {
        backgroundDrawable = if (targetApi(Build.VERSION_CODES.LOLLIPOP)) {
            labelRippleDrawable(accentColor, cornerRadius, borderWidth)
        } else {
            labelLegacyDrawable(accentColor, cornerRadius, borderWidth)
        }
    }

    private fun updateTextColor() {
        setTextColor(ColorStateList(
            arrayOf(STATE_ON_ENABLED, STATE_OFF_ENABLED),
            intArrayOf(Color.WHITE, textColor)
        ))
    }

    private fun labelLegacyDrawable(
        @ColorInt color: Int,
        radius: Float,
        border: Int
    ): Drawable {
        val enabled = GradientDrawable().apply {
            cornerRadius = radius
            setStroke(border, color)
            setColor(Color.WHITE)
        }

        val pressed = GradientDrawable().apply {
            cornerRadius = radius
            setStroke(border, color)
            setColor(color.lighter.lighter)
        }

        val checkedEnabled = GradientDrawable().apply {
            cornerRadius = radius
            setStroke(border, color)
            setColor(color)
        }

        val checkedPressed = GradientDrawable().apply {
            cornerRadius = radius
            setStroke(border, Color.TRANSPARENT)
            setColor(color.lighter.lighter)
        }

        return StateListDrawable().apply {
            addState(STATE_ON_PRESSED, checkedPressed)
            addState(STATE_ON_ENABLED, checkedEnabled)
            addState(STATE_OFF_PRESSED, pressed)
            addState(STATE_OFF_ENABLED, enabled)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun labelRippleDrawable(
        @ColorInt color: Int,
        radius: Float,
        border: Int
    ): Drawable {
        val enabled = GradientDrawable().apply {
            cornerRadius = radius
            setStroke(border, color)
            setColor(backgroundColor)
        }
        val checkedEnabled = GradientDrawable().apply {
            cornerRadius = radius
            setColor(color)
        }

        return rippleDrawable(
            color.lighter.lighter,
            StateListDrawable().apply {
                addState(STATE_ON_ENABLED, checkedEnabled)
                addState(STATE_OFF_ENABLED, enabled)
            }
        )
    }

    override fun setText(text: CharSequence?, type: BufferType?) {
        super.setText(text, type)
        textOff = text
        textOn = text
    }
}
