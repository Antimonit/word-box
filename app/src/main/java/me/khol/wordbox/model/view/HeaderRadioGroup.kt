package me.khol.wordbox.model.view

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import androidx.annotation.Px
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.base.extensions.dpf
import me.khol.wordbox.extensions.elevationCompat
import me.khol.wordbox.database.model.Language
import org.jetbrains.anko._RadioGroup
import org.jetbrains.anko.padding

/**
 * Contains several items laid out horizontally. When changing selected item, background is animated
 * by translating selection highlight to the newly selected item.
 */
open class HeaderRadioGroup(
    context: Context
) : _RadioGroup(context) {

    companion object {
        // Background selector animation durations
        private const val animationDuration = 300L
        private const val animationDelay = 100L
    }

    @Px
    private val margin = dp(8)

    private var checkedId: Int = View.NO_ID
    private val checkedView: View?
        get() = findViewById(checkedId)

    private var onCheckedChangeListener: OnCheckedChangeListener? = null

    private val selectorBounds = RectF()
    private val selectorCornerRadius = dpf(8)
    private val selectorPaint = Paint().apply {
        color = Color.WHITE
    }

    private var currentAnimation: Animator? = null

    private var checkedExternally = false

    init {
        orientation = LinearLayout.HORIZONTAL
        elevationCompat = dpf(0)
        padding = dp(16)

        /**
         * We make use of onCheckedListener to get hold of current checkedId.
         * Every time an item is selected, try to animate background selector
         */
        super.setOnCheckedChangeListener { group, newCheckedId ->
            checkChanged(checkedId, newCheckedId)
            checkedId = newCheckedId
            if (!checkedExternally) {
                onCheckedChangeListener?.onCheckedChanged(group, newCheckedId)
            }
        }

        setWillNotDraw(false)
    }

    override fun setOnCheckedChangeListener(listener: OnCheckedChangeListener?) {
        // We make use of onCheckedListener internally. Don't pass the listener to super class!
        onCheckedChangeListener = listener
    }

    open fun checkLanguage(selectedLanguage: Language) {
        checkedExternally = true
        findViewWithTag<HeaderRadioButton>(selectedLanguage)?.isChecked = true
        checkedExternally = false
    }

    open fun checkChanged(old: Int, new: Int) {
        val startView: View? = findViewById(old)
        val endView: View? = findViewById(new)

        // cancel any animation currently in progress, if any
        currentAnimation?.cancel()

        if (startView == null && endView == null) {
            // don't draw the background selector
            selectorBounds.set(0f, 0f, 0f, 0f)
            invalidate()

        } else if (startView == null || endView == null) {
            // draw the background selector behind it
            (startView ?: endView)!!.also {
                selectorBounds.set(
                    it.x - margin,
                    it.y - margin,
                    it.x + it.width + margin,
                    it.y + it.height + margin
                )
                invalidate()
            }

        } else {
            // draw and animate the background selector from the first one to the other one
            selectorBounds.top = startView.y - margin
            selectorBounds.bottom = startView.y + startView.height + margin

            val delayRight = if (startView.x - endView.x > 0) animationDelay else 0L
            val delayLeft = if (startView.x - endView.x < 0) animationDelay else 0L

            val left = ValueAnimator.ofFloat(selectorBounds.left, endView.x - margin).apply {
                startDelay = delayLeft
                addUpdateListener {
                    selectorBounds.left = it.animatedValue as Float
                    invalidate()
                }
            }

            val right = ValueAnimator.ofFloat(selectorBounds.right, endView.x + endView.width + margin).apply {
                startDelay = delayRight
                addUpdateListener {
                    selectorBounds.right = it.animatedValue as Float
                    invalidate()
                }
            }

            currentAnimation = AnimatorSet().apply {
                duration = animationDuration
                interpolator = AccelerateDecelerateInterpolator()
                playTogether(listOf(left, right))
                start()
            }
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        // When the radio group is first initialized, it incorrectly measures
        // the checked view as zero size. Wait for the layout pass and measure
        // it again.
        checkedView?.also {
            selectorBounds.set(
                it.x - margin,
                it.y - margin,
                it.x + it.width + margin,
                it.y + it.height + margin
            )
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawRoundRect(
            selectorBounds,
            selectorCornerRadius,
            selectorCornerRadius,
            selectorPaint
        )
    }
}
