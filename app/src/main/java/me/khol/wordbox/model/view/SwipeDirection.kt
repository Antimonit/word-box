package me.khol.wordbox.model.view

import androidx.annotation.FloatRange
import kotlin.math.abs
import kotlin.math.max

/**
 * Helper class to calculate in which direction and how far did the user drag [SwipeCard].
 * [progress] indicate how far the card was dragged. Value higher than 1 means that the user
 * has dragged far enough to interpret it as a swipe gesture.
 */
sealed class SwipeDirection(val progress: Float) {
    class LEFT(progress: Float) : SwipeDirection(progress)
    class RIGHT(progress: Float) : SwipeDirection(progress)
    class TOP(progress: Float) : SwipeDirection(progress)
    object NONE : SwipeDirection(0f)

    companion object {
        private const val HORIZONTAL_PROGRESS = 0.3f
        private const val TOP_PROGRESS = 0.2f

        /**
         * Returns the most appropriate direction for [progressX] and [progressY] arguments.
         * Negative values of progressX indicate drag to the [LEFT], positive to the [RIGHT].
         * Negative values of progressY indicate drag to the [TOP], positive are ignored and
         * will always result into [LEFT] or [RIGHT] direction.
         * Returns [NONE] if both arguments are zero.
         */
        fun of(
            @FloatRange(from = -1.0, to = 1.0) progressX: Float,
            @FloatRange(from = -1.0, to = 1.0) progressY: Float
        ): SwipeDirection {
            val x = progressX / HORIZONTAL_PROGRESS
            val y = progressY / TOP_PROGRESS

            val left = max(0f, -x)
            val right = max(0f, x)
            val top = max(0f, -y)
            val horizontal = abs(x)

            return when {
                left > 0 && left > top -> LEFT(left)
                right > 0 && right > top -> RIGHT(right)
                top > 0 && top > horizontal -> TOP(top)
                else -> NONE
            }
        }
    }
}
