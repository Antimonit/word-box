package me.khol.wordbox.model.repository

import io.reactivex.Completable
import io.reactivex.Flowable
import me.khol.wordbox.extensions.subscribeOnIO
import me.khol.wordbox.database.dao.HistoryDao
import me.khol.wordbox.database.model.PracticeDayHistory
import me.khol.wordbox.database.model.PracticeHistoryType
import me.khol.wordbox.database.model.PracticeMonthHistory
import org.threeten.bp.DayOfWeek
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneId
import javax.inject.Inject

class PracticeHistoryRepository @Inject constructor(
    private val historyDao: HistoryDao
) {

    fun insertEvent(wordId: Long, type: PracticeHistoryType, time: Instant): Completable {
        return historyDao.insert(wordId, type, time).subscribeOnIO()
    }

    fun yearlyEvents(date: LocalDate): Flowable<List<PracticeMonthHistory>> {
        return historyDao.getYearlyOverview(date.year)
    }

    fun monthlyEvents(date: LocalDate): Flowable<List<PracticeDayHistory>> {
        return historyDao.getMonthlyOverview(date.year, date.monthValue)
    }

    fun weeklyEvents(date: LocalDate): Flowable<List<PracticeDayHistory>> {
        val start = date.with(DayOfWeek.MONDAY)
        val end = start.plusWeeks(1)

        val offset = ZoneId.systemDefault().rules.getOffset(Instant.now())

        return historyDao.getWeeklyOverview(
            start.atStartOfDay().toEpochSecond(offset),
            end.atStartOfDay().toEpochSecond(offset)
        )
    }
}


