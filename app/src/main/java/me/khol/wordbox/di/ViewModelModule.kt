package me.khol.wordbox.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import me.khol.wordbox.model.viewmodel.SingletonViewModelFactory
import me.khol.wordbox.screens.add.AddViewModel
import me.khol.wordbox.screens.backup.BackupViewModel
import me.khol.wordbox.screens.main.note.NoteListViewModel
import me.khol.wordbox.screens.main.practice.PracticeViewModel
import me.khol.wordbox.screens.main.profile.ProfileViewModel
import me.khol.wordbox.screens.main.word.WordListViewModel
import me.khol.wordbox.screens.manage.labels.ManageLabelsViewModel
import me.khol.wordbox.screens.manage.languages.ManageLanguagesViewModel
import me.khol.wordbox.screens.note.NoteDetailViewModel
import me.khol.wordbox.screens.word.detail.WordDetailViewModel
import me.khol.wordbox.screens.word.edit.WordEditViewModel

/**
 * Dagger Module for providing general viewmodels and viewmodel factory
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ManageLabelsViewModel::class)
    abstract fun bindManageLabelsViewModel(vm: ManageLabelsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManageLanguagesViewModel::class)
    abstract fun bindManageLanguagesViewModel(vm: ManageLanguagesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WordListViewModel::class)
    abstract fun bindWordListViewModel(vm: WordListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WordDetailViewModel::class)
    abstract fun bindWordDetailViewModel(vm: WordDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WordEditViewModel::class)
    abstract fun bindWordEditViewModel(vm: WordEditViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NoteListViewModel::class)
    abstract fun bindNoteListViewModel(vm: NoteListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PracticeViewModel::class)
    abstract fun bindPracticeViewModel(vm: PracticeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddViewModel::class)
    abstract fun bindAddViewModel(vm: AddViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NoteDetailViewModel::class)
    abstract fun bindNoteDetailViewModel(vm: NoteDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(vm: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BackupViewModel::class)
    abstract fun bindBackupViewModel(vm: BackupViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: SingletonViewModelFactory): ViewModelProvider.Factory
}
