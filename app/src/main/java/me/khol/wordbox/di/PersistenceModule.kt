package me.khol.wordbox.di

import android.content.Context
import dagger.Module
import dagger.Provides
import me.khol.wordbox.database.WordBoxDatabaseWrapper
import org.jetbrains.anko.defaultSharedPreferences
import javax.inject.Singleton

/**
 * Dagger module for providing persistence related dependencies
 */
@Module
class PersistenceModule {

    @Singleton
    @Provides
    fun provideDatabaseWrapper(context: Context) = WordBoxDatabaseWrapper(context)

    @Provides
    fun provideWordDao(database: WordBoxDatabaseWrapper) = database.wordDao()

    @Provides
    fun provideLanguageDao(database: WordBoxDatabaseWrapper) = database.languageDao()

    @Provides
    fun provideLabelDao(database: WordBoxDatabaseWrapper) = database.labelDao()

    @Provides
    fun provideExampleDao(database: WordBoxDatabaseWrapper) = database.exampleDao()

    @Provides
    fun provideWordWordDao(database: WordBoxDatabaseWrapper) = database.wordWordDao()

    @Provides
    fun provideWordLabelDao(database: WordBoxDatabaseWrapper) = database.wordLabelDao()

    @Provides
    fun provideNoteDao(database: WordBoxDatabaseWrapper) = database.noteDao()

    @Provides
    fun provideHistoryDao(database: WordBoxDatabaseWrapper) = database.historyDao()

    @Provides
    @Singleton
    fun provideSharedPrefs(context: Context) = context.defaultSharedPreferences
}
