package me.khol.wordbox.di

import dagger.Module
import me.khol.wordbox.model.repository.LabelRepository
import me.khol.wordbox.model.repository.NoteRepository
import me.khol.wordbox.model.repository.PracticeHistoryRepository
import me.khol.wordbox.model.repository.WordRepository

/**
 * Module for providing repositories
 */
@Module
abstract class RepositoryModule {

    abstract fun provideWordRepository(): WordRepository

    abstract fun provideLabelRepository(): LabelRepository

    abstract fun provideNoteRepository(): NoteRepository

    abstract fun providePracticeHistoryRepository(): PracticeHistoryRepository
}
