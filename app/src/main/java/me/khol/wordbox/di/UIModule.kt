package me.khol.wordbox.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import me.khol.wordbox.screens.add.AddActivity
import me.khol.wordbox.screens.backup.BackupActivity
import me.khol.wordbox.screens.backup.RestoreDialogFragment
import me.khol.wordbox.screens.main.MainActivity
import me.khol.wordbox.screens.main.note.NoteListFragment
import me.khol.wordbox.screens.main.practice.PracticeFragment
import me.khol.wordbox.screens.main.profile.ProfileFragment
import me.khol.wordbox.screens.main.word.WordListFragment
import me.khol.wordbox.screens.manage.labels.ManageLabelsActivity
import me.khol.wordbox.screens.manage.languages.ManageLanguagesActivity
import me.khol.wordbox.screens.note.NoteActivity
import me.khol.wordbox.screens.note.NoteDetailFragment
import me.khol.wordbox.screens.settings.SettingsActivity
import me.khol.wordbox.screens.word.WordActivity
import me.khol.wordbox.screens.word.detail.WordDetailFragment
import me.khol.wordbox.screens.word.edit.WordEditFragment
import me.khol.wordbox.service.FloatingButton

/**
 * Module that creates subcomponents and modules for UI components as Fragments or Activities
 */
@Module
abstract class UIModule {

    @ContributesAndroidInjector
    abstract fun bindManageLabelsActivity(): ManageLabelsActivity

    @ContributesAndroidInjector
    abstract fun bindManageLanguagesActivity(): ManageLanguagesActivity

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindAddActivity(): AddActivity

    @ContributesAndroidInjector
    abstract fun bindWordActivity(): WordActivity

    @ContributesAndroidInjector
    abstract fun bindNoteActivity(): NoteActivity

    @ContributesAndroidInjector
    abstract fun bindSettingsActivity(): SettingsActivity

    @ContributesAndroidInjector
    abstract fun bindBackupActivity(): BackupActivity


    @ContributesAndroidInjector
    abstract fun bindPracticeFragment(): PracticeFragment

    @ContributesAndroidInjector
    abstract fun bindWordListFragment(): WordListFragment

    @ContributesAndroidInjector
    abstract fun bindNoteListFragment(): NoteListFragment

    @ContributesAndroidInjector
    abstract fun bindProfileFragment(): ProfileFragment

    @ContributesAndroidInjector
    abstract fun bindWordDetailFragment(): WordDetailFragment

    @ContributesAndroidInjector
    abstract fun bindWordEditFragment(): WordEditFragment

    @ContributesAndroidInjector
    abstract fun bindNoteDetailFragment(): NoteDetailFragment


    @ContributesAndroidInjector
    abstract fun bindRestoreDialogFragment(): RestoreDialogFragment

    @ContributesAndroidInjector
    abstract fun bindFloatingButton(): FloatingButton
}
