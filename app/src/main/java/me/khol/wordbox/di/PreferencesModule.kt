package me.khol.wordbox.di

import dagger.Binds
import dagger.Module
import me.khol.wordbox.interactor.PreferencesInteractor
import me.khol.wordbox.interactor.PreferencesInteractorImpl

/**
 * Dagger module for providing preferences
 */
@Module
abstract class PreferencesModule {

    @Binds
    abstract fun bindPreferencesInteractor(preferences: PreferencesInteractorImpl): PreferencesInteractor
}
