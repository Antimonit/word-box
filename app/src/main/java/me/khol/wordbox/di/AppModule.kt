package me.khol.wordbox.di

import android.content.Context
import dagger.Module
import dagger.Provides
import me.khol.wordbox.App
import javax.inject.Singleton

/**
 * Main application module
 */
@Module(includes = [
    PersistenceModule::class,
    PreferencesModule::class,
    ViewModelModule::class,
    RepositoryModule::class
])
class AppModule {

    @Provides
    @Singleton
    fun provideContext(context: App): Context = context
}
