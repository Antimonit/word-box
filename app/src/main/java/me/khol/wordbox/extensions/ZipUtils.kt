package me.khol.wordbox.extensions

import java.io.File
import java.util.*
import java.util.zip.ZipEntry
import java.util.zip.ZipException
import java.util.zip.ZipFile
import java.util.zip.ZipOutputStream

/**
 * Packages file together with metadata into a zip file.
 */
object ZipUtils {

    private const val ZIP_ENTRY_DATABASE = "wordbox.db"
    private const val ZIP_ENTRY_METADATA = "METADATA"

    fun zipDatabaseWithMetadata(databaseFile: File, metadata: Properties, zipFile: File) {
        ZipOutputStream(zipFile.outputStream().buffered()).use { output ->
            output.putNextEntry(ZipEntry(ZIP_ENTRY_DATABASE))
            databaseFile.inputStream().buffered().use { input ->
                input.copyTo(output)
            }

            output.putNextEntry(ZipEntry(ZIP_ENTRY_METADATA))
            metadata.store(output.writer(), "METADATA")
        }
    }

    @Throws(ZipException::class)
    fun unzipMetadata(zipFile: File): Properties {
        val zipFile = ZipFile(zipFile)
        val metadataEntry = zipFile.getEntry(ZIP_ENTRY_METADATA)

        zipFile.getInputStream(metadataEntry).buffered().use { input ->
            return Properties().apply {
                load(input.reader())
            }
        }
    }

    @Throws(ZipException::class)
    fun unzipDatabase(zipFile: File, databaseFile: File) {
        val zipFile = ZipFile(zipFile)
        val databaseEntry = zipFile.getEntry(ZIP_ENTRY_DATABASE)

        databaseFile.outputStream().buffered().use { output ->
            zipFile.getInputStream(databaseEntry).buffered().use { input ->
                input.copyTo(output)
            }
        }
    }
}
