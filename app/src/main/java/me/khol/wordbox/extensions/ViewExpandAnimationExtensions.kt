package me.khol.wordbox.extensions

import android.view.View
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.Transformation
import org.jetbrains.anko.wrapContent

fun View.expand() {
    val initial = measuredHeight
    measure(
        View.MeasureSpec.makeMeasureSpec(measuredWidth, View.MeasureSpec.EXACTLY),
        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    )
    val target = measuredHeight

    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    if (layoutParams.height == 0) {
        layoutParams.height = 1
    }

    simpleAnimate(initial, target, wrapContent)
}

fun View.collapse() {
    val initial = measuredHeight
    val target = 0

    simpleAnimate(initial, target, 0)
}

private fun View.simpleAnimate(initial: Int, target: Int, finalHeight: Int) {
    startAnimation(object : Animation() {
        init {
            duration = context.resources.getInteger(android.R.integer.config_mediumAnimTime).toLong()
            interpolator = DecelerateInterpolator(2f)
        }

        override fun applyTransformation(interpolation: Float, t: Transformation) {
            layoutParams.height = if (interpolation >= 1f) {
                finalHeight
            } else {
                (initial + (target - initial) * interpolation).toInt()
            }
            requestLayout()
        }
    })
}
