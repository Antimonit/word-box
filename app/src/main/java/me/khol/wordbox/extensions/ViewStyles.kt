package me.khol.wordbox.extensions

import android.view.Gravity
import android.widget.TextView
import me.khol.wordbox.R
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.base.extensions.dpf
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.padding
import org.jetbrains.anko.singleLine
import org.jetbrains.anko.textColor

fun TextView.languagesButtonStyleBorderless() {
    backgroundDrawable = getDrawableAttr(R.attr.selectableItemBackgroundBorderless)
    gravity = Gravity.CENTER
    textSize = 14f
    singleLine = true
    font = R.font.roboto_medium
    padding = dp(0)

    minWidth = dp(32)
    minHeight = dp(32)
    minimumWidth = dp(32)
    minimumHeight = dp(32)
}

fun TextView.languagesButtonStyle() {
    textColor = color(R.color.white)
    backgroundDrawable = roundDrawable(
        backgroundColor = color(R.color.colorPrimary),
        stroke = dp(0),
        corner = dpf(2)
    )
    gravity = Gravity.CENTER
    textSize = 14f
    singleLine = true
    font = R.font.roboto_medium
    padding = dp(0)

    minWidth = dp(32)
    minHeight = dp(32)
    minimumWidth = dp(32)
    minimumHeight = dp(32)
}
