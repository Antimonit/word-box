package me.khol.wordbox.extensions

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.res.TypedArray
import android.util.TypedValue
import android.view.ContextThemeWrapper
import android.view.View
import android.view.ViewManager
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.StyleRes
import org.jetbrains.anko.internals.AnkoInternals

//inline fun ViewManager.textView(text: CharSequence?, styleRes: Int = 0, init: TextView.() -> Unit = {}): TextView {
//    return ankoView({
//        if (styleRes == 0) {
//            TextView(it)
//        } else {
//            TextView(ContextThemeWrapper(it, styleRes), null, 0)
//        }
//    }, theme = 0) {
//        init()
//        setText(text)
//    }
//}


val View.contextThemeWrapper: ContextThemeWrapper
    get() = context.contextThemeWrapper

val Context.contextThemeWrapper: ContextThemeWrapper
    get() = when (this) {
        is ContextThemeWrapper -> this
        is ContextWrapper -> baseContext.contextThemeWrapper
        else -> throw IllegalStateException("Context is not an Activity, can't get theme: $this")
    }

@StyleRes
fun View.attrStyle(@AttrRes attrColor: Int): Int {
    return contextThemeWrapper.attrStyle(attrColor)
}

@StyleRes
private fun ContextThemeWrapper.attrStyle(@AttrRes attrRes: Int): Int {
    return attr(attrRes) {
        it.getResourceId(0, 0)
    }
}

private fun <R> ContextThemeWrapper.attr(@AttrRes attrRes: Int, block: (TypedArray) -> R): R {
    val typedValue = TypedValue()
    if (!theme.resolveAttribute(attrRes, typedValue, true)) throw IllegalArgumentException("$attrRes is not resolvable")
    val a = obtainStyledAttributes(typedValue.data, intArrayOf(attrRes))
    val result = block(a)
    a.recycle()
    return result
}


fun wrapContextIfNeeded(ctx: Context, @StyleRes theme: Int): Context {
    return if (theme == 0) {
        ctx
    } else {
//        if (ctx is ContextThemeWrapper && ctx.themeResId == theme) {
//            ctx
//        } else {
        // If the context isn't a ContextThemeWrapper, or it is but does not have
        // the same theme as we need, wrap it in a new wrapper
        ContextThemeWrapper(ctx, theme)
//        }
    }
}

inline fun <T : View> ViewManager.ankoView(@StyleRes theme: Int = 0, factory: (ctx: Context) -> T,
    init: T.() -> Unit
): T {
    val ctx = wrapContextIfNeeded(AnkoInternals.getContext(this), theme)
    val view = factory(ctx)
    view.init()
    AnkoInternals.addView(this, view)
    return view
}

inline fun <T : View> Context.ankoView(@StyleRes theme: Int, factory: (ctx: Context) -> T, init: T.() -> Unit): T {
    val ctx = wrapContextIfNeeded(this, theme)
    val view = factory(ctx)
    view.init()
    AnkoInternals.addView(this, view)
    return view
}

inline fun <reified T : View> ViewManager.customView(@StyleRes theme: Int = 0, init: T.() -> Unit = {}): T {
    return ankoView(theme, { ctx -> AnkoInternals.initiateView(ctx, T::class.java) }) { init() }
}

inline fun <reified T : View> Context.customView(@StyleRes theme: Int = 0, init: T.() -> Unit = {}): T {
    return ankoView(theme, { ctx -> AnkoInternals.initiateView(ctx, T::class.java) }) { init() }
}

inline fun <reified T : View> Activity.customView(@StyleRes theme: Int = 0, init: T.() -> Unit = {}): T {
    return ankoView(theme, { ctx -> AnkoInternals.initiateView(ctx, T::class.java) }) { init() }
}

//inline fun Activity.verticalLayout(@StyleRes theme: Int = 0, init: _LinearLayout.() -> Unit = {}): LinearLayout {
//    return ankoView(theme, `$$Anko$Factories$CustomViews`.VERTICAL_LAYOUT_FACTORY, init)
//}
//
//inline fun ViewManager.appBarLayout(@StyleRes theme: Int, init: _AppBarLayout.() -> Unit = {}): AppBarLayout {
//    return ankoView(theme, `$$Anko$Factories$DesignViewGroup`.APP_BAR_LAYOUT, init)
//}


inline fun ViewManager.textView(text: CharSequence?, @StyleRes style: Int = 0,
    init: TextView.() -> Unit = {}
): TextView {
    return ankoView(style, { context -> TextView(context) }) {
        init()
        setText(text)
    }
}
