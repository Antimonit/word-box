package me.khol.wordbox.extensions

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.transition.Transition
import android.transition.TransitionManager
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.Dimension
import androidx.annotation.DrawableRes
import androidx.annotation.IntRange
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.ViewCompat
import java.util.*
import kotlin.math.min

//region Font
private val typefaceMap: WeakHashMap<Context, MutableMap<Int, Typeface?>> = WeakHashMap()

var TextView.font: Int
    get() {
        throw IllegalStateException()
    }
    set(value) {
        var contextMap = typefaceMap[context]
        if (contextMap == null) {
            contextMap = HashMap()
            typefaceMap[context] = contextMap
        }

        var font = contextMap[value]
        if (font == null) {
            font = ResourcesCompat.getFont(context, value)
            contextMap[value] = font
        }

        this.typeface = font
    }
//endregion

//region Padding
var View.startPadding: Int
    inline get() = if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
        paddingStart
    } else {
        paddingLeft
    }
    inline set(value) = if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
        setPaddingRelative(value, paddingTop, paddingEnd, paddingBottom)
    } else {
        setPadding(value, paddingTop, paddingRight, paddingBottom)
    }

var View.endPadding: Int
    inline get() = if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
        paddingEnd
    } else {
        paddingRight
    }
    inline set(value) = if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
        setPaddingRelative(paddingStart, paddingTop, value, paddingBottom)
    } else {
        setPadding(paddingLeft, paddingTop, value, paddingBottom)
    }
//endregion

//region Margin
var ViewGroup.MarginLayoutParams.startMargin: Int
    inline get() = if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
        marginStart
    } else {
        leftMargin
    }
    inline set(value) {
        if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
            marginStart = value
        } else {
            leftMargin = value
        }
    }

var ViewGroup.MarginLayoutParams.endMargin: Int
    inline get() = if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
        marginEnd
    } else {
        rightMargin
    }
    inline set(value) {
        if (targetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)) {
            marginEnd = value
        } else {
            rightMargin = value
        }
    }
//endregion

var View.elevationCompat: Float
    get() {
        return ViewCompat.getElevation(this)
    }
    set(value) {
        ViewCompat.setElevation(this, value)
    }

var View.transitionNameCompat: String?
    get() {
        return ViewCompat.getTransitionName(this)
    }
    set(value) {
        ViewCompat.setTransitionName(this, value)
    }

var View.backgroundTintListCompat: ColorStateList?
    get() {
        throw UnsupportedOperationException("No get")
    }
    set(value) {
        ViewCompat.setBackgroundTintList(this, value)
    }


//region Attr
@ColorInt
fun View.getColorAttr(@AttrRes attr: Int) = context.getColorAttr(attr)

@ColorInt
fun Context.getColorAttr(@AttrRes attr: Int): Int {
    val ta = obtainStyledAttributes(intArrayOf(attr))
    val value = ta.getColor(0, 0)
    ta.recycle()
    return value
}

fun View.getColorStateListAttr(@AttrRes attr: Int) = context.getColorStateListAttr(attr)
fun Context.getColorStateListAttr(@AttrRes attr: Int): ColorStateList? {
    val ta = obtainStyledAttributes(intArrayOf(attr))
    val value = ta.getColorStateList(0)
    ta.recycle()
    return value
}

fun View.getDrawableAttr(@AttrRes attr: Int) = context.getDrawableAttr(attr)
fun Context.getDrawableAttr(@AttrRes attr: Int): Drawable? {
    val ta = obtainStyledAttributes(intArrayOf(attr))
    val value = ta.getDrawable(0)
    ta.recycle()
    return value
}

@Dimension
fun View.getDimensionAttr(@AttrRes attr: Int) = context.getDimensionAttr(attr)

@Dimension
fun Context.getDimensionAttr(@AttrRes attr: Int): Int {
    val ta = obtainStyledAttributes(intArrayOf(attr))
    val value = ta.getDimensionPixelSize(0, 0)
    ta.recycle()
    return value
}
//endregion

//region Colors
@ColorInt
fun Context.colorWithAlpha(@ColorRes colorId: Int, @IntRange(from = 0, to = 100) alphaPercent: Int): Int {
    val color = ContextCompat.getColor(this, colorId)
    return Color.argb((alphaPercent * 255 / 100f).toInt(), Color.red(color), Color.green(color), Color.blue(color))
}

@ColorInt
fun View.colorWithAlpha(@ColorRes colorId: Int, @IntRange(from = 0, to = 100) alphaPercent: Int): Int {
    return context.colorWithAlpha(colorId, alphaPercent)
}

@ColorInt
fun Context.color(@ColorRes colorId: Int): Int {
    return ContextCompat.getColor(this, colorId)
}

@ColorInt
fun View.color(@ColorRes colorId: Int): Int {
    return context.color(colorId)
}

fun Context.colors(@ColorRes colorId: Int): ColorStateList? {
    return ContextCompat.getColorStateList(this, colorId)
}

fun View.colors(@ColorRes colorId: Int): ColorStateList? {
    return context.colors(colorId)
}
//endregion

//region Drawable
fun Context.drawable(@DrawableRes drawableId: Int): Drawable? {
    return ContextCompat.getDrawable(this, drawableId)
}

fun View.drawable(@DrawableRes drawableId: Int): Drawable? {
    return context.drawable(drawableId)
}

fun Context.tintedDrawable(@DrawableRes drawableId: Int, @ColorInt tint: Int): Drawable? {
    val drawable: Drawable? = ContextCompat.getDrawable(this, drawableId)
    if (drawable != null) {
        drawable.mutate()
        DrawableCompat.setTint(drawable, tint)
    }
    return drawable
}

fun View.tintedDrawable(@DrawableRes drawableId: Int, @ColorInt tint: Int): Drawable? {
    return context.tintedDrawable(drawableId, tint)
}
//endregion

@Dimension
fun View.getDimension(@DimenRes dimensionId: Int): Int {
    return context.resources.getDimensionPixelSize(dimensionId)
}

var View.foregroundResource: Int
    get() {
        throw UnsupportedOperationException("No get")
    }
    @TargetApi(Build.VERSION_CODES.M)
    set(@DrawableRes value) {
        foreground = ResourcesCompat.getDrawable(resources, value, null)
    }

var View.isVisible: Boolean
    get() = visibility == View.VISIBLE
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

fun ViewGroup.beginDelayedTransition(lambda: () -> Transition? = { null }) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        TransitionManager.beginDelayedTransition(this, lambda())
    }
}

fun EditText.setTextWithRetainedSelection(text: CharSequence) {
    val start = selectionStart
    val end = selectionEnd
    setText(text)
    setSelection(min(start, text.length), min(end, text.length))
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}
