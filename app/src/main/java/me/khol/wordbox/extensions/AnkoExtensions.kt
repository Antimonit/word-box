package me.khol.wordbox.extensions

import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import androidx.cardview.widget.CardView
import com.airbnb.epoxy.EpoxyRecyclerView
import com.l4digital.fastscroll.FastScroller
import me.khol.wordbox.R
import me.khol.wordbox.model.view.BackdropAppBarLayout
import me.khol.wordbox.model.view.BackdropToolbarLayout
import me.khol.wordbox.model.view.CenteredToolbar
import me.khol.wordbox.model.view.HeaderRadioButton
import me.khol.wordbox.model.view.HeaderRadioGroup
import me.khol.wordbox.model.view.LabelButton
import me.khol.wordbox.model.view.LabelView
import me.khol.wordbox.model.view.LanguagesButton
import me.khol.wordbox.model.view.SwipeStack
import me.khol.wordbox.screens.base.layout.BaseLayout
import org.jetbrains.anko.AnkoViewDslMarker
import org.jetbrains.anko.custom.ankoView
import org.jetbrains.anko.internals.AnkoInternals

/**
 * Additional extension functions for anko
 */

inline fun ViewManager.epoxyRecyclerView(init: (@AnkoViewDslMarker EpoxyRecyclerView).() -> Unit = {}): EpoxyRecyclerView {
    return ankoView({ context -> EpoxyRecyclerView(context) }, theme = 0) { init() }
}

inline fun ViewManager.languagesButton(init: (@AnkoViewDslMarker LanguagesButton).() -> Unit = {}): LanguagesButton {
    return ankoView({ context -> LanguagesButton(context) }, theme = 0) { init() }
}

inline fun ViewManager.labelButton(init: (@AnkoViewDslMarker LabelButton).() -> Unit = {}): LabelButton {
    return ankoView({ context -> LabelButton(context) }, theme = 0) { init() }
}

inline fun ViewManager.labelView(init: (@AnkoViewDslMarker LabelView).() -> Unit = {}): LabelView {
    return ankoView({ context -> LabelView(context) }, theme = 0) { init() }
}

inline fun ViewManager.backdropAppBarLayout(init: (@AnkoViewDslMarker BackdropAppBarLayout).() -> Unit = {}): BackdropAppBarLayout {
    return ankoView({ context -> BackdropAppBarLayout(context) }, theme = 0) { init() }
}

inline fun ViewManager.backdropToolbarLayout(init: (@AnkoViewDslMarker BackdropToolbarLayout).() -> Unit = {}): BackdropToolbarLayout {
    return ankoView({ context -> BackdropToolbarLayout(context) }, theme = 0) { init() }
}

inline fun ViewManager.headerRadioGroup(init: (@AnkoViewDslMarker HeaderRadioGroup).() -> Unit = {}): HeaderRadioGroup {
    return ankoView({ context -> HeaderRadioGroup(context) }, theme = 0) { init() }
}

inline fun ViewManager.headerRadioButton(init: (@AnkoViewDslMarker HeaderRadioButton).() -> Unit = {}): HeaderRadioButton {
    return ankoView({ context -> HeaderRadioButton(context) }, theme = 0) { init() }
}

inline fun ViewManager.swipeStack(init: (@AnkoViewDslMarker SwipeStack).() -> Unit = {}): SwipeStack {
    return ankoView({ context -> SwipeStack(context) }, theme = 0) { init() }
}

inline fun ViewManager.cardView(init: (@AnkoViewDslMarker CardView).() -> Unit = {}): CardView {
    return ankoView({ context -> CardView(context) }, theme = 0) { init() }
}

inline fun ViewManager.fastScroller(init: (@AnkoViewDslMarker FastScroller).() -> Unit = {}): FastScroller {
    return ankoView({ context -> FastScroller(context) }, theme = 0) { init() }
}

inline fun ViewManager.centeredToolbar(init: (@AnkoViewDslMarker CenteredToolbar).() -> Unit = {}): CenteredToolbar {
    return ankoView({ context -> CenteredToolbar(context) }, R.style.WordBoxTheme_DarkAppBarOverlay) { init() }
}

inline fun <reified T : BaseLayout> ViewGroup.customLayout(layout: T, initView: View.() -> Unit = {}): T {
    AnkoInternals.addView(this, layout.view)
    layout.view.initView()
    return layout
}
