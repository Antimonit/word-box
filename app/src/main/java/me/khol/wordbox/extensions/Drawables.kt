package me.khol.wordbox.extensions

import android.animation.ObjectAnimator
import android.animation.StateListAnimator
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.graphics.drawable.RippleDrawable
import android.os.Build
import android.view.View
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import me.khol.wordbox.R
import me.khol.wordbox.base.extensions.dp
import me.khol.wordbox.base.extensions.dpf

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun rippleDrawable(@ColorInt pressedColor: Int, backgroundDrawable: Drawable?): RippleDrawable {
    return RippleDrawable(ColorStateList.valueOf(pressedColor), backgroundDrawable, null)
}

fun View.mainContentBackground(): GradientDrawable {
    return GradientDrawable().apply {
        setColor(color(R.color.white))
        cornerRadii = floatArrayOf(dpf(16), dpf(16), dpf(16), dpf(16), 0f, 0f, 0f, 0f)
    }
}

fun View.mainContentDimForeground(): GradientDrawable {
    return mainContentBackground().apply {
        setColor(colorWithAlpha(R.color.white, 50))
    }
}

fun View.colorDrawable(@ColorRes colorId: Int): ColorDrawable {
    return context.colorDrawable(colorId)
}

fun Context.colorDrawable(@ColorRes colorId: Int): ColorDrawable {
    return ColorDrawable(color(colorId))
}

fun View.headerItemDrawable(): GradientDrawable {
    return roundDrawable(color(R.color.colorPrimaryLight), corner = dpf(32))
}

fun View.headerLayoutDrawable(): GradientDrawable {
    return roundDrawable(
        context.color(R.color.colorPrimaryLight),
        stroke = 0
    )
}

fun View.roundDrawable(
    @ColorInt backgroundColor: Int = Color.WHITE,
    @ColorInt strokeColor: Int = Color.TRANSPARENT,
    stroke: Int = dp(2),
    corner: Float = dpf(8)
) = context.roundDrawable(backgroundColor, strokeColor, stroke, corner)

fun Context.roundDrawable(
    @ColorInt backgroundColor: Int = Color.WHITE,
    @ColorInt strokeColor: Int = Color.TRANSPARENT,
    stroke: Int = dp(2),
    corner: Float = dpf(8)
): GradientDrawable {
    return GradientDrawable().apply {
        setColor(backgroundColor)
        setStroke(stroke, strokeColor)
        cornerRadius = corner
    }
}

fun View.selectableDrawable(
    drawable: Drawable?,
    @ColorInt color: Int
) = context.selectableDrawable(drawable, color)

fun Context.selectableDrawable(
    drawable: Drawable?,
    @ColorInt color: Int
): Drawable {
    return if (targetApi(Build.VERSION_CODES.LOLLIPOP)) {
        rippleDrawable(color, drawable)
    } else {
        LayerDrawable(arrayOf(
            drawable,
            getDrawableAttr(R.attr.selectableItemBackground)
        ))
    }
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.stateListAnimatorTest() {
    val view = this
    val duration = 300L

    stateListAnimator = StateListAnimator().apply {

        // Enabled and liftable, but not lifted means not elevated
        addState(intArrayOf(
            android.R.attr.state_enabled,
            com.google.android.material.R.attr.state_liftable,
            -com.google.android.material.R.attr.state_liftable
        ), ObjectAnimator.ofFloat(view, "elevation", 0f).setDuration(duration))

        // Default enabled state
        addState(intArrayOf(
            android.R.attr.state_enabled
        ), ObjectAnimator.ofFloat(view, "elevation", elevation).setDuration(duration))

        // Disabled state
        addState(IntArray(
            0
        ), ObjectAnimator.ofFloat(view, "elevation", 0f).setDuration(0))
    }
}

fun View.setElevationStateList() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        stateListAnimator = StateListAnimator().apply {
            addState(intArrayOf(android.R.attr.state_activated),
                ObjectAnimator.ofFloat(this@setElevationStateList, "elevation", elevation)
            )
            addState(IntArray(0),
                ObjectAnimator.ofFloat(this@setElevationStateList, "elevation", 0f)
            )
        }
    }
}

