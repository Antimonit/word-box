package me.khol.wordbox.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.os.Build
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewTreeObserver
import android.view.animation.DecelerateInterpolator
import androidx.annotation.RequiresApi


/**
 * Circular Reveal animations extensions
 */

/**
 * Return lambda which when called cancels this circular reveal
 */
@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.circularReveal(
    x: Float = 0.5f,
    y: Float = 0.5f,
    duration: Long = 800,
    delay: Long = 0,
    onStart: View.() -> Unit = {},
    onEnd: View.() -> Unit = {}
): () -> Unit {

    var animator: Animator? = null

    fun circularReveal() {
        val cx: Int = (width * x).toInt()
        val cy: Int = (height * y).toInt()
        val dx: Double = (width - cx).toDouble()
        val dy: Double = (height - cy).toDouble()
        val finalRadius = Math.hypot(dx, dy).toFloat()
        val startRadius = 0f

        val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, startRadius, finalRadius)
        animator = anim

        visibility = View.INVISIBLE

        anim.duration = duration
        anim.interpolator = DecelerateInterpolator(2f)
        anim.startDelay = delay
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationStart(animation: Animator?) {
                super.onAnimationStart(animation)
                visibility = View.VISIBLE
                onStart()
            }

            override fun onAnimationEnd(animation: Animator?) {
                super.onAnimationEnd(animation)
                onEnd()
            }
        })
        anim.start()
    }

    val globalLayoutListener = object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            circularReveal()
            viewTreeObserver.removeIfAlive(this)
        }
    }

    viewTreeObserver.addIfAlive(globalLayoutListener)

    return {
        viewTreeObserver.removeIfAlive(globalLayoutListener)
        if (animator != null) {
            animator!!.cancel()
        }
    }
}

private fun ViewTreeObserver.addIfAlive(listener: ViewTreeObserver.OnGlobalLayoutListener) {
    if (isAlive) {
        add(listener)
    }
}

private fun ViewTreeObserver.removeIfAlive(listener: ViewTreeObserver.OnGlobalLayoutListener) {
    if (isAlive) {
        remove(listener)
    }
}

private fun ViewTreeObserver.add(listener: ViewTreeObserver.OnGlobalLayoutListener) {
    addOnGlobalLayoutListener(listener)
}

private fun ViewTreeObserver.remove(listener: ViewTreeObserver.OnGlobalLayoutListener) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
        @Suppress("DEPRECATION")
        removeGlobalOnLayoutListener(listener)
    } else {
        removeOnGlobalLayoutListener(listener)
    }
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun View.circularConceal(
    x: Float = 0.5f,
    y: Float = 0.5f,
    duration: Long = 800,
    onStart: View.() -> Unit = {},
    onEnd: View.() -> Unit = {}
) {
    val cx: Int = (width * x).toInt()
    val cy: Int = (height * y).toInt()
    val dx: Double = (width - cx).toDouble()
    val dy: Double = (height - cy).toDouble()
    val startRadius = Math.hypot(dx, dy).toFloat()
    val finalRadius = 0f
    val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, startRadius, finalRadius)

    visibility = View.VISIBLE

    anim.duration = duration
    anim.interpolator = DecelerateInterpolator(2f)
    anim.addListener(object : AnimatorListenerAdapter() {
        override fun onAnimationStart(animation: Animator?) {
            super.onAnimationStart(animation)
            onStart()
        }

        override fun onAnimationEnd(animation: Animator?) {
            super.onAnimationEnd(animation)
            visibility = View.INVISIBLE
            onEnd()
        }
    })
    anim.start()
}
