package me.khol.wordbox.extensions

import android.graphics.Color

/**
 * Makes the color darker
 */
val Int.darker: Int get() {
    val hsv = FloatArray(3)
    Color.colorToHSV(this, hsv)
    hsv[2] *= 0.8f // value component
    return Color.HSVToColor(hsv)
}

/**
 * Makes the color lighter
 */
val Int.lighter: Int get() {
    val hsv = FloatArray(3)
    Color.colorToHSV(this, hsv)
    hsv[2] /= 0.8f // value component
    return Color.HSVToColor(hsv)
}
