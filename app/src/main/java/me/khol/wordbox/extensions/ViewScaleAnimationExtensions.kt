package me.khol.wordbox.extensions

import android.os.Build
import android.view.View

/**
 * Animates scale of a view configurable with [onStart] and [onEnd] actions
 */
private fun View.animateScale(scale: Float, onStart: View.() -> Unit, onEnd: View.() -> Unit) {
    animate().apply {
        scaleX(scale)
        scaleY(scale)
        pivotX = width / 2f
        pivotY = width / 2f
        duration = 300
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            withStartAction { onStart() }
            withEndAction { onEnd() }
        } else {
            onStart()
            onEnd()
        }
    }.start()
}

fun View.animateScaleAppear(onStart: View.() -> Unit = {}, onEnd: View.() -> Unit = {}) {
    animateScale(1f, onStart, onEnd)
}

fun View.animateScaleDisappear(onStart: View.() -> Unit = {}, onEnd: View.() -> Unit = {}) {
    animateScale(0f, onStart, onEnd)
}
