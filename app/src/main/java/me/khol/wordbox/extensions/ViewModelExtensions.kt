package me.khol.wordbox.extensions

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

/**
 * Extensions for Activities and Fragments for easier retrieval of ViewModels
 *
 * Register ViewModels in [me.khol.wordbox.di.ViewModelModule]
 */

inline fun <reified T : ViewModel> androidx.fragment.app.Fragment.getViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory).get(T::class.java)
}

inline fun <reified T : ViewModel> androidx.fragment.app.FragmentActivity.getViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory).get(T::class.java)
}
