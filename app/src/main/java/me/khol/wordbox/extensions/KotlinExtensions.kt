package me.khol.wordbox.extensions

/**
 * Moves an element at [from] position to [to] position and shifts all items in between by one.
 */
inline fun <reified T> Array<T>.moveItem(from: Int, to: Int): Array<T> {
    val movingItem = this[from]
    // if (from < to) then shift items in between lower
    for (index in from until to) {
        this[index] = this[index + 1]
    }
    // if (from > to) then shift items in between higher
    for (index in (to until from).reversed()) {
        this[index + 1] = this[index]
    }
    this[to] = movingItem
    return this
}

inline fun <reified T> List<T>.moveItem(from: Int, to: Int): List<T> {
    return this.toTypedArray().moveItem(from, to).toList()
}
