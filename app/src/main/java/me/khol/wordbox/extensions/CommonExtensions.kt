package me.khol.wordbox.extensions

import android.os.Build
import me.khol.wordbox.BuildConfig

/**
 * Common extensions
 */
val isDebug = BuildConfig.DEBUG

fun targetApi(targetApi: Int): Boolean {
    return Build.VERSION.SDK_INT >= targetApi
}
