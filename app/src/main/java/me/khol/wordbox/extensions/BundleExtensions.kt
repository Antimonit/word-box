package me.khol.wordbox.extensions

import android.os.Bundle

fun Bundle.getBooleanNullable(key: String): Boolean? = if (containsKey(key)) getBoolean(key) else null
fun Bundle.getByteNullable(key: String): Byte? = if (containsKey(key)) getByte(key) else null
fun Bundle.getCharNullable(key: String): Char? = if (containsKey(key)) getChar(key) else null
fun Bundle.getShortNullable(key: String): Short? = if (containsKey(key)) getShort(key) else null
fun Bundle.getIntNullable(key: String): Int? = if (containsKey(key)) getInt(key) else null
fun Bundle.getLongNullable(key: String): Long? = if (containsKey(key)) getLong(key) else null
fun Bundle.getFloatNullable(key: String): Float? = if (containsKey(key)) getFloat(key) else null
fun Bundle.getDoubleNullable(key: String): Double? = if (containsKey(key)) getDouble(key) else null
fun Bundle.getStringNullable(key: String): String? = if (containsKey(key)) getString(key) else null
