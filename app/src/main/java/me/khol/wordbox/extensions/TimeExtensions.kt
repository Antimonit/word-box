package me.khol.wordbox.extensions

import org.ocpsoft.prettytime.PrettyTime
import org.ocpsoft.prettytime.units.Century
import org.ocpsoft.prettytime.units.Decade
import org.ocpsoft.prettytime.units.Millennium
import org.ocpsoft.prettytime.units.Week
import org.threeten.bp.Duration
import org.threeten.bp.Instant
import java.util.*

fun Instant.relatedToNow(): String {
    with(PrettyTime()) {
        removeUnit(Millennium::class.java)
        removeUnit(Century::class.java)
        removeUnit(Decade::class.java)
        return format(approximateDuration(Date(toEpochMilli())))
    }
}

fun Duration.relatedToNow(): String {
    with(PrettyTime(Date(0))) {
        removeUnit(Millennium::class.java)
        removeUnit(Century::class.java)
        removeUnit(Decade::class.java)
        removeUnit(Week::class.java)
        return formatDuration(calculatePreciseDuration(Date(toMillis())))
    }
}
