package me.khol.wordbox

import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.reactivex.Completable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import me.khol.wordbox.database.WordBoxDatabaseTestWrapper
import me.khol.wordbox.database.dao.HistoryDao
import me.khol.wordbox.database.model.PracticeDayHistory
import me.khol.wordbox.database.model.PracticeHistoryType
import me.khol.wordbox.database.model.PracticeMonthHistory
import me.khol.wordbox.model.repository.PracticeHistoryRepository
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class PracticeHistoryRepositoryTest {

    companion object {

        @BeforeClass
        fun setupClass() {
            RxJavaPlugins.setInitIoSchedulerHandler { Schedulers.trampoline() }
            RxJavaPlugins.setInitComputationSchedulerHandler { Schedulers.trampoline() }
            RxJavaPlugins.setInitSingleSchedulerHandler { Schedulers.trampoline() }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { Schedulers.trampoline() }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        }
    }

    private lateinit var db: WordBoxDatabaseTestWrapper
    private lateinit var historyDao: HistoryDao
    private lateinit var repo: PracticeHistoryRepository

    private val date = LocalDate.of(2020, 1, 1)

    @Before
    fun initDb() {
        db = WordBoxDatabaseTestWrapper(ApplicationProvider.getApplicationContext())

        historyDao = db.historyDao()

        repo = PracticeHistoryRepository(historyDao)

        insertEvents()
    }

    private fun insertEvents() {
        fun event(dateTime: LocalDateTime) = repo.insertEvent(
            0, PracticeHistoryType.Easy, dateTime.toInstant(ZoneOffset.UTC)
        )

        Completable.merge(listOf(
            event(LocalDateTime.of(2019, 1, 1, 0, 0)),
            event(LocalDateTime.of(2019, 12, 31, 0, 0)),
            event(LocalDateTime.of(2020, 1, 1, 0, 0)),
            event(LocalDateTime.of(2020, 1, 1, 23, 59)),
            event(LocalDateTime.of(2020, 1, 31, 23, 59)),
            event(LocalDateTime.of(2020, 2, 1, 23, 59))
        )).blockingAwait()
    }

    @Test
    fun monthlyPracticeHistory() {
        repo.monthlyEvents(date)
            .test()
            .awaitCount(1)
            .assertValues(listOf(
                PracticeDayHistory(2, 2020, 1, 1),
                PracticeDayHistory(1, 2020, 1, 31)
            ))
    }

    @Test
    fun weeklyPracticeHistory() {
        repo.weeklyEvents(date)
            .test()
            .awaitCount(1)
            .assertValues(listOf(
                PracticeDayHistory(
                    1,
                    2019,
                    12,
                    31
                ),
                PracticeDayHistory(2, 2020, 1, 1)
            ))
    }

    @Test
    fun yearlyPracticeHistory() {
        repo.yearlyEvents(date)
            .test()
            .awaitCount(1)
            .assertValues(listOf(
                PracticeMonthHistory(3, 2020, 1),
                PracticeMonthHistory(1, 2020, 2)
            ))
    }
}
