package me.khol.wordbox.base.extensions

import android.content.Context
import android.content.res.Resources
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.util.TypedValue.COMPLEX_UNIT_SP
import android.view.View
import androidx.fragment.app.Fragment

/**
 * Extensions for easy transformation of DP to pixels.
 *
 * Note that there are actually 3 ways to convert DP to pixels:
 * 1) Float -> Float - [TypedValue.complexToDimension]
 * 2) Float -> Int - [TypedValue.complexToDimensionPixelOffset]
 * 3) Float -> Int - [TypedValue.complexToDimensionPixelSize]
 *
 * In these extensions only options (1) and (2) are implemented.
 */

fun Fragment.dp(value: Number): Int = resources.dp(value)
fun Fragment.dpf(value: Number): Float = resources.dpf(value)
fun Fragment.sp(value: Number): Int = resources.sp(value)
fun Fragment.spf(value: Number): Float = resources.spf(value)

fun Resources.dp(value: Number): Int = dpf(value).toInt()
fun Resources.dpf(value: Number): Float = densityIndependentPixel(value)
fun Resources.sp(value: Number): Int = spf(value).toInt()
fun Resources.spf(value: Number): Float = scaledPixel(value)

fun Context.dp(value: Number): Int = resources.dp(value)
fun Context.dpf(value: Number): Float = resources.dpf(value)
fun Context.sp(value: Number): Int = resources.sp(value)
fun Context.spf(value: Number): Float = resources.spf(value)

fun View.dp(value: Number): Int = resources.dp(value)
fun View.dpf(value: Number): Float = resources.dpf(value)
fun View.sp(value: Number): Int = resources.sp(value)
fun View.spf(value: Number): Float = resources.spf(value)

private fun Resources.densityIndependentPixel(value: Number): Float {
    // value * resources.displayMetrics.density
    return TypedValue.applyDimension(COMPLEX_UNIT_DIP, value.toFloat(), displayMetrics)
}

private fun Resources.scaledPixel(value: Number): Float {
    // value * resources.displayMetrics.scaledDensity
    return TypedValue.applyDimension(COMPLEX_UNIT_SP, value.toFloat(), displayMetrics)
}
